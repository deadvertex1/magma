#if 0 // Disabling for now
#include <cmath>
#include <cfloat>  // For FLT_EPSILON
#include <cstring> // For memcpy

#include "unity.h"

#include "platform.h"
#include "dictionary.h"
#include "hash.h"
#include "math_utils.h"
#include "math_lib.h"
#include "entity_world.h"

#include "aabb_tree.h"
#include "collision_world.h"

#define PROF_SCOPE()

#include "game_events.h"
#include "game_player.h"
#include "game_entities.h"

#include "inventory.h"

#include "game_deployables.h"

#include "input.h"
#include "asset_ids.h" // For Mesh_

#include "entity_world.cpp" // For GET_COMPONENT

#define LOG_MSG(MSG, ...)

#include "inventory.cpp" // For MapBeltSlotToInventorySlot

#define DRAW_LINE(START, END, COLOR, LIFETIME)

internal RayIntersectWorldResult RayIntersectCollisionWorld(
    CollisionWorld *collisionWorld, vec3 rayOrigin, vec3 rayDirection, f32 tmax,
    u32 ignoreId = 0, u32 flags = CollisionWorldDebugDrawFlags_None)
{
    RayIntersectWorldResult result = {};
    result.t = 0.1f;
    return result;
}

#include "game_entities.cpp" // For AddPlayer
#include "game_player.cpp"

global MemoryArena g_Arena;

void setUp()
{
    u32 size = Kilobytes(512); // To handle hardcoded arena sizes!
    InitializeMemoryArena(&g_Arena, malloc(size), size);
}

void tearDown() { free(g_Arena.base); }

void TestPlaceDeployable()
{
    EntityWorld entityWorld = {};
    CollisionWorld collisionWorld = {};
    GameEventQueue eventQueue = {};
    CharacterControllerComponent characterController = {};
    PlayerComponent player = {};
    TransformComponent transform = {};
    WeaponControllerComponent weaponController = {};
    ColliderComponent collider = {};
    u32 entityId = 1;
    PlayerCommand cmd = {};
    ItemDatabase itemDatabase = {};
    DeployableItemDataTable deployableItemDataTable = {};

    InitializeEntityWorld(&entityWorld, &g_Arena);
    // Register entity definitions
    {
        EntityDefinition entityDef = {Vec3(0), Vec3(1),
            HashStringU32("materials/wood_floor"),
            HashStringU32("meshes/building_parts/ceiling"), true};
        RegisterEntityDefinition(&entityWorld, "entity_test", &entityDef);
    }

    InitializeItemDatabase(&itemDatabase, &g_Arena);

    // Register deployable items
    RegisterItem(&itemDatabase, HashStringU32("item_test"), "Test Item",
        Texture_ItemIcon_Hammer, 1, ItemFlag_Deployable);

    RegisterDeployableItemData(
        &deployableItemDataTable, HashStringU32("item_test"), "entity_test");

    cmd.actions = PlayerCommandAction_PrimaryAttack;

    // Create player entity
    u32 playerEntityId = AddPlayer(&entityWorld, Vec3(2, 0, 5));

    // Add deployable item to inventory
    AddItemToInventory(&entityWorld, &itemDatabase, playerEntityId,
        HashStringU32("item_test"));

    // Move item into active belt slot
    InventoryCommand inventoryCommand = {};
    inventoryCommand.srcContainer = playerEntityId;
    inventoryCommand.dstContainer = playerEntityId;
    inventoryCommand.srcSlot = 0; // TODO: Guessing for now
    inventoryCommand.dstSlot = MapBeltSlotToInventorySlot(0);
    ProcessTransferItemCommand(&entityWorld, &itemDatabase, inventoryCommand);

    // TODO: Stub RayIntersectCollisionWorld

    ProcessCommandForPlayer(&entityWorld, &collisionWorld, &eventQueue,
        &characterController, &player, &transform, &weaponController, &collider,
        entityId, cmd, &itemDatabase, &deployableItemDataTable);

    Dictionary *componentTable =
        GetComponentTable(&entityWorld, TransformComponent_TypeID);
    TEST_ASSERT_NOT_NULL(componentTable);
    TEST_ASSERT_EQUAL_UINT(2, componentTable->count);
}

int main()
{
    UNITY_BEGIN();
    RUN_TEST(TestPlaceDeployable);

    return UNITY_END();
}
#endif
