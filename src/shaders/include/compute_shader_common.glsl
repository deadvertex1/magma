#define U32_MAX 0xffffffffu
#define F32_MAX 3.402823466e+38
#define PI 3.14159265359f
#define EPSILON FLT_EPSILON

// NOTE: This needs to be correctly seeded in main()
uint rng_state;

// Reference implementation from https://en.wikipedia.org/wiki/Xorshift
uint XorShift32()
{
    uint x = rng_state;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
    rng_state = x;
	return x;
}

float RandomUnilateral()
{
    float numerator = float(XorShift32());
    float denom = float(U32_MAX);
    float result = numerator / denom;
    return result;
}

float RandomBilateral()
{
    float result = -1.0f + 2.0f * RandomUnilateral();
    return result;
} 

#define CUBE_MAP_POSITIVE_X 0
#define CUBE_MAP_NEGATIVE_X 1
#define CUBE_MAP_POSITIVE_Y 2
#define CUBE_MAP_NEGATIVE_Y 3
#define CUBE_MAP_POSITIVE_Z 4
#define CUBE_MAP_NEGATIVE_Z 5
#define MAX_CUBE_MAP_FACES 6

#define u32 uint
#define f32 float
#define Vec3 vec3

struct BasisVectors
{
    vec3 forward;
    vec3 up;
    vec3 right;
};

BasisVectors MapCubeMapLayerIndexToBasisVectors(u32 layerIndex)
{
    vec3 forward = vec3(0, 0, 0);
    vec3 up = vec3(0, 0, 0);
    vec3 right = vec3(0, 0, 0);

    // NOTE: Doesn't match the cross product
    // https://en.wikipedia.org/wiki/Cube_mapping#/media/File:Cube_map.svg
    switch (layerIndex)
    {
    case CUBE_MAP_POSITIVE_X:
        forward = Vec3(1, 0, 0);
        up = Vec3(0, 1, 0);
        right = Vec3(0, 0, -1);
        break;
    case CUBE_MAP_NEGATIVE_X:
        forward = Vec3(-1, 0, 0);
        up = Vec3(0, 1, 0);
        right = Vec3(0, 0, 1);
        break;
    case CUBE_MAP_POSITIVE_Y:
        forward = Vec3(0, 1, 0);
        up = Vec3(0, 0, -1);
        right = Vec3(1, 0, 0);
        break;
    case CUBE_MAP_NEGATIVE_Y:
        forward = Vec3(0, -1, 0);
        up = Vec3(0, 0, 1);
        right = Vec3(1, 0, 0);
        break;
    case CUBE_MAP_POSITIVE_Z:
        forward = Vec3(0, 0, 1);
        up = Vec3(0, 1, 0);
        right = Vec3(1, 0, 0);
        break;
    case CUBE_MAP_NEGATIVE_Z:
        forward = Vec3(0, 0, -1);
        up = Vec3(0, 1, 0);
        right = Vec3(-1, 0, 0);
        break;
    default:
        forward = vec3(0, 0, 0);
        up = vec3(0, 0, 0);
        right = vec3(0, 0, 0);
        break;
    }

    BasisVectors result;
    result.forward = forward;
    result.up = up;
    result.right = right;

    return result;
}

// https://en.wikipedia.org/wiki/List_of_common_coordinate_transformations#From_spherical_coordinates
// But we swap Z and Y axis to match OpenGL/Vulkan coordinate system
vec3 MapSphericalToCartesianCoordinates(vec2 sphereCoords)
{
    vec3 result = vec3(0, 0, 0);
    result.x = sin(sphereCoords.y) * cos(sphereCoords.x);
    result.z = sin(sphereCoords.y) * sin(sphereCoords.x);
    result.y = cos(sphereCoords.y);
    return result;
}

// NOTE: v must be a unit vector
// RETURNS: (azimuth, inclination)
vec2 ToSphericalCoordinates(vec3 v)
{
    // TODO: Do we always want these assertions compiled in?
    //Assert(LengthSq(v) - 1.0f <= EPSILON);

    //f32 r = 1.0f;
    f32 inc = atan(sqrt(v.x * v.x + v.z * v.z), v.y);

    f32 azimuth = atan(v.z,v.x);

    return vec2(azimuth, inc);
}

vec2 MapToEquirectangular(vec2 sphereCoords)
{
    vec2 uv = vec2(0, 0);
    if (sphereCoords.x < 0.0f)
    {
        sphereCoords.x += 2.0f * PI;
    }
    uv.x = sphereCoords.x / (2.0f * PI);
    uv.y = cos(sphereCoords.y) * 0.5f + 0.5f;

    return uv;
}
