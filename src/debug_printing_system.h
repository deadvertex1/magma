#pragma once

struct DebugPrintEntry
{
    char text[128];
    u32 length;
    vec4 color;
};

struct DebugPrintSystem
{
    DebugPrintEntry entries[32];
    u32 count;
};
