#include "unity_fixture.h"

static void RunAllTests()
{
    RUN_TEST_GROUP(CVar);
    RUN_TEST_GROUP(EntityWorld);
    RUN_TEST_GROUP(Inventory);
    RUN_TEST_GROUP(Terrain);
    RUN_TEST_GROUP(CollisionWorld);
    // RUN_TEST_GROUP(GameSystems);
    RUN_TEST_GROUP(GameNewEntities);
}

int main(int argc, const char **argv)
{
    return UnityMain(argc, argv, RunAllTests);
}
