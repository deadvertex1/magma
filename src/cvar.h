#pragma once

enum
{
    CVarDataType_U32,
    CVarDataType_F32,
};

struct CVar
{
    const char *name;
    u32 dataType;
    union
    {
        u32 u;
        f32 f;
    } value;
};

struct CVarSystem
{
    Dictionary dict;

    MemoryArena nameArena;
};

inline void InitializeCVarSystem(CVarSystem *cvarSystem, MemoryArena *arena,
    u32 capacity, u32 maxNameLength = 60)
{
    cvarSystem->dict = Dict_CreateFromArena(arena, CVar, capacity);
    cvarSystem->nameArena = SubAllocateArena(arena, maxNameLength * capacity);
}

inline void SetCVarU32(CVarSystem *cvarSystem, const char *key, u32 value)
{
    CVar *cvar = Dict_FindItem(&cvarSystem->dict, CVar, HashStringU32(key));
    if (cvar == NULL)
    {
        cvar = Dict_AddItem(&cvarSystem->dict, CVar, HashStringU32(key));
        ASSERT(cvar != NULL);
        cvar->dataType = CVarDataType_U32;
        cvar->name = CopyString(key, &cvarSystem->nameArena);
    }
    ASSERT(cvar->dataType == CVarDataType_U32);
    cvar->value.u = value;
}

inline void SetCVarF32(CVarSystem *cvarSystem, const char *key, f32 value)
{
    CVar *cvar = Dict_FindItem(&cvarSystem->dict, CVar, HashStringU32(key));
    if (cvar == NULL)
    {
        cvar = Dict_AddItem(&cvarSystem->dict, CVar, HashStringU32(key));
        ASSERT(cvar != NULL);
        cvar->dataType = CVarDataType_F32;
        cvar->name = CopyString(key, &cvarSystem->nameArena);
    }
    ASSERT(cvar->dataType == CVarDataType_F32);
    cvar->value.f = value;
}

inline CVar *FindCVar(CVarSystem *cvarSystem, const char *key)
{
    CVar *cvar = Dict_FindItem(&cvarSystem->dict, CVar, HashStringU32(key));
    return cvar;
}

inline b32 GetCVarU32(CVarSystem *cvarSystem, const char *key, u32 *value)
{
    b32 found = false;
    CVar *cvar = FindCVar(cvarSystem, key);
    if (cvar != NULL)
    {
        ASSERT(cvar->dataType == CVarDataType_U32);
        *value = cvar->value.u;
        found = true;
    }
    return found;
}

inline b32 GetCVarF32(CVarSystem *cvarSystem, const char *key, f32 *value)
{
    b32 found = false;
    CVar *cvar = FindCVar(cvarSystem, key);
    if (cvar != NULL)
    {
        ASSERT(cvar->dataType == CVarDataType_F32);
        *value = cvar->value.f;
        found = true;
    }
    return found;
}

inline b32 IsCVarTrue(CVarSystem *cvarSystem, const char *key)
{
    u32 value = false;
    GetCVarU32(cvarSystem, key, &value);
    return (value != 0);
}
