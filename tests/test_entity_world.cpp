#include <cmath>
#include <cfloat>  // For FLT_EPSILON
#include <cstring> // For memcpy

#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "dictionary.h"
#include "hash.h"
#include "math_utils.h"
#include "math_lib.h"
#include "entity_world.h"
#include "assertions.h"

#define PROF_SCOPE()

#include "entity_world.cpp"

TEST_GROUP(EntityWorld);

global MemoryArena g_Arena;

TEST_SETUP(EntityWorld)
{
    u32 size = Kilobytes(32);
    InitializeMemoryArena(&g_Arena, malloc(size), size);
}

TEST_TEAR_DOWN(EntityWorld) { free(g_Arena.base); }

struct TestComponent
{
    vec3 v;
    u32 u;
};

struct TestComponent2
{
    u32 __unused;
};

internal EntityWorldConfig CreateTestEntityWorldConfig()
{
    EntityWorldConfig config = {};
    config.worldArenaSize = Kilobytes(16);
    config.maxEntities = 4;
    config.maxEntityDefinitions = 4;
    config.deletionQueueCapacity = 2;
    return config;
}

TEST(EntityWorld, FindEntityDefinition)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    EntityDefinition entityDef = {};
    PushValue_(&entityDef, 0, 1);
    RegisterEntityDefinition(&entityWorld, "test_entity", &entityDef);

    EntityDefinition foundEntityDef = {};
    b32 found =
        FindEntityDefinition(&entityWorld, "test_entity", &foundEntityDef);
    TEST_ASSERT_EQUAL_UINT(1, foundEntityDef.count);
    TEST_ASSERT_EQUAL_UINT(sizeof(u32), foundEntityDef.entries[0].size);
}

TEST(EntityWorld, EntityDefinitionOverrideValue)
{
    u32 componentTypeId = 123;

    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    EntityDefinition entityDef = {};
    PUSH_VALUE(&entityDef, position, Vec3(1, 2, 3));
    PUSH_VALUE(&entityDef, position, Vec3(2, 4, 6));

    u32 entityId = CreateEntityFromDefinition(&entityWorld, &entityDef);
    Entity *entity = FindEntityById(&entityWorld, entityId);

    AssertWithinVec3(EPSILON, Vec3(2, 4, 6), entity->position);
}

TEST(EntityWorld, NewEntityFindById)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    u32 entityId = 1234;
    Entity entity = {};
    AddEntity(&entityWorld, entityId, &entity);

    TEST_ASSERT_EQUAL_UINT(1, GetEntityCount(&entityWorld));

    Entity *foundEntity = FindEntityById(&entityWorld, entityId);
    TEST_ASSERT_NOT_NULL(foundEntity);

    TEST_ASSERT_NULL(FindEntityById(&entityWorld, 0));
}

TEST(EntityWorld, NewEntityDestroy)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    u32 entityId = 1234;
    Entity entity = {};
    AddEntity(&entityWorld, entityId, &entity);

    DestroyEntity(&entityWorld, entityId);
    TEST_ASSERT_EQUAL_UINT(0, GetEntityCount(&entityWorld));
}

TEST_GROUP_RUNNER(EntityWorld)
{
    RUN_TEST_CASE(EntityWorld, FindEntityDefinition);
    RUN_TEST_CASE(EntityWorld, EntityDefinitionOverrideValue);
    RUN_TEST_CASE(EntityWorld, NewEntityFindById);
    RUN_TEST_CASE(EntityWorld, NewEntityDestroy);
}
