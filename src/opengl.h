#pragma once

#pragma pack(push, 1)
struct Vertex
{
    vec3 position;
    vec3 color;
    vec3 normal;
    vec2 textureCoord;
};

struct SkinnedVertex
{
    vec3 position;
    vec3 normal;
    vec3 tangent;
    vec3 bitangent;
    vec2 textureCoord;
    vec3 jointWeights;
    u32 jointIndices; // NOTE: Contains 4 1-byte indices
};
#pragma pack(pop)

struct VertexPC
{
    vec3 position;
    vec3 color;
};

struct CameraUniformBuffer
{
    mat4 viewMatrices[8];
    mat4 projectionMatrices[8];

    mat4 matrixPalette[255];

    f32 t;
};

#pragma pack(push, 1)
struct LightingUniformBuffer
{
    vec3 sunDirection;
    f32 __unused0;
    vec3 sunColor;
    f32 __unused1;
    vec3 skyColor;
    f32 __unused2;
    vec3 cameraPosition;
    f32 __unused3;
    vec3 fogColor;
    f32 irradianceCubeMapContribution;
    f32 fogMultiplier;
};
#pragma pack(pop)

struct MeshState
{
    u32 vertexBuffer;
    u32 elementBuffer;
    u32 vertexArrayObject;
    u32 elementCount;
};

struct DebugDrawVerticesBuffer
{
    VertexPC *vertices;
    u32 max;

    u32 vertexBuffer;
    u32 vertexArrayObject;
};

struct ShadowMapBuffer
{
    u32 framebufferObject;
    u32 texture;
};

struct HdrFramebuffer
{
    u32 framebufferObject;
    u32 colorTexture;
    u32 depthTexture;
};

#pragma pack(push, 1)
struct VertexText
{
    vec2 position;
    vec2 textureCoordinates;
};
#pragma pack(pop)

struct FontGlyph
{
    f32 x0, y0, u0, v0; // top-left
    f32 x1, y1, u1, v1; // bottom-right
    f32 advance, lsb;
};

struct Font
{
    FontGlyph *glyphs;
    u32 glyphCount;
    f32 lineSpacing;
    f32 height;
    u32 texture;
};

struct TextVerticesBuffer
{
    VertexText *vertices;
    u32 max;

    u32 vertexBuffer;
    u32 vertexArrayObject;
};

enum
{
    ComputeShader_ConvertEquirectanglarToCubeMap,
    MAX_COMPUTE_SHADERS,
};

struct RenderData
{
    u32 cameraUniformBuffer;
    u32 lightingUniformBuffer;
    DebugDrawVerticesBuffer debugDrawVerticesBuffer;
    TextVerticesBuffer textVerticesBuffer;
    ShadowMapBuffer shadowMapBuffer;
    HdrFramebuffer hdrFramebuffer;

    u32 timeElapsedQueryObjects[32];
    u32 writeIndex;
    u32 readIndex;

    u32 samplesPassedQueryObject;

    u32 *computeShaders[MAX_COMPUTE_SHADERS];

    AssetSystem *assetSystem;
};
