#pragma once

enum
{
    Sense_Pickups,
    Sense_TeamCharacters,
    Sense_Resources,
    Sense_TeamLumberCamps,
};

struct SensingRequest
{
    vec3 min;
    vec3 max;
    u32 interest;
    u32 teamIndex;
};

struct SensingResult
{
    u32 entityIds[16];
    vec3 positions[16];
    u32 types[16];
    u32 count;
};

enum
{
    Action_None,
    Action_MoveToPosition,
    Action_InteractWithEntity,
    Action_ObserveAndPlan,
    Action_AttackEntity,
    Action_TransferItems,
    Action_PlaceDeployable,
    Action_ClearGoal,
};

struct DecisionResult
{
    u32 action;
    vec3 moveToPosition;
};

struct ActionPlanStep
{
    u32 action;
    vec3 moveToPosition;
    u32 targetEntity;
    f32 targetDistance;
    u32 newGoal;
};

struct ActionPlan
{
    ActionPlanStep steps[8];
    u32 length;
};

enum
{
    AiCommandAction_None = 0,
    AiCommandAction_Interact = BIT(0),
    AiCommandAction_Attack = BIT(1),
    AiCommandAction_DepositInventory = BIT(2),
    AiCommandAction_PlaceDeployable = BIT(3),
};

struct AiCommand
{
    f32 speed;      // Normalized
    vec3 targetDir; // Normalized

    u32 action;
    u32 targetEntity;
};

struct AiState
{
    vec3 position;
};

struct ExecutePlanResult
{
    AiCommand command;
    u32 nextStepIdx;

    b32 updateGoal;
    u32 newGoal;
};

struct AiPlanningState
{
    ActionPlan plan;
    u32 currentStep;
};

struct AiPlanningSystem
{
    Dictionary entries;
    u32 nextId;
};
