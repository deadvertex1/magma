#pragma once

#define DEBUG_SHOW_MOUSE_CURSOR(NAME) void NAME(b32 showCursor)
typedef DEBUG_SHOW_MOUSE_CURSOR(DEBUGShowMouseCursorFunction);

// FIXME: Not the right place for this but main.cpp needs access
#define TERRAIN_PATCH_WIDTH 64
#define TERRAIN_SCALE_XZ 2048
#define TERRAIN_SCALE_Y 250
#define TERRAIN_TEXTURE_WIDTH 2048

struct RenderData;
struct LoggingSystem;
struct LogBuffer;
struct ProfilingSystem;

struct GameMemory
{
    u64 persistentStorageSize;
    void *persistentStorage;

    u64 tempStorageSize;
    void *tempStorage;

    RenderData *renderData;
    LoggingSystem *loggingSystem;
    LogBuffer *logBuffer;
    ProfilingSystem *profilingSystem;

    b32 codeReloaded;

    b32 runAutomatedTests;
    b32 enableTerrain;

    // Debug platform layer functions
    DEBUGShowMouseCursorFunction *debugShowMouseCursor;
};

#define GAME_UPDATE_AND_RENDER(NAME)                                           \
    void NAME(GameMemory *memory, InputState *input, f32 dt)
typedef GAME_UPDATE_AND_RENDER(GameUpdateAndRenderFunction);

extern "C" GAME_UPDATE_AND_RENDER(GameUpdateAndRender);
