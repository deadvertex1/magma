// Standard vertex shader output
layout(location = 0) in VS_OUT
{
    vec3 color;
    vec2 texture_coordinates;
    vec3 local_position;
    vec3 world_position;
    vec3 world_normal;
    vec4 light_position;
} fs_in;

// Standard fragment shader output
layout(location = 0) out vec4 o_color;
