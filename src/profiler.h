#pragma once

struct ProfilingEvent
{
    u64 timestamp;
    u32 metaDataGuid;
    u32 type;
};

struct ProfilingMetaData
{
    u32 guid;
    const char *file;
    const char *function;
    const char *annotation;
    ProfilingMetaData *next;
    u32 lineNumber;
};

enum
{
    ProfilerEvent_BeginBlock,
    ProfilerEvent_EndBlock,
};

// Needs to be purged on code reload as we don't copy/store any string data
struct ProfilingMetaDataSystem
{
    ProfilingMetaData *buffer;
    u32 count;
    u32 capacity;

    ProfilingMetaData **hashTable;
};

struct ProfilingEventsBuffer
{
    ProfilingEvent *events;
    u32 count;
    u32 capacity;
};

struct ProfilingSystemConfig
{
    u32 maxBuffers;
    u32 maxEventsPerBuffer;
    u32 maxMetaData;
};

enum
{
    ProfilingMode_Overwrite,
    ProfilingMode_Fill,
};

struct ProfilingSystem
{
    ProfilingEventsBuffer *buffers;
    u32 bufferCount;

    i32 writeIndex;

    u32 mode;

    ProfilingMetaDataSystem metaData;
};

struct ProfiledBlock
{
    u64 cyclesElapsed;
    u32 metaDataGuid;
    u32 callCount;
};

global ProfilingSystem *g_ProfilingSystem;

#define STRINGIFY(X) #X
#define PROF_GUID(FILE, LINE) HashStringU32(FILE STRINGIFY(LINE))

#define PROF_BEGIN_BLOCK(KEY)                                                  \
    u32 __GUID_##KEY = PROF_GUID(__FILE__, __LINE__);                          \
    prof_StoreMetaData(&g_ProfilingSystem->metaData, __GUID_##KEY, __FILE__,   \
        __LINE__, __FUNCTION__, #KEY);                                         \
    prof_RecordEvent(g_ProfilingSystem, ProfilerEvent_BeginBlock, __GUID_##KEY);

#define PROF_END_BLOCK(KEY)                                                    \
    prof_RecordEvent(g_ProfilingSystem, ProfilerEvent_EndBlock, __GUID_##KEY);

inline void prof_RecordEvent(
    ProfilingSystem *system, u32 type, u32 metaDataGuid)
{
    ProfilingEventsBuffer *buffer = &system->buffers[system->writeIndex];
    if (buffer->count < buffer->capacity)
    {
        ProfilingEvent *event = buffer->events + buffer->count++;
        event->timestamp = __rdtsc();
        event->type = type;
        event->metaDataGuid = metaDataGuid;
    }
}

inline void prof_StoreMetaData(ProfilingMetaDataSystem *system, u32 guid,
    const char *file, u32 lineNumber, const char *function,
    const char *annotation)
{
    u32 index = guid % system->capacity;
    ProfilingMetaData *metaData = system->hashTable[index];
    // Iterate through chain in case of slot collision
    // Using internal chaining
    while (metaData != NULL && metaData->guid != guid)
    {
        metaData = metaData->next;
    }

    if (metaData == NULL)
    {
        // Allocate new meta data and store
        ASSERT(system->count < system->capacity);
        metaData = system->buffer + system->count++;
        metaData->next = system->hashTable[index];
        system->hashTable[index] = metaData;

        // Set meta data
        metaData->guid = guid;
        metaData->file = file;
        metaData->lineNumber = lineNumber;
        metaData->function = function;
        metaData->annotation = annotation;
    }
}

inline ProfilingMetaData *prof_FindMetaData(
    ProfilingMetaDataSystem *system, u32 guid)
{
    u32 index = guid % system->capacity;
    ProfilingMetaData *metaData = system->hashTable[index];
    // Iterate through chain in case of slot collision
    // Using internal chaining
    while (metaData != NULL && metaData->guid != guid)
    {
        metaData = metaData->next;
    }

    return metaData;
}

#define PROF_SCOPE()                                                           \
    ProfileScope __profile_scope = ProfileScope(                               \
        PROF_GUID(__FILE__, __LINE__), __FILE__, __LINE__, __FUNCTION__)

class ProfileScope
{
  public:
    ProfileScope(u32 guid, const char *file, u32 line, const char *function)
    {
        prof_StoreMetaData(
            &g_ProfilingSystem->metaData, guid, file, line, function, NULL);
        prof_RecordEvent(g_ProfilingSystem, ProfilerEvent_BeginBlock, guid);
        _guid = guid;
    }

    ~ProfileScope()
    {
        prof_RecordEvent(g_ProfilingSystem, ProfilerEvent_EndBlock, _guid);
    }

  private:
    u32 _guid;
};
