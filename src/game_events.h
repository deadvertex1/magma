#pragma once

enum
{
    EventType_Unknown,
    EventType_HarvestResource,
    EventType_TransferItem,
    EventType_ReceiveDamage,
    EventType_Interaction,
    EventType_ReceiveItem,
    EventType_ConsumeFood,
    EventType_PlaceDeployable,
    EventType_PlayAnimation,
    EventType_SpawnBullet,
    EventType_BulletImpact,
};

struct HarvestResourceEvent
{
    u32 toolItemId;
    u32 entityId;
};

struct TransferItemEvent
{
    u32 itemId;
    u32 quantity;
    u32 srcEntityId;
    u32 dstEntityId;
};

struct ReceiveDamageEvent
{
    i32 amount;
    u32 srcEntityId;
    u32 dstEntityId;
};

struct InteractionEvent
{
    u32 srcEntityId;
    u32 dstEntityId;
};

struct ReceiveItemEvent
{
    u32 itemId;
    u32 quantity;
    u32 dstEntityId;
};

struct ConsumeFoodEvent
{
    u32 itemId;
    u32 dstEntityId; // NOTE: Also treated as the owner of the inventory
    u32 slot;
};

struct PlaceDeployableEvent
{
    vec3 position;
    u32 itemId;
    u32 entityId;
    u32 hitEntityId;
    quat desiredRotation;
};

struct PlayAnimationEvent
{
    u32 entityId;
    b32 applyScreenShake;
};

struct SpawnBulletEvent
{
    vec3 position;
    vec3 velocity;
    u32 owner;
};

struct BulletImpactEvent
{
    vec3 position;
    vec3 normal;
    u32 bulletOwner;
    u32 hitEntityId;
};

struct GameEvent
{
    u32 type;

    union
    {
        HarvestResourceEvent harvestResource;
        TransferItemEvent transferItem;
        ReceiveDamageEvent receiveDamage;
        InteractionEvent interaction;
        ReceiveItemEvent receiveItem;
        ConsumeFoodEvent consumeFood;
        PlaceDeployableEvent placeDeployable;
        PlayAnimationEvent playAnimation;
        SpawnBulletEvent spawnBullet;
        BulletImpactEvent bulletImpact;
    };
};

struct GameEventQueue
{
    GameEvent *buffer;
    u32 capacity;
    u32 length;
};

inline GameEvent *QueueEvent(GameEventQueue *queue, u32 type)
{
    GameEvent *event = NULL;
    if (queue->length < queue->capacity)
    {
        event = queue->buffer + queue->length++;
        event->type = type;
    }

    return event;
}
