internal void DebugDrawCollisionMeshTriangleAabbs(
    CollisionMesh *mesh, DebugDrawingBuffer *debugDrawingBuffer)
{
    // Foreach leaf node
    AabbTree *aabbTree = &mesh->aabbTree;
    for (u32 i = 0; i < aabbTree->count; i++)
    {
        AabbTreeNode *node = aabbTree->nodes + i;
        if (node->children[0] == NULL)
        {
            DrawBox(
                debugDrawingBuffer, node->min, node->max, Vec3(0.2, 0.6, 0.6));
        }
    }
}

internal void DebugDrawCollisionMeshTriangles(CollisionMesh *collisionMesh,
    mat4 transform, DebugDrawingBuffer *debugDrawingBuffer)
{
    u32 triangleCount = collisionMesh->indexCount / 3;
    for (u32 i = 0; i < triangleCount; i++)
    {
        u32 indices[3] = {
            collisionMesh->indices[i * 3 + 0],
            collisionMesh->indices[i * 3 + 1],
            collisionMesh->indices[i * 3 + 2],
        };

        vec3 vertices[3] = {
            collisionMesh->vertices[indices[0]],
            collisionMesh->vertices[indices[1]],
            collisionMesh->vertices[indices[2]],
        };

        vertices[0] = TransformPoint(vertices[0], transform);
        vertices[1] = TransformPoint(vertices[1], transform);
        vertices[2] = TransformPoint(vertices[2], transform);

        // for (u32 j = 0; j < 3; j++)
        //{
        // DrawPoint(debugDrawingBuffer, vertices[j], 0.25f, Vec3(1, 0, 0));
        //}
        DrawTriangle(debugDrawingBuffer, vertices[0], vertices[1], vertices[2],
            Vec3(0.8, 0.1, 0.85));

#if DEBUG_DRAW_TRIANGLE_NORMAL
        // Compute and draw triangle normal
        vec3 e1 = vertices[1] - vertices[0];
        vec3 e2 = vertices[2] - vertices[0];
        vec3 n = Normalize(Cross(e1, e2));

        vec3 center = (vertices[0] + vertices[1] + vertices[2]) * (1.0f / 3.0f);
        DrawLine(debugDrawingBuffer, center, center + n * 0.5f, Vec3(1, 0, 1));
#endif
    }
}

internal void DebugDrawCollisionMeshVertices(CollisionMesh *collisionMesh,
    mat4 transform, DebugDrawingBuffer *debugDrawingBuffer)
{
    for (u32 i = 0; i < collisionMesh->vertexCount; i++)
    {
        vec3 vertex = TransformPoint(collisionMesh->vertices[i], transform);
        DrawPoint(debugDrawingBuffer, vertex, 0.25f, Vec3(0.1, 0.8, 0.85));
    }
}

internal void DebugDrawCollisionWorld(CollisionWorld *collisionWorld,
    DebugDrawingBuffer *debugDrawingBuffer, vec3 min, vec3 max,
    u32 flags = CollisionWorldDebugDrawFlags_None)
{
    PROF_SCOPE();
    Dictionary *objects = &collisionWorld->objects;
    for (u32 i = 0; i < objects->count; i++)
    {
        u32 id = objects->keys[i];
        CollisionObject *object =
            Dict_FindItem(&collisionWorld->objects, CollisionObject, id);

        if (object != NULL)
        {
            if (flags & CollisionWorldDebugDrawFlags_Aabbs)
            {
                DrawBox(debugDrawingBuffer, object->aabb.min, object->aabb.max,
                    Vec3(1, 0, 1));
            }
            if (AabbOverlap(min, max, object->aabb.min, object->aabb.max))
            {

                CollisionMesh *collisionMesh = Dict_FindItem(
                    &collisionWorld->meshes, CollisionMesh, object->mesh);
                if (collisionMesh != NULL)
                {
                    if (flags & CollisionWorldDebugDrawFlags_Triangles)
                    {
                        DebugDrawCollisionMeshTriangles(collisionMesh,
                            object->transform, debugDrawingBuffer);
                    }

                    if (flags & CollisionWorldDebugDrawFlags_TriangleAabbs)
                    {
                        DebugDrawCollisionMeshTriangleAabbs(
                            collisionMesh, debugDrawingBuffer);
                    }

                    if (flags & CollisionWorldDebugDrawFlags_MeshVertices)
                    {
                        DebugDrawCollisionMeshVertices(collisionMesh,
                            object->transform, debugDrawingBuffer);
                    }
                }
            }
        }
    }
}
