#extension GL_GOOGLE_include_directive : require

#include "material_interface.frag.glsl"

layout(location = 0) uniform sampler2D u_texture;
layout(location = 3) uniform vec4 u_color_multiplier = vec4(1);

void main()
{
    vec4 color = texture(u_texture, fs_in.texture_coordinates).rgba;
    color *= u_color_multiplier;
    o_color = color;
}
