internal void InitializeEntityWorld(
    EntityWorld *world, MemoryArena *arena, EntityWorldConfig config)
{
    memset(world, 0, sizeof(*world));

    world->arena = SubAllocateArena(arena, config.worldArenaSize);

    world->deletionQueueCapacity = config.deletionQueueCapacity;
    world->deletionQueue =
        ALLOCATE_ARRAY(&world->arena, u32, world->deletionQueueCapacity);

    // Treat 0 as invalid entity id
    world->nextEntityId = 1;

    world->entityDefinitions = Dict_CreateFromArena(
        &world->arena, EntityDefinition, config.maxEntityDefinitions);

    world->entities =
        Dict_CreateFromArena(&world->arena, Entity, config.maxEntities);
}

inline u32 AllocateEntityId(EntityWorld *world)
{
    u32 entityId = world->nextEntityId++;
    return entityId;
}

// WARNING: Never call this directly! Use QueueEntityForDeletion instead
internal void DestroyEntity(EntityWorld *world, u32 entityId)
{
    Dict_RemoveItem(&world->entities, entityId);
}

internal b32 QueueEntityForDeletion(EntityWorld *world, u32 entityId)
{
    b32 result = false;
    if (world->deletionQueueLength < world->deletionQueueCapacity)
    {
        world->deletionQueue[world->deletionQueueLength++] = entityId;
        result = true;
    }
    return result;
}

internal void ProcessEntityDeletionQueue(EntityWorld *world)
{
    for (u32 i = 0; i < world->deletionQueueLength; i++)
    {
        DestroyEntity(world, world->deletionQueue[i]);
    }
    world->deletionQueueLength = 0;
}

inline Camera *GetDebugCamera(EntityWorld *world) { return &world->camera; }

inline SunLight *GetSunLight(EntityWorld *world) { return &world->sunLight; }

inline SkyLight *GetSkyLight(EntityWorld *world) { return &world->skyLight; }

inline void ClearEntityWorld(EntityWorld *world)
{
    Dict_Clear(&world->entities);
}

internal Entity *AddEntity(
    EntityWorld *entityWorld, u32 entityId, Entity *entity)
{
    Entity *dst = Dict_AddItem(&entityWorld->entities, Entity, entityId);
    if (dst != NULL)
    {
        memcpy(dst, entity, sizeof(Entity));
    }

    return dst;
}

internal Entity *FindEntityById(EntityWorld *entityWorld, u32 entityId)
{
    Entity *entity = Dict_FindItem(&entityWorld->entities, Entity, entityId);
    return entity;
}

internal u32 GetEntityCount(EntityWorld *entityWorld)
{
    return entityWorld->entities.count;
}

inline EntityDefinitionEntry *AllocateEntityDefinitionEntry(
    EntityDefinition *entityDef)
{
    ASSERT(entityDef->count < ARRAY_LENGTH(entityDef->entries));
    EntityDefinitionEntry *result = entityDef->entries + entityDef->count++;
    return result;
}

internal u32 CreateEntityFromDefinition(
    EntityWorld *entityWorld, EntityDefinition *entityDef)
{
    u32 entityId = AllocateEntityId(entityWorld);
    Entity entity = {};
    for (u32 i = 0; i < entityDef->count; i++)
    {
        EntityDefinitionEntry *entry = entityDef->entries + i;
        ASSERT(entry->offset + entry->size <= sizeof(entity));
        if (entry->offset == 0)
        {
            // Special case for flags field at offset 0
            ASSERT(entry->size == sizeof(u32));
            entity.flags |= entry->value.u;
        }
        else
        {
            void *dst = (u8 *)&entity + entry->offset;
            void *src = &entry->value;
            memcpy(dst, src, entry->size);
        }
    }

    if (AddEntity(entityWorld, entityId, &entity) == NULL)
    {
        // Failed to allocate entity
        entityId = 0;
    }

    return entityId;
}

internal void RegisterEntityDefinition(
    EntityWorld *entityWorld, const char *name, EntityDefinition *entityDef)
{
    EntityDefinition *ptr = Dict_AddItem(
        &entityWorld->entityDefinitions, EntityDefinition, HashStringU32(name));
    memcpy(ptr, entityDef, sizeof(EntityDefinition));
}

internal b32 FindEntityDefinitionByID(
    EntityWorld *entityWorld, u32 id, EntityDefinition *entityDef)
{
    b32 result = false;
    EntityDefinition *value =
        Dict_FindItem(&entityWorld->entityDefinitions, EntityDefinition, id);
    if (value != NULL)
    {
        memcpy(entityDef, value, sizeof(EntityDefinition));
        result = true;
    }

    return result;
}

internal b32 FindEntityDefinition(
    EntityWorld *entityWorld, const char *name, EntityDefinition *entityDef)
{
    b32 result =
        FindEntityDefinitionByID(entityWorld, HashStringU32(name), entityDef);
    return result;
}

internal EntityWorldConfig CreateDefaultEntityWorldConfig()
{
    EntityWorldConfig config = {};
    config.worldArenaSize = Megabytes(2);
    config.maxEntities = 1024;
    config.maxEntityDefinitions = 256;
    config.deletionQueueCapacity = 64;
    return config;
}

internal void PushValue_(EntityDefinition *entityDef, u32 offset, vec3 value)
{
    EntityDefinitionEntry *entry = AllocateEntityDefinitionEntry(entityDef);
    entry->offset = offset;
    entry->size = sizeof(value);
    entry->value.v3 = value;
}

internal void PushValue_(EntityDefinition *entityDef, u32 offset, quat value)
{
    EntityDefinitionEntry *entry = AllocateEntityDefinitionEntry(entityDef);
    entry->offset = offset;
    entry->size = sizeof(value);
    entry->value.q = value;
}

internal void PushValue_(EntityDefinition *entityDef, u32 offset, u32 value)
{
    EntityDefinitionEntry *entry = AllocateEntityDefinitionEntry(entityDef);
    entry->offset = offset;
    entry->size = sizeof(value);
    entry->value.u = value;
}

internal void PushValueF32(EntityDefinition *entityDef, u32 offset, f32 value)
{
    EntityDefinitionEntry *entry = AllocateEntityDefinitionEntry(entityDef);
    entry->offset = offset;
    entry->size = sizeof(value);
    entry->value.f = value;
}

#define PUSH_VALUE(ENTITY_DEF, MEMBER, VALUE)                                  \
    PushValue_(ENTITY_DEF, OffsetOf(Entity, MEMBER), VALUE)
