internal void InitializeBulletSystem(
    BulletSystem *bulletSystem, MemoryArena *arena, u32 maxBullets)
{
    memset(bulletSystem, 0, sizeof(*bulletSystem));
    bulletSystem->bullets = ALLOCATE_ARRAY(arena, Bullet, maxBullets);
    bulletSystem->capacity = maxBullets;
}

internal void CreateBullet(
    BulletSystem *bulletSystem, vec3 position, vec3 velocity, u32 owner)
{
    if (bulletSystem->count < bulletSystem->capacity)
    {
        Bullet *bullet = bulletSystem->bullets + bulletSystem->count++;
        bullet->position = position;
        bullet->prevPosition = position;
        bullet->velocity = velocity;
        bullet->owner = owner;
        bullet->lifeTime = 30.0f;
    }
    else
    {
        LOG_MSG(
            "Failed to create bullet, %u bullets exist out of max capacity %u",
            bulletSystem->count, bulletSystem->capacity);
    }
}

internal void UpdateBullets(BulletSystem *bulletSystem,
    EntityWorld *entityWorld, CollisionWorld *collisionWorld,
    GameEventQueue *eventQueue, f32 dt, b32 enableDebugDraw)
{
    vec3 gravity = Vec3(0, -10, 0); // TODO: CVAR
    f32 airFriction = 0.001f;

    for (u32 i = 0; i < bulletSystem->count; i++)
    {
        Bullet *bullet = bulletSystem->bullets + i;

        bullet->velocity += gravity * dt;
        bullet->velocity -= bullet->velocity * airFriction * dt;

        vec3 newPosition = bullet->position + bullet->velocity * dt;

        vec3 direction = Normalize(bullet->velocity);
        f32 range = Length(bullet->velocity) * dt;

        RayIntersectWorldResult result = RayIntersectCollisionWorld(
            collisionWorld, bullet->position, direction, range, bullet->owner);

        if (result.t > 0.0f)
        {
            u32 entityId = FindEntityWithCollisionObjetId(
                entityWorld, result.collisionObjectId);
            if (entityId != NULL_ENTITY_ID)
            {
                // TODO: Despawn bullet
                GameEvent *event =
                    QueueEvent(eventQueue, EventType_BulletImpact);
                if (event != NULL)
                {
                    event->bulletImpact.position =
                        bullet->position + direction * result.t;
                    event->bulletImpact.normal = result.normal;
                    event->bulletImpact.bulletOwner = bullet->owner;
                    event->bulletImpact.hitEntityId = entityId;
                }
            }

            bullet->lifeTime = 0.0f;
            newPosition = bullet->position + direction * result.t;
        }

        bullet->prevPosition = bullet->position;
        bullet->position = newPosition;
    }

    if (enableDebugDraw)
    {
        for (u32 i = 0; i < bulletSystem->count; i++)
        {
            Bullet *bullet = bulletSystem->bullets + i;
            DRAW_LINE(
                bullet->prevPosition, bullet->position, Vec3(1, 0, 1), 5.0f);
        }
    }

    u32 i = 0;
    while (i < bulletSystem->count)
    {
        Bullet *bullet = bulletSystem->bullets + i;

        if (bullet->lifeTime <= 0.0f)
        {
            // Swap and drop
            u32 last = bulletSystem->count - 1;

            bulletSystem->bullets[i] = bulletSystem->bullets[last];
            bulletSystem->count--;
        }
        else
        {
            bullet->lifeTime -= dt;
            i++;
        }
    }
}
