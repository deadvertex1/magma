internal void DrawStaticMesh(RenderData *renderer, RenderCommand command)
{
    PROF_SCOPE();
    // Get material data
    Asset_Material material = {};
    if (GetMaterial(renderer->assetSystem, command.material, &material))
    {
        // Apply material
        u32 shader = GetShader(renderer->assetSystem, material.shader);
        if (shader != 0)
        {
            glUseProgram(shader);

            // --- Standard PBR material vertex shader interface ---
            {
                // Camera uniform buffer
                glBindBufferBase(
                    GL_UNIFORM_BUFFER, 0, renderer->cameraUniformBuffer);

                // Camera index
                glUniform1ui(2, command.cameraIndex);

                // Model matrix
                glUniformMatrix4fv(1, 1, false, command.modelMatrix.data);
            }

            // -- Standard PBR material fragment shader interface ---
            {
#if ENABLE_LIGHTING
                // Lighting uniform buffer
                glBindBufferBase(
                    GL_UNIFORM_BUFFER, 1, renderer->lightingUniformBuffer);
#endif

                // Albedo
                u32 albedo = GetTexture(renderer->assetSystem, material.albedo);
                if (albedo != 0)
                {
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, albedo);
                }

                // TODO: Metallic
                // TODO: Roughness
                // TODO: Normal

#if ENABLE_LIGHTING
                // Shadow map
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, command.shadowMapTexture);
                glUniform1i(3, 1); // Use GL_TEXTURE1 for shadow map

                // Irradiance cube map
                if (command.irradianceCubeMap != 0)
                {
                    glActiveTexture(GL_TEXTURE2);
                    glBindTexture(
                        GL_TEXTURE_CUBE_MAP, command.irradianceCubeMap);
                    glUniform1i(4, 2);
                }
#endif

                // Ewwww, this uniform is getting optimized out on the
                // default shader which is then generating an error when
                // we try set it.
                if (material.shader == Shader_WorldTriplanar)
                {
                    // u_world_texture_scaling
                    vec3 worldTextureScaling = Vec3(1);
                    glUniform3fv(7, 1, worldTextureScaling.data);
                }
            }

            // Get mesh data
            MeshState *mesh = GetMesh(renderer->assetSystem, command.mesh);
            if (mesh != NULL)
            {
                glBindVertexArray(mesh->vertexArrayObject);
                glDrawElements(
                    GL_TRIANGLES, mesh->elementCount, GL_UNSIGNED_INT, 0);
                glBindVertexArray(0);
            }
        }
    }
}

internal void ProcessRenderCommands(
    RenderData *renderer, RenderCommandBuffer buffer)
{
    for (u32 i = 0; i < buffer.count; i++)
    {
        DrawStaticMesh(renderer, buffer.commands[i]);
    }
}

internal u32 GenerateNormalMapFromHeightMap(
    u32 program, u32 heightMap, u32 textureWidth)
{
    PROF_SCOPE();

    LOG_MSG("Generating normal map from height map...");

    u32 normalMap;
    glGenTextures(1, &normalMap);
    glBindTexture(GL_TEXTURE_2D, normalMap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, textureWidth, textureWidth);

    glUseProgram(program);

    glActiveTexture(GL_TEXTURE0);
    glBindImageTexture(0, normalMap, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA8);
    glBindTexture(GL_TEXTURE_2D, normalMap);
    glUniform1i(0, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, heightMap);
    glUniform1i(1, 1);

    glUniform1ui(2, textureWidth);

    glDispatchCompute(textureWidth, textureWidth, 1);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
    // glEndQuery(GL_TIME_ELAPSED);

    // u64 gpuTimeElapsedNanoSeconds = 0;
    // glGetQueryObjectui64v(
    // queryObject, GL_QUERY_RESULT, &gpuTimeElapsedNanoSeconds);
    // LOG_MSG("Preetham Sky generated in %g ms",
    //(f64)gpuTimeElapsedNanoSeconds / (1000.0 * 1000.0));

    // glDeleteQueries(1, &queryObject);

    LOG_MSG("Done!");
    return normalMap;
}

internal u32 GenerateHeightMapTexture(u32 program, u32 textureWidth)
{
    PROF_SCOPE();

    LOG_MSG("Generating height map...");

    u32 heightMap;
    glGenTextures(1, &heightMap);
    glBindTexture(GL_TEXTURE_2D, heightMap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, textureWidth, textureWidth);

    glUseProgram(program);

    glActiveTexture(GL_TEXTURE0);
    glBindImageTexture(0, heightMap, 0, GL_TRUE, 0, GL_READ_WRITE, GL_R32F);
    glBindTexture(GL_TEXTURE_2D, heightMap);
    glUniform1i(0, 0);

    glUniform1ui(1, textureWidth);

    glDispatchCompute(textureWidth, textureWidth, 1);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
    // glEndQuery(GL_TIME_ELAPSED);

    // u64 gpuTimeElapsedNanoSeconds = 0;
    // glGetQueryObjectui64v(
    // queryObject, GL_QUERY_RESULT, &gpuTimeElapsedNanoSeconds);
    // LOG_MSG("Preetham Sky generated in %g ms",
    //(f64)gpuTimeElapsedNanoSeconds / (1000.0 * 1000.0));

    // glDeleteQueries(1, &queryObject);

    LOG_MSG("Done!");
    return heightMap;
}

internal u32 GeneratePreethamSkyTexture(
    u32 program, u32 cubeMapWidth, vec3 sunDirection, f32 turbidity)
{
    PROF_SCOPE();

    LOG_MSG("Generating Preetham sky texture...");
    // u32 queryObject;
    // glGenQueries(1, &queryObject);

    u32 outputTexture;
    glGenTextures(1, &outputTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, outputTexture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glTexStorage2D(
        GL_TEXTURE_CUBE_MAP, 1, GL_RGBA16F, cubeMapWidth, cubeMapWidth);

    // glBeginQuery(GL_TIME_ELAPSED, queryObject);

    glUseProgram(program);

    // Bind resources
    glActiveTexture(GL_TEXTURE0);
    glBindImageTexture(
        0, outputTexture, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16F);
    glBindTexture(GL_TEXTURE_CUBE_MAP, outputTexture);
    glUniform1i(0, 0);

    glUniform1ui(1, cubeMapWidth);
    glUniform3fv(2, 1, sunDirection.data);
    glUniform1f(3, turbidity);

    // Dispatch compute shader
    glDispatchCompute(cubeMapWidth, cubeMapWidth, 6);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
    // glEndQuery(GL_TIME_ELAPSED);

    // u64 gpuTimeElapsedNanoSeconds = 0;
    // glGetQueryObjectui64v(
    // queryObject, GL_QUERY_RESULT, &gpuTimeElapsedNanoSeconds);
    // LOG_MSG("Preetham Sky generated in %g ms",
    //(f64)gpuTimeElapsedNanoSeconds / (1000.0 * 1000.0));

    // glDeleteQueries(1, &queryObject);

    LOG_MSG("Done!");
    return outputTexture;
}

internal b32 GenerateSkyTexture(AssetSystem *assetSystem, u32 cubeMapWidth,
    vec3 sunDirection, f32 turbidity)
{
    b32 result = false;
    u32 preethamSkyTexture = 0;
    u32 shader = GetComputeShader(
        assetSystem, HashStringU32("shaders/preetham_sky_comp"));
    if (shader != 0)
    {
        Asset_Texture texture = {};
        if (GetAssetData_(assetSystem, AssetType_Texture,
                Texture_PreethamSky_CubeMap, sizeof(Asset_Texture), &texture))
        {
            if (texture.handle != 0)
            {
                glDeleteTextures(1, &texture.handle);
            }
        }

        preethamSkyTexture = GeneratePreethamSkyTexture(
            shader, cubeMapWidth, -sunDirection, turbidity);

        texture.handle = preethamSkyTexture;
        SetAssetData_(assetSystem, AssetType_Texture,
            Texture_PreethamSky_CubeMap, sizeof(texture), &texture);
        result = true;
    }
    else
    {
        LOG_MSG("Missing preetham sky compute shader");
    }

    return result;
}

internal u32 GenerateIrradianceCubeMap(u32 program, u32 inputTexture,
    u32 cubeMapWidth = 32, f32 sampleIncrement = 0.01f)
{
    LOG_MSG("Generating irradiance cube map...");

    // Create output cube map
    u32 outputTexture;
    glGenTextures(1, &outputTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, outputTexture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexStorage2D(
        GL_TEXTURE_CUBE_MAP, 1, GL_RGBA32F, cubeMapWidth, cubeMapWidth);

    glUseProgram(program);

    // Bind resources
    glActiveTexture(GL_TEXTURE0);
    glBindImageTexture(
        0, outputTexture, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA32F);
    glBindTexture(GL_TEXTURE_CUBE_MAP, outputTexture);
    glUniform1i(0, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, inputTexture);
    glUniform1i(1, 1);

    glUniform1ui(2, cubeMapWidth);
    glUniform1f(3, sampleIncrement);

    vec3 hsvMultiplier = Vec3(1, 1.2f, 1.2f);
    glUniform3fv(4, 1, hsvMultiplier.data);

    // Dispatch compute shader
    glDispatchCompute(cubeMapWidth, cubeMapWidth, 6);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

    LOG_MSG("Done!");
    return outputTexture;
}

internal void GenerateSkyIrradianceCubeMap(
    AssetSystem *assetSystem, f32 sampleIncrement, u32 irradianceResolution)
{
    u32 irradianceComputeShader = GetComputeShader(
        assetSystem, HashStringU32("shaders/irradiance_cubemap"));
    if (irradianceComputeShader != 0)
    {
        Asset_Texture texture = {};
        if (GetAssetData_(assetSystem, AssetType_Texture,
                Texture_PreethamSky_Irradiance, sizeof(Asset_Texture),
                &texture))
        {
            if (texture.handle != 0)
            {
                glDeleteTextures(1, &texture.handle);
            }
        }

        u32 preethamSkyTexture =
            GetTexture(assetSystem, Texture_PreethamSky_CubeMap);

        if (preethamSkyTexture)
        {
            u32 irradianceCubeMap =
                GenerateIrradianceCubeMap(irradianceComputeShader,
                    preethamSkyTexture, irradianceResolution, sampleIncrement);

            texture.handle = irradianceCubeMap;
            SetAssetData_(assetSystem, AssetType_Texture,
                Texture_PreethamSky_Irradiance, sizeof(texture), &texture);
        }
    }
}

internal void DrawSkybox(
    RenderData *renderer, vec3 cameraPosition, f32 exposure)
{
    AssetSystem *assetSystem = renderer->assetSystem;

    u32 cubeMapShader = GetShader(assetSystem, Shader_CubeMap);
    MeshState *cubeMesh = GetMesh(assetSystem, Mesh_Cube);
    if (cubeMesh != NULL && cubeMapShader != 0)
    {
        // Draw skybox
        glDepthMask(GL_FALSE); // Disable depth write
        glUseProgram(cubeMapShader);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, renderer->cameraUniformBuffer);

        u32 texture = GetTexture(assetSystem, Texture_PreethamSky_CubeMap);
        if (texture != 0)
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
        }

        glBindVertexArray(cubeMesh->vertexArrayObject);
        // FIXME: Wouldn't it be better to just have a view matrix with rotation
        // only and identity model matrix?
        mat4 skyboxModelMatrix =
            Translate(cameraPosition); // Eliminate translation from view matrix
        glUniformMatrix4fv(1, 1, false, skyboxModelMatrix.data);

        glUniform1f(3, exposure);

        glDrawElements(
            GL_TRIANGLES, cubeMesh->elementCount, GL_UNSIGNED_INT, 0);
        glDepthMask(GL_TRUE); // Enable depth write
    }
}

internal void GenerateTerrainNormalMap(AssetSystem *assetSystem)
{
    u32 generateNormalMapShader = GetComputeShader(
        assetSystem, HashStringU32("shaders/generate_normal_map"));
    if (generateNormalMapShader != 0)
    {
        u32 heightMap = GetTexture(assetSystem, Texture_TerrainHeightMap);

        if (generateNormalMapShader != 0 && heightMap != 0)
        {
            Asset_Texture texture = {};
            if (GetAssetData_(assetSystem, AssetType_Texture,
                    Texture_TerrainNormalMap, sizeof(Asset_Texture), &texture))
            {
                if (texture.handle != 0)
                {
                    glDeleteTextures(1, &texture.handle);
                }
            }

            u32 terrainNormalMap = GenerateNormalMapFromHeightMap(
                generateNormalMapShader, heightMap, TERRAIN_TEXTURE_WIDTH);

            texture.handle = terrainNormalMap;
            SetAssetData_(assetSystem, AssetType_Texture,
                Texture_TerrainNormalMap, sizeof(texture), &texture);
        }
    }
    else
    {
        LOG_MSG("Missing generate normal map compute shader");
    }
}

internal void GenerateTerrainHeightMap(AssetSystem *assetSystem)
{
    u32 shader = GetComputeShader(
        assetSystem, HashStringU32("shaders/generate_height_map"));

    if (shader != 0)
    {
        Asset_Texture texture = {};
        if (GetAssetData_(assetSystem, AssetType_Texture,
                Texture_TerrainHeightMap, sizeof(Asset_Texture), &texture))
        {
            if (texture.handle != 0)
            {
                glDeleteTextures(1, &texture.handle);
            }
        }

        u32 heightMap = GenerateHeightMapTexture(shader, TERRAIN_TEXTURE_WIDTH);

        texture.handle = heightMap;
        SetAssetData_(assetSystem, AssetType_Texture, Texture_TerrainHeightMap,
            sizeof(texture), &texture);
    }
    else
    {
        LOG_MSG("Missing generate heightmap shader");
    }
}

internal HeightMap CreateHeightMapFromTexture(
    AssetSystem *assetSystem, u32 textureId, u32 width, MemoryArena *arena)
{
    HeightMap heightMap = {};

    u32 texture = GetTexture(assetSystem, textureId);
    if (texture != 0)
    {
        // FIXME: Don't assume texture2D
        glBindTexture(GL_TEXTURE_2D, texture);

        // TODO: Readback width with glGetTexParameter
        f32 *data = ALLOCATE_ARRAY(arena, f32, width * width);

        // FIXME: Don't assume GL_R32F!
        // FIXME: Gives me the ick not passing in the expected size
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, data);

        heightMap.values = data;
        heightMap.width = width;
        heightMap.height = width;
    }

    return heightMap;
}
