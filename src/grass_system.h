#pragma once

struct GrassSystem
{
    MemoryArena arena;
    u32 modelMatrixBuffer;
    mat4 *modelMatrices;
    u32 count;
    u32 capacity;
};
