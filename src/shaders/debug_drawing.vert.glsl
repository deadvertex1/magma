#extension GL_GOOGLE_include_directive : require

#include "camera_uniform_buffer.glsl"

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_color;

layout(location = 0) out VS_OUT
{
    vec3 color;
} vs_out;

void main()
{
    mat4 projection_matrix = projection_matrices[0];
    mat4 view_matrix = view_matrices[0];

    gl_Position = projection_matrix * view_matrix * vec4(v_position, 1.0);
    vs_out.color = v_color;
}
