#pragma once

const u32 Item_Wood = HashStringU32("item_wood");
const u32 Item_Meat = HashStringU32("item_meat");
const u32 Item_Stone = HashStringU32("item_stone");
const u32 Item_MetalOre = HashStringU32("item_metal_ore");
const u32 Item_MetalIngot = HashStringU32("item_metal_ingot");
const u32 Item_Hatchet = HashStringU32("item_hatchet");
const u32 Item_PickAxe = HashStringU32("item_pick_axe");
const u32 Item_Hammer = HashStringU32("item_hammer");
const u32 Item_FirePit = HashStringU32("item_fire_pit");
const u32 Item_CookedMeat = HashStringU32("item_cooked_meat");
const u32 Item_LumberCamp = HashStringU32("item_lumber_camp");
const u32 Item_CraftingBench = HashStringU32("item_crafting_bench");
const u32 Item_WoodenBow = HashStringU32("item_wooden_bow");
const u32 Item_WoodenArrow = HashStringU32("item_wooden_arrow");
const u32 Item_Carrot = HashStringU32("item_carrot");
const u32 Item_WoodenSpear = HashStringU32("item_wooden_spear");
const u32 Item_FriendlyAiCamp = HashStringU32("item_friendly_ai_camp");
const u32 Item_TestGun = HashStringU32("item_test_gun");

const u32 Recipe_Hatchet = HashStringU32("recipe_hatchet");
const u32 Recipe_PickAxe = HashStringU32("recipe_pick_axe");
const u32 Recipe_Hammer = HashStringU32("recipe_hammer");
const u32 Recipe_FirePit = HashStringU32("recipe_fire_pit");
const u32 Recipe_LumberCamp = HashStringU32("recipe_lumber_camp");
const u32 Recipe_CraftingBench = HashStringU32("recipe_crafting_bench");
const u32 Recipe_WoodenBow = HashStringU32("recipe_wooden_bow");
const u32 Recipe_WoodenArrow = HashStringU32("recipe_wooden_arrow");
const u32 Recipe_WoodenSpear = HashStringU32("recipe_wooden_spear");
const u32 Recipe_MetalIngot = HashStringU32("recipe_metal_ingot");

enum
{
    ItemFlag_None = 0x0,
    ItemFlag_Tool = 0x1,
    ItemFlag_Deployable = 0x2,
    ItemFlag_Consumable = 0x4,
    ItemFlag_Bow = 0x8,
};

struct ItemDatabaseEntry
{
    const char *name;
    u32 iconTexture;
    u32 maxStackSize;
    u32 flags; // TODO: Maybe rather use action/event here
    vec4 iconTintColor;
};

struct RecipeInputRequirement
{
    u32 itemId;
    u32 quantity;
};

struct Recipe
{
    const char *name;
    u32 itemId;
    u32 quantity;
    RecipeInputRequirement inputs[4];
    u32 inputCount;
    b32 requiresCraftingStation;
};

struct ItemDatabase
{
    MemoryArena arena;
    Dictionary items;
    Dictionary recipes;
};

enum
{
    InventoryCommandType_TransferItem,
    InventoryCommandType_CraftItem,
};

struct InventoryCommand
{
    u32 type; // Only Move item for now

    u32 srcContainer;
    u32 dstContainer;
    u32 srcSlot;
    u32 dstSlot;
    u32 recipeId;
};

inline Recipe CreateRecipe(const char *name, u32 itemId, u32 quantity = 1)
{
    Recipe recipe = {};
    recipe.name = name;
    recipe.itemId = itemId;
    recipe.quantity = quantity;
    return recipe;
}

inline void AddRecipeInput(Recipe *recipe, u32 itemId, u32 quantity)
{
    ASSERT(recipe->inputCount < ARRAY_LENGTH(recipe->inputs));
    u32 index = recipe->inputCount++;
    recipe->inputs[index].itemId = itemId;
    recipe->inputs[index].quantity = quantity;
}

struct Inventory
{
    u32 items[30];
};

struct ItemInstance
{
    u32 itemId;
    u32 quantity;
};

struct InventorySystem
{
    Dictionary inventories;
    Dictionary items;

    u32 nextInventoryId;
    u32 nextItemInstanceId;

    MemoryArena arena;
};

struct InventorySystemConfig
{
    u32 arenaSize;
    u32 maxInventories;
    u32 maxItems;
};
