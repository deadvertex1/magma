#extension GL_GOOGLE_include_directive : require

#include "pbr_material_interface.glsl"

layout(location = 9) uniform sampler2D u_rock;

void main()
{
    TriplanarUV tri_uv = GetTriplanarUV(fs_in.world_position * u_world_texture_scaling);
    vec3 tri_w = GetTriplanarWeights(fs_in.world_normal);

    vec3 N = texture(u_normal, fs_in.texture_coordinates).rgb;
    N = normalize(2.0f * N - vec3(1));

    float cosine = dot(N, vec3(0, 1, 0));

    vec3 grass_x = texture(u_albedo, tri_uv.x).rgb;
    vec3 grass_y = texture(u_albedo, tri_uv.y).rgb;
    vec3 grass_z = texture(u_albedo, tri_uv.z).rgb;
    vec3 grass_albedo = grass_x * tri_w.x + grass_y * tri_w.y + grass_z * tri_w.z;

    vec3 rock_x = texture(u_rock, tri_uv.x).rgb;
    vec3 rock_y = texture(u_rock, tri_uv.y).rgb;
    vec3 rock_z = texture(u_rock, tri_uv.z).rgb;
    vec3 rock_albedo = rock_x * tri_w.x + rock_y * tri_w.y + rock_z * tri_w.z;
    rock_albedo *= 0.5;

    float t = smoothstep(0.2, 0.3, cosine); //cosine < 0.2 ? 0.0 : 1.0;
    vec3 albedo = mix(rock_albedo, grass_albedo, t);

    vec3 color = pbr_material_model(albedo, N);
    o_color = vec4(color, 1.0);
}
