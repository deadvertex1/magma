#extension GL_GOOGLE_include_directive : require

#include "vertex_format.vert.glsl"
#include "material_interface.vert.glsl"
layout(location = 5) uniform sampler2D u_heightmap;
layout(location = 6) uniform vec4 u_textureCoordsOffset;

void main()
{
    vec2 textureCoords = v_texture_coordinates * u_textureCoordsOffset.zw +
                         u_textureCoordsOffset.xy;
    float heightScale = 1.0;
    float height = texture(u_heightmap, textureCoords).r * heightScale;

    vec3 position = v_position + vec3(0, height, 0);

    StandardVertexShaderSetup(position, model_matrix);
    vs_out.texture_coordinates = textureCoords;
}
