internal void DrawStaticMeshesSystem(EntityWorld *entityWorld,
    RenderData *renderer, u32 cameraIndex, u32 shadowMapTexture,
    RenderCommandBuffer *commandBuffer)
{
    PROF_SCOPE();

    u32 irradianceCubeMap =
        GetTexture(renderer->assetSystem, Texture_PreethamSky_Irradiance);

    for (u32 i = 0; i < GetEntityCount(entityWorld); i++)
    {
        u32 entityId = entityWorld->entities.keys[i];

        Entity *entity = FindEntityById(entityWorld, entityId);
        ASSERT(entity != NULL);

        if (entity->visible)
        {
            RenderCommand *command = AllocateRenderCommand(commandBuffer);
            command->modelMatrix = entity->worldTransform;
            command->material = entity->material;
            command->mesh = entity->mesh;
            command->cameraIndex = cameraIndex;
            command->shadowMapTexture = shadowMapTexture;
            command->irradianceCubeMap = irradianceCubeMap;
        }
    }
}

internal void CreateMissingCollisionObjects(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, AssetSystem *assetSystem,
    MemoryArena *tempArena)
{
    PROF_SCOPE();

    // FIXME: Don't loop through every entity!
    for (u32 i = 0; i < GetEntityCount(entityWorld); i++)
    {
        u32 entityId = entityWorld->entities.keys[i];

        Entity *entity = FindEntityById(entityWorld, entityId);
        if (entity->flags & EntityFlags_Collider)
        {
            if (entity->collisionObjectId == 0)
            {
                entity->collisionObjectId = CreateCollisionObjectFromEntity(
                    entity, collisionWorld, assetSystem, tempArena);
            }
        }
    }
}

inline u32 HashCollisionObjectState(Entity *entity, u32 collisionMesh)
{
    u8 buffer[sizeof(vec3) + sizeof(vec3) + sizeof(quat) + sizeof(u32)];
    memcpy(buffer, &entity->position, sizeof(entity->position));
    memcpy(buffer + sizeof(vec3), &entity->rotation, sizeof(entity->rotation));
    memcpy(buffer + sizeof(vec3) + sizeof(quat), &entity->scale,
        sizeof(entity->scale));
    memcpy(buffer + sizeof(vec3) + sizeof(quat) + sizeof(vec3), &collisionMesh,
        sizeof(collisionMesh));
    u32 hash = HashU32(buffer, sizeof(buffer));
    return hash;
}

internal void SyncCollisionObjectStateWithEntityState(
    Entity *entity, CollisionWorld *collisionWorld)
{
    if (entity->collisionObjectId != 0)
    {
        CollisionObject *collisionObject =
            Dict_FindItem(&collisionWorld->objects, CollisionObject,
                entity->collisionObjectId);

        if (collisionObject != NULL)
        {
            u32 hash = HashCollisionObjectState(entity, collisionObject->mesh);
            if (hash != collisionObject->stateHash)
            {
                //  TODO: CollisionWorld should provide API for this
                // FIXME: This doesn't work for child entities!
                collisionObject->transform = BuildModelMatrix(entity);
                collisionObject->invTransform = BuildInverseModelMatrix(entity);

                CollisionMesh *collisionMesh =
                    Dict_FindItem(&collisionWorld->meshes, CollisionMesh,
                        collisionObject->mesh);
                ASSERT(collisionMesh != NULL);
                ASSERT(collisionMesh->aabbTree.root != NULL);
                Aabb baseAabb;
                baseAabb.min = collisionMesh->aabbTree.root->min;
                baseAabb.max = collisionMesh->aabbTree.root->max;

                collisionObject->aabb =
                    TransformAabb(baseAabb, collisionObject->transform);
                collisionObject->stateHash = hash;
            }
        }
    }
}

internal void SyncCollisionObjectsWithEntities(
    EntityWorld *entityWorld, CollisionWorld *collisionWorld)
{
    // FIXME: Don't loop through every entity!
    for (u32 i = 0; i < GetEntityCount(entityWorld); i++)
    {
        u32 entityId = entityWorld->entities.keys[i];

        Entity *entity = FindEntityById(entityWorld, entityId);
        if (entity->flags & EntityFlags_Collider)
        {
            SyncCollisionObjectStateWithEntityState(entity, collisionWorld);
        }
    }
}

internal void SyncEntityWorldWithCollisionWorld(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, AssetSystem *assetSystem,
    MemoryArena *tempArena)
{
    CreateMissingCollisionObjects(
        entityWorld, collisionWorld, assetSystem, tempArena);

    SyncCollisionObjectsWithEntities(entityWorld, collisionWorld);
}
