#pragma once

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef u32 b32;

typedef float f32;
typedef double f64;

typedef size_t umm;

#define internal static
#define global static

#define PI 3.14159265359f
#define ARRAY_LENGTH(X) (sizeof(X) / sizeof(X[0]))
#define ASSERT(X)                                                              \
    if (!(X))                                                                  \
    {                                                                          \
        fprintf(stderr, "\n\n**** ASSERTION FAILED ****\n%s:%d - %s\n\n\n",    \
            __FILE__, __LINE__, #X);                                           \
        *(volatile i32 *)0 = 0;                                                \
    }
#define INVALID_CODE_PATH(MSG) ASSERT(!MSG)
#define OffsetOf(STRUCT, MEMBER) ((umm)(&(((STRUCT *)0)->MEMBER)))
#define BIT(X) (1 << X)

#define Kilobytes(X) ((X) * 1024)
#define Megabytes(X) (Kilobytes(X) * 1024)
#define Gigabytes(X) (Megabytes(X) * 1024LL)

#define U32_MAX 0xFFFFFFFF
#define U16_MAX 0xFFFF
#define U8_MAX 0xFF

struct MemoryArena
{
    void *base;
    u64 size;
    u64 capacity;
};

inline void InitializeMemoryArena(
    MemoryArena *arena, void *buffer, u64 capacity)
{
    arena->base = buffer;
    arena->capacity = capacity;
    arena->size = 0;
}

inline void ClearMemoryArena(MemoryArena *arena) { arena->size = 0; }

inline void *AllocateBytes(MemoryArena *arena, u64 length)
{
    ASSERT(arena->size + length <= arena->capacity);
    void *result = (u8 *)arena->base + arena->size;
    arena->size += length;

    return result;
}

inline MemoryArena SubAllocateArena(MemoryArena *parent, u64 size)
{
    MemoryArena result = {};
    void *memory = AllocateBytes(parent, size);
    InitializeMemoryArena(&result, memory, size);

    return result;
}

inline void FreeFromMemoryArena(MemoryArena *arena, void *p)
{
    ASSERT(p >= arena->base);
    ASSERT(p < (u8 *)arena->base + arena->size);
    u64 newSize = (u8 *)p - (u8 *)arena->base;
    ASSERT(newSize < arena->size);
    arena->size = newSize;
}

#define ALLOCATE_STRUCT(ARENA, TYPE) (TYPE *)AllocateBytes(ARENA, sizeof(TYPE))

#define ALLOCATE_ARRAY(ARENA, TYPE, LENGTH)                                    \
    (TYPE *)AllocateBytes(ARENA, sizeof(TYPE) * LENGTH)

inline void SwapU32(u32 *a, u32 *b)
{
    u32 temp = *a;
    *a = *b;
    *b = temp;
}

inline const char *CopyString(const char *str, MemoryArena *arena)
{
    u32 len = strlen(str);
    char *copy = ALLOCATE_ARRAY(arena, char, len + 1);
    strncpy(copy, str, len + 1);
    return copy;
}

inline u8 SafeTruncateU32ToU8(u32 i)
{
    ASSERT(i <= U8_MAX);
    u8 result = (u8)i;
    return result;
}
