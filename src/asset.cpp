internal Font LoadFontFromDisk(MemoryArena *arena, const char *path, f32 size)
{
    Font result = {};
    result.glyphCount = 96;
    result.glyphs = ALLOCATE_ARRAY(arena, FontGlyph, result.glyphCount);

    MemoryArena tempArena = SubAllocateArena(arena, Kilobytes(512));

    // TODO: Expose as params
    u32 width = 512;
    u32 height = 512;
    u8 *bitmap = ALLOCATE_ARRAY(&tempArena, u8, width * height);
    stbtt_bakedchar characterData[96]; // FIXME: Where is 96 coming from!

    // TODO: Replace with ReadEntireFile
    FILE *f = fopen(path, "rb");
    u8 *fileData = ALLOCATE_ARRAY(&tempArena, u8, Kilobytes(128));
    fread(fileData, 1, Kilobytes(128), f);

    stbtt_fontinfo fontInfo = {};
    stbtt_InitFont(&fontInfo, (u8 *)fileData, 0);
    stbtt_BakeFontBitmap((u8 *)fileData, 0, size, bitmap, width, height, 32,
        ARRAY_LENGTH(characterData), characterData);

    i32 ascent, descent, lineGap;
    stbtt_GetFontVMetrics(&fontInfo, &ascent, &descent, &lineGap);
    f32 scale = stbtt_ScaleForPixelHeight(&fontInfo, size);

    result.lineSpacing = (ascent + descent + lineGap) * scale;
    result.height = (ascent + descent) * scale;

    result.texture = CreateGlyphSheetTexture(width, height, bitmap);

    for (u32 i = 0; i < ARRAY_LENGTH(characterData); ++i)
    {
         stbtt_aligned_quad q;

         f32 x = 0.0f;
         f32 y = 0.0f;
         stbtt_GetBakedQuad(
             characterData, width, height, (char)i, &x, &y, &q, 1);

         result.glyphs[i].x0 = q.x0;
         result.glyphs[i].y0 = -q.y0;
         result.glyphs[i].u0 = q.s0;
         result.glyphs[i].v0 = q.t0;
         result.glyphs[i].x1 = q.x1;
         result.glyphs[i].y1 = -q.y1;
         result.glyphs[i].u1 = q.s1;
         result.glyphs[i].v1 = q.t1;

         i32 advance, lsb;
         stbtt_GetGlyphHMetrics(&fontInfo, i, &advance, &lsb);

         result.glyphs[i].advance = advance * scale;
         result.glyphs[i].lsb = lsb * scale;
    }

    fclose(f);
    // NOTE: This also frees anything allocated after tempArena
    FreeFromMemoryArena(arena, tempArena.base);

    return result;
}
