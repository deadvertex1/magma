#extension GL_GOOGLE_include_directive : require

#include "pbr_material_interface.glsl"

void main()
{
    // Flip V coord for alpha for some reason
    vec2 uv = fs_in.texture_coordinates;
    uv.y = 1.0 - uv.y;
    float alpha = texture(u_albedo, uv).r;

    if (alpha <= 0.1)
    {
        discard;
    }

    vec3 albedo = mix(vec3(0.6, 0.7, 0.1) * 0.8, vec3(0.8, 0.3, 0.1), 1.0 - uv.y);
#if DISABLE_LIGHTING
    vec3 color = albedo;
#else
    vec3 N = normalize(fs_in.world_normal);
    vec3 color = pbr_material_model(albedo, N);
#endif
    o_color = vec4(color, 1);
}
