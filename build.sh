#!/bin/bash

compiler="clang++"

glfw3_libs=$(pkg-config --libs glfw3)
glew_libs=$(pkg-config --libs glew)
assimp_libs=$(pkg-config --libs assimp)
libs="$glfw3_libs $glew_libs $assimp_libs -ldl"
cflags="-O0 -g -Wall -Werror -Wextra -Wno-missing-braces -Wno-unused-parameter -Wno-unused-function -Wno-unused-variable -Wno-unused-result -ferror-limit=100"
glsl_flags="-DDISABLE_LIGHTING=0 -G --glsl-version 450 -Isrc/shaders/include"
shader_dst_dir=build/shaders
shader_src_dir=src/shaders

mkdir -p build

# Build shaders (TODO: Move to separate build script?)
if [ "$1" = "--build-shaders" ]; then
    mkdir -p "$shader_dst_dir"

    for input_file in "$shader_src_dir"/*.glsl
    do
        shader_name=$(basename "$input_file")
        output_file="$shader_dst_dir"/"${shader_name%.*}.spv"
        glslangValidator $glsl_flags -o "$output_file" "$input_file"
    done
fi

if [ "$1" = "--build-terrain-shaders" ]; then
    input_file="src/shaders/generate_height_map.comp.glsl"
    shader_name=$(basename "$input_file")
    output_file="$shader_dst_dir"/"${shader_name%.*}.spv"
    glslangValidator $glsl_flags -o "$output_file" "$input_file"
fi

if [ "$1" == "--profile" ]; then
    $compiler -ftime-trace $cflags -o build/game.json -c src/game.cpp
    $compiler -ftime-trace $cflags -o build/main.json -I thirdparty -c src/main.cpp
fi

# Build game library
$compiler $cflags -o build/game_temp.so src/game.cpp -fPIC -shared
mv build/game_temp.so build/game.so

# Build game exe
$compiler $cflags -o build/main -I thirdparty src/main.cpp $libs
