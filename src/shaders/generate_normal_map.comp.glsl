#extension GL_GOOGLE_include_directive : require

#include "compute_shader_common.glsl"

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout(binding = 0, rgba8) uniform writeonly image2D normalMap;
layout(location = 1) uniform sampler2D heightMap;
layout(location = 2) uniform uint textureWidth;

void main()
{
    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;

    f32 fx = float(x) / float(textureWidth);
    f32 fy = float(y) / float(textureWidth);

    // Compute normal
    vec2 textureCoords = vec2(fx, fy);
    float offset = 0.001;
    float heightScale = 1.0;
    float hL = texture(heightMap, textureCoords + vec2(-offset, 0)).r * heightScale;
    float hR = texture(heightMap, textureCoords + vec2(offset, 0)).r * heightScale;
    float hU = texture(heightMap, textureCoords + vec2(0, offset)).r * heightScale;
    float hD = texture(heightMap, textureCoords + vec2(0, -offset)).r * heightScale;

    vec3 normal = normalize(vec3(hL - hR, offset*2.0, hU - hD));
    normal = 0.5 * normal + vec3(0.5);

    imageStore(normalMap, ivec2(x, y), vec4(normal, 1));
}
