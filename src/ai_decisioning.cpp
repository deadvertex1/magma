internal i32 FindClosest(
    vec3 origin, vec3 *positions, u32 count, f32 maxDistance = 100.0f)
{
    i32 closest = -1;
    f32 minDistSq = maxDistance * maxDistance;
    for (u32 i = 0; i < count; i++)
    {
        f32 distanceSq = LengthSq(origin - positions[i]);
        if (distanceSq < minDistSq)
        {
            closest = i;
            minDistSq = distanceSq;
        }
    }

    return closest;
}

#if 0
internal DecisionResult DoDecisioning(
    u32 *entityIds, vec3 *positions, u32 count, vec3 myPosition)
{
    DecisionResult result = {};

    i32 closest = FindClosest(myPosition, positions, count);
    if (closest >= 0)
    {
        result.action = Action_MoveToPosition;
        result.moveToPosition = positions[closest];
    }

    return result;
}
#endif
