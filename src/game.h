#pragma once

struct PlayerUIState
{
    b32 showInventory;
    u32 dragSlot;
    u32 entityIdBeingDragged;
    u32 entityIdBeingDraggedFrom; // Entity for inventory
    u32 containerEntityId;
    u32 craftingStationEntityId;

    b32 showOnScreenHint;
    const char *hintText;

    // Pretty sure there will only ever be 1 of these
    InventoryCommand commands[4];
    u32 commandCount;
};

struct ConsoleState
{
    char inputBuffer[80];
    u32 inputBufferLength;
};

struct DebugEntityStateFrame
{
    vec3 position;
    vec3 velocity;
};

enum
{
    DebugEntityRecordingMode_None,
    DebugEntityRecordingMode_Recording,
    DebugEntityRecordingMode_Playback,
};

struct DebugEntityStateRecording
{
    MemoryArena arena;
    DebugEntityStateFrame *frames;
    u32 capacity;
    u32 count;

    u32 mode;

    // Playback
    u32 currentFrame;
};

struct CachedProfilerState
{
    ProfiledBlock blocks[256];
    u32 blockCount;
    b32 update;
};

struct AverageBuffer
{
    f32 accumulator;
    u32 sampleCount;
};

struct GameState
{
    MemoryArena persistentArena;

    // Systems
    EntityWorld entityWorld;
    CollisionWorld collisionWorld;
    DebugDrawingSystem debugDrawingSystem;
    ItemDatabase itemDatabase;
    InventorySystem inventorySystem;
    GameEventQueue eventQueue;
    CVarSystem cvarSystem;
    CommandSystem commandSystem;
    ConsoleState console;
    Terrain terrain;

    DebugEntityStateRecording debugEntityStateRecording;

    PlayerUIState playerUIState;

    RandomNumberGenerator worldGenRng;
    RandomNumberGenerator spawnRng;
    RandomNumberGenerator aiRng;

    PlayerCommand prevPlayerCommand;
    PlayerCommand playerCommands[32];
    u32 playerCommandCount;

    DeployableVisualizationState deployableVisualizationState;
    DeployableItemDataTable deployableItemDataTable;

    ScatterSystem scatterSystem;

    AiPlanningSystem aiPlanningSystem;

    ParticleSystem particleSystem;

    GrassSystem grassSystem;

    BulletSystem bulletSystem;

    CachedProfilerState cachedProfilerState;

    AverageBuffer frameTimeAverage;
    AverageBuffer gpuTimeAverage;

    u32 debugVerticesDrawCount;

    u32 worldTimeSeconds;

    u32 skyHash;
    f32 timeUntilNextSkyUpdate;

    b32 isInitialized;
    f32 dt;
    f32 frameTimeAccumulator; // NOTE: Used for fixed timestep
    b32 lockCameraRotation;
    f64 currentTime;
};

struct TextDrawCall
{
    u32 offset;
    u32 count;
    u32 shaderAssetId;
    u32 texture;
    f32 depth;
    vec4 color;
};

struct TextBuffer
{
    VertexText *vertices;
    u32 count;
    u32 max;

    TextDrawCall *drawCalls;
    u32 drawCallCount;
    u32 maxDrawCalls;
};

// TODO: Where should this live?
inline void CopyVertices(vec3 *dst, Vertex *src, u32 length)
{
    for (u32 i = 0; i < length; i++)
    {
        *dst = src->position;
        dst++;
        src++;
    }
}

inline void OffsetIndices(u32 *indices, u32 count, u32 offset)
{
    for (u32 i = 0; i < count; i++)
    {
        indices[i] += offset;
    }
}

inline void Accumulate(AverageBuffer *buffer, f32 value)
{
    if (buffer->sampleCount > 64)
    {
        buffer->sampleCount = 0;
        buffer->accumulator = 0.0f;
    }

    buffer->accumulator += value;
    buffer->sampleCount++;
}

inline f32 GetAverage(AverageBuffer *buffer)
{
    f32 result = buffer->sampleCount > 0
                     ? buffer->accumulator / (f32)buffer->sampleCount
                     : 0.0f;
    return result;
}
