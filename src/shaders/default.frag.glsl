#extension GL_GOOGLE_include_directive : require

#include "pbr_material_interface.glsl"

void main()
{
    vec3 albedo = texture(u_albedo, fs_in.texture_coordinates).rgb;
#if DISABLE_LIGHTING
    vec3 color = albedo;
#else
    vec3 N = normalize(fs_in.world_normal);
    vec3 color = pbr_material_model(albedo, N);
#endif
    o_color = vec4(color, 1.0);
}
