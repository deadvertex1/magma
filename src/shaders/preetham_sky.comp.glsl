#extension GL_GOOGLE_include_directive : require

#include "compute_shader_common.glsl"

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout(binding = 0, rgba32f) uniform writeonly imageCube outputImage;
layout(location = 1) uniform uint cubeMapWidth;
layout(location = 2) uniform vec3 u_sun_direction = vec3(0, -1, 0);
layout(location = 3) uniform float u_turbidity = 2.0;

// Copied from https://www.shadertoy.com/view/llSSDR#

vec3 xyYToXYZ( in vec3 xyY )
{
    float x = xyY.r;
    float y = xyY.g;
    float Y = xyY.b;

    float X = x * ( Y / y );
    float Z = ( 1.0 - x - y ) * ( Y / y );

    return vec3(X,Y,Z);
}

vec3 XYZToRGB( in vec3 XYZ )
{
    // CIE/E
    mat3 M = mat3
    (
         2.3706743, -0.9000405, -0.4706338,
        -0.5138850,  1.4253036,  0.0885814,
         0.0052982, -0.0146949,  1.0093968
    );

    return XYZ * M;
}


vec3 xyYToRGB( in vec3 xyY )
{
    vec3 XYZ = xyYToXYZ( xyY );
    vec3 RGB = XYZToRGB( XYZ );
    return RGB;
}

// NOTE: These values differ from SunSky implementation
void calculatePerezDistribution( in float t, out vec3 A, out vec3 B, out vec3 C, out vec3 D, out vec3 E )
{
    A = vec3(-0.0193 * t - 0.2592, -0.0167 * t - 0.2608,  0.1787 * t - 1.4630);
    B = vec3(-0.0665 * t + 0.0008, -0.0950 * t + 0.0092, -0.3554 * t + 0.4275);
    C = vec3(-0.0004 * t + 0.2125, -0.0079 * t + 0.2102, -0.0227 * t + 5.3251);
    D = vec3(-0.0641 * t - 0.8989, -0.0441 * t - 1.6537,  0.1206 * t - 2.5771);
    E = vec3(-0.0033 * t + 0.0452, -0.0109 * t + 0.0529, -0.0670 * t + 0.3703);
}

vec3 calculateZenith( in float t, in float thetaS )
{
    float chi       = ( 4.0 / 9.0 - t / 120.0 ) * ( PI - 2.0 * thetaS );
    float Yz        = ( 4.0453 * t - 4.9710 ) * tan( chi ) - 0.2155 * t + 2.4192;

    float theta2    = thetaS * thetaS;
    float theta3    = theta2 * thetaS;
    float T         = t;
    float T2        = t * t;

    float xz =
        ( 0.00165 * theta3 - 0.00375 * theta2 + 0.00209 * thetaS + 0.0)     * T2 +
        (-0.02903 * theta3 + 0.06377 * theta2 - 0.03202 * thetaS + 0.00394) * T +
        ( 0.11693 * theta3 - 0.21196 * theta2 + 0.06052 * thetaS + 0.25886);

    float yz =
        ( 0.00275 * theta3 - 0.00610 * theta2 + 0.00317 * thetaS + 0.0)     * T2 +
        (-0.04214 * theta3 + 0.08970 * theta2 - 0.04153 * thetaS + 0.00516) * T +
        ( 0.15346 * theta3 - 0.26756 * theta2 + 0.06670 * thetaS + 0.26688);

    return vec3( xz, yz, Yz );
}

vec3 calculatePerez( in float theta, in float gamma, in vec3 A, in vec3 B, in vec3 C, in vec3 D, in vec3 E )
{
    return ( 1.0 + A * exp( B / cos( theta ) ) ) * ( 1.0 + C * exp( D * gamma ) + E * cos( gamma ) * cos( gamma ) );
}

vec3 calculatePreethamSkyRGB(in vec3 sunDir, in vec3 viewDir, in float turbidity)
{
    // Calculate Perez Distribution
    vec3 A, B, C, D, E;
    calculatePerezDistribution(turbidity, A, B, C, D, E);

    float thetaS = acos(max(0.0, sunDir.y));
    float thetaV = acos(max(0.0, viewDir.y));
    float gammaV = acos(max(0.0, dot(sunDir, viewDir)));

    // Based on https://github.com/andrewwillmott/sun-sky/blob/master/SunSky.cpp#L463
    if (sunDir.y < 0.0f)    // Handle sun going below the horizon
    {
        float s = clamp(1.0f + sunDir.y * 50.0f, 0.0, 1.0);   // goes from 1 to 0 as the sun sets

        // Take C/E which control sun term to zero
        C *= s;
        E *= s;
    }

    // Calculate zenith xyY
    vec3 zenith = calculateZenith(turbidity, thetaS);

    // Calculate Perez xyY numerator
    vec3 perezNumerator = calculatePerez(thetaV, gammaV, A, B, C, D, E);
    // Calculate Perez xyY denominator
    vec3 perezDenom = calculatePerez(0.0f, thetaS, A, B, C, D, E);

    vec3 xyY = zenith * (perezNumerator / perezDenom);

    // TODO: Use other implementation
    return xyYToRGB(xyY);
}

void main()
{
    uint width = cubeMapWidth;
    uint height = cubeMapWidth;

    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
    uint layerIndex = gl_GlobalInvocationID.z;

    // Map layer index to basis vectors for cube map face
    BasisVectors basis = MapCubeMapLayerIndexToBasisVectors(layerIndex);

    f32 fx = float(x) / float(width);
    f32 fy = float(y) / float(height);

    // Flip Y axis
    fy = 1.0f - fy;

    // Map to -1 to 1
    fx = fx * 2.0f - 1.0f;
    fy = fy * 2.0f - 1.0f;

    vec3 dir = basis.forward + basis.right * fx + basis.up * fy;
    dir = normalize(dir);
    vec3 v = dir;
    vec3 color = calculatePreethamSkyRGB(u_sun_direction, v, u_turbidity);

    // Scale radiance
    color = color * 0.05;

    imageStore(outputImage, ivec3(x, y, layerIndex), vec4(color, 1));
}
