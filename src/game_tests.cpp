internal void ProcessInventoryCommand(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, ItemDatabase *itemDatabase,
    InventoryCommand command);

internal void GiveItemToPlayer(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, ItemDatabase *itemDatabase, u32 itemId,
    u32 quantity = 1)
{
    if (FindItem(itemDatabase, itemId) != NULL)
    {
        u32 playerEntityId = GetPlayerEntityId(entityWorld);
        AddItemToInventory(entityWorld, inventorySystem, itemDatabase,
            playerEntityId, itemId, quantity);
    }
    else
    {
        LOG_MSG("Unknown item id \"%u\"", itemId);
    }
}

internal void RunPickAxeViewModelTest(GameState *gameState)
{
    EntityWorld *world = &gameState->entityWorld;

    world->sunLight.position = Vec3(0, 4, 0);
    world->sunLight.rotation =
        Quat(Vec3(1, 0, 0), PI * -0.25f) * Quat(Vec3(0, 1, 0), PI * -0.25);
    world->sunLight.color = Vec3(1, 0.99, 0.95) * 10.0f;

    world->skyLight.position = Vec3(2, 4, 0);
    world->skyLight.color = Vec3(0.7, 0.7, 1) * 0.4;

    // Given a player with a pick axe
    u32 playerEntityId = AddPlayer(world, Vec3(0, 2, 0));

    CreateMissingInventoriesSystem(world, &gameState->inventorySystem);

    GiveItemToPlayer(world, &gameState->inventorySystem,
        &gameState->itemDatabase, Item_PickAxe);

    // When we equip the pick axe
    InventoryCommand command = {};
    command.type = InventoryCommandType_TransferItem;
    command.srcContainer = playerEntityId;
    command.dstContainer = playerEntityId;
    command.srcSlot = 0;
    command.dstSlot = MapBeltSlotToInventorySlot(0);
    ProcessInventoryCommand(
        world, &gameState->inventorySystem, &gameState->itemDatabase, command);

    // Then its in the active item slot (view model too hard)
    Player player = GetEntityPlayerController(world, playerEntityId);
    u32 activeBeltSlot = player.activeBeltSlot;
    u32 activeItemId = 0;
    GetItemIdInBeltSlot(world, &gameState->inventorySystem, playerEntityId,
        activeBeltSlot, &activeItemId);
    ASSERT(activeItemId == Item_PickAxe);

    // Cleanup
    ClearEntityWorld(world);
    // FIXME: Don't leak inventories
}

internal void RunCraftTest(GameState *gameState)
{
    EntityWorld *world = &gameState->entityWorld;

    // Given a player with 5 wood and 5 stone
    u32 playerEntityId = AddPlayer(world, Vec3(0, 2, 0));
    CreateMissingInventoriesSystem(world, &gameState->inventorySystem);
    GiveItemToPlayer(world, &gameState->inventorySystem,
        &gameState->itemDatabase, Item_Wood, 5);
    GiveItemToPlayer(world, &gameState->inventorySystem,
        &gameState->itemDatabase, Item_Stone, 5);

    // When we craft a hatchet
    InventoryCommand command = {};
    command.type = InventoryCommandType_CraftItem;
    command.srcContainer = playerEntityId;
    command.recipeId = Recipe_Hatchet;
    ProcessInventoryCommand(
        world, &gameState->inventorySystem, &gameState->itemDatabase, command);

    // Then we have a hatchet in the player's inventory
    u32 count = CountItem(
        world, &gameState->inventorySystem, playerEntityId, Item_Hatchet);
    ASSERT(count == 1);

    // Cleanup
    ClearEntityWorld(world);
    // FIXME: Don't leak inventories
}

internal void RunPlayerMovementTest(GameState *gameState)
{
    EntityWorld *world = &gameState->entityWorld;

    // Given a player entity
    u32 playerEntityId = AddPlayer(world, Vec3(0, 2, 0));

    // When we apply a player command for moving forwards
    PlayerCommand command = {};
    command.forward = 1.0f;
    command.dt = 1.0f;

    vec3 prevPosition = GetEntityPosition(world, playerEntityId);
    UpdatePlayer(world, playerEntityId, &gameState->collisionWorld,
        &gameState->eventQueue, &gameState->itemDatabase,
        &gameState->inventorySystem, &command, 1);

    // Then the player's position changes
    vec3 currentPosition = GetEntityPosition(world, playerEntityId);
    vec3 delta = currentPosition - prevPosition;
    ASSERT(Abs(delta.z) > 0.001f);

    // Cleanup
    ClearEntityWorld(world);
    // FIXME: Don't leak inventories
}

internal void RunPlayerPickupTest(
    GameState *gameState, AssetSystem *assetSystem, MemoryArena *tempArena)
{
    EntityWorld *world = &gameState->entityWorld;

    // Given a player entity
    u32 playerEntityId = AddPlayer(world, Vec3(0, 2, 0));
    u32 pickupEntityId = AddItemPickup(world, Vec3(0, 2, -1), Item_Hatchet);

    CreateMissingCollisionObjects(
        world, &gameState->collisionWorld, assetSystem, tempArena);
    CreateMissingInventoriesSystem(world, &gameState->inventorySystem);
    ASSERT(gameState->collisionWorld.objects.count == 1);

    // When we interact with a pickup
    PlayerCommand command = {};
    command.actions = PlayerCommandAction_Interact;

    ASSERT(gameState->eventQueue.length == 0);
    UpdatePlayer(world, playerEntityId, &gameState->collisionWorld,
        &gameState->eventQueue, &gameState->itemDatabase,
        &gameState->inventorySystem, &command, 1);

    // Then an event is added to the queue
    ASSERT(gameState->eventQueue.length == 1);
    ASSERT(gameState->eventQueue.buffer[0].type == EventType_Interaction);

    // When we process the event queue
    ProcessEventQueue(gameState);

    // Then the player receives the item
    u32 count = CountItem(
        world, &gameState->inventorySystem, playerEntityId, Item_Hatchet);
    ASSERT(count == 1);

    // Cleanup
    ClearEntityWorld(world);
    // FIXME: Don't leak inventories
}

internal void RunParticleEmitterTest(GameState *gameState)
{
    EntityWorld *world = &gameState->entityWorld;

    // Given a particleEmitter entity
    u32 entityId = AddParticleEmitter(
        world, Vec3(0, 2, 0), HashStringU32("particles/smoke"));

    // When we run the particle emitter system thing
    ParticleEmitterSystem(world, &gameState->particleSystem);

    // Then the entity has a particle buffer assigned to it
    Entity *entity = FindEntityById(world, entityId);
    ASSERT(entity->particleEmitterId != 0);

    // Cleanup
    ClearEntityWorld(world);
    // FIXME: Don't leak particle emitters
}
