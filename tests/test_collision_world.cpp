#include <cmath>
#include <cfloat>  // For FLT_EPSILON
#include <cstring> // For memcpy

#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "dictionary.h"
#include "hash.h"
#include "math_utils.h"
#include "math_lib.h"
#include "aabb_tree.h"
#include "collision_world.h"

#include "ray_intersection.h"

#define PROF_SCOPE()
#define LOG_MSG(MSG, ...)

// Stubs
f32 RayIntersectAabb(
    vec3 boxMin, vec3 boxMax, vec3 rayOrigin, vec3 rayDirection)
{
    return -1.0f;
}

RayIntersectTriangleResult RayIntersectTriangleMesh(
    CollisionMesh *mesh, vec3 rayOrigin, vec3 rayDirection, f32 tmax)
{
    RayIntersectTriangleResult result = {};
    result.t = -1.0f;
    return result;
}

internal RayIntersectTriangleResult RayIntersectQuadTreeTerrain(
    Terrain *terrain, vec3 rayOrigin, vec3 rayDirection, f32 tmax = F32_MAX,
    u32 flags = CollisionWorldDebugDrawFlags_None)
{
    RayIntersectTriangleResult result = {};
    result.t = -1.0f;
    return result;
}

internal AabbTree BuildAabbTree(vec3 *boxMins, vec3 *boxMaxes, u32 count,
    u32 maxNodes, MemoryArena *arena, MemoryArena *tempArena)
{
    AabbTree result = {};
    return result;
}

#include "collision_world.cpp"

TEST_GROUP(CollisionWorld);

global MemoryArena g_Arena;

TEST_SETUP(CollisionWorld)
{
    u32 size = Kilobytes(1);
    InitializeMemoryArena(&g_Arena, malloc(size), size);
}

TEST_TEAR_DOWN(CollisionWorld) { free(g_Arena.base); }

TEST(CollisionWorld, First)
{
    // Given a collision world
    CollisionWorld collisionWorld = {};

    CollisionWorldConfig config = {};
    config.maxCollisionMeshes = 2;
    config.maxCollisionObjects = 4;
    config.meshDataStorageSize = 0;
    InitializeCollisionWorld(&collisionWorld, &g_Arena, config);

    // Given some collision meshes
    AabbTreeNode rootNode = {};
    rootNode.min = Vec3(-0.5f);
    rootNode.max = Vec3(0.5f);
    CollisionMesh collisionMesh = {};
    collisionMesh.aabbTree.root = &rootNode;
    AddCollisionMesh(&collisionWorld, collisionMesh, 1);
    AddCollisionMesh(&collisionWorld, collisionMesh, 2);

    // Create collision objects
    u32 collisionObjectId1 =
        AddCollisionObject(&collisionWorld, 1, Identity(), Identity());
    u32 collisionObjectId2 =
        AddCollisionObject(&collisionWorld, 2, Identity(), Identity());

    // Delete collision objects
    RemoveCollisionObject(&collisionWorld, collisionObjectId1);

    // Create new ones and check for corruption
    u32 collisionObjectId3 =
        AddCollisionObject(&collisionWorld, 2, Identity(), Identity());
}

TEST_GROUP_RUNNER(CollisionWorld) { RUN_TEST_CASE(CollisionWorld, First); }
