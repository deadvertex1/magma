#pragma once

struct Aabb
{
    vec3 min;
    vec3 max;
};

struct CollisionMesh
{
    vec3 *vertices;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;

    AabbTree aabbTree;
};

enum
{
    CollisionShape_Mesh,
    CollisionShape_Terrain,
};

struct CollisionObject
{
    // Transform
    mat4 transform;
    mat4 invTransform;

    // Bounding box
    Aabb aabb;

    // Collision shape
    u32 mesh;
    u32 shape;

    // Avoid rebuilding AABB every frame
    u32 stateHash;
};

/*
Collision object storage notes:
- Optimized for processing/querying by the collision system (i.e. ray casts)
    - Some sort of acceleration structure
- Will need to be able to add/remove/update/retrieve object data frequently too
    - Need to support lookup by ID, doesn't need to be super efficient though
*/
struct Terrain;
struct CollisionWorld
{
    MemoryArena meshDataArena;

    Dictionary meshes;
    Dictionary objects;

    u32 nextId;

    Terrain *terrain; // TODO: Better way to do this?
};

struct RayIntersectWorldResult
{
    u32 collisionObjectId;
    f32 t;
    vec3 normal;
};

enum
{
    CollisionWorldDebugDrawFlags_None = 0x0,
    CollisionWorldDebugDrawFlags_Triangles = 0x1,
    CollisionWorldDebugDrawFlags_TriangleAabbs = 0x2,
    CollisionWorldDebugDrawFlags_MeshVertices = 0x4,
    CollisionWorldDebugDrawFlags_Aabbs = 0x8,
};

struct CollisionWorldConfig
{
    u32 maxCollisionMeshes;
    u32 maxCollisionObjects;
    u32 meshDataStorageSize;
};
