internal void InitializeFileCache(
    FileCache *cache, GetFileModTimestampFunction *fn)
{
    // TODO: Allocate entries
    cache->getFileModTimestampFunction = fn;
}

internal void AddEntryToCache(
    FileCache *cache, u32 assetType, u32 assetId, const char *path)
{
    ASSERT(cache->count < ARRAY_LENGTH(cache->entries));

    FileCacheEntry *entry = cache->entries + cache->count++;
    entry->path = path;
    entry->modTime = cache->getFileModTimestampFunction(path);
    entry->wasModified = false;
    entry->assetType = assetType;
    entry->assetId = assetId;
}

internal void FindInvalidatedEntries(FileCache *cache)
{
    for (u32 entryIndex = 0; entryIndex < cache->count; ++entryIndex)
    {
        FileCacheEntry *entry = cache->entries + entryIndex;
        u64 modTime = cache->getFileModTimestampFunction(entry->path);
        if (modTime != entry->modTime)
        {
            entry->wasModified = true;
            entry->modTime = modTime;
        }
    }
}
