#pragma once

struct MemoryPool
{
    void *base;
    u8 *occupancy; // TODO: Replace with a bitset
    u32 width;
    u32 count;
};

inline MemoryPool CreateMemoryPool(
    MemoryArena *arena, u32 width, u32 count)
{
    ASSERT(count != U32_MAX); // U32_MAX is used as invalid index

    MemoryPool pool = {};
    pool.width = width;
    pool.count = count;

    pool.occupancy = ALLOCATE_ARRAY(arena, u8, count);
    memset(pool.occupancy, 0, count);

    pool.base = AllocateBytes(arena, width * count);

    return pool;
}

inline void* AllocateFromPool(MemoryPool *pool, u32 size)
{
    void *p = NULL;

    // Find first free slot
    u32 index = U32_MAX;
    for (u32 i = 0; i < pool->count; i++)
    {
        if (pool->occupancy[i] == 0)
        {
            index = i;
            break;
        }
    }

    // Calculate memory slot/offset
    u32 offset = index * pool->width;
    p = (u8 *)pool->base + offset;

    // Mark slot as occupied
    pool->occupancy[index] = 1;

    return p;
}

inline void FreeFromPool(MemoryPool *pool, void *p)
{
    // Safety checks
    ASSERT(p >= pool->base);

    // Calculate slot index
    umm offset = (u8 *)p - (u8 *)pool->base;
    ASSERT(offset < pool->count * pool->width);

    // Check that the pointer provided is for the start of the slot and not
    // offset
    ASSERT(offset % pool->width == 0);

    // TODO: Potential truncation
    u32 index = offset / pool->width;

    // Confirm that slot is actually occupied and not a double free
    ASSERT(pool->occupancy[index]);

    // Clear the slot
    pool->occupancy[index] = 0;
}

inline u32 CountOccupancy(MemoryPool pool)
{
    u32 count = 0;
    for (u32 i = 0; i < pool.count; i++)
    {
        if (pool.occupancy[i])
        {
            count++;
        }
    }

    return count;
}
