layout(std140, binding=0) uniform CameraUniformBuffer
{
    mat4 view_matrices[8];
    mat4 projection_matrices[8];

    mat4 matrixPalette[255];

    float t;
};
