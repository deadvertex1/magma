#include "unity.h"
#include "unity_fixture.h"

#include <cstdint>
#include <cstdio>
#include <cmath>
#include <cfloat>
#include <cstring>

#include "platform.h"
#include "math_utils.h"
#include "math_lib.h"
#include "terrain.h"
// --- Used just for debug drawing..
#include "dictionary.h"
#include "contiguous_list.h"
#include "asset_system.h"
#include "opengl.h"
#include "mesh.h"
#include "debug_drawing.h"
// ----
#include "hash.h"
#include "asset_ids.h"
#include "ray_intersection.h"
#include "aabb_tree.h"
#include "collision_world.h"
#include "assertions.h"

#define PROF_SCOPE()

void DrawLine(DebugDrawingBuffer *buffer, vec3 start, vec3 end, vec3 color);
void DrawBox(DebugDrawingBuffer *buffer, vec3 min, vec3 max, vec3 color);
void DrawTriangle(
    DebugDrawingBuffer *buffer, vec3 a, vec3 b, vec3 c, vec3 color);
void DrawPoint(DebugDrawingBuffer *buffer, vec3 p, f32 size, vec3 color);

u32 GetTexture(AssetSystem *assetSystem, u32 id);
u32 GetShader(AssetSystem *assetSystem, u32 id);
MeshState *GetMesh(AssetSystem *assetSystem, u32 id);

#define LOG_MSG(MSG, ...)

// FIXME: Not the right place for this but main.cpp needs access
#define TERRAIN_PATCH_WIDTH 64
#define TERRAIN_SCALE_XZ 2048
#define TERRAIN_SCALE_Y 50

// TODO: Remove this
#include <GL/glew.h>

#include "ray_intersection.cpp"

#include "terrain.cpp"

TEST_GROUP(Terrain);

global MemoryArena g_Arena;

TEST_SETUP(Terrain)
{
    // To handle hard-coded constants in InitializeTerrain
    u32 size = Megabytes(2);
    InitializeMemoryArena(&g_Arena, malloc(size), size);
}

TEST_TEAR_DOWN(Terrain) { free(g_Arena.base); }

void GenerateTestTerrainHeightMap(Terrain *terrain, f32 height)
{
    u32 width = 64;
    f32 *data = ALLOCATE_ARRAY(&terrain->heightMapArena, f32, width * width);
    for (u32 y = 0; y < width; y++)
    {
        for (u32 x = 0; x < width; x++)
        {
            data[y * width + x] = height;
        }
    }

    HeightMap heightMap = {};
    heightMap.values = data;
    heightMap.width = width;
    heightMap.height = width;

    terrain->heightMap = heightMap;
}

TEST(Terrain, MappingWorldSpaceToTerrainGridCell)
{
    vec3 worldSpaceP = Vec3(15.5f, 12.0f, 7.23f);

    Terrain terrain = {};
    InitializeTerrain(&terrain, &g_Arena);
    terrain.position = Vec3(0, 0, 0);
    terrain.scale = Vec3(64, 20, 64);

    TerrainGridCellCoordinate gridCell =
        MapWorldSpaceToTerrainGridCell(&terrain, worldSpaceP);

    TEST_ASSERT_EQUAL_UINT(15, gridCell.x);
    TEST_ASSERT_EQUAL_UINT(7, gridCell.y);
}

TEST(Terrain, MappingWorldSpaceToTerrainGridCellClamp)
{
    vec3 worldSpaceP = Vec3(66, 0, -5);

    Terrain terrain = {};
    InitializeTerrain(&terrain, &g_Arena);
    terrain.position = Vec3(0, 0, 0);
    terrain.scale = Vec3(64, 20, 64);

    TerrainGridCellCoordinate gridCell =
        MapWorldSpaceToTerrainGridCell(&terrain, worldSpaceP);

    TEST_ASSERT_EQUAL_UINT(64, gridCell.x);
    TEST_ASSERT_EQUAL_UINT(0, gridCell.y);
}

TEST(Terrain, GridCellCoordinatesToAabb)
{
    TerrainGridCellCoordinate coord = {2, 3};

    Terrain terrain = {};
    InitializeTerrain(&terrain, &g_Arena);
    terrain.position = Vec3(0, 0, 0);
    terrain.scale = Vec3(64, 20, 64);

    GenerateTestTerrainHeightMap(&terrain, 0.5f);

    Aabb aabb = GetAabbForTerrainGridCell(&terrain, coord, Vec4(0, 0, 1, 1));
    AssertWithinVec3(EPSILON, Vec3(2.0f, 10.0f, 3.0f), aabb.min);
    AssertWithinVec3(EPSILON, Vec3(3.0f, 10.0f, 4.0f), aabb.max);
}

TEST_GROUP_RUNNER(Terrain)
{
    RUN_TEST_CASE(Terrain, MappingWorldSpaceToTerrainGridCell);
    RUN_TEST_CASE(Terrain, MappingWorldSpaceToTerrainGridCellClamp);
    RUN_TEST_CASE(Terrain, GridCellCoordinatesToAabb);
}
