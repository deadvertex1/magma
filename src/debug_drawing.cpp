internal void DrawLine(
    DebugDrawingBuffer *buffer, vec3 start, vec3 end, vec3 color)
{
    if (buffer->count + 2 <= buffer->max)
    {
        VertexPC *a = buffer->vertices + buffer->count++;
        VertexPC *b = buffer->vertices + buffer->count++;

        a->position = start;
        a->color = color;
        b->position = end;
        b->color = color;
    }
}

internal void DrawBox(
    DebugDrawingBuffer *buffer, vec3 min, vec3 max, vec3 color)
{
    DrawLine(
        buffer, Vec3(min.x, min.y, min.z), Vec3(max.x, min.y, min.z), color);
    DrawLine(
        buffer, Vec3(max.x, min.y, min.z), Vec3(max.x, min.y, max.z), color);
    DrawLine(
        buffer, Vec3(max.x, min.y, max.z), Vec3(min.x, min.y, max.z), color);
    DrawLine(
        buffer, Vec3(min.x, min.y, max.z), Vec3(min.x, min.y, min.z), color);

    DrawLine(
        buffer, Vec3(min.x, max.y, min.z), Vec3(max.x, max.y, min.z), color);
    DrawLine(
        buffer, Vec3(max.x, max.y, min.z), Vec3(max.x, max.y, max.z), color);
    DrawLine(
        buffer, Vec3(max.x, max.y, max.z), Vec3(min.x, max.y, max.z), color);
    DrawLine(
        buffer, Vec3(min.x, max.y, max.z), Vec3(min.x, max.y, min.z), color);

    DrawLine(
        buffer, Vec3(min.x, min.y, min.z), Vec3(min.x, max.y, min.z), color);
    DrawLine(
        buffer, Vec3(max.x, min.y, min.z), Vec3(max.x, max.y, min.z), color);
    DrawLine(
        buffer, Vec3(max.x, min.y, max.z), Vec3(max.x, max.y, max.z), color);
    DrawLine(
        buffer, Vec3(min.x, min.y, max.z), Vec3(min.x, max.y, max.z), color);
}

internal void DrawPoint(
    DebugDrawingBuffer *buffer, vec3 p, f32 size, vec3 color)
{
    DrawLine(buffer, p - Vec3(size, 0, 0), p + Vec3(size, 0, 0), color);
    DrawLine(buffer, p - Vec3(0, size, 0), p + Vec3(0, size, 0), color);
    DrawLine(buffer, p - Vec3(0, 0, size), p + Vec3(0, 0, size), color);
}

internal void DrawTriangle(
    DebugDrawingBuffer *buffer, vec3 a, vec3 b, vec3 c, vec3 color)
{
    DrawLine(buffer, a, b, color);
    DrawLine(buffer, b, c, color);
    DrawLine(buffer, c, a, color);
}

internal void DrawCapsule(DebugDrawingBuffer *buffer, vec3 a, vec3 b,
    f32 radius, u32 segmentCount, vec3 color)
{
    vec3 v = b - a;

    vec3 up = v;
    vec3 right = {};
    if (Abs(Dot(up, Vec3(0, 0, 1))) > 0.0f)
    {
        right = Cross(Vec3(0, 1, 0), up);
    }
    else
    {
        right = Cross(up, Vec3(0, 0, 1));
    }
    vec3 forward = Normalize(Cross(right, up));
    up = Normalize(up);
    right = Normalize(right);

    // Draw circle on local XZ plane
    for (u32 i = 0; i < segmentCount; ++i)
    {
        f32 t0 = (i / (f32)segmentCount) * 2.0f * PI;
        f32 t1 = ((i + 1) / (f32)segmentCount) * 2.0f * PI;

        f32 x0 = Sin(t0) * radius;
        f32 y0 = Cos(t0) * radius;
        vec3 p0 = right * x0 + forward * y0;

        f32 x1 = Sin(t1) * radius;
        f32 y1 = Cos(t1) * radius;
        vec3 p1 = right * x1 + forward * y1;

        DrawLine(buffer, p0 + a, p1 + a, color);
        DrawLine(buffer, p0 + b, p1 + b, color);
    }

    // Draw half circle on XY plane
    for (u32 i = 0; i < segmentCount; ++i)
    {
        f32 t0 = (i / (f32)segmentCount) * PI - 0.5f * PI;
        f32 t1 = ((i + 1) / (f32)segmentCount) * PI - 0.5f * PI;

        f32 x0 = Sin(t0) * radius;
        f32 y0 = Cos(t0) * radius;
        vec3 p0 = right * x0 + -up * y0;
        vec3 q0 = right * x0 + up * y0;

        f32 x1 = Sin(t1) * radius;
        f32 y1 = Cos(t1) * radius;
        vec3 p1 = right * x1 + -up * y1;
        vec3 q1 = right * x1 + up * y1;

        DrawLine(buffer, p0 + a, p1 + a, color);
        DrawLine(buffer, q0 + b, q1 + b, color);
    }

    // Draw half circle on ZY plane
    for (u32 i = 0; i < segmentCount; ++i)
    {
        f32 t0 = (i / (f32)segmentCount) * PI - 0.5f * PI;
        f32 t1 = ((i + 1) / (f32)segmentCount) * PI - 0.5f * PI;

        f32 x0 = Sin(t0) * radius;
        f32 y0 = Cos(t0) * radius;
        vec3 p0 = forward * x0 + -up * y0;
        vec3 q0 = forward * x0 + up * y0;

        f32 x1 = Sin(t1) * radius;
        f32 y1 = Cos(t1) * radius;
        vec3 p1 = forward * x1 + -up * y1;
        vec3 q1 = forward * x1 + up * y1;

        DrawLine(buffer, p0 + a, p1 + a, color);
        DrawLine(buffer, q0 + b, q1 + b, color);
    }

    // Draw vertical lines
    DrawLine(buffer, right * radius + a, right * radius + b, color);
    DrawLine(buffer, -right * radius + a, -right * radius + b, color);
    DrawLine(buffer, forward * radius + a, forward * radius + b, color);
    DrawLine(buffer, -forward * radius + a, -forward * radius + b, color);
}

internal void InitializeDebugDrawingSystem(
    DebugDrawingSystem *system, MemoryArena *arena)
{
    u32 maxShapes = 0x1000;
    memset(system, 0, sizeof(*system));
    system->arena = SubAllocateArena(
        arena, maxShapes * sizeof(DebugDrawingSystem::Shape) + 64);

    system->shapes = CreateContiguousList_(
        &system->arena, sizeof(DebugDrawingSystem::Shape), maxShapes);
}

inline void DrawLine(
    DebugDrawingSystem *system, vec3 start, vec3 end, vec3 color, f32 lifeTime)
{
    DebugDrawingSystem::Shape *shape =
        (DebugDrawingSystem::Shape *)AllocateFromContiguousList_(
            &system->shapes);
    if (shape != NULL)
    {
        shape->type = DebugDrawingSystem::Shape_Line;
        shape->timeRemaining = lifeTime;
        shape->color = color;
        shape->line.start = start;
        shape->line.end = end;
    }
}

inline void DrawBox(
    DebugDrawingSystem *system, vec3 min, vec3 max, vec3 color, f32 lifeTime)
{
    DebugDrawingSystem::Shape *shape =
        (DebugDrawingSystem::Shape *)AllocateFromContiguousList_(
            &system->shapes);
    if (shape != NULL)
    {
        shape->type = DebugDrawingSystem::Shape_Box;
        shape->timeRemaining = lifeTime;
        shape->color = color;
        shape->box.min = min;
        shape->box.max = max;
    }
}

inline void DrawPoint(DebugDrawingSystem *system, vec3 position, f32 size,
    vec3 color, f32 lifeTime)
{
    DebugDrawingSystem::Shape *shape =
        (DebugDrawingSystem::Shape *)AllocateFromContiguousList_(
            &system->shapes);
    if (shape != NULL)
    {
        shape->type = DebugDrawingSystem::Shape_Point;
        shape->timeRemaining = lifeTime;
        shape->color = color;
        shape->point.position = position;
        shape->point.size = size;
    }
}

inline void DrawTriangle(DebugDrawingSystem *system, vec3 a, vec3 b, vec3 c,
    vec3 color, f32 lifeTime)
{
    DebugDrawingSystem::Shape *shape =
        (DebugDrawingSystem::Shape *)AllocateFromContiguousList_(
            &system->shapes);
    if (shape != NULL)
    {
        shape->type = DebugDrawingSystem::Shape_Triangle;
        shape->timeRemaining = lifeTime;
        shape->color = color;
        shape->triangle.a = a;
        shape->triangle.b = b;
        shape->triangle.c = c;
    }
}

inline void DrawCapsule(DebugDrawingSystem *system, vec3 a, vec3 b, f32 radius,
    vec3 color, f32 lifeTime)
{
    DebugDrawingSystem::Shape *shape =
        (DebugDrawingSystem::Shape *)AllocateFromContiguousList_(
            &system->shapes);
    if (shape != NULL)
    {
        shape->type = DebugDrawingSystem::Shape_Capsule;
        shape->timeRemaining = lifeTime;
        shape->color = color;
        shape->capsule.a = a;
        shape->capsule.b = b;
        shape->capsule.radius = radius;
    }
}

inline b32 ShapeHasTimeRemaining(void *entry)
{
    DebugDrawingSystem::Shape *shape = (DebugDrawingSystem::Shape *)entry;
    return shape->timeRemaining > 0.0f;
}

inline void ReduceShapeTimeRemaining(void *entry, void *userData)
{
    DebugDrawingSystem::Shape *shape = (DebugDrawingSystem::Shape *)entry;
    f32 dt = *(f32 *)userData;
    shape->timeRemaining -= dt;
}

internal void UpdateDebugDrawingSystem(DebugDrawingSystem *system, f32 dt)
{
    FilterContiguousList(&system->shapes, ShapeHasTimeRemaining);
    MapContiguousList(&system->shapes, ReduceShapeTimeRemaining, &dt);
}

inline void DrawShape(void *entry, void *userData)
{
    DebugDrawingSystem::Shape *shape = (DebugDrawingSystem::Shape *)entry;
    DebugDrawingBuffer *buffer = (DebugDrawingBuffer *)userData;

    switch (shape->type)
    {
    case DebugDrawingSystem::Shape_Line:
        DrawLine(buffer, shape->line.start, shape->line.end, shape->color);
        break;
    case DebugDrawingSystem::Shape_Box:
        DrawBox(buffer, shape->box.min, shape->box.max, shape->color);
        break;
    case DebugDrawingSystem::Shape_Point:
        DrawPoint(
            buffer, shape->point.position, shape->point.size, shape->color);
        break;
    case DebugDrawingSystem::Shape_Triangle:
        DrawTriangle(buffer, shape->triangle.a, shape->triangle.b,
            shape->triangle.c, shape->color);
        break;
    case DebugDrawingSystem::Shape_Capsule:
        DrawCapsule(buffer, shape->capsule.a, shape->capsule.b,
            shape->capsule.radius, 16, shape->color);
        break;
    default:
        break;
    }
}

internal void DrawDebugDrawingSystem(
    DebugDrawingSystem *system, DebugDrawingBuffer *buffer)
{
    PROF_SCOPE();
    MapContiguousList(&system->shapes, DrawShape, buffer);
}
