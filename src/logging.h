#pragma once

#define MAX_LOG_LINE_LENGTH 256
#define MAX_LOG_ENTRIES 40

struct LogEntry
{
    char *text;
    u32 length;
};

struct LogBuffer
{
    MemoryPool memoryPool;

    // Ring buffer, stores capacity-1 entries
    LogEntry *entries;
    u32 capacity;
    u32 write;
    u32 read;
};

#define LOGGING_SINK(NAME) void NAME(void *userData, const char *msg)
typedef LOGGING_SINK(LoggingSinkFunction);

struct LoggingSink
{
    u32 key;
    void *userData;
    LoggingSinkFunction *function;
};

enum
{
    LoggingSink_Stdout,
    LoggingSink_Console,
};

#define MAX_LOGGING_SINKS 4
struct LoggingSystem
{
    LoggingSink sinks[MAX_LOGGING_SINKS];
    u32 count;
};

global LoggingSystem *g_LoggingSystem;

// FIXME: ##__VA_ARGS__ is GNU specific!
#define LOG_MSG(MSG, ...)                                                      \
    LogMessage_(g_LoggingSystem, MSG, __FILE__, __LINE__, ##__VA_ARGS__)

inline void LogMessage_(LoggingSystem *loggingSystem, const char *msg,
    const char *fileName, u32 lineNumber, ...)
{
    char buffer[256];
    va_list args;
    va_start(args, lineNumber);
    u32 count =
        snprintf(buffer, sizeof(buffer), "%s:%u - ", fileName, lineNumber);
    u32 remainder = sizeof(buffer) - count;
    vsnprintf(buffer + count, remainder, msg, args);
    va_end(args);

    for (u32 i = 0; i < loggingSystem->count; ++i)
    {
        LoggingSink sink = loggingSystem->sinks[i];
        sink.function(sink.userData, buffer);
    }
}

inline void AddSink(LoggingSystem *loggingSystem, u32 key,
    LoggingSinkFunction *function, void *userData)
{
    // Find existing sink
    LoggingSink *sink = NULL;
    for (u32 i = 0; i < loggingSystem->count; ++i)
    {
        if (loggingSystem->sinks[i].key == key)
        {
            sink = &loggingSystem->sinks[i];
            break;
        }
    }

    if (sink == NULL)
    {
        ASSERT(loggingSystem->count < ARRAY_LENGTH(loggingSystem->sinks));
        sink = loggingSystem->sinks + loggingSystem->count++;
    }

    sink->key = key;
    sink->function = function;
    sink->userData = userData;
}

inline u32 LogBufferLength(LogBuffer *logBuffer)
{
    u32 length = 0;
    if (logBuffer->write >= logBuffer->read)
    {
        length = logBuffer->write - logBuffer->read;
    }
    else
    {
        length = (logBuffer->capacity + logBuffer->write) - logBuffer->read;
    }

    return length;
}

