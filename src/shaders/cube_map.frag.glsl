#extension GL_GOOGLE_include_directive : require

#include "material_interface.frag.glsl"

layout(location = 0) uniform samplerCube u_cube_map;
layout(location = 3) uniform float u_exposure = 1.0;

void main()
{
    vec3 color = texture(u_cube_map, fs_in.local_position).rgb;
    color *= u_exposure;
    o_color = vec4(color, 1.0);
}
