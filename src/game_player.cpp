inline b32 WasActionTriggered(
    PlayerCommand cmd, PlayerCommand prevCmd, u32 action)
{
    b32 result = ((cmd.actions & action) && !(prevCmd.actions & action));
    return result;
}

internal PlayerCommand BuildPlayerCommand(
    InputState *input, PlayerCommand prevCmd, u32 inputFlags, f32 dt)
{
    PlayerCommand cmd = {};
    cmd.viewAngles = prevCmd.viewAngles;
    cmd.activeWeaponSlot = prevCmd.activeWeaponSlot;
    cmd.dt = dt;

    // Handle player movement
    f32 sens = 0.01f;
    vec3 newRotation =
        Vec3(-input->mouseRelPosY, -input->mouseRelPosX, 0.0f) * sens;

    if (inputFlags & PlayerInputFlag_ReceiveMouseInput)
    {
        cmd.viewAngles += newRotation;

        if (input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown)
        {
            cmd.actions |= PlayerCommandAction_PrimaryAttack;
        }
        if (input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown)
        {
            cmd.actions |= PlayerCommandAction_SecondaryAttack;
        }
    }

    cmd.viewAngles.x = Clamp(cmd.viewAngles.x, -PI * 0.5f, PI * 0.5f);

    if (cmd.viewAngles.y > 2.0f * PI)
    {
        cmd.viewAngles.y -= 2.0f * PI;
    }
    if (cmd.viewAngles.y < 2.0f * -PI)
    {
        cmd.viewAngles.y += 2.0f * PI;
    }

    if (inputFlags & PlayerInputFlag_ReceiveKeyboardInput)
    {
        if (input->buttonStates[KEY_W].isDown)
        {
            cmd.forward += 1.0f;
        }
        if (input->buttonStates[KEY_S].isDown)
        {
            cmd.forward -= 1.0f;
        }

        if (input->buttonStates[KEY_A].isDown)
        {
            cmd.right -= 1.0f;
        }
        if (input->buttonStates[KEY_D].isDown)
        {
            cmd.right += 1.0f;
        }

        if (input->buttonStates[KEY_F].isDown)
        {
            cmd.actions |= PlayerCommandAction_Interact;
        }

        if (input->buttonStates[KEY_SPACE].isDown)
        {
            cmd.actions |= PlayerCommandAction_Jump;
        }

        if (input->buttonStates[KEY_LEFT_SHIFT].isDown)
        {
            cmd.actions |= PlayerCommandAction_Sprint;
        }

        if (input->buttonStates[KEY_1].isDown)
        {
            cmd.activeWeaponSlot = 0;
        }
        else if (input->buttonStates[KEY_2].isDown)
        {
            cmd.activeWeaponSlot = 1;
        }
        else if (input->buttonStates[KEY_3].isDown)
        {
            cmd.activeWeaponSlot = 2;
        }
        else if (input->buttonStates[KEY_4].isDown)
        {
            cmd.activeWeaponSlot = 3;
        }
        else if (input->buttonStates[KEY_5].isDown)
        {
            cmd.activeWeaponSlot = 4;
        }
        else if (input->buttonStates[KEY_6].isDown)
        {
            cmd.activeWeaponSlot = 5;
        }
    }

    // FIXME: Not used
    if (input->buttonStates[KEY_TAB].isDown)
    {
        cmd.actions |= PlayerCommandAction_OpenInventory;
    }

    return cmd;
}

// TODO: Move into player + system
internal void HandlePlayerInteraction(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, GameEventQueue *eventQueue,
    vec3 cameraEulerAngles, vec3 position, u32 playerEntityId,
    u32 collisionObjectId)
{
    // Cast ray into scene from player camera position
    vec3 origin = position + g_PlayerHeadOffset;
    vec3 direction = GetDirectionFromEulerAngles(cameraEulerAngles);
    f32 range = 3.0f;
    DRAW_LINE(origin, origin + direction * range, Vec3(1, 0, 0), 5.0f);
    RayIntersectWorldResult result = RayIntersectCollisionWorld(
        collisionWorld, origin, direction, range, collisionObjectId);

    // Figure out which entity we hit and trigger interaction
    if (result.t > 0.0f)
    {
        u32 entityId = FindEntityWithCollisionObjetId(
            entityWorld, result.collisionObjectId);
        if (entityId != NULL_ENTITY_ID)
        {
            GameEvent *event = QueueEvent(eventQueue, EventType_Interaction);
            if (event != NULL)
            {
                event->interaction.srcEntityId = playerEntityId;
                event->interaction.dstEntityId = entityId;
            }
        }
    }
}

internal void HandlePlayerPlaceDeployable(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, GameEventQueue *eventQueue, Player player,
    u32 playerEntityId, vec3 position, u32 itemId, u32 collisionObjectId)
{
    // Cast ray into scene from player camera position
    vec3 origin = position + g_PlayerHeadOffset;
    vec3 direction = GetDirectionFromEulerAngles(player.cameraEulerAngles);
    DRAW_LINE(origin, origin + direction * g_PlayerPlacementRange,
        Vec3(1, 0, 0), 5.0f);
    RayIntersectWorldResult result = RayIntersectCollisionWorld(collisionWorld,
        origin, direction, g_PlayerPlacementRange, collisionObjectId);

    if (result.t > 0.0f)
    {
        vec3 p = origin + direction * result.t;

        GameEvent *event = QueueEvent(eventQueue, EventType_PlaceDeployable);
        event->placeDeployable.itemId = itemId;
        event->placeDeployable.position = p;
        event->placeDeployable.desiredRotation =
            player.desiredDeployableRotation;
        event->placeDeployable.entityId = playerEntityId;
        event->placeDeployable.hitEntityId = FindEntityWithCollisionObjetId(
            entityWorld, result.collisionObjectId);
    }
}

internal void HandlePlayerAttack(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, GameEventQueue *eventQueue, Player player,
    u32 playerEntityId, vec3 position, u32 collisionObjectId, i32 damage = 30)
{
    // Cast ray into scene from player camera position
    vec3 origin = position + g_PlayerHeadOffset;
    vec3 direction = GetDirectionFromEulerAngles(player.cameraEulerAngles);
    f32 range = 100.0f;
    DRAW_LINE(origin, origin + direction * range, Vec3(1, 0, 0), 5.0f);
    RayIntersectWorldResult result = RayIntersectCollisionWorld(
        collisionWorld, origin, direction, range, collisionObjectId);

    // Figure out which entity we hit and trigger interaction
    if (result.t > 0.0f)
    {
        u32 entityId = FindEntityWithCollisionObjetId(
            entityWorld, result.collisionObjectId);
        if (entityId != NULL_ENTITY_ID)
        {
            GameEvent *event = QueueEvent(eventQueue, EventType_ReceiveDamage);
            if (event != NULL)
            {
                event->receiveDamage.srcEntityId = playerEntityId;
                event->receiveDamage.dstEntityId = entityId;
                event->receiveDamage.amount = damage;
            }
        }
    }
}

struct GetEntityBeingLookedAtResult
{
    u32 entityId;
    vec3 position;
};

internal GetEntityBeingLookedAtResult GetEntityBeingLookedAt(
    EntityWorld *entityWorld, CollisionWorld *collisionWorld, Entity *entity,
    f32 range, u32 entityFlags)
{
    GetEntityBeingLookedAtResult result = {};

    vec3 position = entity->position;
    vec3 cameraEulerAngles = entity->player.cameraEulerAngles;
    u32 ignoredCollisionObject = entity->collisionObjectId;

    // Cast ray into scene from player camera position
    vec3 origin = position + g_PlayerHeadOffset;
    vec3 direction = GetDirectionFromEulerAngles(cameraEulerAngles);
    DRAW_LINE(origin, origin + direction * range, Vec3(1, 0, 0), 5.0f);
    RayIntersectWorldResult raycastResult = RayIntersectCollisionWorld(
        collisionWorld, origin, direction, range, ignoredCollisionObject);

    // Figure out which entity we hit and trigger
    // interaction
    if (raycastResult.t > 0.0f)
    {
        u32 hitEntityId = FindEntityWithCollisionObjetId(
            entityWorld, raycastResult.collisionObjectId);
        if (hitEntityId != NULL_ENTITY_ID)
        {
            Entity *hitEntity = FindEntityById(entityWorld, hitEntityId);
            if (hitEntity != NULL)
            {
                if (entityFlags == 0 || ((hitEntity->flags & entityFlags) > 0))
                {
                    result.entityId = hitEntityId;
                    result.position = origin + direction * raycastResult.t;
                }
            }
        }
    }

    return result;
}

internal void HandlePrimaryAttack(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, GameEventQueue *eventQueue, u32 entityId,
    u32 activeItemId, ItemDatabaseEntry *item, Entity *entity)
{
    if (item != NULL)
    {
        if ((item->flags & ItemFlag_Deployable) > 0)
        {
            // TODO: Event?
            // HandlePlayerPlaceDeployable(entityWorld, collisionWorld,
            // eventQueue, *player, entityId, position, activeItemId,
            // collisionObjectId);
        }
        else if (activeItemId == Item_WoodenBow)
        {
            WeaponController *weaponController = &entity->weaponController;

            // TODO: Replace with ItemFlag_Bow?
            // Spawn arrow
            f32 speed = 60.0f * (weaponController->drawTime / g_MaxDrawTime);

            // TODO: Handle shoot arrow

            weaponController->drawTime = 0.0f;
        }
        else if (activeItemId == Item_Carrot || activeItemId == Item_CookedMeat)
        {
            // TODO: Replace with ItemFlag_Consumable
            GameEvent *event = QueueEvent(eventQueue, EventType_ConsumeFood);
            if (event != NULL)
            {
                event->consumeFood.itemId = activeItemId;
                event->consumeFood.dstEntityId = entityId;
                event->consumeFood.slot =
                    MapBeltSlotToInventorySlot(entity->player.activeBeltSlot);
            }
        }
        else if (activeItemId == Item_Hammer)
        {
            // What is the hammer for?
            // HandlePlayerAttack(entityWorld, collisionWorld, eventQueue,
            // *player, entityId, position, collisionObjectId, 20000);
        }
    }
    else
    {
        // No item equipped
        // TODO: What do?
        // HandlePlayerAttack(entityWorld, collisionWorld, eventQueue,
        // *player, entityId, position, collisionObjectId);
    }
}

internal void HandleSecondaryAttack(ItemDatabaseEntry *item, Player *player)
{
    if ((item->flags & ItemFlag_Deployable) > 0)
    {
        player->desiredDeployableRotation *= Quat(Vec3(0, 1, 0), 0.5f * PI);
    }
}

internal void UpdateBow(
    u32 activeItemId, WeaponController *weaponController, PlayerCommand cmd)
{
    if (activeItemId == Item_WoodenBow)
    {
        if (cmd.actions & PlayerCommandAction_SecondaryAttack)
        {
            weaponController->drawTime =
                Min(weaponController->drawTime + cmd.dt, g_MaxDrawTime);
        }
        else
        {
            weaponController->drawTime =
                Max(weaponController->drawTime - cmd.dt, 0.0f);
        }
    }
}

internal void HandlePlayerMovement(Player *player, PlayerCommand cmd,
    PlayerCommand prevCmd, CharacterController *characterController)
{
    // TODO: Don't trust cmd to have clamped view angles
    player->cameraEulerAngles = cmd.viewAngles;

    mat4 rotationMatrix = RotateY(player->cameraEulerAngles.y);

    vec3 forwardDir = (rotationMatrix * Vec4(0, 0, -1, 0)).xyz;
    vec3 rightDir = (rotationMatrix * Vec4(1, 0, 0, 0)).xyz;

    characterController->targetDir =
        Normalize(forwardDir * cmd.forward + rightDir * cmd.right);

    characterController->jump = false;
    if (WasActionTriggered(cmd, prevCmd, PlayerCommandAction_Jump))
    {
        characterController->jump = true;
    }

    f32 friction = 0.1f;
    f32 speed = 5.0f;
    if (characterController->isGrounded)
    {
        friction = 12.0f;
        speed = 60.0f;

        if (cmd.actions & PlayerCommandAction_Sprint)
        {
            speed = 110.0f;
            friction = 8.0f;
        }
    }

    characterController->speed = speed;
    characterController->friction = friction;
}

struct MoveResult
{
    vec3 velocity;
    vec3 position;
};

internal MoveResult MoveAndSlide(
    CollisionWorld *collisionWorld, MoveSpec move, u32 collisionObjectId)
{
    vec3 velocity = move.velocity;
    vec3 position = move.position;

    f32 remainingDt = move.dt * Length(velocity);
    u32 iterationCount = 5;
    b32 solutionFound = false;
    while (iterationCount-- > 0 && remainingDt > EPSILON && !solutionFound)
    {
        vec3 rayDirection = Normalize(velocity);
        vec3 nextPosition = position + rayDirection * remainingDt;

        RayIntersectWorldResult result =
            RayIntersectCollisionWorld(collisionWorld, position, rayDirection,
                remainingDt, collisionObjectId);

        if (result.t >= 0.0f)
        {
#if 0
            // FIXME: Still have a few bugs here but they could just be due to
            // the player being represented by a point rather than a volume.
            LogMessage("HIT: %g (%g, %g, %g)", result.t,
                    result.normal.x,
                    result.normal.y,
                    result.normal.z);

            // Move player along as far along as we can
            f32 bias = 0.0001f;
            /*
            player->position =
                player->position +
                (result.tmin - bias) * (nextPosition - player->position);
                */
#endif
            // Correct the velocity
            velocity -= Dot(velocity, result.normal) * result.normal;

            ASSERT(result.t <= remainingDt);
            remainingDt -= result.t;
        }
        else
        {
            // Move full length
            position = nextPosition;
            solutionFound = true;
        }
    }

    MoveResult moveResult;
    // TODO: Does our velocity change even if our position does not?
    moveResult.velocity = velocity;
    // Reset player back to original position if we can't find a valid position
    // for them.
    moveResult.position = solutionFound ? position : move.position;

    return moveResult;
}

internal vec3 UpdateCharacterController(CollisionWorld *collisionWorld,
    CharacterController *characterController, vec3 initialPosition, f32 dt,
    u32 collisionObjectId)
{
    PROF_SCOPE();
    vec3 finalPosition = initialPosition;
    vec3 jumpImpulse = Vec3(0);

    f32 speed = characterController->speed;
    f32 friction = characterController->friction;
    vec3 targetDir = characterController->targetDir;

    // Check if character controller is standing on ground
    b32 isGrounded = false;

    // Check from center of capsule to bottom + margin
    f32 margin = 0.5f;
    // TODO: Take velocity.y into account for ground check distance?
    f32 groundCheckDistance = g_PlayerHeight * 0.5f + margin;
    vec3 center = initialPosition;

    RayIntersectWorldResult result = RayIntersectCollisionWorld(collisionWorld,
        center, Vec3(0, -1, 0), groundCheckDistance, collisionObjectId);
    // DRAW_LINE(center, center + Vec3(0, -1, 0) * groundCheckDistance,
    // Vec3(1, 0, 1), 0.02f);

    f32 distAboveGround = 0.0f;
    if (result.t >= 0.0f)
    {
        if (Dot(result.normal, Vec3(0, 1, 0)) > 0.5f)
        {
            distAboveGround = result.t - g_PlayerHeight * 0.5f;

            vec3 groundP = center + Vec3(0, -1, 0) * result.t;
            // DRAW_POINT(groundP, 0.25f, Vec3(0, 0, 1), 0.02f);

            f32 bias = 0.001f;
            if (distAboveGround < bias)
            {
                // Player penetrating ground, correct position
                finalPosition =
                    groundP + Vec3(0, 1, 0) * (g_PlayerHeight * 0.5f + bias);
                // DRAW_POINT(transform->position, 0.25f, Vec3(1, 0, 0), 0.02f);
                isGrounded = true;
            }

            f32 groundSnapDistance = 0.25f;
            if (distAboveGround < groundSnapDistance)
            {
                if (characterController->velocity.y < 0.01f)
                {
                    // Player close to ground and falling, snap position
                    finalPosition =
                        groundP +
                        Vec3(0, 1, 0) * (g_PlayerHeight * 0.5f + bias);
                    // DRAW_POINT(transform->position, 0.25f, Vec3(1, 1, 0),
                    // 0.02f);
                    isGrounded = true;
                }
            }
            else
            {
                // Greater than 0.25m off the ground, how much distance are
                // we likely to travel next frame?
                vec3 currentP = finalPosition;
                vec3 newP = currentP + characterController->velocity * dt;
                if (Dot(Vec3(0, -1, 0), newP - currentP) > 0.25f)
                {
                    // Player falling too quickly, snap to ground
                    finalPosition =
                        groundP +
                        Vec3(0, 1, 0) * (g_PlayerHeight * 0.5f + bias);
                    // DRAW_POINT(transform->position, 0.25f, Vec3(0, 1, 1),
                    // 0.02f);
                    isGrounded = true;
                }
            }
        }
        else
        {
            isGrounded = false;
        }
    }
    else
    {
        isGrounded = false;
    }

    if (!isGrounded)
    {
        characterController->velocity += Vec3(0, -60, 0) * dt;
    }
    else
    {
        // Remove downwards velocity
        if (characterController->velocity.y < 0.0f)
        {
            characterController->velocity.y = 0.0f;
        }

        if (characterController->jump)
        {
            jumpImpulse = Vec3(0, 1000, 0);
        }
    }

    vec3 acceleration = targetDir * speed + jumpImpulse;
    vec3 velocity = characterController->velocity;
    velocity -= velocity * friction * dt;
    velocity += acceleration * dt;

    MoveSpec moveSpec = {};
    moveSpec.velocity = velocity;
    moveSpec.position = finalPosition;
    moveSpec.dt = dt;

    MoveResult moveResult =
        MoveAndSlide(collisionWorld, moveSpec, collisionObjectId);

    // 4. Write back (optional)
    finalPosition = moveResult.position;
    characterController->velocity = moveResult.velocity;
    characterController->isGrounded = isGrounded;

    return finalPosition;
}

internal void HandleUseTool(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, GameEventQueue *eventQueue, Entity *entity,
    u32 entityId, u32 activeItemId)
{
    if (entity->weaponController.timeUntilNextAttack <= 0.0f)
    {
        // TODO: Parameterise harvest speed per tool
        entity->weaponController.timeUntilNextAttack = 1.3f;
        entity->weaponController.harvestEventTriggered = false;

        GameEvent *event = QueueEvent(eventQueue, EventType_PlayAnimation);
        if (event != NULL)
        {
            // Assume time to start is now
            event->playAnimation.entityId = entityId;
        }
    }
}

internal void HandleShootGun(
    GameEventQueue *eventQueue, Entity *entity, u32 entityId)
{
    f32 bulletSpeed = 304.0f; // TODO: Cvar
    // TODO: Check ammo
    if (entity->weaponController.timeUntilNextAttack <= 0.0f)
    {
        entity->weaponController.timeUntilNextAttack = 0.12f;

        GameEvent *spawnBulletEvent =
            QueueEvent(eventQueue, EventType_SpawnBullet);
        if (spawnBulletEvent != NULL)
        {
            vec3 cameraEulerAngles = entity->player.cameraEulerAngles;

            spawnBulletEvent->spawnBullet.position =
                entity->position + g_PlayerHeadOffset;
            spawnBulletEvent->spawnBullet.velocity =
                GetDirectionFromEulerAngles(cameraEulerAngles) * bulletSpeed;
            spawnBulletEvent->spawnBullet.owner = entityId;
            // TODO: Damage and flags
        }

        GameEvent *event = QueueEvent(eventQueue, EventType_PlayAnimation);
        if (event != NULL)
        {
            // Assume time to start is now
            event->playAnimation.entityId = entityId;
            event->playAnimation.applyScreenShake = true;
        }
    }
}

internal void UpdatePlayer(EntityWorld *entityWorld, u32 entityId,
    CollisionWorld *collisionWorld, GameEventQueue *eventQueue,
    ItemDatabase *itemDatabase, InventorySystem *inventorySystem,
    PlayerCommand *cmds, u32 cmdCount)
{
    PROF_SCOPE();

    Entity *entity = FindEntityById(entityWorld, entityId);
    if (entity != NULL)
    {
        for (u32 i = 0; i < cmdCount; i++)
        {
            // TODO: Should be taking cmd.dt into account
            PlayerCommand cmd = cmds[i];
            PlayerCommand prevCmd = entity->player.prevCommand;
            HandlePlayerMovement(
                &entity->player, cmd, prevCmd, &entity->characterController);

            entity->player.activeBeltSlot = cmd.activeWeaponSlot;

            if (WasActionTriggered(cmd, prevCmd, PlayerCommandAction_Interact))
            {
                HandlePlayerInteraction(entityWorld, collisionWorld, eventQueue,
                    entity->player.cameraEulerAngles, entity->position,
                    entityId, entity->collisionObjectId);
            }

            // Get item in activeBeltSlot
            u32 activeItemId = 0;
            ItemDatabaseEntry *item = NULL;
            if (GetItemIdInBeltSlot(entityWorld, inventorySystem, entityId,
                    entity->player.activeBeltSlot, &activeItemId))
            {
                item = FindItem(itemDatabase, activeItemId);
            }

            if (item != NULL)
            {
                if ((item->flags & ItemFlag_Tool) > 0)
                {
                    if (cmd.actions & PlayerCommandAction_PrimaryAttack)
                    {
                        HandleUseTool(entityWorld, collisionWorld, eventQueue,
                            entity, entityId, activeItemId);
                    }
                }

                if (activeItemId == Item_TestGun)
                {
                    // NOTE: Assuming automatic weapon?
                    if (cmd.actions & PlayerCommandAction_PrimaryAttack)
                    {
                        HandleShootGun(eventQueue, entity, entityId);
                    }
                }
            }

            // TODO: Switch on item rather than action
            if (WasActionTriggered(
                    cmd, prevCmd, PlayerCommandAction_PrimaryAttack))
            {
                HandlePrimaryAttack(entityWorld, collisionWorld, eventQueue,
                    entityId, activeItemId, item, entity);
            }

            if (WasActionTriggered(
                    cmd, prevCmd, PlayerCommandAction_SecondaryAttack))
            {
                HandleSecondaryAttack(item, &entity->player);
            }

            UpdateBow(activeItemId, &entity->weaponController, cmd);

            // Update weapon controller timers
            if (entity->weaponController.timeUntilNextAttack > 0.0f)
            {
                entity->weaponController.timeUntilNextAttack -= cmd.dt;
            }

            if (entity->weaponController.screenShakeAmount > 0.0f)
            {
                entity->weaponController.screenShakeAmount -= cmd.dt * 2.0f;
            }

            entity->player.prevCommand = cmd;

            entity->position = UpdateCharacterController(collisionWorld,
                &entity->characterController, entity->position, cmd.dt,
                entity->collisionObjectId);

            entity->rotation =
                Quat(Vec3(0, 1, 0), entity->player.cameraEulerAngles.y);

            // Update head
            Entity *head =
                FindEntityById(entityWorld, entity->player.headEntity);
            ASSERT(head != NULL);
            head->rotation =
                Quat(Vec3(1, 0, 0), entity->player.cameraEulerAngles.x);
        }
    }
}

