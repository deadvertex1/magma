#extension GL_GOOGLE_include_directive : require

#include "vertex_format.vert.glsl"
#include "material_interface.vert.glsl"

void main()
{
    StandardVertexShaderSetup(v_position, model_matrix);
}
