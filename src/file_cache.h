#pragma once

#define MAX_FILE_CACHE_ENTRIES 64

typedef u64 GetFileModTimestampFunction(const char *);

struct FileCacheEntry
{
    u32 assetType;
    u32 assetId;
    const char *path;
    u64 modTime;
    b32 wasModified;
};

struct FileCache
{
    FileCacheEntry entries[MAX_FILE_CACHE_ENTRIES];
    u32 count;

    GetFileModTimestampFunction *getFileModTimestampFunction;
};

