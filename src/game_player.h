#pragma once

const f32 g_PlayerHeight = 2.0f;
const f32 g_PlayerRadius = 0.5f;
const vec3 g_PlayerHeadOffset =
    Vec3(0, g_PlayerHeight * 0.5f - g_PlayerRadius, 0);
const f32 g_PlayerPlacementRange = 6.0f;

// FIXME: This should be in the component!
const f32 g_MaxDrawTime = 1.2f;

enum
{
    PlayerInputFlag_ReceiveNone = 0x0,
    PlayerInputFlag_ReceiveMouseInput = 0x1,
    PlayerInputFlag_ReceiveKeyboardInput = 0x2,
};

struct MoveSpec
{
    vec3 velocity;
    vec3 position;
    f32 dt;
};

