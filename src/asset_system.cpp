inline Dictionary *GetAssetTable(AssetSystem *assetSystem, u32 assetTypeId)
{
    ASSERT(assetTypeId < MAX_ASSET_TYPES);
    Dictionary *result = &assetSystem->tables[assetTypeId];
    return result;
}

// TODO: Do we actually need assetTypeId, assetId should be enough in theory
inline b32 GetAssetData_(AssetSystem *assetSystem, u32 assetTypeId, u32 assetId,
    u32 elementSize, void *dst)
{
    b32 result = false;
    Dictionary *table = GetAssetTable(assetSystem, assetTypeId);
    if (table != NULL)
    {
        void *data = Dict_FindItem_(table, assetId, elementSize);
        if (data != NULL)
        {
            memcpy(dst, data, elementSize);
            result = true;
        }
    }
    return result;
}

inline void SetAssetData_(AssetSystem *assetSystem, u32 assetTypeId,
    u32 assetId, u32 elementSize, void *src)
{
    Dictionary *table = GetAssetTable(assetSystem, assetTypeId);
    ASSERT(table != NULL);
    void *data = Dict_FindItem_(table, assetId, elementSize);
    if (data == NULL)
    {
        data = Dict_AddItem_(table, assetId, elementSize);
    }

    ASSERT(data != NULL);
    memcpy(data, src, elementSize);
}

// Convenience functions
inline void AddTextureFromPath(AssetSystem *assetSystem, u32 id,
    const char *path, b32 isCubeMap = false, b32 useAlpha = false,
    b32 repeat = true)
{
    Asset_Texture texture = {};
    texture.path = path;
    texture.isCubeMap = isCubeMap;
    texture.useAlpha = useAlpha;
    texture.repeat = repeat;
    SetAssetData_(
        assetSystem, AssetType_Texture, id, sizeof(texture), &texture);
}

inline void AddTextureHandle(AssetSystem *assetSystem, u32 id, u32 handle)
{
    Asset_Texture texture = {};
    texture.handle = handle;
    texture.repeat = true;
    SetAssetData_(
        assetSystem, AssetType_Texture, id, sizeof(texture), &texture);
}

inline void AddShaderHandle(AssetSystem *assetSystem, u32 id, u32 handle)
{
    Asset_Shader shader = {};
    shader.handle = handle;
    SetAssetData_(assetSystem, AssetType_Shader, id, sizeof(shader), &shader);
}

inline void AddShaderFromPath(AssetSystem *assetSystem, u32 id,
    const char *vertexShaderPath, const char *fragmentShaderPath)
{
    Asset_Shader shader = {};
    shader.vertexShaderPath = vertexShaderPath;
    shader.fragmentShaderPath = fragmentShaderPath;
    SetAssetData_(assetSystem, AssetType_Shader, id, sizeof(shader), &shader);
    AddEntryToCache(
        assetSystem->fileCache, AssetType_Shader, id, vertexShaderPath);
    AddEntryToCache(
        assetSystem->fileCache, AssetType_Shader, id, fragmentShaderPath);
}

inline void AddFontFromPath(
    AssetSystem *assetSystem, u32 id, const char *path, f32 size)
{
    Asset_Font font = {};
    font.path = path;
    font.size = size;
    SetAssetData_(assetSystem, AssetType_Font, id, sizeof(font), &font);
}

inline void AddMeshData(AssetSystem *assetSystem, u32 id, MeshState *data)
{
    Asset_Mesh mesh = {};
    mesh.data = data;
    SetAssetData_(assetSystem, AssetType_Mesh, id, sizeof(mesh), &mesh);
}

inline void AddMeshFromPath(AssetSystem *assetSystem, u32 id, const char *path)
{
    Asset_Mesh mesh = {};
    mesh.path = path;
    SetAssetData_(assetSystem, AssetType_Mesh, id, sizeof(mesh), &mesh);
}

inline void AddAssetToLoadQueue(AssetSystem *assetSystem, u32 type, u32 id,
    u32 flags = AssetLoadRequestFlags_None)
{
    if (assetSystem->loadQueueLength < ARRAY_LENGTH(assetSystem->loadQueue))
    {
        AssetLoadRequest *request =
            assetSystem->loadQueue + assetSystem->loadQueueLength++;
        request->type = type;
        request->id = id;
        request->flags = flags;
    }
}

inline u32 GetTexture(AssetSystem *assetSystem, u32 id)
{
    u32 result = 0;
    Asset_Texture texture = {};
    if (GetAssetData_(assetSystem, AssetType_Texture, id, sizeof(Asset_Texture),
            &texture))
    {
        if (texture.handle != 0)
        {
            result = texture.handle;
        }
        else
        {
            // NOTE: Assume if path is null that texture is generated
            if (texture.path != NULL)
            {
                AddAssetToLoadQueue(assetSystem, AssetType_Texture, id);
            }

            // Show missing texture while we wait
            // FIXME: This is not valid for cube maps!!!
            // result = renderer->textures[Texture_Missing];
        }
    }

    return result;
}

inline u32 GetShader(AssetSystem *assetSystem, u32 id)
{
    u32 result = 0;

    Asset_Shader shader = {};
    if (GetAssetData_(
            assetSystem, AssetType_Shader, id, sizeof(shader), &shader))
    {
        if (shader.handle == 0)
        {
            AddAssetToLoadQueue(assetSystem, AssetType_Shader, id);
            // TODO: Avoid flooding queue when multiple requests for the same
            // missing asset
        }

        result = shader.handle;
    }
    return result;
}

inline u32 GetComputeShader(AssetSystem *assetSystem, u32 id)
{
    u32 result = 0;
    Asset_ComputeShader shader = {};
    if (GetAssetData_(
            assetSystem, AssetType_ComputeShader, id, sizeof(shader), &shader))
    {
        if (shader.handle == 0)
        {
            AddAssetToLoadQueue(assetSystem, AssetType_ComputeShader, id);
            // TODO: Avoid flooding queue when multiple requests for the same
            // missing asset
        }

        result = shader.handle;
    }

    return result;
}

inline Font *GetFont(AssetSystem *assetSystem, u32 id)
{
    Font *result = NULL;

    Asset_Font font = {};
    if (GetAssetData_(assetSystem, AssetType_Font, id, sizeof(font), &font))
    {
        if (font.data == NULL)
        {
            AddAssetToLoadQueue(assetSystem, AssetType_Font, id);
            // TODO: Avoid flooding queue when multiple requests for the same
            // missing asset
        }

        result = font.data;
    }

    return result;
}

// TODO: Generic function for getting assets
inline MeshState *GetMesh(AssetSystem *assetSystem, u32 id)
{
    MeshState *result = NULL;

    Asset_Mesh mesh = {};
    if (GetAssetData_(assetSystem, AssetType_Mesh, id, sizeof(mesh), &mesh))
    {
        if (mesh.data == NULL)
        {
            AddAssetToLoadQueue(assetSystem, AssetType_Mesh, id);
            // TODO: Avoid flooding queue when multiple requests for the same
            // missing asset
        }

        result = mesh.data;
    }
    return result;
}

inline b32 GetMaterial(
    AssetSystem *assetSystem, u32 id, Asset_Material *material)
{
    b32 result = GetAssetData_(
        assetSystem, AssetType_Material, id, sizeof(*material), material);
    if (!result)
    {
        LOG_MSG("Failed to find material %u", id);
    }
    return result;
}

inline void AddSkeletonFromPath(
    AssetSystem *assetSystem, u32 id, const char *path)
{
    Asset_Skeleton skeleton = {};
    skeleton.path = path;
    SetAssetData_(
        assetSystem, AssetType_Skeleton, id, sizeof(skeleton), &skeleton);
}

inline Skeleton *GetSkeleton(AssetSystem *assetSystem, u32 id)
{
    Skeleton *result = NULL;

    Asset_Skeleton skeleton = {};
    if (GetAssetData_(
            assetSystem, AssetType_Skeleton, id, sizeof(skeleton), &skeleton))
    {
        if (skeleton.data == NULL)
        {
            AddAssetToLoadQueue(assetSystem, AssetType_Skeleton, id);
            // TODO: Avoid flooding queue when multiple requests for the same
            // missing asset
        }

        result = skeleton.data;
    }
    return result;
}
