// TODO: This is not a great implementation, the BVH tree code vk_cinematic is
// a better implementation that we should be using
// TODO: Perf of this is not good and causing slow start up times (working
// around for now by using simpler meshes)
internal AabbTree BuildAabbTree(vec3 *boxMins, vec3 *boxMaxes, u32 count,
    u32 maxNodes, MemoryArena *arena, MemoryArena *tempArena)
{
    PROF_SCOPE();
    AabbTree tree = {};
    tree.nodes = ALLOCATE_ARRAY(arena, AabbTreeNode, maxNodes);
    tree.max = maxNodes;

    u32 *unmergedNodeIndices = ALLOCATE_ARRAY(tempArena, u32, maxNodes);
    u32 unmergedNodeCount = 0;

    for (u32 leafIndex = 0; leafIndex < count; ++leafIndex)
    {
        ASSERT(tree.count < tree.max);
        AabbTreeNode *node = tree.nodes + tree.count++;
        node->min = boxMins[leafIndex];
        node->max = boxMaxes[leafIndex];
        node->children[0] = NULL;
        node->children[1] = NULL;
        node->leafIndex = leafIndex;

        unmergedNodeIndices[leafIndex] = leafIndex;
    }
    unmergedNodeCount = count;

    while (unmergedNodeCount > 1)
    {
        u32 newUnmergedNodeCount = 0;

        // Join nodes with their nearest neighbors to build tree
        for (u32 index = 0; index < unmergedNodeCount; ++index)
        {
            u32 nodeIndex = unmergedNodeIndices[index];
            AabbTreeNode *node = tree.nodes + nodeIndex;
            vec3 centroid = (node->max + node->min) * 0.5f;

            f32 closestDistance = F32_MAX;
            AabbTreeNode *closestPartnerNode = NULL;
            u32 closestPartnerIndex = 0;

            // TODO: Spatial hashing
            // Find a node who's centroid is closest to ours
            for (u32 partnerIndex = 0; partnerIndex < unmergedNodeCount;
                 ++partnerIndex)
            {
                if (partnerIndex == index)
                {
                    continue;
                }

                u32 partnerNodeIndex = unmergedNodeIndices[partnerIndex];
                AabbTreeNode *partnerNode = tree.nodes + partnerNodeIndex;

                vec3 partnerCentroid =
                    (partnerNode->max + partnerNode->min) * 0.5f;

                // Find minimal distance via length squared to avoid sqrt
                f32 dist = LengthSq(partnerCentroid - centroid);
                if (dist < closestDistance)
                {
                    closestDistance = dist;
                    closestPartnerNode = partnerNode;
                    closestPartnerIndex = partnerIndex;
                }
            }

            if (closestPartnerNode != NULL)
            {
                // Create combined node
                ASSERT(tree.count < tree.max);
                u32 newNodeIndex = tree.count++;
                AabbTreeNode *newNode = tree.nodes + newNodeIndex;
                newNode->min = Min(node->min, closestPartnerNode->min);
                newNode->max = Max(node->max, closestPartnerNode->max);
                newNode->children[0] = node;
                newNode->children[1] = closestPartnerNode;
                newNode->leafIndex = 0;

                // Start overwriting the old entries with combined nodes, should
                // be roughly half the length as the original array
                ASSERT(newUnmergedNodeCount <= index);
                unmergedNodeIndices[newUnmergedNodeCount++] = newNodeIndex;

                // Remove partner from unmerged node indices array
                u32 last = unmergedNodeCount - 1;
                unmergedNodeIndices[closestPartnerIndex] =
                    unmergedNodeIndices[last];
                unmergedNodeCount--;
            }
        }

        unmergedNodeCount = newUnmergedNodeCount;
    }

    if (tree.count > 0)
    {
        // Assume the last node created is the root node
        tree.root = tree.nodes + (tree.count - 1);
    }

    return tree;
}

struct AabbTreeStackNode
{
    AabbTreeNode *node;
    u32 depth;
};

internal u32 RayIntersectAabbTree(AabbTree tree, vec3 rayOrigin,
    vec3 rayDirection, u32 *leafIndices, f32 *tValues, u32 maxLeaves)
{
    u32 count = 0;

    if (tree.root == NULL)
    {
        return 0;
    }

    AabbTreeStackNode stack[2][AABB_TREE_STACK_SIZE];
    u32 stackSizes[2];
    stack[0][0].node = tree.root;
    stack[0][0].depth = 0;
    stackSizes[0] = 1;
    stackSizes[1] = 0;

    u32 readIndex = 0;
    u32 writeIndex = 1;

    while (stackSizes[readIndex] > 0)
    {
        u32 topIndex = --stackSizes[readIndex];
        AabbTreeStackNode top = stack[readIndex][topIndex];
        AabbTreeNode *node = top.node;

        f32 t = RayIntersectAabb(node->min, node->max, rayOrigin, rayDirection);
        if (t >= 0.0f)
        {
            if (node->children[0] != NULL)
            {
                // Assuming that this is always true?
                ASSERT(node->children[1] != NULL);

                ASSERT(stackSizes[writeIndex] + 2 <= AABB_TREE_STACK_SIZE);
                AabbTreeStackNode newNodes[2] = {
                    {node->children[0], top.depth + 1},
                    {node->children[1], top.depth + 1}};

                u32 entryIndex = stackSizes[writeIndex];
                stack[writeIndex][entryIndex] = newNodes[0];
                stack[writeIndex][entryIndex + 1] = newNodes[1];
                stackSizes[writeIndex] += 2;
            }
            else
            {
                ASSERT(node->children[1] == NULL);
                if (count < maxLeaves)
                {
                    u32 index = count++;
                    leafIndices[index] = node->leafIndex;
                    tValues[index] = t;
                }
                else
                {
                    break;
                }
            }
        }

        if (stackSizes[readIndex] == 0)
        {
            SwapU32(&readIndex, &writeIndex);
        }
    }

    return count;
}

// TODO: Move this
internal RayIntersectTriangleResult RayIntersectTriangleMesh(
    CollisionMesh *mesh, vec3 rayOrigin, vec3 rayDirection, f32 tmax)
{
    PROF_SCOPE();
    RayIntersectTriangleResult result = {};
    result.t = -1.0f;

    u32 triangleCount = mesh->indexCount / 3;

    // TODO: Parameterize
    u32 leafIndices[256];
    f32 tValues[256];

    u32 count = RayIntersectAabbTree(mesh->aabbTree, rayOrigin, rayDirection,
        leafIndices, tValues, ARRAY_LENGTH(leafIndices));

    for (u32 index = 0; index < count; ++index)
    {
        u32 leafIndex = leafIndices[index];
        f32 t = tValues[index];
        (void)t;

        // TODO: Check t value against tmax

        // Test triangle
        u32 meshletIndex = leafIndex;

#define MESHLET_SIZE 1
        u32 startIndex = meshletIndex * MESHLET_SIZE;
        u32 onePastEndIndex = MinU32(startIndex + MESHLET_SIZE, triangleCount);
        for (u32 triangleIndex = startIndex; triangleIndex < onePastEndIndex;
             ++triangleIndex)
        {
            // NOTE: We want clockwise winding order to produce the correct
            // triangle normal.
            // TODO: How is this working since I'm pretty sure the winding
            // order of the triangles in the mesh is actually
            // counter-clockwise?
            u32 indices[3];
            indices[0] = mesh->indices[triangleIndex * 3 + 0];
            indices[1] = mesh->indices[triangleIndex * 3 + 1];
            indices[2] = mesh->indices[triangleIndex * 3 + 2];

            vec3 vertices[3];
            vertices[0] = mesh->vertices[indices[0]];
            vertices[1] = mesh->vertices[indices[1]];
            vertices[2] = mesh->vertices[indices[2]];

            RayIntersectTriangleResult triangleIntersect =
                RayIntersectTriangleMT(rayOrigin, rayDirection, vertices[0],
                    vertices[1], vertices[2]);

            if (triangleIntersect.t >= 0.0f && triangleIntersect.t <= tmax)
            {
                if (triangleIntersect.t < result.t || result.t < 0.0f)
                {
#if 0
                    // Compute UVs from barycentric coordinates
                    f32 w =
                        1.0f - triangleIntersect.uv.x - triangleIntersect.uv.y;
                    vec2 uv =
                        vertices[0].textureCoordinates * w +
                        vertices[1].textureCoordinates *
                            triangleIntersect.uv.x +
                        vertices[2].textureCoordinates * triangleIntersect.uv.y;

                    // TODO: Use UV coords
                    (void)uv;
#endif

                    result.t = triangleIntersect.t;
                    result.normal = triangleIntersect.normal;
                }
            }
        }
    }

    return result;
}
