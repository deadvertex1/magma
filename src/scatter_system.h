#pragma once

struct ScatterSpec
{
    f32 minScale;
    f32 maxScale;

    vec3 positionOffset;

    f32 minTimeUntilMatured;
    f32 maxTimeUntilMatured;

    u32 entityDefinitions[8];
    u32 entityDefinitionsCount;
};

struct ScatterVolume
{
    // AABB
    vec3 min;
    vec3 max;

    u32 specId;
    u32 count;
};

struct ScatterSystemConfig
{
    u32 arenaSize;
    u32 maxScatterSpecs;
    u32 maxVolumes;
    u32 seed;
};

struct ScatterSystem
{
    MemoryArena arena;
    Dictionary scatterSpecs;

    RandomNumberGenerator rng;

    ScatterVolume *volumes;
    u32 volumeCount;
    u32 volumeCapacity;
};
