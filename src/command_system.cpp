// TODO: Don't use persistent arena?
internal void InitializeCommandSystem(CommandSystem *cmdSystem, MemoryArena *arena)
{
    memset(cmdSystem, 0, sizeof(*cmdSystem));

    cmdSystem->arena = SubAllocateArena(arena, Kilobytes(16));
}

internal void QueueCommandForExecution(
    CommandSystem *cmdSystem, const char *text)
{
    u32 length = strlen(text);
    char *dst = (char *)AllocateBytes(&cmdSystem->arena, length + 1);
    strncpy(dst, text, length + 1);
    dst[length] = '\0';

    if (cmdSystem->queueLength < ARRAY_LENGTH(cmdSystem->queue))
    {
        cmdSystem->queue[cmdSystem->queueLength++] = dst;
    }
}

internal u32 ExecuteCommandsInQueue(CommandSystem *cmdSystem,
    MemoryArena *tempArena, CommandData *output, u32 capacity)
{
    u32 count = 0;

    for (u32 i = 0; i < cmdSystem->queueLength; i++)
    {
        CommandData data = {}; 

        char *start = cmdSystem->queue[i];
        // Tokenize
        char *cursor = start;
        while (1)
        {
            if (*cursor == ' ' || *cursor == '\0')
            {
                u32 tokenLength = cursor - start;
                u32 argIndex = data.argc++;
                data.argv[argIndex] =
                    (char*)AllocateBytes(tempArena, tokenLength + 1);
                strncpy(data.argv[argIndex], start, tokenLength);
                data.argv[argIndex][tokenLength] = '\0';

                if (*cursor == '\0')
                {
                    break;
                }
                else
                {
                    start = cursor + 1;
                }
            }

            cursor++;
        }

        if (data.argc > 0)
        {
            if (count < capacity)
            {
                output[count++] = data;
            }
        }

    }

    ClearMemoryArena(&cmdSystem->arena);
    cmdSystem->queueLength = 0;

    return count;
}
