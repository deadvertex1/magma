internal void BuildItemNameAndQuantityString(
    char *buffer, u32 length, const char *name, u32 quantity)
{
    if (quantity > 1)
    {
        snprintf(buffer, length, "%s (%u)", name, quantity);
    }
    else
    {
        snprintf(buffer, length, "%s", name);
    }
}

internal void DrawHoverRecipeInfo(TextBuffer *buffer, AssetSystem *assetSystem,
    vec2 mouseP, f32 depth, Recipe *recipe, ItemDatabase *itemDatabase)
{
    // Draw background panel
    f32 panelWidth = 300.0f;
    f32 panelHeight = 100.0f;
    f32 requirementsOffset = 30.0f;
    vec2 panelP = mouseP;
    rect2 panelRect = Rect2(panelP, panelP + Vec2(panelWidth, -panelHeight));
    vec4 panelColor = Vec4(0.15, 0.13, 0.1, 1);
    DrawQuad(buffer, assetSystem, panelRect, panelColor, depth);

    // Draw item name and output quantity
    Font *font = GetFont(assetSystem, Font_FpsDisplay);
    if (font != NULL)
    {
        char nameAndQuantity[80];
        BuildItemNameAndQuantityString(nameAndQuantity, sizeof(nameAndQuantity),
            recipe->name, recipe->quantity);
        DrawText(buffer, *font, nameAndQuantity,
            panelP + Vec2(10, -font->lineSpacing - 4.0f),
            Vec4(0.8, 0.8, 0.8, 1), depth + 0.01f);
    }

    // Recipe Requirements
    Font *debugFont = GetFont(assetSystem, Font_Debug);
    if (debugFont != NULL)
    {
        /*
        Wooden Arrows (x5)
        - Wood (x10)
        - Stone (x5)
        */

        for (u32 i = 0; i < recipe->inputCount; i++)
        {
            char requirement[80];
            ItemDatabaseEntry *item =
                FindItem(itemDatabase, recipe->inputs[i].itemId);
            if (item != NULL)
            {
                requirement[0] = '-';
                requirement[1] = ' ';
                u32 len = sizeof(requirement) - 2;
                BuildItemNameAndQuantityString(requirement + 2, len, item->name,
                    recipe->inputs[i].quantity);
                DrawText(buffer, *debugFont, requirement,
                    panelP + Vec2(10, -requirementsOffset -
                                          debugFont->lineSpacing * (1 + i)),
                    Vec4(0.8, 0.8, 0.8, 1), depth + 0.01f);
            }
        }
    }
}

internal void DrawHoverItemInfo(ItemDatabase *itemDatabase,
    AssetSystem *assetSystem, TextBuffer *buffer, u32 itemId, u32 quantity,
    vec2 mouseP, f32 depth)
{
    // Draw background panel
    f32 panelWidth = 300.0f;
    f32 panelHeight = 100.0f;
    f32 requirementsOffset = 30.0f;
    vec2 panelP = mouseP;
    rect2 panelRect = Rect2(panelP, panelP + Vec2(panelWidth, -panelHeight));
    vec4 panelColor = Vec4(0.15, 0.13, 0.1, 1);
    DrawQuad(buffer, assetSystem, panelRect, panelColor, depth);

    // Draw item name and output quantity
    Font *font = GetFont(assetSystem, Font_FpsDisplay);
    if (font != NULL)
    {
        ItemDatabaseEntry *itemData = FindItem(itemDatabase, itemId);
        if (itemData != NULL)
        {
            char nameAndQuantity[80];
            BuildItemNameAndQuantityString(nameAndQuantity,
                sizeof(nameAndQuantity), itemData->name, quantity);
            DrawText(buffer, *font, nameAndQuantity,
                panelP + Vec2(10, -font->lineSpacing - 4.0f),
                Vec4(0.8, 0.8, 0.8, 1), depth + 0.01f);
        }
    }
}

internal void DrawPlayerCraftingMenu(TextBuffer *buffer,
    AssetSystem *assetSystem, ItemDatabase *itemDatabase, InputState *input,
    PlayerUIState *playerUIState, u32 playerEntityId,
    b32 showCraftingStationRecipes)
{
    u32 rowCount = 10;
    f32 rowWidth = 200.0f;
    f32 rowHeight = 24.0f; // TODO: Use font height
    f32 rowGap = 2.0f;
    vec4 rowColor = Vec4(0.1, 0.1, 0.1, 1);

    f32 panelWidth = rowWidth;
    f32 panelHeight = (rowHeight + rowGap) * rowCount;
    f32 panelDepth = -0.99f;
    f32 recipeDepth = 0.0f;
    f32 hoverDepth = 0.1f;

    vec2 mouseP =
        Vec2(input->mousePosX, input->framebufferHeight - input->mousePosY);

    vec2 panelP = Vec2(1000, 470);

    // Draw background panel
    rect2 panelRect = Rect2(panelP, panelP + Vec2(panelWidth, panelHeight));
    vec4 panelColor = Vec4(0.15, 0.13, 0.1, 1);
    DrawQuad(buffer, assetSystem, panelRect, panelColor, panelDepth);

    // Draw crafting menu text/title
    // TODO: Better font?
    Font *font = GetFont(assetSystem, Font_FpsDisplay);
    if (font != NULL)
    {
        f32 verticalGap = 4.0f;
        f32 textPanelHeight = font->lineSpacing + 12.0f;
        rect2 textRect =
            Rect2(Vec2(panelRect.min.x, panelRect.max.y + verticalGap),
                Vec2(panelRect.max.x,
                    panelRect.max.y + verticalGap + textPanelHeight));
        DrawQuad(buffer, assetSystem, textRect, panelColor, panelDepth);

        DrawText(buffer, *font, "Crafting Menu",
            Vec2(textRect.min.x + 2.0f, textRect.min.y + 6.0f),
            Vec4(1, 1, 1, 1), panelDepth + 0.1f);
    }

    // TODO: Offset
    vec2 top = panelP + Vec2(0, panelHeight);
    Font *debugFont = GetFont(assetSystem, Font_Debug);
    if (debugFont != NULL)
    {
        u32 drawCount = 0;
        for (u32 i = 0; i < itemDatabase->recipes.count; i++)
        {
            u32 recipeId = itemDatabase->recipes.keys[i];
            Recipe *recipe = FindRecipe(itemDatabase, recipeId);

            if (recipe != NULL)
            {
                if (!recipe->requiresCraftingStation ||
                    (recipe->requiresCraftingStation &&
                        showCraftingStationRecipes))
                {
                    // Draw button + text
                    vec2 p =
                        top + Vec2(0, -rowHeight -
                                          drawCount++ * (rowHeight + rowGap));
                    rect2 buttonRect = Rect2(p, p + Vec2(rowWidth, rowHeight));

                    // Create enlarged rect for testing mouse position against
                    // to avoid situation where mouse position lies between grid
                    // cells and no cell is selected.
                    rect2 testRect =
                        Rect2(p, p + Vec2(rowWidth, rowHeight + rowGap));

                    vec4 color = rowColor;
                    if (RectContainsPoint(testRect, mouseP))
                    {
                        // Hover
                        color = Vec4(0.2, 0.2, 0.25, 1);

                        if (WasPressed(
                                input->buttonStates[KEY_MOUSE_BUTTON_LEFT]))
                        {
                            // Build command and process in fixed timestep
                            // update
                            ASSERT(playerUIState->commandCount <
                                   ARRAY_LENGTH(playerUIState->commands));
                            InventoryCommand *command =
                                playerUIState->commands +
                                playerUIState->commandCount++;

                            command->type = InventoryCommandType_CraftItem;
                            command->srcContainer = playerEntityId;
                            command->recipeId = recipeId;
                        }
                        else
                        {
                            DrawHoverRecipeInfo(buffer, assetSystem, mouseP,
                                hoverDepth, recipe, itemDatabase);
                        }
                    }
                    DrawQuad(
                        buffer, assetSystem, buttonRect, color, recipeDepth);

                    // Vertically center text?
                    DrawText(buffer, *debugFont, recipe->name,
                        p + Vec2(10, debugFont->lineSpacing * 0.5f),
                        Vec4(0.8, 0.8, 0.8, 1), recipeDepth + 0.01f);
                }
            }
        }
    }
}

internal void DrawItemSlot(ItemDatabase *itemDatabase, TextBuffer *buffer,
    AssetSystem *assetSystem, u32 itemId, u32 quantity, rect2 slotRect,
    f32 depth)
{
    u32 iconTexture = Texture_Missing;
    ItemDatabaseEntry *itemData = FindItem(itemDatabase, itemId);
    if (itemData != NULL)
    {
        iconTexture = itemData->iconTexture;
    }
    else
    {
        LOG_MSG("Unknown item ID %u", itemId);
    }

    // Draw item icon
    DrawTexturedQuad(buffer, assetSystem, slotRect, iconTexture, depth,
        itemData->iconTintColor);

    // Draw quantity text
    Font *quantityFont = GetFont(assetSystem, Font_Debug);
    if (quantityFont != NULL)
    {
        char text[8] = {};
        snprintf(text, sizeof(text), "%u", quantity);
        f32 textWidth = CalculateTextWidth(*quantityFont, text);
        vec2 quantityP =
            Vec2(slotRect.max.x - textWidth - 2.0f, slotRect.min.y + 2.0f);
        DrawText(buffer, *quantityFont, text, quantityP,
            Vec4(0.8, 0.8, 0.8, 1.0), depth + 0.01f);
    }
}

internal void DrawPlayerInventory(TextBuffer *buffer, AssetSystem *assetSystem,
    ItemDatabase *itemDatabase, InputState *input, EntityWorld *world,
    InventorySystem *inventorySystem, PlayerUIState *playerUIState)
{
    // Get player inventory
    u32 playerEntityId = GetPlayerEntityId(world);
    Inventory *inventory =
        GetEntityInventory(world, inventorySystem, playerEntityId);
    if (inventory != NULL)
    {
        u32 slotCount = ARRAY_LENGTH(inventory->items);
        u32 columnCount = 6;
        f32 gridCellWidth = 60.0f;
        f32 gridCellSpacing = 2.0f;
        vec4 gridCellColor = Vec4(0.1, 0.1, 0.1, 1);
        vec2 gridCellOffset = Vec2(8, 8);
        f32 panelHeight = (slotCount / 6) * (gridCellWidth + gridCellSpacing) +
                          2 * gridCellOffset.y;
        f32 panelWidth = columnCount * (gridCellWidth + gridCellSpacing) +
                         2 * gridCellOffset.x;
        f32 panelDepth = -0.9f;
        f32 cellDepth = -0.5f;
        f32 iconDepth = 0.0f;
        f32 itemInfoDepth = 0.1f;

        vec2 mouseP =
            Vec2(input->mousePosX, input->framebufferHeight - input->mousePosY);

        // Draw background panel
        rect2 panelRect =
            Rect2(Vec2(600, 400), Vec2(600 + panelWidth, 400 + panelHeight));
        vec4 panelColor = Vec4(0.15, 0.13, 0.1, 1);
        DrawQuad(buffer, assetSystem, panelRect, panelColor, panelDepth);

        // Draw inventory text/title
        // TODO: Better font?
        Font *font = GetFont(assetSystem, Font_FpsDisplay);
        if (font != NULL)
        {
            f32 verticalGap = 4.0f;
            f32 textPanelHeight = font->lineSpacing + 12.0f;
            rect2 textRect =
                Rect2(Vec2(panelRect.min.x, panelRect.max.y + verticalGap),
                    Vec2(panelRect.max.x,
                        panelRect.max.y + verticalGap + textPanelHeight));
            DrawQuad(buffer, assetSystem, textRect, panelColor, panelDepth);

            DrawText(buffer, *font, "Inventory",
                Vec2(textRect.min.x + 2.0f, textRect.min.y + 6.0f),
                Vec4(1, 1, 1, 1), panelDepth + 0.01f);
        }

        i32 selectedSlot = -1;
        b32 containerSelected = false;

        // Draw player grid slots
        for (u32 i = 0; i < slotCount; ++i)
        {
            f32 x = (i % columnCount) * (gridCellWidth + gridCellSpacing);
            f32 y =
                ((i / columnCount) + 1) * -(gridCellWidth + gridCellSpacing);

            rect2 gridCellRect =
                Rect2(Vec2(x, y), Vec2(x + gridCellWidth, y + gridCellWidth));

            vec2 offset = Vec2(gridCellOffset.x + panelRect.min.x,
                panelRect.max.y - gridCellOffset.y);
            gridCellRect = RectOffset(gridCellRect, offset);

            u32 itemInstanceId = inventory->items[i];
            ItemInstance *item =
                FindItemInstanceById(inventorySystem, itemInstanceId);

            // Create enlarged rect for testing mouse position against
            // to avoid situation where mouse position lies between grid
            // cells and no cell is selected.
            rect2 testRect =
                Rect2(gridCellRect.min - Vec2(gridCellSpacing * 0.5f),
                    gridCellRect.max + Vec2(gridCellSpacing * 0.5f));

            vec4 color = gridCellColor;
            if (i >= slotCount - 6)
            {
                color.b *= 1.5f;
            }
            if (RectContainsPoint(testRect, mouseP))
            {
                color = Vec4(0.2, 0.2, 0.25, 1);
                selectedSlot = i;
                if (item != NULL)
                {
                    DrawHoverItemInfo(itemDatabase, assetSystem, buffer,
                        item->itemId, item->quantity, mouseP, itemInfoDepth);
                }
            }

            // Draw empty grid slot
            DrawQuad(buffer, assetSystem, gridCellRect, color, cellDepth);

            // Draw item icon if item occupies inventory slot
            if (item != NULL)
            {
                DrawItemSlot(itemDatabase, buffer, assetSystem, item->itemId,
                    item->quantity, gridCellRect, iconDepth);
            }
        }

        // Draw container inventory
        Inventory *container = GetEntityInventory(
            world, inventorySystem, playerUIState->containerEntityId);
        if (container != NULL)
        {
            // Draw container background panel
            f32 left = panelRect.max.x + 20.0f;
            rect2 panelRect = Rect2(
                Vec2(left, 200), Vec2(left + panelWidth, 200 + panelHeight));
            vec4 panelColor = Vec4(0.15, 0.13, 0.1, 1);
            DrawQuad(buffer, assetSystem, panelRect, panelColor, panelDepth);

            if (font != NULL)
            {
                f32 verticalGap = 4.0f;
                f32 textPanelHeight = font->lineSpacing + 12.0f;
                rect2 textRect =
                    Rect2(Vec2(panelRect.min.x, panelRect.max.y + verticalGap),
                        Vec2(panelRect.max.x,
                            panelRect.max.y + verticalGap + textPanelHeight));
                DrawQuad(buffer, assetSystem, textRect, panelColor, panelDepth);

                DrawText(buffer, *font, "Container",
                    Vec2(textRect.min.x + 2.0f, textRect.min.y + 6.0f),
                    Vec4(1, 1, 1, 1), panelDepth + 0.01f);
            }

            // Draw container grid slots
            u32 containerSlotCount = ARRAY_LENGTH(container->items);
            for (u32 i = 0; i < containerSlotCount; ++i)
            {
                f32 x = (i % columnCount) * (gridCellWidth + gridCellSpacing);
                f32 y = ((i / columnCount) + 1) *
                        -(gridCellWidth + gridCellSpacing);

                rect2 gridCellRect = Rect2(
                    Vec2(x, y), Vec2(x + gridCellWidth, y + gridCellWidth));

                vec2 offset = Vec2(gridCellOffset.x + panelRect.min.x,
                    panelRect.max.y - gridCellOffset.y);
                gridCellRect = RectOffset(gridCellRect, offset);

                // Create enlarged rect for testing mouse position
                // against to avoid situation where mouse position lies
                // between grid cells and no cell is selected.
                rect2 testRect =
                    Rect2(gridCellRect.min - Vec2(gridCellSpacing * 0.5f),
                        gridCellRect.max + Vec2(gridCellSpacing * 0.5f));

                u32 itemInstanceId = container->items[i];
                ItemInstance *item =
                    FindItemInstanceById(inventorySystem, itemInstanceId);

                vec4 color = gridCellColor;
                if (RectContainsPoint(testRect, mouseP))
                {
                    color = Vec4(0.2, 0.2, 0.25, 1);
                    selectedSlot = i;
                    containerSelected = true;
                    if (item != NULL)
                    {
                        DrawHoverItemInfo(itemDatabase, assetSystem, buffer,
                            item->itemId, item->quantity, mouseP,
                            itemInfoDepth);
                    }
                }

                // Draw empty grid slot
                DrawQuad(buffer, assetSystem, gridCellRect, color, cellDepth);

                // Draw item icon if item occupies inventory slot
                if (item != NULL)
                {
                    DrawItemSlot(itemDatabase, buffer, assetSystem,
                        item->itemId, item->quantity, gridCellRect, iconDepth);
                }
            }
        }

        // Initiate drag
        if (selectedSlot != -1)
        {
            if (WasPressed(input->buttonStates[KEY_MOUSE_BUTTON_LEFT]))
            {
                Inventory *src = containerSelected ? container : inventory;
                playerUIState->entityIdBeingDragged = src->items[selectedSlot];
                playerUIState->entityIdBeingDraggedFrom =
                    containerSelected ? playerUIState->containerEntityId
                                      : playerEntityId;
                playerUIState->dragSlot = selectedSlot;
            }

            if (WasPressed(input->buttonStates[KEY_MOUSE_BUTTON_RIGHT]))
            {
                if (playerUIState->containerEntityId != NULL_ENTITY_ID)
                {
                    // Build command and process in fixed timestep update
                    ASSERT(playerUIState->commandCount <
                           ARRAY_LENGTH(playerUIState->commands));
                    InventoryCommand *command =
                        playerUIState->commands + playerUIState->commandCount++;

                    command->type = InventoryCommandType_TransferItem;
                    command->srcContainer =
                        containerSelected ? playerUIState->containerEntityId
                                          : playerEntityId;
                    command->dstContainer =
                        containerSelected ? playerEntityId
                                          : playerUIState->containerEntityId;
                    command->srcSlot = selectedSlot;
                    command->dstSlot = U32_MAX;
                }
            }
        }

        // Process drop
        if (WasReleased(input->buttonStates[KEY_MOUSE_BUTTON_LEFT]))
        {
            if (selectedSlot != -1)
            {
                if (playerUIState->entityIdBeingDragged != 0)
                {
                    // Build command and process in fixed timestep update
                    ASSERT(playerUIState->commandCount <
                           ARRAY_LENGTH(playerUIState->commands));
                    InventoryCommand *command =
                        playerUIState->commands + playerUIState->commandCount++;

                    command->type = InventoryCommandType_TransferItem;
                    command->srcContainer =
                        playerUIState->entityIdBeingDraggedFrom;
                    command->dstContainer =
                        containerSelected ? playerUIState->containerEntityId
                                          : playerEntityId;
                    command->srcSlot = playerUIState->dragSlot;
                    command->dstSlot = selectedSlot;
                }
            }
            playerUIState->entityIdBeingDraggedFrom = 0;
            playerUIState->entityIdBeingDragged = 0;
            playerUIState->dragSlot = -1;
        }
    }
}

internal void DrawPlayerHUD(TextBuffer *buffer, AssetSystem *assetSystem,
    EntityWorld *world, b32 showText)
{
    u32 playerEntityId = GetPlayerEntityId(world);
    i32 currentHealth = GetEntityCurrentHealth(world, playerEntityId);
    i32 maxHealth = GetEntityMaxHealth(world, playerEntityId);

    {
        vec2 p = Vec2(10, 100);
        f32 width = 250.0f;
        f32 height = 30.0f;

        // Draw health bar
        rect2 healthRect = Rect2(p, p + Vec2(width, height));
        DrawProgressBar(buffer, assetSystem, healthRect, Vec4(1, 0.1, 0.1, 1),
            Vec4(0.05, 0.05, 0.05, 1), (f32)currentHealth / (f32)maxHealth,
            0.0f);

        if (showText)
        {
            Font *font = GetFont(assetSystem, Font_FpsDisplay);
            if (font != NULL)
            {
                char healthText[80];
                snprintf(healthText, sizeof(healthText), "Health: %d / %d",
                    currentHealth, maxHealth);
                f32 textWidth = CalculateTextWidth(*font, healthText);
                vec2 textP = RectCenter(healthRect);
                DrawText(buffer, *font, healthText,
                    textP + Vec2(-textWidth * 0.5, -4.0f), Vec4(1), 0.02f);
            }
        }
    }

    Hunger hunger = GetEntityHunger(world, playerEntityId);
    {
        vec2 p = Vec2(10, 65);
        f32 width = 250.0f;
        f32 height = 30.0f;

        // Draw food bar
        rect2 hungerRect = Rect2(p, p + Vec2(width, height));
        DrawProgressBar(buffer, assetSystem, hungerRect, Vec4(1, 0.7, 0.1, 1),
            Vec4(0.05, 0.05, 0.05, 1), (f32)hunger.value / (f32)hunger.max,
            0.0f);

        if (showText)
        {
            Font *font = GetFont(assetSystem, Font_FpsDisplay);
            if (font != NULL)
            {
                char foodText[80];
                snprintf(foodText, sizeof(foodText), "Food: %d / %d",
                    hunger.value, hunger.max);
                f32 textWidth = CalculateTextWidth(*font, foodText);
                vec2 textP = RectCenter(hungerRect);
                DrawText(buffer, *font, foodText,
                    textP + Vec2(-textWidth * 0.5, -4.0f), Vec4(1), 0.02f);
            }
        }
    }
}

internal void DrawPlayerCrosshair(
    TextBuffer *buffer, AssetSystem *assetSystem, vec2 screenCenter)
{
    f32 offset = 2.0f;
    f32 width = 2.0f;
    f32 length = 10.0f;

    f32 halfWidth = width * 0.5f;

    // vertical
    DrawQuad(buffer, assetSystem,
        Rect2(screenCenter.x - halfWidth, screenCenter.y + offset,
            screenCenter.x + halfWidth, screenCenter.y + offset + length),
        Vec4(1, 0, 1, 1), 0.0f);

    DrawQuad(buffer, assetSystem,
        Rect2(screenCenter.x - halfWidth, screenCenter.y - offset - length,
            screenCenter.x + halfWidth, screenCenter.y - offset),
        Vec4(1, 0, 1, 1), 0.0f);

    // horizontal
    DrawQuad(buffer, assetSystem,
        Rect2(screenCenter.x + offset, screenCenter.y - halfWidth,
            screenCenter.x + offset + length, screenCenter.y + halfWidth),
        Vec4(1, 0, 1, 1), 0.0f);

    DrawQuad(buffer, assetSystem,
        Rect2(screenCenter.x - offset - length, screenCenter.y - halfWidth,
            screenCenter.x - offset, screenCenter.y + halfWidth),
        Vec4(1, 0, 1, 1), 0.0f);
}

internal void DrawPlayerItemBelt(TextBuffer *buffer, AssetSystem *assetSystem,
    ItemDatabase *itemDatabase, InputState *input, EntityWorld *world,
    InventorySystem *inventorySystem)
{
    // Get player inventory
    u32 playerEntityId = GetPlayerEntityId(world);
    Player player = GetEntityPlayerController(world, playerEntityId);
    Inventory *inventory =
        GetEntityInventory(world, inventorySystem, playerEntityId);
    if (inventory != NULL)
    {
        u32 activeSlot = player.activeBeltSlot;
        u32 slotCount = 6;

        // FIXME: Duplicate code from DrawPlayerInventory
        u32 columnCount = 6;
        f32 gridCellWidth = 60.0f;
        f32 gridCellSpacing = 2.0f;
        vec4 gridCellColor = Vec4(0.1, 0.1, 0.1, 1);
        vec2 gridCellOffset = Vec2(8, 8);
        f32 panelHeight = (slotCount / 6) * (gridCellWidth + gridCellSpacing) +
                          2 * gridCellOffset.y;
        f32 panelWidth = columnCount * (gridCellWidth + gridCellSpacing) +
                         2 * gridCellOffset.x;
        f32 panelDepth = -0.9f;
        f32 cellDepth = -0.5f;
        f32 iconDepth = 0.0f;

        vec2 screenCenter =
            Vec2(input->framebufferWidth, input->framebufferHeight) * 0.5f;

        vec2 panelBottomLeft = Vec2(screenCenter.x - panelWidth * 0.5f, 40);

        // Draw background panel
        rect2 panelRect = Rect2(
            panelBottomLeft, panelBottomLeft + Vec2(panelWidth, panelHeight));
        vec4 panelColor = Vec4(0.15, 0.13, 0.1, 1);
        DrawQuad(buffer, assetSystem, panelRect, panelColor, panelDepth);

        // Draw player grid slots
        for (u32 i = 0; i < slotCount; ++i)
        {
            f32 x = (i % columnCount) * (gridCellWidth + gridCellSpacing);
            f32 y =
                ((i / columnCount) + 1) * -(gridCellWidth + gridCellSpacing);

            rect2 gridCellRect =
                Rect2(Vec2(x, y), Vec2(x + gridCellWidth, y + gridCellWidth));

            vec2 offset = Vec2(gridCellOffset.x + panelRect.min.x,
                panelRect.max.y - gridCellOffset.y);
            gridCellRect = RectOffset(gridCellRect, offset);

            u32 mappedSlot = MapBeltSlotToInventorySlot(i);
            u32 itemInstanceId = inventory->items[mappedSlot];
            ItemInstance *item =
                FindItemInstanceById(inventorySystem, itemInstanceId);

            vec4 color = gridCellColor;
            if (i == activeSlot)
            {
                color = Vec4(0.2, 0.2, 0.25, 1);
            }

            // Draw empty grid slot
            DrawQuad(buffer, assetSystem, gridCellRect, color, cellDepth);

            // TODO: Extract into common function for drawing item slot
            // Draw item icon if item occupies inventory slot
            u32 inventoryIndex =
                (ARRAY_LENGTH(inventory->items) - slotCount) + i;
            if (item != NULL)
            {
                DrawItemSlot(itemDatabase, buffer, assetSystem, item->itemId,
                    item->quantity, gridCellRect, iconDepth);
            }
        }
    }
}
