inline u32 GetPlayerEntityId(EntityWorld *entityWorld)
{
    u32 entityId = NULL_ENTITY_ID;
    for (u32 i = 0; i < entityWorld->entities.count; i++)
    {
        u32 key = entityWorld->entities.keys[i];
        Entity *entity = FindEntityById(entityWorld, key);
        if (entity->flags & EntityFlags_Player)
        {
            entityId = key;
            break;
        }
    }
    return entityId;
}

inline vec3 GetEntityPosition(EntityWorld *entityWorld, u32 entityId)
{
    Entity *entity = FindEntityById(entityWorld, entityId);
    vec3 result = (entity != NULL) ? entity->position : Vec3(0);

    return result;
}

inline i32 GetEntityCurrentHealth(EntityWorld *entityWorld, u32 entityId)
{
    Entity *entity = FindEntityById(entityWorld, entityId);
    i32 currentHealth = (entity != NULL) ? entity->currentHealth : 0;

    return currentHealth;
}

inline i32 GetEntityMaxHealth(EntityWorld *entityWorld, u32 entityId)
{
    Entity *entity = FindEntityById(entityWorld, entityId);
    i32 maxHealth = (entity != NULL) ? entity->maxHealth : 0;
    return maxHealth;
}

inline Hunger GetEntityHunger(EntityWorld *entityWorld, u32 entityId)
{
    Hunger hunger = {};
    Entity *entity = FindEntityById(entityWorld, entityId);
    if (entity != NULL)
    {
        hunger = entity->hunger;
    }
    return hunger;
}

inline Player GetEntityPlayerController(EntityWorld *entityWorld, u32 entityId)
{
    Player player = {};
    Entity *entity = FindEntityById(entityWorld, entityId);
    if (entity != NULL)
    {
        player = entity->player;
    }
    return player;
}

inline WeaponController *GetEntityWeaponController(
    EntityWorld *entityWorld, u32 entityId)
{
    WeaponController *weaponController = NULL;
    Entity *entity = FindEntityById(entityWorld, entityId);
    if (entity != NULL)
    {
        weaponController = &entity->weaponController;
    }

    return weaponController;
}

internal u32 FindEntityWithCollisionObjetId(
    EntityWorld *world, u32 collisionObjectId)
{
    u32 result = NULL_ENTITY_ID;

    // FIXME: Don't loop over all entities!
    for (u32 i = 0; i < GetEntityCount(world); i++)
    {
        u32 entityId = world->entities.keys[i];

        Entity *entity = FindEntityById(world, entityId);
        if (entity->flags & EntityFlags_Collider)
        {
            if (entity->collisionObjectId == collisionObjectId)
            {
                result = entityId;
                break;
            }
        }
    }

    return result;
}

internal void BuildStaticMeshEntityDefinition(EntityDefinition *entityDef,
    vec3 position, vec3 scale, u32 material, u32 mesh, b32 collision)
{
    PUSH_VALUE(entityDef, position, position);
    PUSH_VALUE(entityDef, rotation, Quat());
    PUSH_VALUE(entityDef, scale, scale);
    PUSH_VALUE(entityDef, material, material);
    PUSH_VALUE(entityDef, mesh, mesh);
    PUSH_VALUE(entityDef, visible, true);
    if (collision)
    {
        PUSH_VALUE(entityDef, flags, EntityFlags_Collider);
    }
}

inline u32 GetItemPickUpMaterial(u32 itemId)
{
    u32 material = Material_Missing;
    if (itemId == Item_Wood)
        material = Material_Bark;
    else if (itemId == Item_Carrot)
        material = Material_CarrotPickup;
    else if (itemId == Item_Stone)
        material = HashStringU32("materials/rock");
    return material;
}

inline u32 GetItemPickupMesh(u32 itemId)
{
    u32 mesh = Mesh_Cube;
    if (itemId == Item_Wood)
        mesh = HashStringU32("meshes/log_pile");
    else if (itemId == Item_Stone)
        mesh = HashStringU32("meshes/rock02");
    return mesh;
}

internal EntityDefinition BuildItemPickupEntityDefinition(
    EntityWorld *world, u32 itemId, u32 quantity)
{
    EntityDefinition entityDef = {};
    BuildStaticMeshEntityDefinition(
        &entityDef, Vec3(0), Vec3(1), Material_Missing, Mesh_Cube, true);
    PUSH_VALUE(&entityDef, flags, EntityFlags_Interactable);
    PUSH_VALUE(&entityDef, interactionType, Interaction_Pickup);
    PUSH_VALUE(&entityDef, itemId, itemId);
    PUSH_VALUE(&entityDef, quantity, quantity);
    PUSH_VALUE(&entityDef, material, GetItemPickUpMaterial(itemId));
    PUSH_VALUE(&entityDef, mesh, GetItemPickupMesh(itemId));

    return entityDef;
}

internal u32 AddTestAI(EntityWorld *entityWorld, vec3 position, u32 teamIndex)
{
    u32 entityId = NULL_ENTITY_ID;
    EntityDefinition entityDef = {};
    const char *entityDefName =
        teamIndex == 1 ? "entity_friendly_ai" : "entity_enemy_ai";
    if (FindEntityDefinition(entityWorld, entityDefName, &entityDef))
    {
        PUSH_VALUE(&entityDef, position, position);
        entityId = CreateEntityFromDefinition(entityWorld, &entityDef);
    }

    return entityId;
}

internal u32 AddParticleEmitter(
    EntityWorld *entityWorld, vec3 p, u32 particleSpecId, f32 lifeTime = 0.0f)
{
    EntityDefinition entityDef = {};

    PUSH_VALUE(&entityDef, position, p);
    PUSH_VALUE(&entityDef, flags, EntityFlags_ParticleEmitter);
    PUSH_VALUE(&entityDef, particleSpecId, particleSpecId);
    if (lifeTime > 0.0f)
    {
        PUSH_VALUE(&entityDef, flags, EntityFlags_ScheduledDeletion);
        PushValueF32(&entityDef, OffsetOf(Entity, timeRemaining), lifeTime);
    }

    return CreateEntityFromDefinition(entityWorld, &entityDef);
}

inline void BuildPartEntityName(
    char *buffer, u32 length, const char *type, const char *part)
{
    snprintf(buffer, length, "entity_%s_%s", type, part);
}

inline void RegisterStaticMeshEntity(EntityWorld *entityWorld, const char *name,
    u32 material, u32 mesh, b32 collisionEnabled = true)
{
    EntityDefinition entityDef = {};
    BuildStaticMeshEntityDefinition(
        &entityDef, Vec3(0), Vec3(1), material, mesh, collisionEnabled);
    RegisterEntityDefinition(entityWorld, name, &entityDef);
}

internal void RegisterEntityDefinitions(EntityWorld *entityWorld)
{
    u32 woodenBuildingPartHealth = 60;
    {
        EntityDefinition entityDef = {};
        BuildStaticMeshEntityDefinition(&entityDef, Vec3(0), Vec3(1),
            Material_Missing, HashStringU32("meshes/error"), false);
        RegisterEntityDefinition(
            entityWorld, "entity_error_visualization", &entityDef);
    }

    {
        EntityDefinition entityDef = {};
        PUSH_VALUE(
            &entityDef, flags, EntityFlags_Player | EntityFlags_Inventory);
        PUSH_VALUE(&entityDef, scale, Vec3(1));
        PUSH_VALUE(&entityDef, characterController.speed,
            g_DefaultCharacterControllerSpeed);
        PUSH_VALUE(&entityDef, characterController.friction,
            g_DefaultCharacterControllerFriction);
        PUSH_VALUE(&entityDef, currentHealth, 100);
        PUSH_VALUE(&entityDef, maxHealth, 100);
        PUSH_VALUE(&entityDef, mesh, Mesh_Capsule);
        PUSH_VALUE(&entityDef, material, Material_Missing);
        PUSH_VALUE(&entityDef, visible, false);
        PUSH_VALUE(&entityDef, hunger.value, 30);
        PUSH_VALUE(&entityDef, hunger.max, 100);
        RegisterEntityDefinition(entityWorld, "entity_player", &entityDef);
    }

    {
        EntityDefinition entityDef = {};
        BuildItemPickupEntityDefinition(entityWorld, 0, 0);
        RegisterEntityDefinition(entityWorld, "entity_item_pickup", &entityDef);
    }

    {
        EntityDefinition entityDef =
            BuildItemPickupEntityDefinition(entityWorld, Item_Wood, 5);
        RegisterEntityDefinition(
            entityWorld, "entity_item_pickup_wood", &entityDef);
    }
    {
        EntityDefinition entityDef =
            BuildItemPickupEntityDefinition(entityWorld, Item_Stone, 5);
        RegisterEntityDefinition(
            entityWorld, "entity_item_pickup_stone", &entityDef);
    }
    {
        EntityDefinition entityDef =
            BuildItemPickupEntityDefinition(entityWorld, Item_Carrot, 1);
        RegisterEntityDefinition(
            entityWorld, "entity_item_pickup_carrot", &entityDef);
    }

    {
        EntityDefinition entityDef = {};
        BuildStaticMeshEntityDefinition(
            &entityDef, Vec3(0), Vec3(1), Material_Bark, Mesh_PineTree, true);
        PUSH_VALUE(&entityDef, flags, EntityFlags_Harvestable);
        PUSH_VALUE(&entityDef, itemId, Item_Wood);
        PUSH_VALUE(&entityDef, quantity, 3000);
        RegisterEntityDefinition(entityWorld, "entity_pine_tree0", &entityDef);

        EntityDefinition variant1 = entityDef;
        PUSH_VALUE(&variant1, mesh, HashStringU32("meshes/pine_tree2"));
        RegisterEntityDefinition(entityWorld, "entity_pine_tree1", &variant1);

        EntityDefinition variant2 = entityDef;
        PUSH_VALUE(&variant2, mesh, HashStringU32("meshes/pine_tree3"));
        RegisterEntityDefinition(entityWorld, "entity_pine_tree2", &variant2);

        EntityDefinition sapling = {};
        BuildStaticMeshEntityDefinition(&sapling, Vec3(0), Vec3(0.5),
            Material_Bark, HashStringU32("meshes/sapling"), false);
        PUSH_VALUE(&sapling, flags, EntityFlags_Sapling);
        PushValueF32(&sapling, OffsetOf(Entity, timeUntilMatured), 30.0);
        RegisterEntityDefinition(entityWorld, "entity_sapling", &sapling);
    }

    {
        EntityDefinition baseDef = {};
        BuildStaticMeshEntityDefinition(&baseDef, Vec3(0), Vec3(1),
            HashStringU32("materials/rock"), HashStringU32("meshes/rock01"),
            true);
        PUSH_VALUE(&baseDef, flags, EntityFlags_Harvestable);
        PUSH_VALUE(&baseDef, itemId, Item_Stone);
        PUSH_VALUE(&baseDef, quantity, 4000);
        RegisterEntityDefinition(entityWorld, "entity_rock01", &baseDef);

        EntityDefinition variant1 = baseDef;
        PUSH_VALUE(&variant1, mesh, HashStringU32("meshes/rock02"));
        RegisterEntityDefinition(entityWorld, "entity_rock02", &variant1);

        EntityDefinition variant2 = baseDef;
        PUSH_VALUE(&variant2, mesh, HashStringU32("meshes/rock03"));
        RegisterEntityDefinition(entityWorld, "entity_rock03", &variant2);
    }
    RegisterStaticMeshEntity(entityWorld, "entity_shack",
        Material_PrototypeTriplanar, HashStringU32("meshes/shack"));
    RegisterStaticMeshEntity(entityWorld, "entity_warehouse",
        Material_PrototypeTriplanar, HashStringU32("meshes/warehouse"));

    {
        EntityDefinition entityDef = {};
        BuildStaticMeshEntityDefinition(&entityDef, Vec3(0), Vec3(1),
            HashStringU32("materials/grass_alpha"),
            HashStringU32("meshes/grass"), false);
        RegisterEntityDefinition(entityWorld, "entity_grass", &entityDef);
    }

    {
        EntityDefinition entityDef = {};
        BuildStaticMeshEntityDefinition(&entityDef, Vec3(0), Vec3(0.5),
            HashStringU32("materials/enemy_ai"), Mesh_Cube, true);
        PUSH_VALUE(&entityDef, flags,
            EntityFlags_Inventory | EntityFlags_Interactable);
        PUSH_VALUE(&entityDef, interactionType, Interaction_Container);
        RegisterEntityDefinition(
            entityWorld, "entity_corpse_container", &entityDef);
    }

    {
        EntityDefinition entityDef = {};
        BuildStaticMeshEntityDefinition(&entityDef, Vec3(0), Vec3(1),
            HashStringU32("materials/concrete"), HashStringU32("meshes/target"),
            true);
        RegisterEntityDefinition(entityWorld, "entity_target", &entityDef);
    }

    {
        EntityDefinition entityDef = {};
        BuildStaticMeshEntityDefinition(&entityDef, Vec3(0.03, -0.03, -0.1),
            Vec3(1), HashStringU32("materials/builder_ai"),
            HashStringU32("meshes/test_gun"), false);
        PUSH_VALUE(&entityDef, rotation, Quat(Vec3(0, 1, 0), PI));
        RegisterEntityDefinition(entityWorld, "entity_vm_test_gun", &entityDef);
    }
}

internal void PushEntityDefinition(ScatterSpec *spec, const char *entityName)
{
    if (spec->entityDefinitionsCount < ARRAY_LENGTH(spec->entityDefinitions))
    {
        spec->entityDefinitions[spec->entityDefinitionsCount++] =
            HashStringU32(entityName);
    }
    else
    {
        LOG_MSG("Failed to add entity definition \"%s\" to scatter spec");
    }
}

internal void RegisterEntityScatterSpecs(ScatterSystem *scatterSystem)
{
    {
        ScatterSpec *spec =
            AddScatterSpec(scatterSystem, "pine_trees", 0.8f, 1.35f);
        PushEntityDefinition(spec, "entity_pine_tree0");
        PushEntityDefinition(spec, "entity_pine_tree1");
        PushEntityDefinition(spec, "entity_pine_tree2");
    }

    {
        ScatterSpec *spec =
            AddScatterSpec(scatterSystem, "saplings", 0.8f, 1.4f);
        spec->minTimeUntilMatured = 5.0f;
        spec->maxTimeUntilMatured = 30.0f;
        PushEntityDefinition(spec, "entity_sapling");
    }

    {
        ScatterSpec *spec = AddScatterSpec(scatterSystem, "rocks", 0.8f, 2.2f);
        PushEntityDefinition(spec, "entity_rock01");
        PushEntityDefinition(spec, "entity_rock02");
        PushEntityDefinition(spec, "entity_rock03");
    }
    {
        ScatterSpec *spec = AddScatterSpec(scatterSystem, "grass", 0.8f, 2.2f);
        PushEntityDefinition(spec, "entity_grass");
    }
    {
        ScatterSpec *spec = AddScatterSpec(scatterSystem, "wandering_enemies");
        spec->positionOffset = Vec3(0, 1, 0);
        PushEntityDefinition(spec, "entity_enemy_ai");
    }
    {
        ScatterSpec *spec =
            AddScatterSpec(scatterSystem, "wandering_enemy_builders");
        spec->positionOffset = Vec3(0, 1, 0);
        PushEntityDefinition(spec, "entity_enemy_builder_ai");
    }
    {
        ScatterSpec *spec =
            AddScatterSpec(scatterSystem, "wandering_gatherers");
        spec->positionOffset = Vec3(0, 1, 0);
        PushEntityDefinition(spec, "entity_friendly_ai");
    }
    {
        ScatterSpec *spec = AddScatterSpec(scatterSystem, "item_pickups");
        spec->positionOffset = Vec3(0, 0.5, 0);
        PushEntityDefinition(spec, "entity_item_pickup_wood");
        PushEntityDefinition(spec, "entity_item_pickup_stone");
        PushEntityDefinition(spec, "entity_item_pickup_carrot");
    }
    {
        ScatterSpec *spec = AddScatterSpec(scatterSystem, "enemy_camps");
        PushEntityDefinition(spec, "entity_cannibal_camp");
    }
    {
        ScatterSpec *spec = AddScatterSpec(scatterSystem, "metal_ore");
        spec->positionOffset = Vec3(0, 0.5, 0);
        PushEntityDefinition(spec, "entity_metal_ore");
    }

    {
        ScatterSpec *spec = AddScatterSpec(scatterSystem, "buildings");
        PushEntityDefinition(spec, "entity_shack");
        PushEntityDefinition(spec, "entity_warehouse");
    }
}

internal u32 AddItemPickup(
    EntityWorld *entityWorld, vec3 position, u32 itemId, u32 quantity = 1)
{
    EntityDefinition entityDef =
        BuildItemPickupEntityDefinition(entityWorld, itemId, quantity);
    PUSH_VALUE(&entityDef, position, position);
    u32 entityId = CreateEntityFromDefinition(entityWorld, &entityDef);

    return entityId;
}

internal u32 AddSkeletalAnimationTestEntity(
    EntityWorld *entityWorld, vec3 position)
{
    EntityDefinition entityDef = {};
    BuildStaticMeshEntityDefinition(&entityDef, position, Vec3(0.05f),
        Material_Missing, HashStringU32("meshes/xbot/0"), false);
    u32 entityId = CreateEntityFromDefinition(entityWorld, &entityDef);

    return entityId;
}

internal void SetToFirstTestMap(
    EntityWorld *world, ScatterSystem *scatterSystem)
{
    vec3 testP = GAME_TEST_ORIGIN;

    f32 volumeScale = 200.0f;
    AddScatterVolume(scatterSystem, testP, Vec3(-volumeScale),
        Vec3(volumeScale), 250, "pine_trees");
    AddScatterVolume(scatterSystem, testP, Vec3(-volumeScale),
        Vec3(volumeScale), 100, "saplings");
    AddScatterVolume(scatterSystem, testP, Vec3(-volumeScale),
        Vec3(volumeScale), 5, "metal_ore");
    AddScatterVolume(scatterSystem, testP, Vec3(-volumeScale),
        Vec3(volumeScale), 40, "rocks");
    // AddScatterVolume(scatterSystem, testP, Vec3(-volumeScale),
    // Vec3(volumeScale), 460, "grass");
    // AddScatterVolume(
    // scatterSystem, testP, Vec3(-800), Vec3(800), 30, "buildings");

    world->sunLight.position = Vec3(0, 4, 0);
    world->sunLight.rotation =
        Quat(Vec3(1, 0, 0), PI * -0.25f) * Quat(Vec3(0, 1, 0), PI * -0.25);
    world->sunLight.color = Vec3(1, 0.99, 0.95) * 10.0f;

    world->skyLight.position = Vec3(2, 4, 0);
    world->skyLight.color = Vec3(0.7, 0.7, 1) * 0.4;

    // AddSkeletalAnimationTestEntity(world, testP + Vec3(0, -5, 0));
}

internal u32 SpawnChildEntityDefinition(
    EntityWorld *entityWorld, const char *name, u32 parentEntityId)
{
    u32 entityId = NULL_ENTITY_ID;
    EntityDefinition entityDef = {};
    if (FindEntityDefinition(entityWorld, name, &entityDef))
    {
        PUSH_VALUE(&entityDef, parentEntityId, parentEntityId);
        entityId = CreateEntityFromDefinition(entityWorld, &entityDef);
    }

    return entityId;
}

internal u32 SpawnEntityDefinition(
    EntityWorld *entityWorld, const char *name, vec3 position)
{
    u32 entityId = NULL_ENTITY_ID;
    EntityDefinition entityDef = {};
    if (FindEntityDefinition(entityWorld, name, &entityDef))
    {
        PUSH_VALUE(&entityDef, position, position);
        entityId = CreateEntityFromDefinition(entityWorld, &entityDef);
    }

    return entityId;
}

internal u32 AddPlayer(EntityWorld *world, vec3 position)
{
    u32 entityId = SpawnEntityDefinition(world, "entity_player", position);
    if (entityId != NULL_ENTITY_ID)
    {
        Entity *player = FindEntityById(world, entityId);
        ASSERT(player != NULL);

        EntityDefinition headEntityDef = {};
        PUSH_VALUE(&headEntityDef, parentEntityId, entityId);
        PUSH_VALUE(&headEntityDef, position, g_PlayerHeadOffset);
        PUSH_VALUE(&headEntityDef, rotation, Quat());
        PUSH_VALUE(&headEntityDef, scale, Vec3(1));
        u32 headEntityId = CreateEntityFromDefinition(world, &headEntityDef);
        player->player.headEntity = headEntityId;

        SpawnChildEntityDefinition(world, "entity_vm_test_gun", headEntityId);
    }

    return entityId;
}

internal void SetToGamePlayTestMap(EntityWorld *entityWorld)
{
    vec3 origin = GAME_TEST_ORIGIN;
    // AddPlayer(entityWorld, origin + Vec3(0, 4, 0));

    // Ground
    EntityDefinition entityDef = {};
    BuildStaticMeshEntityDefinition(&entityDef, origin + Vec3(0, -1, 0),
        Vec3(1000, 1, 1000), HashStringU32("materials/prototyping/light"),
        Mesh_Cube, true);
    u32 entityId = CreateEntityFromDefinition(entityWorld, &entityDef);

    entityWorld->sunLight.position = Vec3(0, 4, 0);
    entityWorld->sunLight.rotation =
        Quat(Vec3(1, 0, 0), PI * -0.25f) * Quat(Vec3(0, 1, 0), PI * -0.25);
    entityWorld->sunLight.color = Vec3(1, 0.99, 0.95) * 10.0f;

    entityWorld->skyLight.position = Vec3(2, 4, 0);
    entityWorld->skyLight.color = Vec3(0.7, 0.7, 1) * 0.4f;

    SpawnEntityDefinition(
        entityWorld, "entity_item_pickup_wood", origin + Vec3(0, 0, -5));
    SpawnEntityDefinition(
        entityWorld, "entity_item_pickup_stone", origin + Vec3(3, 0, -5));
    SpawnEntityDefinition(
        entityWorld, "entity_pine_tree0", origin + Vec3(6, 0, -5));
    SpawnEntityDefinition(
        entityWorld, "entity_rock01", origin + Vec3(10, 0, -5));

    AddParticleEmitter(entityWorld, origin + Vec3(-4, 1, -5),
        HashStringU32("particles/smoke"));

    SpawnEntityDefinition(
        entityWorld, "entity_target", origin + Vec3(0, 0, -35));
}
