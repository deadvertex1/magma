inline vec3 ToVec3(aiVector3D v)
{
    vec3 result = {v.x, v.y, v.z};
    return result;
}

inline quat ToQuat(aiQuaternion q)
{
    quat result = {q.x, q.y, q.z, q.w};
    return result;
}
inline mat4 ToMat4(aiMatrix4x4 *m)
{
    mat4 result;
    // TODO: This assumes that they both have the same memory layout
    memcpy(&result, m, sizeof(result));
    result = Transpose(result);
    return result;
}

struct AssimpNodeStack
{
    aiNode *nodes[MAX_SKELETON_JOINTS]; // FIXME: Don't hardcode this
    u32 length;
};

inline void Push(AssimpNodeStack *stack, aiNode *node)
{
    ASSERT(stack->length < ARRAY_LENGTH(stack->nodes));
    stack->nodes[stack->length++] = node;
}

internal void Flatten(AssimpNodeStack *stack, aiNode *node)
{
    Push(stack, node);
    for (u32 i = 0; i < node->mNumChildren; ++i)
    {
        Flatten(stack, node->mChildren[i]);
    }
}

inline const char *CopyString(aiString *str, MemoryArena *arena)
{
    char *result = ALLOCATE_ARRAY(arena, char, str->length + 1);
    memcpy(result, str->data, str->length);
    result[str->length] = '\0';
    return result;
}

internal i32 FindParentIndex(AssimpNodeStack *stack, aiNode *node)
{
    i32 result = -1;
    for (u32 i = 0; i < stack->length; ++i)
    {
        if (node->mParent == stack->nodes[i])
        {
            result = (i32)i;
            break;
        }
    }

    return result;
}

inline b32 CompareAiStrings(const aiString *a, const aiString *b)
{
    return (a->length == b->length && memcmp(a->data, b->data, a->length) == 0);
}

// NOTE: Names are assumed to be unique so we only return the first match
inline aiBone *FindBone(aiMesh *mesh, aiString *name)
{
    aiBone *result = NULL;
    for (u32 i = 0; i < mesh->mNumBones; ++i)
    {
        aiBone *bone = mesh->mBones[i];
        if (CompareAiStrings(name, &bone->mName))
        {
            result = bone;
            break;
        }
    }

    return result;
}

// NOTE: This function assumes the scene root node is the skeleton root node
internal Skeleton CreateSkeleton(AssimpNodeStack *stack, MemoryArena *arena,
    SkeletonPose *bindPose, aiMesh *mesh)
{
    Skeleton result = {};

    result.count = stack->length;
    result.joints = ALLOCATE_ARRAY(arena, SkeletonJoint, stack->length);

    bindPose->localPose =
        ALLOCATE_ARRAY(arena, SkeletonJointPose, stack->length);

    for (u32 i = 0; i < stack->length; ++i)
    {
        aiNode *node = stack->nodes[i];
        SkeletonJoint *joint = result.joints + i;

        joint->name = CopyString(&node->mName, arena);
        joint->nameId = HashStringU32(joint->name);
        joint->parent = FindParentIndex(stack, node);

        // Copy inverse bind pose matrix
        aiBone *bone = FindBone(mesh, &node->mName);
        joint->invBindPose =
            (bone != NULL) ? ToMat4(&bone->mOffsetMatrix) : Identity();

        // Create bind pose
        SkeletonJointPose *localPose = bindPose->localPose + i;

        // Decompose transform into position, orientation and scale
        aiVector3D position;
        aiVector3D scaling;
        aiQuaternion rotation;
        aiDecomposeMatrix(
            &node->mTransformation, &scaling, &rotation, &position);

        localPose->rotation = ToQuat(rotation);
        localPose->translation = ToVec3(position);
        localPose->scaling = scaling.x; // Limit to uniform scaling only
    }

    return result;
}

inline i32 FindJointIndex(Skeleton skeleton, u32 stringId)
{
    i32 result = -1;
    for (u32 i = 0; i < skeleton.count; i++)
    {
        SkeletonJoint *joint = skeleton.joints + i;
        if (joint->nameId == stringId)
        {
            result = (i32)i;
        }
    }

    return result;
}

// NOTE: This function will only return the first aiVertexWeight which matches
// as the vertexId should be unique.
inline aiVertexWeight *FindVertexWeight(aiBone *bone, u32 vertexId)
{
    aiVertexWeight *result = NULL;
    for (u32 i = 0; i < bone->mNumWeights; i++)
    {
        aiVertexWeight *weight = bone->mWeights + i;

        // NOTE: Should be only one instance of the vertex in weights list
        if (weight->mVertexId == vertexId)
        {
            result = weight;
            break;
        }
    }

    return result;
}

// NOTE: This function also sorts the index weight pairs so the joint with the
// most influence on a vertex will be in slot 0.
internal u32 GetVertexWeights(aiMesh *mesh, u32 vertexId, Skeleton skeleton,
    JointIndexWeightPair *pairs, u32 capacity)
{
    u32 count = 0;

    for (u32 boneIndex = 0; boneIndex < mesh->mNumBones; boneIndex++)
    {
        aiBone *bone = mesh->mBones[boneIndex];

        aiVertexWeight *weight = FindVertexWeight(bone, vertexId);
        if (weight != NULL)
        {
            // TODO: Can use HashStringU32 as string is nul terminated
            u32 nameId =
                HashU32((const u8 *)bone->mName.data, bone->mName.length);

            // Create joint index and weight pair
            JointIndexWeightPair pair;
            pair.jointIndex = FindJointIndex(skeleton, nameId);
            pair.weight = weight->mWeight;
            ASSERT(pair.jointIndex != -1);

            // Add to list
            ASSERT(count < capacity);
            pairs[count++] = pair;
        }
    }

    // Sort pairs by weight
    // TODO: Replace with our own sort
    qsort(pairs, count, sizeof(pairs[0]), CompareJointIndexWeightPairs);

    return count;
}

inline void PopulateVertexWeightsAndIndices(
    SkinnedVertex *vertex, JointIndexWeightPair *pairs, u32 pairCount)
{
    u32 n = Min(pairCount, 4);
    u8 indices[4] = {};
    f32 weights[4] = {}; // NOTE: Make sure weights are initialized to 0
    for (u32 i = 0; i < n; i++)
    {
        indices[i] = SafeTruncateU32ToU8(pairs[i].jointIndex);
        weights[i] = pairs[i].weight;
    }

    vertex->jointWeights = Vec3(weights[0], weights[1], weights[2]);
    vertex->jointIndices = (indices[0] << 24) | (indices[1] << 16) |
                           (indices[2] << 8) | indices[3];
}

internal void DumpSkeleton(Skeleton skeleton)
{
    LOG_MSG("============= DUMPING SKELETON ==================");
    for (u32 i = 0; i < skeleton.count; i++)
    {
        SkeletonJoint *joint = skeleton.joints + i;
        LOG_MSG("[%u] - \"%s\" nameId: %u parent: %d", i, joint->name,
            joint->nameId, joint->parent);
    }
    LOG_MSG("================================================");
}

internal aiMesh *FindFirstMeshWithBones(const aiScene *scene)
{
    aiMesh *mesh = NULL;
    for (u32 i = 0; i < scene->mNumMeshes; i++)
    {
        if (scene->mMeshes[i]->mBones != NULL)
        {
            mesh = scene->mMeshes[i];
            break;
        }
    }
    return mesh;
}

struct ImportSceneResult
{
    Skeleton skeleton;
    SkeletonPose bindPose;
    u32 materialIndices[32];
    MeshData meshes[32];
    u32 count;
};

internal ImportSceneResult ImportSceneFromDisk(
    const char *path, MemoryArena *arena)
{
    ImportSceneResult result = {};

    const struct aiScene *scene = aiImportFile(
        path, aiProcess_CalcTangentSpace | aiProcess_Triangulate |
                  aiProcess_JoinIdenticalVertices | aiProcess_SortByPType |
                  aiProcess_ImproveCacheLocality);

    if (scene)
    {
        AssimpNodeStack stack = {};
        Flatten(&stack, scene->mRootNode);

        LOG_MSG("============== DUMPING SCENE ===================");
        LOG_MSG("Num Meshes: %u", scene->mNumMeshes);
        LOG_MSG("Num Materials: %u", scene->mNumMaterials);
        LOG_MSG("Num Animations: %u", scene->mNumAnimations);
        LOG_MSG("Num Textures: %u", scene->mNumTextures);
        LOG_MSG("Num Skeletons: %u", scene->mNumSkeletons);
        LOG_MSG("Num Nodes: %u", stack.length);
        for (u32 i = 0; i < stack.length; i++)
        {
            LOG_MSG("[%u] - %s - numMeshes: %u", i, stack.nodes[i]->mName.data,
                stack.nodes[i]->mNumMeshes);
        }
        LOG_MSG("================================================");

        aiMesh *skinnedMesh = FindFirstMeshWithBones(scene);
        ASSERT(skinnedMesh != NULL);

        result.skeleton =
            CreateSkeleton(&stack, arena, &result.bindPose, skinnedMesh);
        DumpSkeleton(result.skeleton);

        JointIndexWeightPair pairs[MAX_SKELETON_JOINTS];
        for (u32 meshIndex = 0; meshIndex < scene->mNumMeshes; meshIndex++)
        {
            aiMesh *mesh = scene->mMeshes[meshIndex];

            // Only import meshes which are bound to the skeleton
            if (mesh->mBones != NULL)
            {
                u32 vertexCount = mesh->mNumVertices;
                SkinnedVertex *vertices =
                    ALLOCATE_ARRAY(arena, SkinnedVertex, vertexCount);
                u32 indexCount =
                    mesh->mNumFaces * 3; // We only support triangles
                u32 *indices = ALLOCATE_ARRAY(arena, u32, indexCount);

                ASSERT(mesh->mNormals != NULL);
                ASSERT(mesh->mTextureCoords != NULL);

                for (u32 vertexIndex = 0; vertexIndex < vertexCount;
                     ++vertexIndex)
                {
                    SkinnedVertex *vertex =
                        (SkinnedVertex *)vertices + vertexIndex;
                    vertex->position.x = mesh->mVertices[vertexIndex].x;
                    vertex->position.y = mesh->mVertices[vertexIndex].y;
                    vertex->position.z = mesh->mVertices[vertexIndex].z;

                    vertex->normal.x = mesh->mNormals[vertexIndex].x;
                    vertex->normal.y = mesh->mNormals[vertexIndex].y;
                    vertex->normal.z = mesh->mNormals[vertexIndex].z;
                    vertex->normal = Normalize(vertex->normal);

                    vertex->textureCoord.x =
                        mesh->mTextureCoords[0][vertexIndex].x;
                    vertex->textureCoord.y =
                        mesh->mTextureCoords[0][vertexIndex].y;

                    u32 pairCount = GetVertexWeights(mesh, vertexIndex,
                        result.skeleton, pairs, ARRAY_LENGTH(pairs));

                    PopulateVertexWeightsAndIndices(vertex, pairs, pairCount);
                }

                for (u32 triangleIndex = 0; triangleIndex < mesh->mNumFaces;
                     ++triangleIndex)
                {
                    aiFace *face = mesh->mFaces + triangleIndex;
                    indices[triangleIndex * 3 + 0] = face->mIndices[0];
                    indices[triangleIndex * 3 + 1] = face->mIndices[1];
                    indices[triangleIndex * 3 + 2] = face->mIndices[2];
                }

                result.materialIndices[meshIndex] = mesh->mMaterialIndex;

                MeshData meshData = CreateMeshData(vertices,
                    sizeof(vertices[0]), vertexCount, indices, indexCount);

                result.meshes[meshIndex] = meshData;
                result.count++;
            }
            else
            {
                // TODO: Log skipping mesh
            }
        }

        aiReleaseImport(scene);
    }
    else
    {
        LOG_MSG("Scene import failed!\n%s", aiGetErrorString());
    }

    return result;
}
