#pragma once

#define MAX_COMMANDS_PER_FRAME 256

struct CommandSystem
{
    MemoryArena arena;

    char *queue[MAX_COMMANDS_PER_FRAME];
    u32 queueLength;
};

struct CommandData
{
    char *argv[8];
    u32 argc;
};

