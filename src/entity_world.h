#pragma once

#define NULL_ENTITY_ID 0

struct Camera
{
    vec3 position;
    vec3 rotation;

    mat4 viewMatrix;
    mat4 projectionMatrix;
};

struct SunLight
{
    vec3 position;
    vec3 color;
    quat rotation;
};

struct SkyLight
{
    vec3 position;
    vec3 color;
};

struct EntityDefinitionEntry
{
    u32 offset;
    u32 size;
    union
    {
        vec3 v3;
        quat q;
        u32 u;
        f32 f;
        i32 i;
    } value;
};

struct EntityDefinition
{
    EntityDefinitionEntry entries[32];
    u32 count;
};

struct EntityWorldConfig
{
    u32 worldArenaSize;
    u32 maxEntities;
    u32 maxEntityDefinitions;
    u32 deletionQueueCapacity;
};

enum
{
    PlayerCommandAction_None = 0x0,
    PlayerCommandAction_Interact = BIT(0),
    PlayerCommandAction_PrimaryAttack = BIT(1),
    PlayerCommandAction_SecondaryAttack = BIT(2),
    PlayerCommandAction_Jump = BIT(3),
    PlayerCommandAction_Sprint = BIT(4),
    PlayerCommandAction_OpenInventory = BIT(5),
};

struct PlayerCommand
{
    vec3 viewAngles;
    f32 dt;
    f32 forward;
    f32 right;
    u32 actions;
    u8 activeWeaponSlot;
};

struct Player
{
    vec3 cameraEulerAngles;
    u32 activeBeltSlot;
    PlayerCommand prevCommand;
    quat desiredDeployableRotation;

    u32 headEntity;
};

struct Hunger
{
    i32 value;
    i32 max;
    f32 timeUntilNextDecrement;
};

struct CharacterController
{
    // --- INPUT ---
    vec3 targetDir;
    f32 speed;
    f32 friction;
    b32 jump;

    // --- OUTPUT ---
    vec3 velocity;
    b32 isGrounded;
};

struct WeaponController
{
    u32 type; // Just bow for now?
    f32 drawTime;

    f32 timeUntilNextAttack;
    f32 animationStartTime;
    b32 isAnimationPlaying;
    b32 harvestEventTriggered;

    f32 screenShakeAmount;
};

struct Entity
{
    // Common fields
    u32 flags;

    // Transform
    quat rotation;
    vec3 position;
    vec3 scale;

    mat4 worldTransform;

    // Static mesh
    u32 material;
    u32 mesh;
    b32 visible;

    // Collider
    u32 collisionObjectId;

    // Health
    i32 currentHealth;
    i32 maxHealth;

    // Inventory
    u32 inventoryId;

    // Team
    u8 team;

    // Particle Emitter
    u32 particleEmitterId; // NOTE: This is set by the ParticleEmitterSystem
    u32 particleSpecId;

    // Item spawner
    u32 itemId;
    u32 quantity;

    // Interactable
    u32 interactionType;

    f32 timeUntilMatured;

    f32 timeRemaining;

    u32 parentEntityId;

    Player player;
    CharacterController characterController;
    WeaponController weaponController;
    Hunger hunger;
};

enum
{
    EntityFlags_None = 0x0,
    EntityFlags_Collider = BIT(0),
    EntityFlags_Harvestable = BIT(1),
    EntityFlags_Player = BIT(2),
    EntityFlags_Inventory = BIT(3),
    EntityFlags_ParticleEmitter = BIT(4),
    EntityFlags_Interactable = BIT(5),
    EntityFlags_Sapling = BIT(6),
    EntityFlags_ScheduledDeletion = BIT(7),
};

struct EntityWorld
{
    MemoryArena arena;

    // TODO: Get rid of these!
    SunLight sunLight;
    SkyLight skyLight;
    Camera camera;

    u32 nextEntityId;

    u32 *deletionQueue;
    u32 deletionQueueCapacity;
    u32 deletionQueueLength;

    Dictionary entityDefinitions;

    Dictionary entities;
};
