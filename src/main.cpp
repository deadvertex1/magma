#include <cstdint>
#include <cstdio>
#include <cmath>
#include <cfloat>
#include <cstring>
#include <cstdarg>
// #include <x86intrin.h> // Only needed on GCC

// ---- Linux specific headers ----
#include <dlfcn.h>
#include <sys/stat.h>
#include <sys/mman.h>
// ----

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "platform.h"
#include "memory_pool.h"
#include "logging.h"
#include "math_utils.h"
#include "math_lib.h"
#include "input.h"
#include "hash.h"
#include "dictionary.h"
#include "file_cache.h"
#include "asset_system.h"
#include "opengl.h"
#include "mesh.h"
#include "game_interface.h"
#include "asset_ids.h"
#include "profiler.h"
#include "skeletal_animation.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#include "asset_data.cpp"
#include "opengl.cpp"
#include "glfw.cpp"
#include "mesh.cpp"
#include "file_cache.cpp"
#include "skeletal_animation_import.cpp"
#include "asset_system.cpp"
#include "asset.cpp"
#include "asset_system_platform_layer.cpp"
#include "profiler.cpp"

struct GameCode
{
    void *handle;
    GameUpdateAndRenderFunction *updateAndRender;
    time_t lastWriteTime;
};

#define GAME_LIB "./game.so"

internal time_t GetFileModificationTime(const char *path)
{
    struct stat attr;
    stat(path, &attr);
    return attr.st_mtime;
}

internal u64 GetLastWriteTimeU64(const char *path)
{
    u64 result = (u64)GetFileModificationTime(path);
    return result;
}

internal GameCode LoadGameCode()
{
    GameCode result = {};
    result.lastWriteTime = GetFileModificationTime(GAME_LIB);
    result.handle = dlopen(GAME_LIB, RTLD_NOW);
    if (result.handle)
    {
        dlerror(); // Clear any existing error
        result.updateAndRender = (GameUpdateAndRenderFunction *)dlsym(
            result.handle, "GameUpdateAndRender");
        ASSERT(result.updateAndRender);
    }
    else
    {
        LOG_MSG("Failed to load game code. %s", dlerror());
    }
    return result;
}

internal void UnloadGameCode(GameCode gameCode) { dlclose(gameCode.handle); }

internal b32 WasGameCodeModified(GameCode *code)
{
    b32 result = false;
    time_t newWriteTime = GetFileModificationTime(GAME_LIB);
    if (newWriteTime != code->lastWriteTime)
    {
        result = true;
    }
    return result;
}

internal void *PlatformAllocateMemory(u64 size, u64 baseAddress = 0)
{
    LOG_MSG("AllocateMemory called for %lu bytes", size);
    // Memory is cleared to zero by the kernel
    void *result = mmap((void *)baseAddress, size, PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    ASSERT(result != MAP_FAILED);

    return result;
}

internal void PlatformFreeMemory(void *p) { munmap(p, 0); }

internal LOGGING_SINK(LogToStdout) { fprintf(stdout, "%s\n", msg); }

// FIXME: Don't forward declare
internal void AppendToLogBuffer(LogBuffer *logBuffer, const char *text);
internal LOGGING_SINK(LogToConsole)
{
    LogBuffer *logBuffer = (LogBuffer *)userData;
    AppendToLogBuffer(logBuffer, msg);
}

internal LogBuffer CreateLogBuffer(MemoryArena *arena)
{
    u32 maxLineLength = MAX_LOG_LINE_LENGTH;
    u32 maxEntries = MAX_LOG_ENTRIES;

    LogBuffer logBuffer = {};
    logBuffer.capacity = maxEntries;

    // Create memory pool for storing the actual log message text
    logBuffer.memoryPool = CreateMemoryPool(arena, maxLineLength, maxEntries);

    // Create entries ring buffer for storing the log line meta data
    logBuffer.entries = ALLOCATE_ARRAY(arena, LogEntry, maxEntries);
    memset(logBuffer.entries, 0, sizeof(LogEntry) * maxEntries);

    return logBuffer;
}

internal void AppendToLogBuffer(LogBuffer *logBuffer, const char *text)
{
    u32 index = logBuffer->write;

    // Advance read and write cursors
    logBuffer->write = (logBuffer->write + 1) % logBuffer->capacity;
    if (logBuffer->write == logBuffer->read)
    {
        logBuffer->read = (logBuffer->read + 1) % logBuffer->capacity;
    }

    LogEntry *entry = logBuffer->entries + index;

    // Free any text data for the entry
    if (entry->text != NULL)
    {
        // TODO: We probably don't actually to allocate/free from a memory
        // pool, could just allocate all entries text data up front and clear
        // it when 'freed'
        FreeFromPool(&logBuffer->memoryPool, entry->text);
        entry->text = NULL;
    }

    u32 length = MinU32(strlen(text), MAX_LOG_LINE_LENGTH);

    entry->text = (char *)AllocateFromPool(&logBuffer->memoryPool, length);
    memset(entry->text, 0, MAX_LOG_LINE_LENGTH);
    strncpy(entry->text, text, length);
    entry->length = length;
}

int main(int argc, char **argv)
{
    LoggingSystem loggingSystem = {};
    g_LoggingSystem = &loggingSystem;

    // TODO: Compute this from persistent, transient and asset arena size!
    u64 platformMemorySize = Megabytes(256);
    void *platformMemory = PlatformAllocateMemory(platformMemorySize);
    ASSERT(platformMemory != NULL);

    MemoryArena platformArena = {};
    InitializeMemoryArena(&platformArena, platformMemory, platformMemorySize);

    LogBuffer logBuffer = CreateLogBuffer(&platformArena);

    // FIXME: Bad design, log messages generated before sinks are registered/up
    // will be lost! Logging system should keep an internal buffer which the
    // sinks then read from.
    AddSink(&loggingSystem, LoggingSink_Stdout, LogToStdout, NULL);
    AddSink(&loggingSystem, LoggingSink_Console, LogToConsole, &logBuffer);

    ProfilingSystemConfig profilingConfig = {};
    profilingConfig.maxBuffers = 8;
    profilingConfig.maxEventsPerBuffer = 0x4000;
    profilingConfig.maxMetaData = 1024;
    ProfilingSystem profilingSystem =
        CreateProfilingSystem(&platformArena, profilingConfig);
    profilingSystem.mode = ProfilingMode_Overwrite;

    glfwSetErrorCallback(GlfwErrorCallback);
    if (!glfwInit())
    {
        return -1;
    }

    // Don't know why but FPS doubles when specifying the context to use!
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    b32 windowedMode = false;
    b32 enableVsync = true;

    GLFWmonitor *monitor = NULL;
    u32 windowWidth = 1920;
    u32 windowHeight = 1080;
    if (!windowedMode)
    {
        monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode *mode = glfwGetVideoMode(monitor);

        glfwWindowHint(GLFW_RED_BITS, mode->redBits);
        glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
        glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
        glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
        windowWidth = mode->width;
        windowHeight = mode->height;
    }

    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);

    GLFWwindow *window = glfwCreateWindow(
        windowWidth, windowHeight, "Hello World", monitor, NULL);
    if (window == NULL)
    {
        glfwTerminate();
        return -1;
    }
    g_Window = window;

    glfwSetKeyCallback(window, GlfwKeyCallback);
    glfwSetMouseButtonCallback(window, GlfwMouseButtonCallback);
    glfwSetCursorPosCallback(window, GlfwCursorPositionCallback);
    glfwSetCharCallback(window, GlfwCharCallback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(enableVsync ? 1 : 0); // Enable VSync

    GLenum glewError = glewInit();
    if (glewError != GLEW_OK)
    {
        fprintf(stderr, "GLEW error: %s\n", glewGetErrorString(glewError));
        glfwTerminate();
        return -1;
    }
    LOG_MSG("Using GLEW %s", glewGetString(GLEW_VERSION));

    LOG_MSG("GL_VENDOR: %s", (const char *)glGetString(GL_VENDOR));
    LOG_MSG("GL_RENDERER: %s", (const char *)glGetString(GL_RENDERER));
    LOG_MSG("GL_VERSION: %s", (const char *)glGetString(GL_VERSION));
    LOG_MSG("GL_SHADING_LANGUAGE_VERSION: %s",
        (const char *)glGetString(GL_SHADING_LANGUAGE_VERSION));

    ASSERT(GLEW_ARB_gl_spirv);

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(OpenGLMessageCallback, 0);

    // Enable all debug messages
    glDebugMessageControl(
        GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, true);

    GameCode gameCode = LoadGameCode();
    ASSERT(gameCode.handle);

    FileCache fileCache = {};
    InitializeFileCache(&fileCache, GetLastWriteTimeU64);

    AssetSystem assetSystem = {};
    AssetSystemConfig assetSystemConfig = {};
    assetSystemConfig.assetMemorySize = Megabytes(8);
    assetSystemConfig.maxTextures = 64;
    assetSystemConfig.maxShaders = 64;
    assetSystemConfig.maxFonts = 32;
    assetSystemConfig.maxMeshes = 64;
    assetSystemConfig.maxScenes = 32;
    assetSystemConfig.maxMaterials = 64;
    assetSystemConfig.maxSkeletons = 32;
    assetSystemConfig.maxComputeShaders = 32;
    assetSystemConfig.fileCache = &fileCache;
    InitializeAssetSystem(&assetSystem, &platformArena, assetSystemConfig);

    RenderData renderData = {};
    renderData.assetSystem = &assetSystem;

    // TODO: Transient arena
    GameMemory gameMemory = {};
    gameMemory.persistentStorageSize = Megabytes(128);
    gameMemory.persistentStorage =
        AllocateBytes(&platformArena, gameMemory.persistentStorageSize);
    gameMemory.tempStorageSize = Megabytes(1);
    gameMemory.tempStorage =
        AllocateBytes(&platformArena, gameMemory.tempStorageSize);
    gameMemory.debugShowMouseCursor = &GlfwShowMouseCursor;
    gameMemory.renderData = &renderData;
    gameMemory.loggingSystem = &loggingSystem;
    gameMemory.logBuffer = &logBuffer;
    gameMemory.profilingSystem = &profilingSystem;
    // gameMemory.runAutomatedTests = true;

    renderData.cameraUniformBuffer = CreateCameraUniformBuffer();
    renderData.lightingUniformBuffer = CreateLightingUniformBuffer();
    renderData.debugDrawVerticesBuffer =
        CreateDebugDrawVerticesBuffer(2 * 1024 * 1024);
    renderData.textVerticesBuffer = CreateTextVerticesBuffer(30000);
    renderData.shadowMapBuffer = CreateShadowMapBuffer(2048, 2048);
    renderData.hdrFramebuffer = CreateHdrFramebuffer(windowWidth, windowHeight);

    glGenQueries(ARRAY_LENGTH(renderData.timeElapsedQueryObjects),
        renderData.timeElapsedQueryObjects);

    // Pre-populate asset cache
    MeshData planeMeshData = CreatePlaneMeshData();
    MeshData cubeMeshData = CreateCubeMeshData();
    MeshData patchMeshData =
        CreatePatchMeshData(&assetSystem.arena, TERRAIN_PATCH_WIDTH);

    MeshState planeMesh = CreateMeshFromData(planeMeshData);
    MeshState cubeMesh = CreateMeshFromData(cubeMeshData);
    MeshState patchMesh = CreateMeshFromData(patchMeshData);

    {
        Asset_Mesh mesh = {};
        mesh.data = &planeMesh;
        mesh.meshData = &planeMeshData;
        SetAssetData_(
            &assetSystem, AssetType_Mesh, Mesh_Plane, sizeof(mesh), &mesh);
    }
    {
        Asset_Mesh mesh = {};
        mesh.data = &cubeMesh;
        mesh.meshData = &cubeMeshData;
        SetAssetData_(
            &assetSystem, AssetType_Mesh, Mesh_Cube, sizeof(mesh), &mesh);
    }
    {
        Asset_Mesh mesh = {};
        mesh.data = &patchMesh;
        mesh.meshData = &patchMeshData;
        SetAssetData_(&assetSystem, AssetType_Mesh, Mesh_TerrainPatch,
            sizeof(mesh), &mesh);
    }
    AddMeshFromPath(&assetSystem, Mesh_Sphere, "../assets/meshes/sphere.obj");
    AddMeshFromPath(&assetSystem, Mesh_Capsule, "../assets/meshes/capsule.obj");
    AddMeshFromPath(&assetSystem, Mesh_Hatchet, "../assets/meshes/hatchet.obj");
    AddMeshFromPath(&assetSystem, Mesh_PickAxe, "../assets/meshes/pickaxe.obj");
    AddMeshFromPath(&assetSystem, Mesh_Hammer, "../assets/meshes/hammer.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/log_pile"),
        "../assets/meshes/log_pile.obj");
    AddMeshFromPath(&assetSystem, Mesh_CraftingBench,
        "../assets/meshes/crafting_bench.obj");
    AddMeshFromPath(
        &assetSystem, Mesh_WoodenBow, "../assets/meshes/wooden_bow.obj");
    AddMeshFromPath(
        &assetSystem, Mesh_WoodenSpear, "../assets/meshes/wooden_spear.obj");
    AddMeshFromPath(
        &assetSystem, Mesh_PineTree, "../assets/meshes/pine_tree.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/pine_tree2"),
        "../assets/meshes/pine_tree2.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/pine_tree3"),
        "../assets/meshes/pine_tree3.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/sapling"),
        "../assets/meshes/sapling.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/rock01"),
        "../assets/meshes/rock01.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/rock02"),
        "../assets/meshes/rock02.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/rock03"),
        "../assets/meshes/rock03.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/grass"),
        "../assets/meshes/grass.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/firepit"),
        "../assets/meshes/firepit.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/lumber_camp"),
        "../assets/meshes/lumber_camp.obj");
    AddMeshFromPath(&assetSystem,
        HashStringU32("meshes/building_parts/foundation"),
        "../assets/meshes/building_parts/foundation.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/building_parts/wall"),
        "../assets/meshes/building_parts/wall.obj");
    AddMeshFromPath(&assetSystem,
        HashStringU32("meshes/building_parts/doorway"),
        "../assets/meshes/building_parts/doorway.obj");
    AddMeshFromPath(&assetSystem,
        HashStringU32("meshes/building_parts/ceiling"),
        "../assets/meshes/building_parts/ceiling.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/building_parts/stairs"),
        "../assets/meshes/building_parts/stairs.obj");
    AddMeshFromPath(&assetSystem,
        HashStringU32("meshes/building_parts/storage_box"),
        "../assets/meshes/building_parts/storage_box.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/building_parts/window"),
        "../assets/meshes/building_parts/window.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/error"),
        "../assets/meshes/error.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/furnace"),
        "../assets/meshes/furnace.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/shack"),
        "../assets/meshes/shack01.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/warehouse"),
        "../assets/meshes/warehouse.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/splinter"),
        "../assets/meshes/splinter.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/target"),
        "../assets/meshes/target.obj");
    AddMeshFromPath(&assetSystem, HashStringU32("meshes/test_gun"),
        "../assets/meshes/test_gun.obj");
    // AddMeshFromPath(&assetSystem, Mesh_Xbot, "../assets/meshes/xbot.fbx");

    // AddSkeletonFromPath(
    //&assetSystem, Skeleton_Xbot, "../assets/meshes/xbot.fbx");
    // TODO: Properly integrate this
    ImportSceneResult importSceneResult =
        ImportSceneFromDisk("../assets/meshes/xbot.fbx", &assetSystem.arena);
    {
        Asset_Skeleton skeleton = {};
        skeleton.data = &importSceneResult.skeleton;
        skeleton.bindPose = &importSceneResult.bindPose;
        skeleton.bindPose->skeleton = skeleton.data; // TODO: Ewwww
        SetAssetData_(&assetSystem, AssetType_Skeleton, Skeleton_Xbot,
            sizeof(skeleton), &skeleton);
    }
    for (u32 i = 0; i < importSceneResult.count; i++)
    {
        Asset_Mesh mesh = {};
        mesh.meshData = &importSceneResult.meshes[i];
        mesh.data = ALLOCATE_STRUCT(&assetSystem.arena, MeshState);
        *mesh.data = CreateMeshFromData(*mesh.meshData);

        char name[80];
        snprintf(name, sizeof(name), "meshes/xbot/%u", i);
        u32 id = HashStringU32(name);
        SetAssetData_(&assetSystem, AssetType_Mesh, id, sizeof(mesh), &mesh);
    }

    u32 missingTextureHandle = CreateMissingTexture();
    u32 solidWhiteTextureHandle = CreateSolidColorTexture(Vec4(1));
    u32 solidGreenTextureHandle = CreateSolidColorTexture(Vec4(0, 1, 0, 1));
    u32 solidRedTextureHandle = CreateSolidColorTexture(Vec4(1, 0, 0, 1));
    u32 solidOrangeTextureHandle = CreateSolidColorTexture(Vec4(1, 0.2, 0, 1));
    u32 solidBlueTextureHandle = CreateSolidColorTexture(Vec4(0.4, 0.4, 1, 1));
    u32 metalOreTextureHandle =
        CreateSolidColorTexture(Vec4(0.18, 0.1, 0.1, 1));
    u32 terrainHeightMapTextureHandle = CreateTerrainHeighMapTexture(64, 64);
    AddTextureHandle(&assetSystem, Texture_Missing, missingTextureHandle);
    AddTextureHandle(&assetSystem, Texture_SolidWhite, solidWhiteTextureHandle);
    AddTextureHandle(&assetSystem, Texture_SolidGreen, solidGreenTextureHandle);
    AddTextureHandle(&assetSystem, Texture_SolidRed, solidRedTextureHandle);
    AddTextureHandle(
        &assetSystem, Texture_SolidOrange, solidOrangeTextureHandle);
    AddTextureHandle(&assetSystem, Texture_SolidBlue, solidBlueTextureHandle);
    AddTextureHandle(&assetSystem, HashStringU32("textures/metal_ore"),
        metalOreTextureHandle);
    // AddTextureHandle(&assetSystem, Texture_HDRI_CubeMap, hdriCubeMap);
    // AddTextureHandle(
    //&assetSystem, Texture_HDRI_CubeMapIrradiance, irradianceCubeMap);
    AddTextureHandle(
        &assetSystem, Texture_TerrainHeightMap, terrainHeightMapTextureHandle);
    AddTextureHandle(&assetSystem, Texture_PreethamSky_CubeMap, 0);
    AddTextureHandle(&assetSystem, Texture_PreethamSky_Irradiance, 0);
    // AddTextureHandle(&assetSystem, HashStringU32("textures/preetham_sky"),
    // preethamSkyTexture);

    AddTextureFromPath(
        &assetSystem, Texture_Grass, "../assets/textures/grass_color.tga");
    AddTextureFromPath(&assetSystem, Texture_Concrete,
        "../assets/textures/concrete_color.tga");
    AddTextureFromPath(
        &assetSystem, Texture_Bark, "../assets/textures/Bark001_1K_Color.png");
    AddTextureFromPath(&assetSystem, Texture_PrototypeGround,
        "../assets/textures/prototyping/dark/texture_09.png");
    AddTextureFromPath(&assetSystem,
        HashStringU32("textures/prototyping/light/texture_08"),
        "../assets/textures/prototyping/light/texture_08.png");
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_Wood,
        "../assets/textures/test_item_icons/drift_wood.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_Meat,
        "../assets/textures/test_item_icons/meat.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_CookedMeat,
        "../assets/textures/test_item_icons/cooked_meat.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_Stone,
        "../assets/textures/test_item_icons/stone.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_Hatchet,
        "../assets/textures/test_item_icons/hatchet.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_Hammer,
        "../assets/textures/test_item_icons/hammer.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_WoodenBow,
        "../assets/textures/test_item_icons/wooden_bow.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_WoodenArrow,
        "../assets/textures/test_item_icons/arrow.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_Carrot,
        "../assets/textures/test_item_icons/carrot.png", false, true);
    AddTextureFromPath(&assetSystem,
        HashStringU32("textures/item_icons/metal_ore"),
        "../assets/textures/test_item_icons/metal_ore.png", false, true);
    AddTextureFromPath(&assetSystem,
        HashStringU32("textures/item_icons/metal_ingot"),
        "../assets/textures/test_item_icons/metal_ingot.png", false, true);
    AddTextureFromPath(&assetSystem, Texture_ItemIcon_WoodenSpear,
        "../assets/textures/test_item_icons/wooden_spear.png", false, true);
    AddTextureFromPath(&assetSystem,
        HashStringU32("textures/item_icons/test_gun"),
        "../assets/textures/test_item_icons/test_gun.png", false, true);
    AddTextureFromPath(&assetSystem, HashStringU32("textures/rock"),
        "../assets/textures/rock.jpg");
    AddTextureFromPath(&assetSystem, HashStringU32("textures/grass_alpha"),
        "../assets/textures/grass_alpha.png", false, true, false);
    AddTextureFromPath(&assetSystem, HashStringU32("textures/wood_floor"),
        "../assets/textures/WoodFloor041_1K_Color.png");

    AddTextureFromPath(&assetSystem, HashStringU32("textures/particles/smoke"),
        "../assets/textures/particles/smoke_06.png", false, true);

    AddComputeShaderFromPath(&assetSystem,
        HashStringU32("shaders/irradiance_cubemap"),
        "shaders/generate_irradiance_cubemap.comp.spv");
    AddComputeShaderFromPath(&assetSystem,
        HashStringU32("shaders/preetham_sky_comp"),
        "shaders/preetham_sky.comp.spv");
    AddComputeShaderFromPath(&assetSystem,
        HashStringU32("shaders/generate_normal_map"),
        "shaders/generate_normal_map.comp.spv");
    AddComputeShaderFromPath(&assetSystem,
        HashStringU32("shaders/generate_height_map"),
        "shaders/generate_height_map.comp.spv");

    AddShaderFromPath(&assetSystem, Shader_Default, "shaders/default.vert.spv",
        "shaders/default.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_Particle, "shaders/default.vert.spv",
        "shaders/particle.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_CubeMap, "shaders/default.vert.spv",
        "shaders/cube_map.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_WorldTriplanar,
        "shaders/default.vert.spv", "shaders/world_triplanar.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_DebugDrawing,
        "shaders/debug_drawing.vert.spv", "shaders/debug_drawing.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_Text, "shaders/text.vert.spv",
        "shaders/text.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_UITexture, "shaders/text.vert.spv",
        "shaders/ui_texture.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_PostProcessing,
        "shaders/post_processing.vert.spv", "shaders/post_processing.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_Terrain, "shaders/terrain.vert.spv",
        "shaders/terrain.frag.spv");
    AddShaderFromPath(&assetSystem, HashStringU32("shaders/grass"),
        "shaders/default.vert.spv", "shaders/grass.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_Skinning,
        "shaders/skinning.vert.spv", "shaders/default.frag.spv");
    AddShaderFromPath(&assetSystem, Shader_Instancing,
        "shaders/instancing.vert.spv", "shaders/grass.frag.spv");

    AddFontFromPath(
        &assetSystem, Font_Debug, "../assets/Inconsolata-Regular.ttf", 16.0f);
    AddFontFromPath(
        &assetSystem, Font_FpsDisplay, "../assets/spleen-32x64.otf", 20.0f);

    // NOTE: Any shader that wants to work with the material system needs to
    // have the same interface
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_Missing;
        SetAssetData_(&assetSystem, AssetType_Material, Material_Missing,
            sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_WorldTriplanar;
        material.albedo = Texture_PrototypeGround;
        SetAssetData_(&assetSystem, AssetType_Material,
            Material_PrototypeTriplanar, sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_WorldTriplanar;
        material.albedo =
            HashStringU32("textures/prototyping/light/texture_08");
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/prototyping/light"), sizeof(material),
            &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_ItemIcon_Wood;
        SetAssetData_(&assetSystem, AssetType_Material, Material_WoodPickup,
            sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_ItemIcon_Carrot;
        SetAssetData_(&assetSystem, AssetType_Material, Material_CarrotPickup,
            sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_ItemIcon_Stone;
        SetAssetData_(&assetSystem, AssetType_Material, Material_StonePickup,
            sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_Bark;
        SetAssetData_(&assetSystem, AssetType_Material, Material_Bark,
            sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_SolidGreen;
        SetAssetData_(&assetSystem, AssetType_Material,
            Material_DeployableVisualization, sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = HashStringU32("textures/rock");
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/rock"), sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = HashStringU32("shaders/grass");
        material.albedo = HashStringU32("textures/grass_alpha");
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/grass_alpha"), sizeof(material),
            &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_SolidGreen;
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/friendly_ai"), sizeof(material),
            &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_SolidBlue;
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/harvester_ai"), sizeof(material),
            &material);
    }
    {
        // AddPlayer(world, testP + Vec3(2, 0, 5));
        // AddPlayer(world, testP + Vec3(2, 0, 5));
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_SolidOrange;
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/builder_ai"), sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_SolidRed;
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/enemy_ai"), sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = HashStringU32("textures/wood_floor");
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/wood_floor"), sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = Texture_Concrete;
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/concrete"), sizeof(material), &material);
    }
    {
        Asset_Material material = {};
        material.shader = Shader_Default;
        material.albedo = HashStringU32("textures/metal_ore");
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/metal_ore"), sizeof(material), &material);
    }

    {
        Asset_Material material = {};
        material.shader = Shader_Skinning;
        material.albedo = Texture_Missing;
        SetAssetData_(&assetSystem, AssetType_Material,
            HashStringU32("materials/skinning"), sizeof(material), &material);
    }

    b32 running = true;
    InputState *input = &g_InputState;
    f64 currentTime = glfwGetTime();
    f64 frameTimeAccumulator = 0.0;
    f32 cappedFrameTime = 1.0f / 60.0f;
    while (running)
    {
        f64 newTime = glfwGetTime();
        f64 frameTime = newTime - currentTime;
        if (frameTime > 0.25) // TODO: Make this configurable
        {
            frameTime = 0.25;
        }
        currentTime = newTime;

        gameMemory.codeReloaded = false;
        if (WasGameCodeModified(&gameCode))
        {
            LOG_MSG("Reloading game code...");

            // NOTE: Need to clear profiling meta data which uses strings stored
            // in the old DLL
            prof_Clear(&profilingSystem);

            UnloadGameCode(gameCode);
            gameCode = LoadGameCode();
            ASSERT(gameCode.handle);
            gameMemory.codeReloaded = true;
        }

        InputBeginFrame(input);

        glfwPollEvents();

        if (glfwWindowShouldClose(window))
        {
            running = false;
        }

        // Asset reloading
        CheckForAssetsToReload(&assetSystem);

        // Process asset load queue
        ProcessAssetLoadQueue(&assetSystem, &renderData);

        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        if ((u32)width != input->framebufferWidth ||
            (u32)height != input->framebufferHeight)
        {
            DestroyHdrFramebuffer(renderData.hdrFramebuffer);
            // Jemima is awesome and loves David so much xoxo <3
            // however, she does not endorse this curly brace style
            // however, she fixed it for this if statement but the rest of the
            // file is abhorrent.
            renderData.hdrFramebuffer =
                CreateHdrFramebuffer((u32)width, (u32)height);
            input->framebufferWidth = width;
            input->framebufferHeight = height;
        }

        gameCode.updateAndRender(&gameMemory, input, frameTime);

        // Finish rendering
        glfwSwapBuffers(window);

        prof_SwapBuffers(&profilingSystem);
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
