internal void InitializeItemDatabase(ItemDatabase *db, MemoryArena *arena)
{
    memset(db, 0, sizeof(*db));

    u32 itemDbStorageSize = Kilobytes(128);
    db->arena = SubAllocateArena(arena, itemDbStorageSize);

    u32 maxItems = 1024;
    db->items = Dict_CreateFromArena(&db->arena, ItemDatabaseEntry, maxItems);

    u32 maxRecipes = 64;
    db->recipes = Dict_CreateFromArena(&db->arena, Recipe, maxRecipes);
}

internal void ClearItemDatabase(ItemDatabase *db)
{
    Dict_Clear(&db->items);
    Dict_Clear(&db->recipes);
}

internal b32 RegisterItem(ItemDatabase *itemDatabase, u32 id, const char *name,
    u32 iconTexture, u32 maxStackSize, u32 flags = ItemFlag_None,
    vec4 iconTintColor = Vec4(1))
{
    ItemDatabaseEntry *entry =
        Dict_AddItem(&itemDatabase->items, ItemDatabaseEntry, id);
    if (entry != NULL)
    {
        entry->name = name; // TODO: Don't assume string is safe to store
        entry->iconTexture = iconTexture;
        entry->maxStackSize = maxStackSize;
        entry->flags = flags;
        entry->iconTintColor = iconTintColor;
    }
    else
    {
        LOG_MSG("Item database is full. Failed to register item %u - \"%s\"",
            id, name);
    }

    return (entry != NULL);
}

internal b32 RegisterRecipe(ItemDatabase *itemDatabase, u32 id, Recipe recipe)
{
    Recipe *entry = Dict_AddItem(&itemDatabase->recipes, Recipe, id);
    if (entry != NULL)
    {
        *entry = recipe;
    }
    else
    {
        LOG_MSG(
            "Recipe database is full. Failed to register recipe %u - \"%s\"",
            id, recipe.name);
    }

    return (entry != NULL);
}

internal ItemDatabaseEntry *FindItem(ItemDatabase *itemDatabase, u32 id)
{
    ItemDatabaseEntry *result =
        Dict_FindItem(&itemDatabase->items, ItemDatabaseEntry, id);
    return result;
}

internal Recipe *FindRecipe(ItemDatabase *itemDatabase, u32 id)
{
    Recipe *result = Dict_FindItem(&itemDatabase->recipes, Recipe, id);
    return result;
}

internal void InitializeInventorySystem(InventorySystem *inventorySystem,
    InventorySystemConfig config, MemoryArena *arena)
{
    memset(inventorySystem, 0, sizeof(*inventorySystem));

    inventorySystem->arena = SubAllocateArena(arena, config.arenaSize);

    inventorySystem->inventories = Dict_CreateFromArena(
        &inventorySystem->arena, Inventory, config.maxInventories);
    inventorySystem->items = Dict_CreateFromArena(
        &inventorySystem->arena, ItemInstance, config.maxItems);
    inventorySystem->nextInventoryId = 1;
    inventorySystem->nextItemInstanceId = 1;
}

internal u32 AllocateInventory(InventorySystem *inventorySystem)
{
    u32 id = inventorySystem->nextInventoryId++;

    Inventory *inventory =
        Dict_AddItem(&inventorySystem->inventories, Inventory, id);
    if (inventory != NULL)
    {
        memset(inventory, 0, sizeof(*inventory));
    }
    else
    {
        LOG_MSG("Failed to allocate inventory");
        id = 0;
    }
    return id;
}

internal Inventory *FindInventoryById(InventorySystem *inventorySystem, u32 id)
{
    Inventory *inventory =
        Dict_FindItem(&inventorySystem->inventories, Inventory, id);
    return inventory;
}

internal void RemoveInventory(InventorySystem *inventorySystem, u32 id)
{
    Dict_RemoveItem(&inventorySystem->inventories, id);
}

inline i32 FindInventorySlotById(Inventory *inventory, u32 id)
{
    i32 result = -1;
    for (u32 i = 0; i < ARRAY_LENGTH(inventory->items); ++i)
    {
        if (inventory->items[i] == id)
        {
            result = (u32)i;
            break;
        }
    }

    return result;
}

inline i32 FindFreeSlotInInventory(Inventory *inventory)
{
    i32 result = FindInventorySlotById(inventory, 0);
    return result;
}

inline b32 IsInventoryEmpty(Inventory inventory)
{
    b32 empty = true;
    for (u32 i = 0; i < ARRAY_LENGTH(inventory.items); i++)
    {
        if (inventory.items[i] != 0)
        {
            empty = false;
            break;
        }
    }

    return empty;
}

inline Inventory *GetEntityInventory(
    EntityWorld *entityWorld, InventorySystem *inventorySystem, u32 entityId)
{
    u32 inventoryId = 0;
    Entity *entity = FindEntityById(entityWorld, entityId);
    if (entity != NULL)
    {
        inventoryId = entity->inventoryId;
    }

    Inventory *inventory = FindInventoryById(inventorySystem, inventoryId);
    return inventory;
}

internal b32 IsInventoryEmpty(
    EntityWorld *entityWorld, InventorySystem *inventorySystem, u32 entityId)
{
    b32 result = true;
    Inventory *inventory =
        GetEntityInventory(entityWorld, inventorySystem, entityId);
    if (inventory != NULL)
    {
        result = IsInventoryEmpty(*inventory);
    }

    return result;
}

internal u32 AllocateItemInstance(InventorySystem *inventorySystem)
{
    u32 id = inventorySystem->nextItemInstanceId++;

    ItemInstance *instance =
        Dict_AddItem(&inventorySystem->items, ItemInstance, id);
    if (instance != NULL)
    {
        memset(instance, 0, sizeof(*instance));
    }
    else
    {
        LOG_MSG("Failed to allocate item instance");
        id = 0;
    }
    return id;
}

internal ItemInstance *FindItemInstanceById(
    InventorySystem *inventorySystem, u32 id)
{
    ItemInstance *instance =
        Dict_FindItem(&inventorySystem->items, ItemInstance, id);
    return instance;
}

internal void RemoveItemInstance(InventorySystem *inventorySystem, u32 id)
{
    Dict_RemoveItem(&inventorySystem->items, id);
}

internal b32 AddItemToInventory(EntityWorld *world, ItemDatabase *itemDatabase,
    InventorySystem *inventorySystem, Inventory *inventory, u32 itemId,
    u32 quantity = 1)
{
    b32 inventoryUpdated = false;
    u32 originalQuantity = quantity;

    // First check if we can stack the item
    ItemDatabaseEntry *itemData = FindItem(itemDatabase, itemId);
    ASSERT(itemData != NULL);
    if (itemData->maxStackSize > 1)
    {
        // Find existing stack that we can combine with
        for (u32 i = 0; i < ARRAY_LENGTH(inventory->items); i++)
        {
            u32 existingItemId = inventory->items[i];
            if (existingItemId != 0)
            {
                ItemInstance *existingItem =
                    FindItemInstanceById(inventorySystem, existingItemId);
                if (existingItem != NULL)
                {
                    if (existingItem->itemId == itemId)
                    {
                        // We can stack
                        u32 combinedQuantity =
                            existingItem->quantity + quantity;
                        if (combinedQuantity <= itemData->maxStackSize)
                        {
                            // Can combine the entire amount into a single stack
                            existingItem->quantity += quantity;
                            quantity = 0;
                            break; // Done
                        }
                        else
                        {
                            // Multiple stacks
                            u32 amount =
                                itemData->maxStackSize - existingItem->quantity;
                            existingItem->quantity = itemData->maxStackSize;
                            ASSERT(quantity >= amount);
                            quantity -= amount;
                        }
                    }
                }
            }

            if (quantity == 0)
            {
                // Been able to stack all incoming items over multiple stacks
                break;
            }
        }
    }
    else
    {
        // Can't stack
        // Find free slot, allocate entity, etc
    }

    while (quantity > 0)
    {
        i32 slot = FindFreeSlotInInventory(inventory);
        if (slot != -1)
        {
            u32 itemInstanceId = AllocateItemInstance(inventorySystem);
            ItemInstance *item =
                FindItemInstanceById(inventorySystem, itemInstanceId);

            item->itemId = itemId;
            item->quantity = MinU32(quantity, itemData->maxStackSize);

            ASSERT(quantity >= item->quantity);
            quantity -= item->quantity;

            // Add item to inventory
            inventory->items[slot] = itemInstanceId;
            inventoryUpdated = true;
        }
        else
        {
            // Can't handle full amount, items lost!
            break;
        }
    }

    if (quantity > 0)
    {
        LOG_MSG("Insufficient space to add %u \"%s\" to inventory, %u have "
                "been lost!",
            originalQuantity, itemData->name, quantity);
    }

    return inventoryUpdated;
}

internal u32 CountItem(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, Inventory *inventory, u32 itemId)
{
    u32 total = 0;
    for (u32 i = 0; i < ARRAY_LENGTH(inventory->items); i++)
    {
        u32 itemInstanceId = inventory->items[i];
        if (itemInstanceId != 0)
        {
            ItemInstance *item =
                FindItemInstanceById(inventorySystem, itemInstanceId);
            if (item != NULL)
            {
                if (item->itemId == itemId)
                {
                    total += item->quantity;
                }
            }
        }
    }

    return total;
}

internal u32 CountItem(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, u32 inventoryOwnerId, u32 itemId)
{
    u32 count = 0;
    Inventory *inventory =
        GetEntityInventory(entityWorld, inventorySystem, inventoryOwnerId);
    if (inventory != NULL)
    {
        count = CountItem(entityWorld, inventorySystem, inventory, itemId);
    }

    return count;
}

internal b32 ConsumeItemFromSlot(InventorySystem *inventorySystem,
    Inventory *inventory, u32 slot, u32 itemId, u32 quantity)
{
    ASSERT(slot < ARRAY_LENGTH(inventory->items));

    b32 result = false;
    u32 itemInstanceId = inventory->items[slot];
    ItemInstance *item = FindItemInstanceById(inventorySystem, itemInstanceId);
    if (item != NULL)
    {
        if (item->itemId == itemId)
        {
            if (item->quantity >= quantity)
            {
                item->quantity -= quantity;
                if (item->quantity == 0)
                {
                    // Item has been fully consumed, remove it
                    RemoveItemInstance(inventorySystem, itemInstanceId);
                    inventory->items[slot] = 0;
                }
                else
                {
                    // Update remaining quantity
                }
                result = true;
            }
            else
            {
                LOG_MSG("Insufficient quantity");
            }
        }
    }
    return result;
}

internal b32 ConsumeItemFromSlot(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, u32 entityId, u32 slot, u32 itemId,
    u32 quantity)
{
    b32 result = false;
    Inventory *inventory =
        GetEntityInventory(entityWorld, inventorySystem, entityId);

    if (inventory != NULL)
    {
        result = ConsumeItemFromSlot(
            inventorySystem, inventory, slot, itemId, quantity);
    }
    else
    {
        LOG_MSG("Unable to find inventory for entity %u", entityId);
    }

    return result;
}

// NOTE: This function assumes the caller has already verified that the
// inventory has the items we're trying to remove.
// RETURN: Whether the inventory was updated or not
internal void ConsumeItem(InventorySystem *inventorySystem,
    Inventory *inventory, u32 itemId, u32 quantity)
{
    for (u32 i = 0; i < ARRAY_LENGTH(inventory->items); i++)
    {
        u32 itemInstanceId = inventory->items[i];
        if (itemInstanceId != 0)
        {
            ItemInstance *item =
                FindItemInstanceById(inventorySystem, itemInstanceId);
            if (item != NULL)
            {
                if (item->itemId == itemId)
                {
                    u32 amountToRemove = MinU32(item->quantity, quantity);

                    // Logic check
                    ASSERT(item->quantity >= amountToRemove);
                    ASSERT(quantity >= amountToRemove);

                    item->quantity -= amountToRemove;
                    quantity -= amountToRemove;

                    if (item->quantity == 0)
                    {
                        // Item has been fully consumed, remove it
                        RemoveItemInstance(inventorySystem, itemInstanceId);
                        inventory->items[i] = 0;
                    }
                    else
                    {
                        // Update remaining quantity
                    }

                    if (quantity == 0)
                    {
                        // We're done
                        break;
                    }
                }
            }
        }
    }
}

internal void ConsumeItem(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, u32 owner, u32 itemId, u32 quantity)
{
    Inventory *inventory =
        GetEntityInventory(entityWorld, inventorySystem, owner);
    ConsumeItem(inventorySystem, inventory, itemId, quantity);
}

inline u32 MapBeltSlotToInventorySlot(u32 slot)
{
    u32 inventorySlot = (ARRAY_LENGTH(((Inventory *)0)->items) - 6) + slot;
    return inventorySlot;
}

internal u32 GetItemInstanceIdInBeltSlot(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, u32 entityId, u32 slot)
{
    u32 itemInstanceId = 0;
    // Map into inventoy slot
    u32 inventorySlot = MapBeltSlotToInventorySlot(slot);

    Inventory *inventory =
        GetEntityInventory(entityWorld, inventorySystem, entityId);
    if (inventory != NULL)
    {
        itemInstanceId = inventory->items[inventorySlot];
    }
    return itemInstanceId;
}

internal b32 GetItemIdInBeltSlot(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, u32 entityId, u32 slot, u32 *itemId)
{
    b32 result = false;

    u32 itemInstanceId = GetItemInstanceIdInBeltSlot(
        entityWorld, inventorySystem, entityId, slot);
    ItemInstance *item = FindItemInstanceById(inventorySystem, itemInstanceId);
    if (item != NULL)
    {
        *itemId = item->itemId;
        result = true;
    }
    return result;
}

internal b32 CheckForRecipeRequirements(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, Inventory *inventory,
    RecipeInputRequirement *inputs, u32 inputCount)
{
    b32 allInputsSatisfied = true;
    for (u32 i = 0; i < inputCount; i++)
    {
        RecipeInputRequirement *input = inputs + i;

        u32 count =
            CountItem(entityWorld, inventorySystem, inventory, input->itemId);

        if (count < input->quantity)
        {
            allInputsSatisfied = false;
            break;
        }
    }
    return allInputsSatisfied;
}

internal b32 CheckForRecipeRequirements(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, u32 entityId,
    RecipeInputRequirement *inputs, u32 inputCount)
{
    b32 result = false;

    Inventory *inventory =
        GetEntityInventory(entityWorld, inventorySystem, entityId);
    if (inventory != NULL)
    {
        result = CheckForRecipeRequirements(
            entityWorld, inventorySystem, inventory, inputs, inputCount);
    }

    return result;
}

internal void CraftItem(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, ItemDatabase *itemDatabase, u32 owner,
    u32 recipeId)
{
    Recipe *recipe = FindRecipe(itemDatabase, recipeId);
    if (recipe != NULL)
    {
        Inventory *inventory =
            GetEntityInventory(entityWorld, inventorySystem, owner);
        if (inventory != NULL)
        {
            if (CheckForRecipeRequirements(entityWorld, inventorySystem,
                    inventory, recipe->inputs, recipe->inputCount))
            {
                // Remove input items from inventory
                for (u32 i = 0; i < recipe->inputCount; i++)
                {
                    ConsumeItem(inventorySystem, inventory,
                        recipe->inputs[i].itemId, recipe->inputs[i].quantity);
                }

                // Add crafted item to inventory
                AddItemToInventory(entityWorld, itemDatabase, inventorySystem,
                    inventory, recipe->itemId, recipe->quantity);
            }
        }
    }
}

internal void AddItemToInventory(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, ItemDatabase *itemDatabase, u32 entityId,
    u32 itemId, u32 quantity = 1)
{
    Inventory *inventory =
        GetEntityInventory(entityWorld, inventorySystem, entityId);
    if (inventory != NULL)
    {
        AddItemToInventory(entityWorld, itemDatabase, inventorySystem,
            inventory, itemId, quantity);
    }
}

internal void ProcessTransferItemCommand(EntityWorld *entityWorld,
    ItemDatabase *itemDatabase, InventorySystem *inventorySystem,
    InventoryCommand command)
{
    LOG_MSG("TransferItem: srcContainer = %u, dstContainer = %u, srcSlot = %u "
            "dstSlot = %u",
        command.srcContainer, command.dstContainer, command.srcSlot,
        command.dstSlot);

    if (command.srcContainer == command.dstContainer &&
        command.srcSlot == command.dstSlot)
    {
        // Skip as nothing to do
        return;
    }

    // NOTE: This is just to make sure srcContainer and dstContainer
    // point to the same local copy of Inventory on the stack
    // if the command is for modifying the same entity.
    Inventory *srcContainer =
        GetEntityInventory(entityWorld, inventorySystem, command.srcContainer);
    Inventory *dstContainer =
        GetEntityInventory(entityWorld, inventorySystem, command.dstContainer);

    ASSERT(srcContainer != NULL);
    ASSERT(dstContainer != NULL);

    u32 srcSlotId = srcContainer->items[command.srcSlot];
    u32 dstSlotId = command.dstSlot < ARRAY_LENGTH(dstContainer->items)
                        ? dstContainer->items[command.dstSlot]
                        : 0;

    if (srcSlotId == dstSlotId)
    {
        LOG_MSG(
            "ERROR - srcSlotId and dstSlotId are both entity %u", srcSlotId);
        return;
    }

    if (dstSlotId == 0)
    {
        // Dst slot is empty, just move src item
        u32 dstSlot = U32_MAX;
        if (command.dstSlot == U32_MAX)
        {
            i32 freeSlot = FindFreeSlotInInventory(dstContainer);
            if (freeSlot >= 0)
            {
                dstSlot = (u32)freeSlot;
            }
            else
            {
                LOG_MSG(
                    "Inventory for entity %u is full", command.dstContainer);
            }
        }
        else
        {
            dstSlot = command.dstSlot;
        }

        if (dstSlot != U32_MAX)
        {
            dstContainer->items[dstSlot] = srcSlotId;
            srcContainer->items[command.srcSlot] = 0;
        }
    }
    else
    {
        ItemInstance *srcItem =
            FindItemInstanceById(inventorySystem, srcSlotId);
        ItemInstance *dstItem =
            FindItemInstanceById(inventorySystem, dstSlotId);
        if (srcItem != NULL && dstItem != NULL)
        {
            if (dstItem->itemId == srcItem->itemId)
            {
                ItemDatabaseEntry *itemData =
                    FindItem(itemDatabase, dstItem->itemId);
                if (itemData != NULL)
                {
                    u32 combinedQuantity =
                        dstItem->quantity + srcItem->quantity;
                    if (combinedQuantity <= itemData->maxStackSize)
                    {
                        // Combine item stacks and clear src slot
                        dstItem->quantity += srcItem->quantity;

                        RemoveItemInstance(inventorySystem, srcSlotId);
                        srcContainer->items[command.srcSlot] = 0;
                    }
                    else
                    {
                        // Transfer some amount from src to dst slot but
                        // src slot remains
                        u32 remainder =
                            combinedQuantity - itemData->maxStackSize;
                        dstItem->quantity = itemData->maxStackSize;
                        srcItem->quantity = remainder;
                    }
                }
            }
        }
    }
}

// WARNING: This will overwrite the contents of the dstInventory which could
// leave behind dangling items if it was not empty
internal void TransferInventory(
    Inventory *dstInventory, Inventory *srcInventory)
{
    // Ensure dst inventory is empty to avoid dangling items
    ASSERT(IsInventoryEmpty(*dstInventory));

    for (u32 i = 0; i < ARRAY_LENGTH(srcInventory->items); i++)
    {
        dstInventory->items[i] = srcInventory->items[i];
        srcInventory->items[i] = NULL_ENTITY_ID;
    }
}

internal void TransferInventory(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, u32 dstEntity, u32 srcEntity)
{
    Inventory *srcInventory =
        GetEntityInventory(entityWorld, inventorySystem, srcEntity);
    Inventory *dstInventory =
        GetEntityInventory(entityWorld, inventorySystem, dstEntity);

    if (srcInventory != NULL && dstInventory != NULL)
    {
        TransferInventory(dstInventory, srcInventory);
    }
    else
    {
        LOG_MSG("src or dst Inventory is NULL");
    }
}

