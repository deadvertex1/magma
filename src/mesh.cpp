internal MeshData CreateMeshData(void *vertices, u32 vertexSize,
    u32 vertexCount, u32 *indices, u32 indexCount)
{
    MeshData result = {};
    result.vertices = vertices;
    result.vertexSize = vertexSize;
    result.vertexCount = vertexCount;
    result.indices = indices;
    result.indexCount = indexCount;
    return result;
}

internal MeshData CreatePlaneMeshData()
{
    MeshData result = CreateMeshData(g_PlaneVertices,
        sizeof(g_PlaneVertices[0]), ARRAY_LENGTH(g_PlaneVertices),
        g_PlaneIndices, ARRAY_LENGTH(g_PlaneIndices));
    return result;
}

internal MeshData CreateCubeMeshData()
{
    MeshData result = CreateMeshData(g_CubeVertices, sizeof(g_CubeVertices[0]),
        ARRAY_LENGTH(g_CubeVertices), g_CubeIndices,
        ARRAY_LENGTH(g_CubeIndices));

    return result;
}

internal MeshData CreatePatchMeshData(MemoryArena *arena, u32 gridDim)
{
    u32 vertDim = gridDim + 1;
    u32 totalVertices = vertDim * vertDim;
    Vertex *vertices = ALLOCATE_ARRAY(arena, Vertex, totalVertices);

    u32 totalIndices = gridDim * gridDim * 2 * 3;
    u32 *indices = ALLOCATE_ARRAY(arena, u32, totalIndices);

    for (u32 y = 0; y < vertDim; ++y)
    {
        for (u32 x = 0; x < vertDim; ++x)
        {
            f32 fx = x / (f32)gridDim;
            f32 fy = y / (f32)gridDim;

            Vertex *vertex = &vertices[y * vertDim + x];
            // Map to -0.5 to 0.5 range
            vertex->position = Vec3(fx, 0.0f, fy);
            vertex->normal = Vec3(0.0f, 1.0f, 0.0f);
            vertex->textureCoord = Vec2(fx, fy);
        }
    }

    u32 index = 0;
    for (u32 y = 0; y < gridDim; y++)
    {
        for (u32 x = 0; x < gridDim; x++)
        {
            indices[index++] = ((x + 1) + vertDim * (y + 1));
            indices[index++] = ((x + 1) + vertDim * y);
            indices[index++] = (x + vertDim * y);

            indices[index++] = ((x + 1) + vertDim * (y + 1));
            indices[index++] = (x + vertDim * y);
            indices[index++] = (x + vertDim * (y + 1));
        }
    }

    MeshData result = CreateMeshData(
        vertices, sizeof(vertices[0]), totalVertices, indices, totalIndices);

    // CalculateTangents(result);

    return result;
}

internal MeshImportResult LoadMeshFromDisk(const char *path, MemoryArena *arena)
{
    MeshImportResult result = {};

    const struct aiScene *scene = aiImportFile(
        path, aiProcess_CalcTangentSpace | aiProcess_Triangulate |
                  aiProcess_JoinIdenticalVertices | aiProcess_SortByPType |
                  aiProcess_ImproveCacheLocality);

    if (!scene)
    {
        fprintf(stderr, "Mesh import failed!\n%s\n", aiGetErrorString());
        return result;
    }

    for (u32 meshIndex = 0; meshIndex < scene->mNumMeshes; meshIndex++)
    {
        aiMesh *mesh = scene->mMeshes[meshIndex];

        u32 vertexCount = mesh->mNumVertices;
        Vertex *vertices = ALLOCATE_ARRAY(arena, Vertex, vertexCount);
        u32 indexCount = mesh->mNumFaces * 3; // We only support triangles
        u32 *indices = ALLOCATE_ARRAY(arena, u32, indexCount);

        // NOTE: mesh->mVertices is always present
        ASSERT(mesh->mNormals);
        ASSERT(mesh->mTextureCoords);

        for (u32 vertexIndex = 0; vertexIndex < vertexCount; ++vertexIndex)
        {
            Vertex *vertex = (Vertex *)vertices + vertexIndex;
            vertex->position.x = mesh->mVertices[vertexIndex].x;
            vertex->position.y = mesh->mVertices[vertexIndex].y;
            vertex->position.z = mesh->mVertices[vertexIndex].z;

            vertex->normal.x = mesh->mNormals[vertexIndex].x;
            vertex->normal.y = mesh->mNormals[vertexIndex].y;
            vertex->normal.z = mesh->mNormals[vertexIndex].z;
            vertex->normal = Normalize(vertex->normal);

            vertex->textureCoord.x = mesh->mTextureCoords[0][vertexIndex].x;
            vertex->textureCoord.y = mesh->mTextureCoords[0][vertexIndex].y;
        }

        for (u32 triangleIndex = 0; triangleIndex < mesh->mNumFaces;
             ++triangleIndex)
        {
            aiFace *face = mesh->mFaces + triangleIndex;
            indices[triangleIndex * 3 + 0] = face->mIndices[0];
            indices[triangleIndex * 3 + 1] = face->mIndices[1];
            indices[triangleIndex * 3 + 2] = face->mIndices[2];
        }

        result.materialIndices[meshIndex] = mesh->mMaterialIndex;

        MeshData meshData = CreateMeshData(
            vertices, sizeof(vertices[0]), vertexCount, indices, indexCount);
        result.meshes[meshIndex] = meshData;
    }
    result.count = scene->mNumMeshes;

    aiReleaseImport(scene);

    return result;
}
