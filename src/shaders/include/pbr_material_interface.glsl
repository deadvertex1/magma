#include "material_interface.frag.glsl"

// Standard uniforms for PBR-style material
layout(std140, binding = 1) uniform LightingUniformBuffer
{
    vec3 u_sun_direction; // Direction from surface to sun
    vec3 u_sun_color;
    vec3 u_sky_color; // TODO: When do we use this vs u_irradiance_cube_map
    vec3 u_camera_position;
    vec3 u_fog_color;
    float u_irradiance_cube_map_contribution;
    float u_fog_multiplier;
};

layout(location = 0) uniform sampler2D u_albedo;
// --- From vertex shader ---
// layout(location = 1) uniform mat4 model_matrix;
// layout(location = 2) uniform uint u_camera_index;
// --- From terrain vertex shader ---
// layout(location = 5) uniform sampler2D u_heightmap;
// layout(location = 6) uniform vec4 u_textureCoordsOffset;
// ----------
layout(location = 3) uniform sampler2DShadow u_shadow_map;
layout(location = 4) uniform samplerCube u_irradiance_cube_map;
layout(location = 7) uniform vec3 u_world_texture_scaling = vec3(1, 1, 1);
// TODO: Metallic
// TODO: Roughness
// TODO: Normal
layout(location = 8) uniform sampler2D u_normal;

float calculate_shadow(vec4 p)
{
    // Perform perspective divide in case of a perspective projection matrix
    // (has no effect for orthographic)
    vec3 projected_coords = p.xyz / p.w;

    // Map to 0-1 range for texture lookup
    projected_coords = projected_coords * 0.5 + 0.5;

#define USE_PCF 1
#if USE_PCF
    projected_coords.z -= 0.005;
    float shadow = 1.0 - texture(u_shadow_map, projected_coords, 0.0);
#else
    float closest_depth = texture(u_shadow_map, projected_coords.xy).r;
    float current_depth = projected_coords.z;

    float bias = 0.001;
    float shadow = current_depth - bias > closest_depth ? 1.0 : 0.0;

    if (projected_coords.z > 1.0)
        shadow = 0.0;
#endif

    return shadow;
}

// Based on https://iquilezles.org/articles/fog/
vec3 calculate_fog(vec3 inputColor)
{
    float dist = length(fs_in.world_position - u_camera_position);

    vec3 rd = normalize(fs_in.world_position - u_camera_position);

    // Exponential fog
    float fogAmount = 1.0 - exp(-dist * u_fog_multiplier);

    // TODO: Pass these in/tweak them for terrain height
    float heightMin = 0.0;
    float heightMax = 100.0;
    float heightT =
        1.0 - ((fs_in.world_position.y - heightMin) / (heightMax - heightMin));
    heightT = clamp(heightT, 0.0, 1.0);
    heightT = smoothstep(0.4, 0.9, heightT);
    fogAmount *= heightT;
    float sunAmount = max(dot(rd, u_sun_direction), 0.0);
    vec3 fogColor =
        u_fog_color; // mix(u_fog_color, u_sun_color, pow(sunAmount, 8.0));

    vec3 outputColor = mix(inputColor, fogColor, fogAmount);
    return outputColor;
}

vec3 pbr_material_model(vec3 albedo, vec3 N)
{
    vec3 L = normalize(u_sun_direction);
    vec3 C = u_sun_color;

    float visiblity = 1.0 - calculate_shadow(fs_in.light_position);

    vec3 irradiance = texture(u_irradiance_cube_map, N).rgb *
                      u_irradiance_cube_map_contribution;

    vec3 color = albedo * (visiblity * C * max(0.0, dot(L, N)) + irradiance);
    color = calculate_fog(color);

    return color;
}

struct TriplanarUV
{
    vec2 x, y, z;
};

TriplanarUV GetTriplanarUV(vec3 p)
{
    TriplanarUV result;
    result.x = p.zy;
    result.y = p.xz;
    result.z = p.xy;
    return result;
}

vec3 GetTriplanarWeights(vec3 n)
{
    vec3 tri_w = abs(n);
    return tri_w / (tri_w.x + tri_w.y + tri_w.z);
}
