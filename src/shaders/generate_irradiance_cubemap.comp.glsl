#extension GL_GOOGLE_include_directive : require

#include "compute_shader_common.glsl"

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout(binding = 0, rgba32f) uniform writeonly imageCube outputImage;
layout(location = 1) uniform samplerCube inputImage;
layout(location = 2) uniform uint cubeMapWidth;
layout(location = 3) uniform float u_sampleIncrement = 0.01f;
layout(location = 4) uniform vec3 u_hsv_multiplier = vec3(1, 1, 1);

// From https://gist.github.com/983/e170a24ae8eba2cd174f
vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
    // TODO: Pass this in as a parameter
    uint width = cubeMapWidth;
    uint height = cubeMapWidth;

    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
    uint layerIndex = gl_GlobalInvocationID.z;

    rng_state = x * 0xC35A4531 ^ y * 0xF125EA62 ^ layerIndex ^ 0xDF70F989;

    BasisVectors basis = MapCubeMapLayerIndexToBasisVectors(layerIndex);

    // Convert pixel to cartesian direction vector
    f32 fx = float(x) / float(width);
    f32 fy = float(y) / float(height);

    // Flip Y axis
    fy = 1.0f - fy;

    // Map to -1 to 1
    fx = fx * 2.0f - 1.0f;
    fy = fy * 2.0f - 1.0f;

    vec3 dir = basis.forward + basis.right * fx + basis.up * fy;
    dir = normalize(dir);

    vec3 irradiance = vec3(0, 0, 0);

    // From https://learnopengl.com/PBR/IBL/Diffuse-irradiance
    vec3 normal = dir;
    vec3 tangent = normalize(cross(basis.up, normal));
    vec3 bitangent = normalize(cross(normal, tangent));

    f32 sampleDelta = u_sampleIncrement;
    u32 sampleCount = 0;
    for (f32 phi = 0.0f; phi < 2.0f * PI; phi += sampleDelta)
    {
        for (f32 theta = 0.0f; theta < 0.5f * PI; theta += sampleDelta)
        {
            // Get sample vector in tangent space
            vec3 tangentDir = MapSphericalToCartesianCoordinates(
                    vec2(phi, theta));

            // Map vector from tangent space to world space so we
            // can sample the environment map
            vec3 worldDir = normal * tangentDir.y +
                tangent * tangentDir.x +
                bitangent * tangentDir.z;

            vec3 radiance = texture(inputImage, worldDir).xyz;

            // Add irradiance contribution for integral
            irradiance += radiance * cos(theta) * sin(theta);
            sampleCount++;
        }
    }

    irradiance = PI * irradiance * (1.0f / float(sampleCount));

    // Arbitrary boost to satution to fake more depth to irradiance
    vec3 irradianceHsv = rgb2hsv(irradiance);
    irradianceHsv *= u_hsv_multiplier;
    irradiance = hsv2rgb(irradianceHsv);

    imageStore(outputImage, ivec3(x, y, layerIndex), vec4(irradiance, 1));
}
