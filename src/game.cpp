/* BUGS
 */
#define ENABLE_LIGHTING 1
#define GAME_TEST_ORIGIN Vec3(500, 150, 500)

#include <cstdint>
#include <cstdio>
#include <cmath>
#include <cfloat>
#include <cstring>
#include <cstdarg>
// #include <x86intrin.h> // Only needed on GCC

#include "platform.h"
#include "memory_pool.h"
#include "logging.h"
#include "math_utils.h"
#include "math_lib.h"
#include "input.h"
#include "hash.h"
#include "dictionary.h"
#include "asset_system.h"
#include "opengl.h"
#include "mesh.h"
#include "contiguous_list.h"

#define ENABLE_GLOBAL_DEBUG_DRAWING
#include "debug_drawing.h"

#include "game_interface.h"
#include "aabb_tree.h"
#include "collision_world.h"
#include "entity_world.h"
#include "profiler.h"
#include "inventory.h"
#include "command_system.h"
#include "terrain.h"
#include "game_events.h"
#include "cvar.h"
#include "ai.h"
#include "game_deployable.h"
#include "game_entities.h"
#include "particle_system.h"
#include "scatter_system.h"
#include "grass_system.h"
#include "bullet_system.h"
#include "game.h"
#include "ray_intersection.h"
#include "asset_ids.h"
#include "file_cache.h"
#include "debug_printing_system.h"
#include "game_player.h"
#include "render.h"
#include "skeletal_animation.h"

// Didn't expect to be able to call OpenGL functions from a DLL which gets
// reloaded
#include <GL/glew.h>

#include "debug_drawing.cpp"
#include "ray_intersection.cpp"
#include "aabb_tree.cpp"
#include "asset_data.cpp"
#include "file_cache.cpp"
#include "asset_system.cpp"
#include "perlin.cpp"
#include "render.cpp"
#include "terrain.cpp"
#include "collision_world.cpp"
#include "collision_world_ray_intersection.cpp"
#include "collision_world_debug.cpp"
#include "entity_world.cpp"
#include "ui.cpp"
#include "debug_printing_system.cpp"
#include "profiler.cpp"
#include "inventory.cpp"
#include "scatter_system.cpp"
#include "game_entities.cpp"
#include "ai_sensing.cpp"
#include "ai_decisioning.cpp"
#include "ai_action_planning.cpp"
#include "ai_controller.cpp"
#include "game_ui.cpp"
#include "game_player.cpp" // TODO: Eventually remove this
#include "ai.cpp"
#include "command_system.cpp"
#include "debug_entity.cpp"
#include "particle_system.cpp"
#include "game_systems.cpp"
#include "game_new_entities.cpp"
#include "console.cpp"
#include "game_deployable.cpp"
#include "bullet_system.cpp"
#include "game_events.cpp"
#include "skeletal_animation.cpp"
#include "grass_system.cpp"
#include "game_tests.cpp"

#define FAR_CLIP_DIST 8000.0f

internal void UpdateCamera(Camera *camera, InputState *input, f32 dt,
    DEBUGShowMouseCursorFunction *debugShowMouseCursor, b32 receiveInput)
{
    f32 speed = 4.0f;
    vec3 velocity = {};
    if (receiveInput)
    {
        if (input->buttonStates[KEY_W].isDown)
        {
            velocity.z = -1.0f;
        }
        if (input->buttonStates[KEY_S].isDown)
        {
            velocity.z = 1.0f;
        }
        if (input->buttonStates[KEY_A].isDown)
        {
            velocity.x = -1.0f;
        }
        if (input->buttonStates[KEY_D].isDown)
        {
            velocity.x = 1.0f;
        }

        if (input->buttonStates[KEY_LEFT_SHIFT].isDown)
        {
            speed = 300.0f;
        }
    }

    f32 sens = 0.01f;
    f32 mouseX = -input->mouseRelPosY * sens;
    f32 mouseY = -input->mouseRelPosX * sens;

    vec3 newRotation = Vec3(mouseX, mouseY, 0.0f);

    if (input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown && receiveInput)
    {
        camera->rotation += newRotation;
        debugShowMouseCursor(false);
    }
    else
    {
        debugShowMouseCursor(true);
    }

    camera->rotation.x = Clamp(camera->rotation.x, -PI * 0.5f, PI * 0.5f);

    if (camera->rotation.y > 2.0f * PI)
    {
        camera->rotation.y -= 2.0f * PI;
    }
    if (camera->rotation.y < 2.0f * -PI)
    {
        camera->rotation.y += 2.0f * PI;
    }

    mat4 rotationMatrix =
        RotateY(camera->rotation.y) * RotateX(camera->rotation.x);
    vec3 rotatedVelocity = (rotationMatrix * Vec4(velocity, 0)).xyz;
    camera->position += rotatedVelocity * speed * dt;

    camera->viewMatrix = RotateX(-camera->rotation.x) *
                         RotateY(-camera->rotation.y) *
                         Translate(-camera->position);

    f32 aspect = (f32)input->framebufferWidth / (f32)input->framebufferHeight;
    camera->projectionMatrix = Perspective(70.0f, aspect, 0.01f, FAR_CLIP_DIST);
}

// WARNING: We store const char *, need to reset this on code reload
internal void RegisterItems(ItemDatabase *itemDatabase)
{
    RegisterItem(itemDatabase, Item_Wood, "Wood", Texture_ItemIcon_Wood, 200);
    RegisterItem(itemDatabase, Item_Meat, "Meat", Texture_ItemIcon_Meat, 10);
    RegisterItem(
        itemDatabase, Item_Stone, "Stone", Texture_ItemIcon_Stone, 100);
    RegisterItem(itemDatabase, Item_MetalOre, "Metal Ore",
        HashStringU32("textures/item_icons/metal_ore"), 60);
    RegisterItem(itemDatabase, Item_MetalIngot, "Metal Ingot",
        HashStringU32("textures/item_icons/metal_ingot"), 20);
    RegisterItem(itemDatabase, Item_Hatchet, "Hatchet",
        Texture_ItemIcon_Hatchet, 1, ItemFlag_Tool);
    RegisterItem(itemDatabase, Item_PickAxe, "Pick Axe",
        Texture_ItemIcon_Hatchet, 1, ItemFlag_Tool, Vec4(1, 0, 1, 1));
    RegisterItem(
        itemDatabase, Item_Hammer, "Hammer", Texture_ItemIcon_Hammer, 1);
    RegisterItem(itemDatabase, Item_FirePit, "Fire Pit", Texture_Missing, 1,
        ItemFlag_Deployable);
    RegisterItem(itemDatabase, Item_CookedMeat, "Cooked Meat",
        Texture_ItemIcon_CookedMeat, 10);
    RegisterItem(itemDatabase, Item_LumberCamp, "Lumber Camp",
        Texture_ItemIcon_Hammer, 1, ItemFlag_Deployable);
    RegisterItem(itemDatabase, Item_CraftingBench, "Crafting Bench",
        Texture_ItemIcon_Hammer, 1, ItemFlag_Deployable);
    RegisterItem(itemDatabase, Item_WoodenBow, "Wooden Bow",
        Texture_ItemIcon_WoodenBow, 1);
    RegisterItem(itemDatabase, Item_WoodenArrow, "Wooden Arrow",
        Texture_ItemIcon_WoodenArrow, 30);
    RegisterItem(
        itemDatabase, Item_Carrot, "Carrot", Texture_ItemIcon_Carrot, 10);
    RegisterItem(itemDatabase, Item_WoodenSpear, "Wooden Spear",
        Texture_ItemIcon_WoodenSpear, 1);
    RegisterItem(itemDatabase, HashStringU32("item_wooden_foundation"),
        "Wooden Foundation", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_wooden_wall"), "Wooden Wall",
        Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_wooden_doorway"),
        "Wooden Doorway", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_wooden_ceiling"),
        "Wooden Ceiling", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_wooden_stairs"),
        "Wooden Stairs", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_wooden_window"),
        "Wooden Window", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_wooden_storage_box"),
        "Wooden Storage Box", Texture_ItemIcon_Hammer, 1, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_friendly_ai_camp"),
        "Friendly AI Camp", Texture_ItemIcon_WoodenArrow, 1,
        ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_furnace"), "Furnace",
        Texture_ItemIcon_Hammer, 1, ItemFlag_Deployable);
    RegisterItem(itemDatabase, Item_TestGun, "Test Gun",
        HashStringU32("textures/item_icons/test_gun"), 1, ItemFlag_None);

    RegisterItem(itemDatabase, HashStringU32("item_concrete_foundation"),
        "Concrete Foundation", Texture_ItemIcon_Hammer, 32,
        ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_concrete_wall"),
        "Concrete Wall", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_concrete_doorway"),
        "Concrete Doorway", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_concrete_ceiling"),
        "Concrete Ceiling", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_concrete_stairs"),
        "Concrete Stairs", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
    RegisterItem(itemDatabase, HashStringU32("item_concrete_window"),
        "Concrete Window", Texture_ItemIcon_Hammer, 32, ItemFlag_Deployable);
}

// WARNING: We store const char *, need to reset this on code reload
internal void RegisterRecipes(ItemDatabase *itemDatabase)
{
    {
        Recipe recipe = CreateRecipe("Hatchet", Item_Hatchet);
        AddRecipeInput(&recipe, Item_Wood, 1);
        AddRecipeInput(&recipe, Item_Stone, 1);
        RegisterRecipe(itemDatabase, Recipe_Hatchet, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Pick Axe", Item_PickAxe);
        AddRecipeInput(&recipe, Item_Wood, 2);
        AddRecipeInput(&recipe, Item_Stone, 5);
        RegisterRecipe(itemDatabase, Recipe_PickAxe, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Hammer", Item_Hammer);
        AddRecipeInput(&recipe, Item_Wood, 5);
        RegisterRecipe(itemDatabase, Recipe_Hammer, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Fire Pit", Item_FirePit);
        AddRecipeInput(&recipe, Item_Wood, 10);
        RegisterRecipe(itemDatabase, Recipe_FirePit, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Furnace", HashStringU32("item_furnace"));
        AddRecipeInput(&recipe, Item_Stone, 30);
        AddRecipeInput(&recipe, Item_Wood, 40);
        RegisterRecipe(itemDatabase, HashStringU32("recipe_furnace"), recipe);
    }
    {
        Recipe recipe = CreateRecipe("Metal Ingot", Item_MetalIngot);
        AddRecipeInput(&recipe, Item_Wood, 5);
        AddRecipeInput(&recipe, Item_MetalOre, 1);
        RegisterRecipe(itemDatabase, Recipe_MetalIngot, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Lumber Camp", Item_LumberCamp);
        AddRecipeInput(&recipe, Item_Wood, 10);
        RegisterRecipe(itemDatabase, Recipe_LumberCamp, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Crafting Bench", Item_CraftingBench);
        AddRecipeInput(&recipe, Item_Wood, 10);
        RegisterRecipe(itemDatabase, Recipe_CraftingBench, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Wooden Bow", Item_WoodenBow);
        AddRecipeInput(&recipe, Item_Wood, 10);
        recipe.requiresCraftingStation = true;
        RegisterRecipe(itemDatabase, Recipe_WoodenBow, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Wooden Arrow", Item_WoodenArrow, 5);
        AddRecipeInput(&recipe, Item_Wood, 5);
        RegisterRecipe(itemDatabase, Recipe_WoodenArrow, recipe);
    }
    {
        Recipe recipe = CreateRecipe("Wooden Spear", Item_WoodenSpear);
        AddRecipeInput(&recipe, Item_Wood, 10);
        RegisterRecipe(itemDatabase, Recipe_WoodenSpear, recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Wooden Foundation", HashStringU32("item_wooden_foundation"));
        AddRecipeInput(&recipe, Item_Wood, 10);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_wooden_foundation"), recipe);
    }
    {
        Recipe recipe =
            CreateRecipe("Wooden Wall", HashStringU32("item_wooden_wall"));
        AddRecipeInput(&recipe, Item_Wood, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_wooden_wall"), recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Wooden Doorway", HashStringU32("item_wooden_doorway"));
        AddRecipeInput(&recipe, Item_Wood, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_wooden_doorway"), recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Wooden Ceiling", HashStringU32("item_wooden_ceiling"));
        AddRecipeInput(&recipe, Item_Wood, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_wooden_ceiling"), recipe);
    }
    {
        Recipe recipe =
            CreateRecipe("Wooden Stairs", HashStringU32("item_wooden_stairs"));
        AddRecipeInput(&recipe, Item_Wood, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_wooden_stairs"), recipe);
    }
    {
        Recipe recipe =
            CreateRecipe("Wooden Window", HashStringU32("item_wooden_window"));
        AddRecipeInput(&recipe, Item_Wood, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_wooden_window"), recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Wooden Storage Box", HashStringU32("item_wooden_storage_box"));
        AddRecipeInput(&recipe, Item_Wood, 20);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_wooden_storage_box"), recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Friendly AI Camp", HashStringU32("item_friendly_ai_camp"), 1);
        AddRecipeInput(&recipe, Item_Wood, 30);
        AddRecipeInput(&recipe, Item_WoodenSpear, 3);
        AddRecipeInput(&recipe, Item_Carrot, 3);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_friendly_ai_camp"), recipe);
    }

    {
        Recipe recipe = CreateRecipe(
            "Concrete Foundation", HashStringU32("item_concrete_foundation"));
        AddRecipeInput(&recipe, Item_Stone, 10);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_concrete_foundation"), recipe);
    }
    {
        Recipe recipe =
            CreateRecipe("Concrete Wall", HashStringU32("item_concrete_wall"));
        AddRecipeInput(&recipe, Item_Stone, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_concrete_wall"), recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Concrete Doorway", HashStringU32("item_concrete_doorway"));
        AddRecipeInput(&recipe, Item_Stone, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_concrete_doorway"), recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Concrete Ceiling", HashStringU32("item_concrete_ceiling"));
        AddRecipeInput(&recipe, Item_Stone, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_concrete_ceiling"), recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Concrete Stairs", HashStringU32("item_concrete_stairs"));
        AddRecipeInput(&recipe, Item_Stone, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_concrete_stairs"), recipe);
    }
    {
        Recipe recipe = CreateRecipe(
            "Concrete Window", HashStringU32("item_concrete_window"));
        AddRecipeInput(&recipe, Item_Stone, 5);
        RegisterRecipe(
            itemDatabase, HashStringU32("recipe_concrete_window"), recipe);
    }
}

internal void RegisterDeployableItemData(DeployableItemDataTable *table)
{
    RegisterDeployableItemData(table, HashStringU32("item_wooden_foundation"),
        "entity_wooden_foundation");
    RegisterDeployableItemData(
        table, HashStringU32("item_wooden_wall"), "entity_wooden_wall");
    RegisterDeployableItemData(
        table, HashStringU32("item_wooden_doorway"), "entity_wooden_doorway");
    RegisterDeployableItemData(
        table, HashStringU32("item_wooden_ceiling"), "entity_wooden_ceiling");
    RegisterDeployableItemData(
        table, HashStringU32("item_wooden_stairs"), "entity_wooden_stairs");
    RegisterDeployableItemData(
        table, HashStringU32("item_wooden_window"), "entity_wooden_window");
    RegisterDeployableItemData(table, HashStringU32("item_wooden_storage_box"),
        "entity_wooden_storage_box");
    RegisterDeployableItemData(
        table, Item_CraftingBench, "entity_crafting_bench");
    RegisterDeployableItemData(table, Item_FirePit, "entity_firepit");
    RegisterDeployableItemData(table, Item_LumberCamp, "entity_lumber_camp");
    RegisterDeployableItemData(
        table, Item_FriendlyAiCamp, "entity_friendly_camp");

    RegisterDeployableItemData(table, HashStringU32("item_concrete_foundation"),
        "entity_concrete_foundation");
    RegisterDeployableItemData(
        table, HashStringU32("item_concrete_wall"), "entity_concrete_wall");
    RegisterDeployableItemData(table, HashStringU32("item_concrete_doorway"),
        "entity_concrete_doorway");
    RegisterDeployableItemData(table, HashStringU32("item_concrete_ceiling"),
        "entity_concrete_ceiling");
    RegisterDeployableItemData(
        table, HashStringU32("item_concrete_stairs"), "entity_concrete_stairs");
    RegisterDeployableItemData(
        table, HashStringU32("item_concrete_window"), "entity_concrete_window");

    RegisterDeployableItemData(
        table, HashStringU32("item_furnace"), "entity_furnace");
}

internal void RegisterParticleSpecs(ParticleSystem *particleSystem)
{
    {
        ParticleSpec spec = {};
        spec.flags =
            ParticleFlags_Continuous | ParticleFlags_UpdateScaleOverTime;
        spec.mesh = Mesh_Plane;
        spec.scale[0] = 4.0f;
        spec.scale[1] = 4.0f;
        spec.acceleration = Vec3(0.0f, 0.5f, 0.0f);
        spec.initialVelocity[0] = Vec3(-0.1f);
        spec.initialVelocity[1] = Vec3(0.1f);
        spec.initialRotationalVelocity[0] = Vec3(0, 0, -0.8f);
        spec.initialRotationalVelocity[1] = Vec3(0, 0, 0.8f);
        RegisterParticleSpec(
            particleSystem, spec, HashStringU32("particles/smoke"));
    }
    {
        ParticleSpec spec = {};
        spec.flags = ParticleFlags_Mesh;
        spec.mesh = HashStringU32("meshes/rock01");
        spec.scale[0] = 0.005f;
        spec.scale[1] = 0.08f;
        spec.acceleration = Vec3(0.0f, -10.0f, 0.0f);
        spec.initialVelocity[0] = Vec3(-3.0f);
        spec.initialVelocity[1] = Vec3(3.0f);
        spec.initialRotationalVelocity[0] = Vec3(-10);
        spec.initialRotationalVelocity[1] = Vec3(10);
        RegisterParticleSpec(
            particleSystem, spec, HashStringU32("particles/smoke_one_shot"));
    }
    {
        ParticleSpec spec = {};
        spec.flags = ParticleFlags_Mesh;
        spec.mesh = HashStringU32("meshes/splinter");
        spec.scale[0] = 0.2f;
        spec.scale[1] = 0.3f;
        spec.acceleration = Vec3(0.0f, -10.0f, 0.0f);
        spec.initialVelocity[0] = Vec3(-3.0f);
        spec.initialVelocity[1] = Vec3(3.0f);
        spec.initialRotationalVelocity[0] = Vec3(-10);
        spec.initialRotationalVelocity[1] = Vec3(10);
        RegisterParticleSpec(
            particleSystem, spec, HashStringU32("particles/splinters"));
    }
}

internal void ProcessInventoryCommand(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, ItemDatabase *itemDatabase,
    InventoryCommand command)
{
    switch (command.type)
    {
    case InventoryCommandType_CraftItem:
        CraftItem(entityWorld, inventorySystem, itemDatabase,
            command.srcContainer, command.recipeId);
        break;
    case InventoryCommandType_TransferItem:
        ProcessTransferItemCommand(
            entityWorld, itemDatabase, inventorySystem, command);
        break;
    default:
        LOG_MSG("Unknown inventory command type %u", command.type);
        break;
    }
}

internal void UpdateTrees(EntityWorld *entityWorld,
    ScatterSystem *scatterSystem, RandomNumberGenerator *rng, f32 dt)
{
    for (u32 i = 0; i < entityWorld->entities.count; i++)
    {
        u32 entityId = entityWorld->entities.keys[i];
        Entity *entity = FindEntityById(entityWorld, entityId);
        ASSERT(entity != NULL);
        if (entity->flags & EntityFlags_Sapling)
        {
            if (entity->timeUntilMatured > 0.0f)
            {
                entity->timeUntilMatured -= dt;
                entity->scale += entity->scale * 0.01f * dt;
            }
            else
            {
                QueueEntityForDeletion(entityWorld, entityId);
                ScatterSpec *spec =
                    FindScatterSpecByName(scatterSystem, "pine_trees");
                if (spec != NULL)
                {
                    EntityDefinition entityDef = {};
                    if (PickRandomEntityDefinition(
                            &entityDef, entityWorld, spec, rng))
                    {
                        PUSH_VALUE(&entityDef, position, entity->position);
                        PUSH_VALUE(&entityDef, rotation, entity->rotation);

                        f32 scale = spec->maxScale > 0.0f
                                        ? Lerp(spec->minScale, spec->maxScale,
                                              RandomUnilateral(rng))
                                        : 1.0f;
                        PUSH_VALUE(&entityDef, scale, Vec3(scale));

                        u32 entityId =
                            CreateEntityFromDefinition(entityWorld, &entityDef);
                        if (entityId == NULL_ENTITY_ID)
                        {
                            LOG_MSG("Failed to create entity from definition");
                        }
                    }
                }
                else
                {
                    LOG_MSG("Unable to find scatter spec for \"pine_trees\".");
                }
            }
        }
    }
}

internal void GameFixedUpdate(GameState *gameState, AssetSystem *assetSystem,
    MemoryArena *tempArena, f32 fixedTimestep)
{
    PROF_SCOPE();
    if (!gameState->isInitialized)
    {
        // Don't update if we're not initialized
        return;
    }

    EntityWorld *entityWorld = &gameState->entityWorld;
    CollisionWorld *collisionWorld = &gameState->collisionWorld;
    ItemDatabase *itemDatabase = &gameState->itemDatabase;
    PlayerUIState *playerUIState = &gameState->playerUIState;
    GameEventQueue *eventQueue = &gameState->eventQueue;
    InventorySystem *inventorySystem = &gameState->inventorySystem;
    ParticleSystem *particleSystem = &gameState->particleSystem;
    CVarSystem *cvarSystem = &gameState->cvarSystem;

    gameState->timeUntilNextSkyUpdate -= fixedTimestep;

    u32 worldTimeIncrement = 1.0f;
    GetCVarU32(cvarSystem, "time_of_day_increment", &worldTimeIncrement);

    gameState->worldTimeSeconds += worldTimeIncrement;
    if (gameState->worldTimeSeconds > 3600 * 24)
    {
        gameState->worldTimeSeconds -= 3600 * 24;
    }

    CreateMissingInventoriesSystem(entityWorld, inventorySystem);

    SyncEntityWorldWithCollisionWorld(
        entityWorld, collisionWorld, assetSystem, tempArena);

    ParticleEmitterSystem(entityWorld, particleSystem);

    // TODO: Which RNG should we be using here?
    UpdateTrees(entityWorld, &gameState->scatterSystem,
        &gameState->scatterSystem.rng, fixedTimestep);

    u32 enableBulletDebugDraw = 0;
    GetCVarU32(cvarSystem, "debug_draw_bullets", &enableBulletDebugDraw);
    UpdateBullets(&gameState->bulletSystem, entityWorld, collisionWorld,
        eventQueue, fixedTimestep, enableBulletDebugDraw);

    UpdatePlayer(entityWorld, GetPlayerEntityId(entityWorld), collisionWorld,
        eventQueue, itemDatabase, inventorySystem, gameState->playerCommands,
        gameState->playerCommandCount);
    gameState->playerCommandCount = 0;

    // Process player inventory commands
    for (u32 i = 0; i < playerUIState->commandCount; i++)
    {
        ProcessInventoryCommand(entityWorld, inventorySystem, itemDatabase,
            playerUIState->commands[i]);
    }
    playerUIState->commandCount = 0;

    UpdateEntityWorldTransforms(entityWorld);

    UpdateParticles(particleSystem, fixedTimestep);

    ProcessEventQueue(gameState);

    gameState->lockCameraRotation = playerUIState->showInventory;

    ProcessEntitiesScheduledForDeletion(entityWorld, fixedTimestep);

    CleanUpCollisionObjectsSystem(entityWorld, collisionWorld);
    CleanUpParticleEmitters(entityWorld, particleSystem);

    ProcessEntityDeletionQueue(entityWorld);
}

internal void DrawProfilerUI(ProfilingSystem *profilingSystem,
    RenderData *renderer, TextBuffer *textBuffer, vec2 position,
    CachedProfilerState *cachedProfilerState, i32 readIndex)
{
    PROF_SCOPE();

    if (cachedProfilerState->update)
    {
        cachedProfilerState->blockCount = prof_ProcessEvents(profilingSystem,
            readIndex, cachedProfilerState->blocks,
            ARRAY_LENGTH(cachedProfilerState->blocks));
    }

    rect2 panelRect = Rect2(position, position + Vec2(1040, -600));
    vec4 panelColor = Vec4(0, 0, 0, 0.6f);
    DrawQuad(textBuffer, renderer->assetSystem, panelRect, panelColor, -0.1f);

    Font *font = GetFont(renderer->assetSystem, Font_FpsDisplay);
    if (font != NULL)
    {
        f32 yOffset = font->lineSpacing + 2.0f;
        char buffer[256];
        snprintf(buffer, sizeof(buffer), "%35s | %20s | %10s | %10s | %10s",
            "FUNCTION", "ANNOTATION", "CYCLE COUNT", "CALL COUNT",
            "AVG CYCLES");
        vec2 offset = Vec2(0.0f, -yOffset);
        DrawText(textBuffer, *font, buffer, position + offset, Vec4(1), 0.0f);
        position.y -= yOffset;

        for (u32 i = 0; i < cachedProfilerState->blockCount; i++)
        {
            ProfilingMetaData *metaData =
                prof_FindMetaData(&profilingSystem->metaData,
                    cachedProfilerState->blocks[i].metaDataGuid);
            ASSERT(metaData != NULL);

            snprintf(buffer, sizeof(buffer),
                "%35s | %20s | %8lu K | %10u | %8lu K", metaData->function,
                metaData->annotation,
                cachedProfilerState->blocks[i].cyclesElapsed / 1000,
                cachedProfilerState->blocks[i].callCount,
                (cachedProfilerState->blocks[i].cyclesElapsed /
                    cachedProfilerState->blocks[i].callCount) /
                    1000);

            offset = Vec2(0.0f, -yOffset);
            DrawText(
                textBuffer, *font, buffer, position + offset, Vec4(1), 0.0f);
            position.y -= yOffset;
        }
    }
}

internal void DrawPlayerHintText(TextBuffer *textBuffer,
    AssetSystem *assetSystem, PlayerUIState *playerUIState, vec2 center)
{
    if (playerUIState->showOnScreenHint)
    {
        Font *font = GetFont(assetSystem, Font_FpsDisplay);
        if (font != NULL)
        {
            f32 width = CalculateTextWidth(*font, playerUIState->hintText);
            vec4 color = Vec4(1);
            DrawText(textBuffer, *font, playerUIState->hintText,
                center + Vec2(-width * 0.5f, 0.0f), color, 0.0f);
        }
    }
}

inline f32 NormalizeWorldTime(u32 worldTime)
{
    // Map from 24 hour time to normalized
    f32 t = (f32)((f64)worldTime / (24.0 * 3600.0));
    return t;
}

internal quat ComputeSunLightRotation(f32 t)
{
    quat result = Quat(Vec3(1, 0, 0), (0.5f + 2.0f * t) * PI) *
                  Quat(Vec3(0, 1, 0), PI * -0.05);
    return result;
}

internal void UpdateSunLightForTimeOfDay(SunLight *sunLight, u32 worldTime)
{
    f32 t = NormalizeWorldTime(worldTime);

    sunLight->rotation = ComputeSunLightRotation(t);

    f32 sunT = Max(0.0f, 1.0f - Pow(t - 0.5f, 2.0f) * 4.0f - 0.4f);
    sunLight->color = Vec3(1, 1, 0.98) * 10.0f * sunT;
}

internal void UpdateSky(
    GameState *gameState, RenderData *renderer, vec3 sunDirection)
{
    AssetSystem *assetSystem = renderer->assetSystem;

    if (gameState->timeUntilNextSkyUpdate <= 0.0f)
    {
        u32 cubeMapWidth = 64;
        GetCVarU32(
            &gameState->cvarSystem, "r_skybox_resolution", &cubeMapWidth);

        f32 turbidity = 3.2f;
        GetCVarF32(&gameState->cvarSystem, "r_sky_turbidity", &turbidity);

        f32 sampleIncrement = 0.01f;
        GetCVarF32(&gameState->cvarSystem, "r_irradiance_sample_increment",
            &sampleIncrement);

        u32 irradianceResolution = 1;
        GetCVarU32(&gameState->cvarSystem, "r_irradiance_resolution",
            &irradianceResolution);

        u8 hashInput[sizeof(vec3) + sizeof(u32) + sizeof(f32)];
        memcpy(hashInput, sunDirection.data, sizeof(sunDirection));
        memcpy(hashInput + sizeof(sunDirection), &cubeMapWidth,
            sizeof(cubeMapWidth));
        memcpy(hashInput + sizeof(sunDirection) + sizeof(cubeMapWidth),
            &turbidity, sizeof(turbidity));
        u32 hash = HashU32(hashInput, sizeof(hashInput));
        if (hash != gameState->skyHash)
        {
            if (GenerateSkyTexture(
                    assetSystem, cubeMapWidth, sunDirection, turbidity))
            {
                gameState->skyHash = hash;
                GenerateSkyIrradianceCubeMap(
                    assetSystem, sampleIncrement, irradianceResolution);
            }
        }
        gameState->timeUntilNextSkyUpdate = 1.6f;
    }
}

internal void DrawDebugHUD(GameState *gameState, GameMemory *memory,
    RenderData *renderer, f32 framebufferWidth, f32 framebufferHeight,
    DebugDrawingBuffer debugDrawingBuffer, TextBuffer *textBuffer)
{
    EntityWorld *entityWorld = &gameState->entityWorld;
    CollisionWorld *collisionWorld = &gameState->collisionWorld;
    AssetSystem *assetSystem = renderer->assetSystem;
    DebugDrawingSystem *debugDrawingSystem = &gameState->debugDrawingSystem;
    GrassSystem *grassSystem = &gameState->grassSystem;
    ParticleSystem *particleSystem = &gameState->particleSystem;

    DebugPrintSystem debugPrintSystem = {};

    // Draw FPS and frametime display
    vec2 topRight = Vec2(framebufferWidth - 10, framebufferHeight - 10);

    // TODO: What do we DebugPrint if player is dead?
    u32 playerEntityId = GetPlayerEntityId(entityWorld);
    vec3 playerPosition = GetEntityPosition(entityWorld, playerEntityId);

    f32 averageFrameTime = GetAverage(&gameState->frameTimeAverage);
    f32 averageGpuFrameTime = GetAverage(&gameState->gpuTimeAverage);

    ProfilingSystem *profiler = memory->profilingSystem;
    u32 profilerReadIndex = 0;
    GetCVarU32(&gameState->cvarSystem, "prof_read_index", &profilerReadIndex);
    profilerReadIndex = MinU32(profilerReadIndex, profiler->bufferCount - 1);

    DebugPrintf(&debugPrintSystem, Vec4(1), "FPS: %0.3g (%0.4g ms)",
        1.0f / averageFrameTime, averageFrameTime * 1000.0f);
    DebugPrintf(
        &debugPrintSystem, Vec4(1), "GPU Time: %0.4g ms", averageGpuFrameTime);
    DebugPrintVec3(&debugPrintSystem, Vec4(1), "position", playerPosition);
    DebugPrintCapacity(&debugPrintSystem, "DEBUG SHAPES",
        debugDrawingSystem->shapes.count, debugDrawingSystem->shapes.capacity);
    DebugPrintCapacity(&debugPrintSystem, "DEBUG VERTICES",
        gameState->debugVerticesDrawCount, debugDrawingBuffer.max);

    DebugPrintCapacity(&debugPrintSystem, "ASSET LOAD QUEUE",
        assetSystem->loadQueueLength, ARRAY_LENGTH(assetSystem->loadQueue));
    DebugPrintCapacity(&debugPrintSystem, "ASSET SYSTEM ARENA",
        assetSystem->arena.size, assetSystem->arena.capacity);

    DebugPrintCapacity(&debugPrintSystem, "COLLISION MESH DATA ARENA",
        collisionWorld->meshDataArena.size,
        collisionWorld->meshDataArena.capacity);
    DebugPrintCapacity(&debugPrintSystem, "COLLISION OBJECTS",
        collisionWorld->objects.count, collisionWorld->objects.capacity);
    DebugPrintCapacity(&debugPrintSystem, "COLLISION MESHES",
        collisionWorld->meshes.count, collisionWorld->meshes.capacity);

    ASSERT(profilerReadIndex < profiler->bufferCount);
    DebugPrintCapacity(&debugPrintSystem, "PROFILER EVENTS",
        profiler->buffers[profilerReadIndex].count,
        profiler->buffers[profilerReadIndex].capacity);
    DebugPrintCapacity(&debugPrintSystem, "PROFILER METADATA",
        profiler->metaData.count, profiler->metaData.capacity);

    DebugPrintCapacity(&debugPrintSystem, "ENTITY WORLD ARENA",
        entityWorld->arena.size, entityWorld->arena.capacity);
    DebugPrintCapacity(&debugPrintSystem, "ENTITIES",
        entityWorld->entities.count, entityWorld->entities.capacity);
    DebugPrintCapacity(&debugPrintSystem, "ENTITY WORLD DELETION QUEUE",
        entityWorld->deletionQueueLength, entityWorld->deletionQueueCapacity);
    DebugPrintCapacity(&debugPrintSystem, "GRASS INSTANCE COUNT",
        grassSystem->count, grassSystem->capacity);

    DebugPrintCapacity(&debugPrintSystem, "PARTICLE BUFFER COUNT",
        particleSystem->buffers.count, particleSystem->buffers.capacity);

    DebugPrintDrawStuff(&debugPrintSystem, renderer, textBuffer, topRight);
}

internal void DrawSkeletonPose(SkeletonPose *pose, mat4 transform,
    DebugDrawingBuffer *debugDrawingBuffer, MemoryArena *tempArena)
{
    SkeletonJointPose *globalPose = BuildGlobalPose(*pose, tempArena);
    for (u32 i = 0; i < pose->skeleton->count; ++i)
    {
        vec3 p = TransformPoint(globalPose[i].translation, transform);
        DrawPoint(debugDrawingBuffer, p, 0.25f, Vec3(0.8, 1, 0.6));
    }
}

internal void BuildMatrixPalette(
    Skeleton *skeleton, mat4 *globalMarices, mat4 *matrixPalette)
{
    for (u32 i = 0; i < skeleton->count; ++i)
    {
        matrixPalette[i] = skeleton->joints[i].invBindPose * globalMarices[i];
    }
}

internal void GameRender(GameState *gameState, GameMemory *memory,
    InputState *input, RenderData *renderer, f32 framebufferWidth,
    f32 framebufferHeight, MemoryArena *tempArena)
{
    PROF_SCOPE();
    EntityWorld *entityWorld = &gameState->entityWorld;
    CVarSystem *cvarSystem = &gameState->cvarSystem;
    CollisionWorld *collisionWorld = &gameState->collisionWorld;
    ItemDatabase *itemDatabase = &gameState->itemDatabase;
    InventorySystem *inventorySystem = &gameState->inventorySystem;
    AssetSystem *assetSystem = renderer->assetSystem;
    PlayerUIState *playerUIState = &gameState->playerUIState;
    DebugDrawingSystem *debugDrawingSystem = &gameState->debugDrawingSystem;

    DebugDrawingBuffer debugDrawingBuffer = {};
    debugDrawingBuffer.vertices = renderer->debugDrawVerticesBuffer.vertices;
    debugDrawingBuffer.max = renderer->debugDrawVerticesBuffer.max;

    TextBuffer textBuffer = {};
    textBuffer.vertices = renderer->textVerticesBuffer.vertices;
    textBuffer.max = renderer->textVerticesBuffer.max;
    textBuffer.drawCalls = ALLOCATE_ARRAY(tempArena, TextDrawCall, 1024);
    textBuffer.maxDrawCalls = 1024;

    // Update camera uniform buffer
    CameraUniformBuffer cameraUniformBufferData = {};

    PROF_BEGIN_BLOCK(query_gpu_time);
    u64 gpuTimeElapsedNanoSeconds = 0;
    glGetQueryObjectui64v(
        renderer->timeElapsedQueryObjects[renderer->readIndex],
        GL_QUERY_RESULT_NO_WAIT, &gpuTimeElapsedNanoSeconds);
    if (gpuTimeElapsedNanoSeconds > 0)
    {
        renderer->readIndex = (renderer->readIndex + 1) %
                              ARRAY_LENGTH(renderer->timeElapsedQueryObjects);
    }

    glBeginQuery(GL_TIME_ELAPSED,
        renderer->timeElapsedQueryObjects[renderer->writeIndex]);
    renderer->writeIndex = (renderer->writeIndex + 1) %
                           ARRAY_LENGTH(renderer->timeElapsedQueryObjects);
    PROF_END_BLOCK(query_gpu_time);

    SunLight *sunLight = GetSunLight(&gameState->entityWorld);
    SkyLight *skyLight = GetSkyLight(&gameState->entityWorld);

    UpdateSunLightForTimeOfDay(sunLight, gameState->worldTimeSeconds);

    vec3 sunDirection =
        Normalize(RotateVector(Vec3(0, 0, -1), sunLight->rotation));

    UpdateSky(gameState, renderer, sunDirection);

    u32 useDebugCamera = 0;
    GetCVarU32(cvarSystem, "use_debug_camera", &useDebugCamera);
    if (!useDebugCamera)
    {
        b32 showHealthText = playerUIState->showInventory;
        DrawPlayerHUD(&textBuffer, assetSystem, entityWorld, showHealthText);
        if (!playerUIState->showInventory)
        {
            DrawPlayerCrosshair(&textBuffer, assetSystem,
                Vec2(framebufferWidth * 0.5f, framebufferHeight * 0.5f));
            DrawPlayerItemBelt(&textBuffer, assetSystem, itemDatabase, input,
                entityWorld, inventorySystem);

            DrawPlayerHintText(&textBuffer, assetSystem, playerUIState,
                Vec2(framebufferWidth * 0.5f,
                    framebufferHeight * 0.5f - 200.0f));
        }
    }

    if (playerUIState->showInventory && !useDebugCamera)
    {
        DrawPlayerInventory(&textBuffer, assetSystem, itemDatabase, input,
            entityWorld, inventorySystem, playerUIState);
        if (playerUIState->containerEntityId == NULL_ENTITY_ID)
        {
            u32 playerEntityId = GetPlayerEntityId(entityWorld);
            b32 showCraftingStationRecipes =
                (playerUIState->craftingStationEntityId != NULL_ENTITY_ID);
            DrawPlayerCraftingMenu(&textBuffer, assetSystem, itemDatabase,
                input, playerUIState, playerEntityId,
                showCraftingStationRecipes);
        }
    }

    DebugDrawSunLight(sunLight, &debugDrawingBuffer);
    DebugDrawSkyLight(skyLight, &debugDrawingBuffer);

    if (IsCVarTrue(cvarSystem, "show_character_controllers"))
    {
        // TODO
    }

    if (IsCVarTrue(cvarSystem, "show_scatter_volumes"))
    {
        DebugDrawScatterVolumes(&gameState->scatterSystem, &debugDrawingBuffer);
    }

    if (IsCVarTrue(cvarSystem, "show_ai_plans"))
    {
        // TODO
    }

    // TODO: Better system for debug drawing textures
    u32 showTextureDebug = 0;
    GetCVarU32(cvarSystem, "show_texture_debug", &showTextureDebug);

    if (showTextureDebug > 0)
    {
        vec2 p = Vec2(100, 200);
        DrawTexturedQuad(&textBuffer, assetSystem, Rect2(p, p + Vec2(800)),
            showTextureDebug == 1 ? Texture_TerrainHeightMap
                                  : Texture_TerrainNormalMap,
            0.0f);
    }

#if 0
    RenderCommand drawSkinnedMeshCommand = {};
    {
        Asset_Skeleton asset = {};
        if (GetAssetData_(assetSystem, AssetType_Skeleton, Skeleton_Xbot,
                sizeof(asset), &asset))
        {
            if (asset.data != NULL && asset.bindPose != NULL)
            {
                mat4 transform = Translate(GAME_TEST_ORIGIN + Vec3(5, -5, 0)) *
                                 Scale(Vec3(0.05f));

                SkeletonPose pose = CreateSkeletonPose(asset.data, tempArena);
                pose.localPose[4].rotation *= Quat(Vec3(1, 0, 0), PI * 0.15f);
                SkeletonJointPose *globalPose =
                    BuildGlobalPose(pose, tempArena);
                mat4 *globalMat = BuildGlobalMatrices(pose, tempArena);

                for (u32 i = 0; i < 255; i++)
                {
                    cameraUniformBufferData.matrixPalette[i] = Identity();
                }

                BuildMatrixPalette(asset.data, globalMat,
                    cameraUniformBufferData.matrixPalette);

                DrawSkeletonPose(
                    asset.bindPose, transform, &debugDrawingBuffer, tempArena);

                drawSkinnedMeshCommand.modelMatrix = transform;
                drawSkinnedMeshCommand.material =
                    HashStringU32("materials/skinning");
                drawSkinnedMeshCommand.mesh = HashStringU32("meshes/xbot/0");
                drawSkinnedMeshCommand.cameraIndex = 0;
                drawSkinnedMeshCommand.shadowMapTexture = 0;
                drawSkinnedMeshCommand.irradianceCubeMap = GetTexture(
                    renderer->assetSystem, Texture_PreethamSky_Irradiance);
            }
        }
    }
#endif

    DrawDebugDrawingSystem(debugDrawingSystem, &debugDrawingBuffer);

    f32 dt = gameState->dt;
    Accumulate(&gameState->frameTimeAverage, dt);
    if (gpuTimeElapsedNanoSeconds > 0)
    {
        Accumulate(&gameState->gpuTimeAverage,
            (f64)gpuTimeElapsedNanoSeconds / (1000.0 * 1000.0));
    }

    PROF_BEGIN_BLOCK(debug_hud);
    if (IsCVarTrue(cvarSystem, "show_debug_hud"))
    {
        DrawDebugHUD(gameState, memory, renderer, framebufferWidth,
            framebufferHeight, debugDrawingBuffer, &textBuffer);
    }
    PROF_END_BLOCK(debug_hud);

    if (IsCVarTrue(cvarSystem, "show_profiler"))
    {
        u32 readIndex = 0;
        GetCVarU32(cvarSystem, "prof_read_index", &readIndex);
        DrawProfilerUI(memory->profilingSystem, renderer, &textBuffer,
            Vec2(20, 700), &gameState->cachedProfilerState, (i32)readIndex);
    }

    if (IsCVarTrue(cvarSystem, "show_console"))
    {
        vec2 topLeft = Vec2(0.0f, input->framebufferHeight);
        DrawConsole(&gameState->console, renderer, memory->logBuffer,
            &textBuffer, assetSystem, topLeft, input->framebufferWidth,
            input->framebufferHeight);
    }

    // Figure out which camera we're using
    mat4 cameraViewMatrix = {};
    mat4 cameraProjectionMatrix = {};
    vec3 cameraPosition = {};

    f32 aspect = (f32)framebufferWidth / (f32)framebufferHeight;
    if (useDebugCamera)
    {
        Camera *debugCamera = GetDebugCamera(&gameState->entityWorld);
        cameraViewMatrix = debugCamera->viewMatrix;
        cameraProjectionMatrix = debugCamera->projectionMatrix;
        cameraPosition = debugCamera->position;
    }
    else
    {
        u32 playerEntityId = GetPlayerEntityId(entityWorld);
        Entity *entity = FindEntityById(entityWorld, playerEntityId);
        if (entity != NULL)
        {
            vec3 headPosition = entity->position + g_PlayerHeadOffset;

            // Apply screenshake
            vec3 cameraEulerAngles = entity->player.cameraEulerAngles;
            if (entity->weaponController.screenShakeAmount > 0.0f)
            {
                cameraEulerAngles.x +=
                    entity->weaponController.screenShakeAmount * 0.2f *
                    PerlinImproved(20.0f * gameState->currentTime, 1.0f, 2.0f);
                cameraEulerAngles.y +=
                    entity->weaponController.screenShakeAmount * 0.2f *
                    PerlinImproved(
                        20.0f * gameState->currentTime, 16.0f, 62.0f);
            }

            cameraViewMatrix = RotateX(-cameraEulerAngles.x) *
                               RotateY(-cameraEulerAngles.y) *
                               Translate(-headPosition);
            cameraProjectionMatrix =
                Perspective(70.0f, aspect, 0.01f, FAR_CLIP_DIST);
            cameraPosition = headPosition;
        }
    }

    {
        u32 collisionWorldDebugDrawFlags = 0;
        GetCVarU32(&gameState->cvarSystem, "collision_debug_draw",
            &collisionWorldDebugDrawFlags);
        u32 collisionDebugDrawRegion = 0;
        GetCVarU32(&gameState->cvarSystem, "collision_debug_draw_region",
            &collisionDebugDrawRegion);

        vec3 p = Vec3(0);
        u32 playerEntityId = GetPlayerEntityId(entityWorld);
        if (playerEntityId != NULL_ENTITY_ID)
        {
            p = GetEntityPosition(entityWorld, playerEntityId);
        }

        vec3 min = p - Vec3(10);
        vec3 max = p + Vec3(10);
        if (collisionDebugDrawRegion)
        {
            DrawBox(&debugDrawingBuffer, min, max, Vec3(0, 1, 0));
        }

        DebugDrawCollisionWorld(&gameState->collisionWorld, &debugDrawingBuffer,
            min, max, collisionWorldDebugDrawFlags);
    }

#if 0
    if (memory->enableTerrain)
    {
    DebugDrawTerrainQuadTree(&gameState->terrain, &debugDrawingBuffer,
        Vec2(cameraPosition.x, cameraPosition.z));
    Aabb worldQueryAabb = {};
    worldQueryAabb.min = cameraPosition - Vec3(10);
    worldQueryAabb.max = cameraPosition + Vec3(10);
    DebugDrawQuadTreeTerrainCollision(&gameState->terrain, &debugDrawingBuffer,
        worldQueryAabb, CollisionWorldDebugDrawFlags_Triangles);
    }
#endif

    RenderCommandBuffer commandBuffer =
        AllocateRenderCommandBuffer(tempArena, 4096);

    // Default camera
    cameraUniformBufferData.viewMatrices[0] = cameraViewMatrix;
    cameraUniformBufferData.projectionMatrices[0] = cameraProjectionMatrix;

    // Shadow map camera
    f32 s = 50.0f;
    vec3 shadowCameraP = cameraPosition;
    cameraUniformBufferData.viewMatrices[1] =
        Rotate(Conjugate(sunLight->rotation)) * Translate(-shadowCameraP);
    cameraUniformBufferData.projectionMatrices[1] =
        Orthographic(-s, s, -s, s, -50.0, 50.0f);

    // UI Camera
    cameraUniformBufferData.viewMatrices[2] = Identity();
    cameraUniformBufferData.projectionMatrices[2] = Orthographic(
        0.0f, framebufferWidth, 0.0f, framebufferHeight, -1.0f, 1.0f);

    // View model camera
    cameraUniformBufferData.viewMatrices[3] = Identity();
    cameraUniformBufferData.projectionMatrices[3] =
        Perspective(60.0f, aspect, 0.01f, 100.0f);

    // FIXME: Handle floating point precision, how do we wrap?
    cameraUniformBufferData.t = gameState->currentTime;

    glBindBuffer(GL_UNIFORM_BUFFER, renderer->cameraUniformBuffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(cameraUniformBufferData),
        &cameraUniformBufferData);

    // Update lighting uniform buffer
    LightingUniformBuffer lightingUniformBuffer = {};
    lightingUniformBuffer.sunDirection = -sunDirection;
    lightingUniformBuffer.sunColor = sunLight->color;
    lightingUniformBuffer.skyColor = skyLight->color;
    lightingUniformBuffer.cameraPosition = cameraPosition;
    lightingUniformBuffer.fogColor = Vec3(0.4f, 0.6f, 0.9f);
    lightingUniformBuffer.irradianceCubeMapContribution = 0.6f;
    lightingUniformBuffer.fogMultiplier = 0.001f;
    GetCVarF32(&gameState->cvarSystem, "r_irradiance_contribution",
        &lightingUniformBuffer.irradianceCubeMapContribution);
    GetCVarF32(&gameState->cvarSystem, "r_fog_multiplier",
        &lightingUniformBuffer.fogMultiplier);

    glBindBuffer(GL_UNIFORM_BUFFER, renderer->lightingUniformBuffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(lightingUniformBuffer),
        &lightingUniformBuffer);

    // Update debug drawing vertices buffer
    glBindBuffer(
        GL_ARRAY_BUFFER, renderer->debugDrawVerticesBuffer.vertexBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, 0,
        sizeof(VertexPC) * debugDrawingBuffer.count,
        debugDrawingBuffer.vertices);

    // Update text vertices buffer
    glBindBuffer(GL_ARRAY_BUFFER, renderer->textVerticesBuffer.vertexBuffer);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(VertexText) * textBuffer.count,
        textBuffer.vertices);

    // Draw shadow map
    PROF_BEGIN_BLOCK(shadow_pass);
    glBindFramebuffer(
        GL_FRAMEBUFFER, renderer->shadowMapBuffer.framebufferObject);
    glViewport(0, 0, 2048, 2048);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE); // Enable depth write
    glClear(GL_DEPTH_BUFFER_BIT);

    // Shadow map pass
    DrawStaticMeshesSystem(entityWorld, renderer, 1,
        renderer->shadowMapBuffer.texture, &commandBuffer);
    ProcessRenderCommands(renderer, commandBuffer);
    PROF_END_BLOCK(shadow_pass);

    // Render scene to HDR buffer
    PROF_BEGIN_BLOCK(scene_pass);
    glBindFramebuffer(
        GL_FRAMEBUFFER, renderer->hdrFramebuffer.framebufferObject);
    glViewport(0, 0, (i32)framebufferWidth, (i32)framebufferHeight);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE); // Enable depth write
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.05, 0.0, 0.05, 1.0);

    f32 exposure = 1.0f;
    GetCVarF32(&gameState->cvarSystem, "r_skybox_exposure", &exposure);
    DrawSkybox(renderer, cameraPosition, exposure);

    // ScatterGrass(&gameState->grassSystem, &gameState->terrain,
    // cameraPosition); DrawGrass(&gameState->grassSystem, renderer,
    // cameraPosition);
    if (memory->enableTerrain)
    {
        DrawTerrainQuadTree(&gameState->terrain, renderer, 0, cameraPosition);
    }

    ClearRenderCommandBuffer(&commandBuffer);
    DrawStaticMeshesSystem(entityWorld, renderer, 0,
        renderer->shadowMapBuffer.texture, &commandBuffer);

#if 0
    {
         RenderCommand *command = AllocateRenderCommand(&commandBuffer);
        *command = drawSkinnedMeshCommand;
    }
#endif
    ProcessRenderCommands(renderer, commandBuffer);

    DrawParticles(&gameState->particleSystem, renderer, cameraPosition);

    // Draw player view model
    if (!useDebugCamera)
    {
        u32 playerEntityId = GetPlayerEntityId(entityWorld);
        Player player = GetEntityPlayerController(entityWorld, playerEntityId);
        WeaponController *weaponController =
            GetEntityWeaponController(entityWorld, playerEntityId);
        u32 activeBeltSlot = player.activeBeltSlot;
        u32 activeItemId = 0;
        if (GetItemIdInBeltSlot(entityWorld, inventorySystem, playerEntityId,
                activeBeltSlot, &activeItemId))
        {
            DrawViewModel(renderer, &gameState->eventQueue, playerEntityId,
                gameState->currentTime, activeItemId, weaponController);
        }
    }
    PROF_END_BLOCK(scene_pass);

    // Start Post processing
    PROF_BEGIN_BLOCK(post_processing_pass)
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, (i32)framebufferWidth, (i32)framebufferHeight);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE); // Enable depth write
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0, 0.0, 0.0, 1.0);

    // Display post processed image
    u32 postProcessingShader = GetShader(assetSystem, Shader_PostProcessing);
    if (postProcessingShader != 0)
    {
        glUseProgram(postProcessingShader);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, renderer->hdrFramebuffer.colorTexture);

        f32 exposure = 1.0f;
        GetCVarF32(&gameState->cvarSystem, "r_exposure", &exposure);
        glUniform1f(1, exposure);

        // NOTE: Vertex data is hard-coded in the shader
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
    PROF_END_BLOCK(post_processing_pass);

    // Draw debug drawing vertices
    PROF_BEGIN_BLOCK(debug_pass);
    u32 debugDrawingShader = GetShader(assetSystem, Shader_DebugDrawing);
    if (debugDrawingShader != 0)
    {
        glUseProgram(debugDrawingShader);
        glDisable(GL_DEPTH_TEST);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, renderer->cameraUniformBuffer);
        glBindVertexArray(renderer->debugDrawVerticesBuffer.vertexArrayObject);
        glDrawArrays(GL_LINES, 0, debugDrawingBuffer.count);
    }

    gameState->debugVerticesDrawCount = debugDrawingBuffer.count;
    PROF_END_BLOCK(debug_pass);

    // Draw text and UI quads
    PROF_BEGIN_BLOCK(ui_pass);
    for (u32 i = 0; i < textBuffer.drawCallCount; i++)
    {
        TextDrawCall drawCall = textBuffer.drawCalls[i];
        u32 shader = GetShader(assetSystem, drawCall.shaderAssetId);
        if (shader != 0)
        {
            glUseProgram(shader);
            // glDisable(GL_DEPTH_TEST);
            glEnable(GL_DEPTH_TEST);
            glBindBufferBase(
                GL_UNIFORM_BUFFER, 0, renderer->cameraUniformBuffer);
            glBindVertexArray(renderer->textVerticesBuffer.vertexArrayObject);

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, drawCall.texture);
            glUniform1i(0, 0);
            glUniform4fv(1, 1, drawCall.color.data);
            glUniform1f(2, drawCall.depth);
            glDrawArrays(GL_TRIANGLES, drawCall.offset, drawCall.count);
        }
    }
    PROF_END_BLOCK(ui_pass);

    PROF_BEGIN_BLOCK(gl_end_query);
    glEndQuery(GL_TIME_ELAPSED);
    PROF_END_BLOCK(gl_end_query);
}

internal void ExecuteCommands(GameState *gameState, MemoryArena *tempArena)
{
    EntityWorld *entityWorld = &gameState->entityWorld;

    CommandData buffer[MAX_COMMANDS_PER_FRAME];
    u32 count = ExecuteCommandsInQueue(
        &gameState->commandSystem, tempArena, buffer, ARRAY_LENGTH(buffer));
    for (u32 i = 0; i < count; i++)
    {
        CommandData cmd = buffer[i];
        if (strcmp(cmd.argv[0], "echo") == 0)
        {
            if (cmd.argc > 1)
            {
                LOG_MSG("%s", cmd.argv[1]);
            }
        }
        else if (strcmp(cmd.argv[0], "kill") == 0)
        {
            u32 playerEntityId = GetPlayerEntityId(entityWorld);
            Entity *playerEntity = FindEntityById(entityWorld, playerEntityId);
            if (playerEntity != NULL)
            {
                playerEntity->currentHealth = 0;
            }
        }
        else if (strcmp(cmd.argv[0], "respawn") == 0)
        {
            u32 playerEntityId = GetPlayerEntityId(entityWorld);
            if (playerEntityId == NULL_ENTITY_ID)
            {
                // Limit to only 1 player for now
                vec3 testP = Vec3(500, 50, 500);
                AddPlayer(entityWorld, testP + Vec3(0, 3, 5));
            }
        }
        else if (strcmp(cmd.argv[0], "spawnbot") == 0)
        {
            vec3 testP = Vec3(500, 50, 500);
            if (cmd.argc > 1)
            {
                if (strcmp(cmd.argv[1], "2") == 0)
                {
                    AddTestAI(entityWorld, testP + Vec3(0, 2, -4), 2);
                }
                else
                {
                    AddTestAI(entityWorld, testP + Vec3(0, 2, 4), 1);
                }
            }
            else
            {
                LOG_MSG("%s : Requires 1 argument", cmd.argv[0]);
            }
        }
        else if (strcmp(cmd.argv[0], "give") == 0)
        {
            if (cmd.argc > 1)
            {
                u32 itemId = HashStringU32(cmd.argv[1]);
                u32 quantity = 1;
                if (cmd.argc > 2)
                {
                    quantity = atol(cmd.argv[2]);
                }
                GiveItemToPlayer(&gameState->entityWorld,
                    &gameState->inventorySystem, &gameState->itemDatabase,
                    itemId, quantity);
            }
        }
        else if (strcmp(cmd.argv[0], "replay") == 0)
        {
            if (cmd.argc > 1)
            {
                gameState->debugEntityStateRecording.mode =
                    DebugEntityRecordingMode_Playback;
                gameState->debugEntityStateRecording.currentFrame =
                    atol(cmd.argv[1]);
            }
        }
        else if (strcmp(cmd.argv[0], "set") == 0)
        {
            if (cmd.argc > 2)
            {
                const char *name = cmd.argv[1];
                CVar *cvar = FindCVar(&gameState->cvarSystem, name);
                if (cvar != NULL)
                {
                    if (cvar->dataType == CVarDataType_U32)
                    {
                        u32 value = atol(cmd.argv[2]);
                        cvar->value.u = value;
                    }
                    else if (cvar->dataType == CVarDataType_F32)
                    {
                        f32 value = (f32)atof(cmd.argv[2]);
                        cvar->value.f = value;
                    }
                    else
                    {
                        INVALID_CODE_PATH("Unhandled CVar Data type");
                    }
                }
                else
                {
                    LOG_MSG("Cannot to find cvar \"%s\"", name);
                }
            }
        }
        else if (strcmp(cmd.argv[0], "dump_collision_meshes") == 0)
        {
            CollisionWorld *collisionWorld = &gameState->collisionWorld;
            for (u32 i = 0; i < collisionWorld->meshes.count; i++)
            {
                u32 key = collisionWorld->meshes.keys[i];
                CollisionMesh *mesh =
                    Dict_FindItem(&collisionWorld->meshes, CollisionMesh, key);
                if (mesh != NULL)
                {
                    LOG_MSG("%u - %u vertices %u indices %u AABBTree nodes",
                        key, mesh->vertexCount, mesh->indexCount,
                        mesh->aabbTree.count);
                }
            }
        }
        else if (strcmp(cmd.argv[0], "hashstring") == 0)
        {
            if (cmd.argc > 1)
            {
                u32 hash = HashStringU32(cmd.argv[1]);
                LOG_MSG("%u", hash);
            }
        }
        else if (strcmp(cmd.argv[0], "list_cvars") == 0)
        {
            CVarSystem *cvarSystem = &gameState->cvarSystem;
            for (u32 i = 0; i < cvarSystem->dict.count; i++)
            {
                u32 key = cvarSystem->dict.keys[i];
                CVar *cvar = Dict_FindItem(&cvarSystem->dict, CVar, key);
                ASSERT(cvar);
                LOG_MSG("%s - %u", cvar->name, cvar->value);
            }
        }
        else if (strcmp(cmd.argv[0], "move_debug_camera_to_entity") == 0)
        {
            if (cmd.argc > 1)
            {
                u32 entityId = atol(cmd.argv[1]);
                Entity *entity =
                    FindEntityById(&gameState->entityWorld, entityId);
                if (entity != NULL)
                {
                    Camera *debugCamera =
                        GetDebugCamera(&gameState->entityWorld);
                    debugCamera->position = entity->position;
                }
                else
                {
                    LOG_MSG("Unable to find entity with ID %u", entityId);
                }
            }
            else
            {
                LOG_MSG("Need to provide an entity ID to move debug camera to");
            }
        }
        else if (strcmp(cmd.argv[0], "set_time_of_day_hours") == 0)
        {
            if (cmd.argc > 1)
            {
                u32 hour = MinU32(24, atol(cmd.argv[1]));
                gameState->worldTimeSeconds = 3600 * hour;
            }
            else
            {
                LOG_MSG("Need to provide a value");
            }
        }
        else
        {
            LOG_MSG("%s : Unknown command", cmd.argv[0]);
        }
    }
}

internal void RegisterCVars(CVarSystem *cvarSystem)
{
    SetCVarU32(cvarSystem, "show_profiler", 0);
    SetCVarU32(cvarSystem, "prof_read_index", 0);
    SetCVarU32(cvarSystem, "collision_debug_draw", 0);
    SetCVarU32(cvarSystem, "collision_debug_draw_region", 0);
    SetCVarU32(cvarSystem, "show_character_controllers", 0);
    SetCVarU32(cvarSystem, "show_component_table_capacities", 0);
    SetCVarU32(cvarSystem, "show_console", 0);
    SetCVarU32(cvarSystem, "use_debug_camera", 1);
    SetCVarU32(cvarSystem, "show_scatter_volumes", 0);
    SetCVarU32(cvarSystem, "show_ai_plans", 0);
    SetCVarF32(cvarSystem, "r_exposure", 1.0f);
    SetCVarF32(cvarSystem, "r_skybox_exposure", 1.0f);
    SetCVarF32(cvarSystem, "r_irradiance_contribution", 0.6f);
    SetCVarU32(cvarSystem, "r_skybox_resolution", 64);
    SetCVarF32(cvarSystem, "r_sky_turbidity", 3.2f);
    SetCVarU32(cvarSystem, "time_of_day_increment", 1);
    SetCVarF32(cvarSystem, "r_irradiance_sample_increment", 0.1f);
    SetCVarU32(cvarSystem, "r_irradiance_resolution", 8);
    SetCVarF32(cvarSystem, "r_fog_multiplier", 0.0001f);
    SetCVarU32(cvarSystem, "show_debug_hud", 1);
    SetCVarU32(cvarSystem, "reset_world_on_reload", 0);
    SetCVarU32(cvarSystem, "show_texture_debug", 0);
    SetCVarU32(cvarSystem, "debug_draw_bullets", 1);
}

internal void GameInitialize(GameState *gameState, GameMemory *memory)
{
    AssetSystem *assetSystem = memory->renderData->assetSystem;

    void *persistentMemory =
        (u8 *)memory->persistentStorage + sizeof(GameState);
    umm persistentMemorySize =
        memory->persistentStorageSize - sizeof(GameState);

    InitializeMemoryArena(
        &gameState->persistentArena, persistentMemory, persistentMemorySize);

    // Initialize core systems
    // TODO: Don't use persistent arena
    InitializeCommandSystem(
        &gameState->commandSystem, &gameState->persistentArena);
    InitializeCVarSystem(
        &gameState->cvarSystem, &gameState->persistentArena, 256);
    RegisterCVars(&gameState->cvarSystem);

    InitializeDebugDrawingSystem(
        &gameState->debugDrawingSystem, &gameState->persistentArena);

    EntityWorldConfig entityWorldConfig = CreateDefaultEntityWorldConfig();
    InitializeEntityWorld(&gameState->entityWorld, &gameState->persistentArena,
        entityWorldConfig);

    gameState->debugEntityStateRecording.mode =
        DebugEntityRecordingMode_Recording;

    CollisionWorldConfig collisionWorldConfig = {};
    collisionWorldConfig.maxCollisionMeshes = 64;
    collisionWorldConfig.maxCollisionObjects = 512;
    collisionWorldConfig.meshDataStorageSize = Megabytes(8);
    InitializeCollisionWorld(&gameState->collisionWorld,
        &gameState->persistentArena, collisionWorldConfig);

    if (memory->enableTerrain)
    {
        InitializeTerrain(&gameState->terrain, &gameState->persistentArena);
        BuildTerrainQuadTree(&gameState->terrain);
        gameState->collisionWorld.terrain = &gameState->terrain;

        if (!memory->runAutomatedTests)
        {
            AddTerrainCollisionObject(&gameState->collisionWorld);
        }
    }

    InventorySystemConfig inventorySystemConfig = {};
    inventorySystemConfig.arenaSize = Kilobytes(128);
    inventorySystemConfig.maxInventories = 256;
    inventorySystemConfig.maxItems = 4096;
    InitializeInventorySystem(&gameState->inventorySystem,
        inventorySystemConfig, &gameState->persistentArena);

    InitializeItemDatabase(
        &gameState->itemDatabase, &gameState->persistentArena);
    RegisterItems(&gameState->itemDatabase);
    RegisterRecipes(&gameState->itemDatabase);

    RegisterEntityDefinitions(&gameState->entityWorld);
    RegisterDeployableItemData(&gameState->deployableItemDataTable);

    InitializeEventQueue(
        &gameState->eventQueue, &gameState->persistentArena, 64);

    InitializeParticleSystem(
        &gameState->particleSystem, &gameState->persistentArena, 64, 32);
    RegisterParticleSpecs(&gameState->particleSystem);

    // TODO: This should be transient arena
    InitializeGrassSystem(&gameState->grassSystem, &gameState->persistentArena,
        Megabytes(2), 32 * 1024);

    InitializeBulletSystem(
        &gameState->bulletSystem, &gameState->persistentArena, 512);

    gameState->worldGenRng.state = 0x7F12C81D;
    gameState->spawnRng.state = 0xC7BF3715;
    gameState->aiRng.state = 0x752C915F;

    gameState->worldTimeSeconds = 10 * 3600;

    if (memory->enableTerrain)
    {
        GenerateTerrainHeightMap(&gameState->terrain, assetSystem);
    }

    ScatterSystemConfig scatterSystemConfig = {};
    scatterSystemConfig.arenaSize = Kilobytes(16);
    scatterSystemConfig.maxScatterSpecs = 64;
    scatterSystemConfig.maxVolumes = 64;
    scatterSystemConfig.seed = XorShift32(&gameState->spawnRng);
    InitializeScatterSystem(&gameState->scatterSystem,
        &gameState->persistentArena, scatterSystemConfig);
    RegisterEntityScatterSpecs(&gameState->scatterSystem);

    gameState->cachedProfilerState.update = true;

    LOG_MSG("Game state initialized!");

    gameState->isInitialized = true;
}

internal void GameClientUpdate(GameState *gameState, InputState *input, f32 dt,
    b32 useDebugCamera, b32 showConsole, b32 runAutomatedTests)
{
    PlayerUIState *playerUIState = &gameState->playerUIState;

    u32 playerInputFlags = PlayerInputFlag_ReceiveNone;
    if (!useDebugCamera && !showConsole && !runAutomatedTests)
    {
        playerInputFlags |= PlayerInputFlag_ReceiveKeyboardInput;
        if (!gameState->lockCameraRotation)
        {
            playerInputFlags |= PlayerInputFlag_ReceiveMouseInput;
        }
    }

    PlayerCommand cmd = BuildPlayerCommand(
        input, gameState->prevPlayerCommand, playerInputFlags, dt);

    // Store command
    ASSERT(gameState->playerCommandCount <
           ARRAY_LENGTH(gameState->playerCommands));
    gameState->playerCommands[gameState->playerCommandCount++] = cmd;
    gameState->prevPlayerCommand = cmd;

    if ((playerInputFlags & PlayerInputFlag_ReceiveKeyboardInput) &&
        WasPressed(input->buttonStates[KEY_TAB]))
    {
        playerUIState->showInventory = !playerUIState->showInventory;
        playerUIState->containerEntityId = NULL_ENTITY_ID;
        playerUIState->craftingStationEntityId = NULL_ENTITY_ID;
    }
}

extern "C" GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
    ASSERT(memory->persistentStorageSize >= sizeof(GameState));
    GameState *gameState = (GameState *)memory->persistentStorage;

    // Setup global systems
    g_LoggingSystem = memory->loggingSystem;
    g_ProfilingSystem = memory->profilingSystem;
    g_DebugDrawingSystem = &gameState->debugDrawingSystem;

    PROF_SCOPE();

    MemoryArena tempArena = {};
    InitializeMemoryArena(
        &tempArena, memory->tempStorage, memory->tempStorageSize);

    EntityWorld *entityWorld = &gameState->entityWorld;
    AssetSystem *assetSystem = memory->renderData->assetSystem;

    if (!gameState->isInitialized)
    {
        GameInitialize(gameState, memory);

        if (memory->runAutomatedTests)
        {
            RunPickAxeViewModelTest(gameState);
            // RunCraftTest(gameState);
            RunPlayerMovementTest(gameState);
            RunParticleEmitterTest(gameState);
            RunPlayerPickupTest(gameState, assetSystem, &tempArena);
            // RunAiMovementTest(gameState);
        }
        else
        {
            SetToGamePlayTestMap(entityWorld);
            // SetToFirstTestMap(entityWorld, &gameState->scatterSystem);
            ScatterEntities(&gameState->scatterSystem, entityWorld,
                &gameState->collisionWorld);
            gameState->entityWorld.camera.position = GAME_TEST_ORIGIN;
        }
    }

    if (WasPressed(input->buttonStates[KEY_F2]) || memory->codeReloaded)
    {
        LOG_MSG("Reloading game code...");
        ClearItemDatabase(&gameState->itemDatabase);
        RegisterItems(&gameState->itemDatabase);
        RegisterRecipes(&gameState->itemDatabase);

        ClearParticleSpecs(&gameState->particleSystem);
        RegisterParticleSpecs(&gameState->particleSystem);

        if (IsCVarTrue(&gameState->cvarSystem, "reset_world_on_reload"))
        {
            ClearEntityWorld(entityWorld);
            ResetCollisionWorld(&gameState->collisionWorld);
            ResetScatterSystem(&gameState->scatterSystem);
            // TODO: ClearInventorySystem

            GenerateTerrainHeightMap(&gameState->terrain, assetSystem);
            AddTerrainCollisionObject(&gameState->collisionWorld);

            RegisterEntityScatterSpecs(&gameState->scatterSystem);

            SetToFirstTestMap(entityWorld, &gameState->scatterSystem);
            // NOTE: Scatter entities seems slow....
            // Is slow because we're doing 750 RayIntersectCollisionWorld
            ScatterEntities(&gameState->scatterSystem, entityWorld,
                &gameState->collisionWorld);
        }

        memset(&gameState->playerUIState, 0, sizeof(gameState->playerUIState));

        memset(&gameState->cachedProfilerState, 0,
            sizeof(gameState->cachedProfilerState));
        gameState->cachedProfilerState.update = true;
        LOG_MSG("Done!");
    }

    // TODO: Eventually replace this with platform events
    if (assetSystem->terrainShaderReloaded)
    {
        LOG_MSG("Terrain shader reloaded");
        GenerateTerrainHeightMap(&gameState->terrain, assetSystem);
        // TODO: Update terrain collision mesh
        assetSystem->terrainShaderReloaded = false;
    }

    if (WasPressed(input->buttonStates[KEY_F1]))
    {
        u32 useDebugCamera = 0;
        GetCVarU32(&gameState->cvarSystem, "use_debug_camera", &useDebugCamera);
        useDebugCamera = !useDebugCamera;
        SetCVarU32(&gameState->cvarSystem, "use_debug_camera", useDebugCamera);

        if (useDebugCamera)
        {
            u32 playerEntityId = GetPlayerEntityId(entityWorld);
            if (playerEntityId != 0)
            {
                vec3 position = GetEntityPosition(entityWorld, playerEntityId);
                Camera *debugCamera = GetDebugCamera(entityWorld);
                debugCamera->position = position;
            }
        }
    }

    u32 showConsole = 0;
    GetCVarU32(&gameState->cvarSystem, "show_console", &showConsole);

    u32 useDebugCamera = 0;
    GetCVarU32(&gameState->cvarSystem, "use_debug_camera", &useDebugCamera);

    if (WasPressed(input->buttonStates[KEY_GRAVE_ACCENT]))
    {
        showConsole = !showConsole;
        SetCVarU32(&gameState->cvarSystem, "show_console", showConsole);
    }

    if (WasPressed(input->buttonStates[KEY_F8]))
    {
        u32 showTextureDebug = 0;
        GetCVarU32(
            &gameState->cvarSystem, "show_texture_debug", &showTextureDebug);
        showTextureDebug++;
        if (showTextureDebug > 2)
        {
            showTextureDebug = 0;
        }
        SetCVarU32(
            &gameState->cvarSystem, "show_texture_debug", showTextureDebug);
    }

    if (WasPressed(input->buttonStates[KEY_F9]))
    {
        b32 showProfiler = false;
        GetCVarU32(&gameState->cvarSystem, "show_profiler", &showProfiler);
        showProfiler = !showProfiler;
        SetCVarU32(&gameState->cvarSystem, "show_profiler", showProfiler);
    }

    if (WasPressed(input->buttonStates[KEY_F10]))
    {
        gameState->cachedProfilerState.update =
            !gameState->cachedProfilerState.update;
    }

    if (WasPressed(input->buttonStates[KEY_EQUAL]))
    {
        ProfilingSystem *profiler = memory->profilingSystem;
        u32 readIndex = 0;
        if (GetCVarU32(&gameState->cvarSystem, "prof_read_index", &readIndex))
        {
            readIndex = MinU32(readIndex + 1, profiler->bufferCount - 1);
            SetCVarU32(&gameState->cvarSystem, "prof_read_index", readIndex);
        }
    }
    if (WasPressed(input->buttonStates[KEY_MINUS]))
    {
        ProfilingSystem *profiler = memory->profilingSystem;
        u32 readIndex = 0;
        if (GetCVarU32(&gameState->cvarSystem, "prof_read_index", &readIndex))
        {
            readIndex = Max((i32)readIndex - 1, 0);
            SetCVarU32(&gameState->cvarSystem, "prof_read_index", readIndex);
        }
    }

    if (showConsole)
    {
        UpdateConsole(&gameState->console, input, &gameState->commandSystem);
    }

    ExecuteCommands(gameState, &tempArena);

    GameClientUpdate(gameState, input, dt, useDebugCamera, showConsole,
        memory->runAutomatedTests);

    UpdateDebugDrawingSystem(&gameState->debugDrawingSystem, dt);

    if (useDebugCamera)
    {
        Camera *debugCamera = GetDebugCamera(entityWorld);
        UpdateCamera(
            debugCamera, input, dt, memory->debugShowMouseCursor, !showConsole);

        if (!showConsole)
        {
            if (WasPressed(input->buttonStates[KEY_SPACE]))
            {
                if (GetPlayerEntityId(&gameState->entityWorld) == 0)
                {
                    AddPlayer(&gameState->entityWorld, debugCamera->position);
                    SetCVarU32(&gameState->cvarSystem, "use_debug_camera", 0);
                }
            }
        }
    }
    else
    {
        memory->debugShowMouseCursor(gameState->lockCameraRotation);
    }

    gameState->dt = dt;
    gameState->frameTimeAccumulator += dt;

    gameState->currentTime += dt;

    f32 fixedTimestep = 1.0f / 60.0f;
    while (gameState->frameTimeAccumulator >= fixedTimestep)
    {
        // FIXME: Should probably have its own tempArena
        GameFixedUpdate(gameState, assetSystem, &tempArena, fixedTimestep);
        gameState->frameTimeAccumulator -= fixedTimestep;
    }

    f32 interp = gameState->frameTimeAccumulator / fixedTimestep;
    GameRender(gameState, memory, input, memory->renderData,
        input->framebufferWidth, input->framebufferHeight, &tempArena);
}

