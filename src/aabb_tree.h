#pragma once

struct AabbTreeNode
{
    vec3 min;
    vec3 max;
    AabbTreeNode *children[2];
    u32 leafIndex;
};

struct AabbTree
{
    AabbTreeNode *root;
    AabbTreeNode *nodes;
    u32 count;
    u32 max;
};

// TODO: This should be a parameter of the tree structure for iteration
#define AABB_TREE_STACK_SIZE 256
