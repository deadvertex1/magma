#pragma once

// TODO: Look at replacing this with a generic asset data table system
struct DeployableItemData
{
    const char *entityDefinitionName;
    u32 itemId; // This is really the key
};

struct DeployableItemDataTable
{
    DeployableItemData entries[64];
    u32 count;
};

struct DeployableVisualizationState
{
    u32 missingEntity;
    Dictionary entities;
};

// FIXME: This is showing a lot of problems, why isn't this a generic
// dictionary?
inline void RegisterDeployableItemData(
    DeployableItemDataTable *table, u32 itemId, const char *entityDefinition)
{
    ASSERT(table->count < ARRAY_LENGTH(table->entries));
    u32 index = table->count++;
    table->entries[index].itemId = itemId;
    table->entries[index].entityDefinitionName = entityDefinition;
}

inline DeployableItemData *FindDeployableItemData(
    DeployableItemDataTable *table, u32 itemId)
{
    DeployableItemData *result = NULL;
    for (u32 i = 0; i < table->count; i++)
    {
        DeployableItemData *entry = table->entries + i;
        if (entry->itemId == itemId)
        {
            result = entry;
            break;
        }
    }

    return result;
}

struct SnapResult
{
    b32 allowPlacement;
    vec3 position;
    quat rotation;
};

enum
{
    SnapBehaviour_BlockPlacement,
    SnapBehaviour_FreePlacement,
    SnapBehaviour_Foundation,
    SnapBehaviour_WallOnFoundation,
    SnapBehaviour_WallOnWall,
    SnapBehaviour_CeilingOnWall,
    SnapBehaviour_CeilingOnCeiling,
    SnapBehaviour_WallOnCeiling,
    SnapBehaviour_StairsOnFoundation,
    SnapBehaviour_StairsOnCeiling,
};
