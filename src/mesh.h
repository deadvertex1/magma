#pragma once

struct MeshData
{
    void *vertices;
    u32 vertexSize;
    u32 vertexCount;
    u32 *indices;
    u32 indexCount;
};

struct MeshImportResult
{
    u32 materialIndices[64];
    MeshData meshes[64];
    u32 count;
};
