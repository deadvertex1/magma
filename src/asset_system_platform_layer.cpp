internal void InitializeAssetSystem(
    AssetSystem *assetSystem, MemoryArena *arena, AssetSystemConfig config)
{
    memset(assetSystem, 0, sizeof(*assetSystem));

    assetSystem->fileCache = config.fileCache;
    assetSystem->arena = SubAllocateArena(arena, config.assetMemorySize);

    assetSystem->tables[AssetType_Texture] = Dict_CreateFromArena(
        &assetSystem->arena, Asset_Texture, config.maxTextures);
    assetSystem->tables[AssetType_Shader] = Dict_CreateFromArena(
        &assetSystem->arena, Asset_Shader, config.maxShaders);
    assetSystem->tables[AssetType_Font] =
        Dict_CreateFromArena(&assetSystem->arena, Asset_Font, config.maxFonts);
    assetSystem->tables[AssetType_Mesh] =
        Dict_CreateFromArena(&assetSystem->arena, Asset_Mesh, config.maxMeshes);
    assetSystem->tables[AssetType_Scene] = Dict_CreateFromArena(
        &assetSystem->arena, Asset_Scene, config.maxScenes);
    assetSystem->tables[AssetType_Material] = Dict_CreateFromArena(
        &assetSystem->arena, Asset_Material, config.maxMaterials);
    assetSystem->tables[AssetType_Skeleton] = Dict_CreateFromArena(
        &assetSystem->arena, Asset_Skeleton, config.maxSkeletons);
    assetSystem->tables[AssetType_ComputeShader] = Dict_CreateFromArena(
        &assetSystem->arena, Asset_ComputeShader, config.maxComputeShaders);
}

internal void CheckForAssetsToReload(AssetSystem *assetSystem)
{
    FileCache *fileCache = assetSystem->fileCache;
    FindInvalidatedEntries(fileCache);
    for (u32 i = 0; i < fileCache->count; ++i)
    {
        FileCacheEntry *entry = fileCache->entries + i;
        if (entry->wasModified)
        {
            // Add to asset load queue with force reload flag
            AddAssetToLoadQueue(assetSystem, entry->assetType, entry->assetId,
                AssetLoadRequestFlags_ForceReload);

            entry->wasModified = false;
        }
    }
}

internal void ProcessAssetLoadQueue(
    AssetSystem *assetSystem, RenderData *renderer)
{
    MemoryArena *assetArena = &assetSystem->arena;

    for (u32 i = 0; i < assetSystem->loadQueueLength; i++)
    {
        AssetLoadRequest request = assetSystem->loadQueue[i];
        switch (request.type)
        {
        case AssetType_Texture:
        {
            Asset_Texture texture = {};
            if (GetAssetData_(assetSystem, AssetType_Texture, request.id,
                    sizeof(Asset_Texture), &texture))
            {
                // Confirm asset is still not loaded
                if (texture.handle == 0 ||
                    (request.flags & AssetLoadRequestFlags_ForceReload))
                {
                    // TODO: Free opengl resources on reload, in practice
                    // we won't reload these often enough to care
                    // Get path to asset on disk
                    ASSERT(texture.path != NULL);

                    // Load asset from disk
                    u32 handle = 0;
                    if (texture.isCubeMap)
                    {
                        handle = LoadSkyboxFromDisk();
                    }
                    else
                    {
                        handle = LoadTextureFromDisk(
                            texture.path, texture.useAlpha, texture.repeat);
                    }

                    texture.handle = handle;
                    SetAssetData_(assetSystem, AssetType_Texture, request.id,
                        sizeof(texture), &texture);
                }
            }
            else
            {
                fprintf(stderr,
                    "No texture entry in asset database for id %u\n",
                    request.id);
            }
            break;
        }
        case AssetType_Shader:
        {
            Asset_Shader shader = {};
            if (GetAssetData_(assetSystem, AssetType_Shader, request.id,
                    sizeof(shader), &shader))
            {
                // Confirm asset is still not loaded
                if (shader.handle == 0 ||
                    (request.flags & AssetLoadRequestFlags_ForceReload))
                {
                    if (shader.handle != 0)
                    {
                        DestroyShader(shader.handle);
                    }

                    // Get path to asset on disk
                    ASSERT(shader.vertexShaderPath != NULL);
                    ASSERT(shader.fragmentShaderPath != NULL);

                    u32 handle = LoadSPIRVShaderFromDisk(
                        shader.vertexShaderPath, shader.fragmentShaderPath);

                    shader.handle = handle;
                    SetAssetData_(assetSystem, AssetType_Shader, request.id,
                        sizeof(shader), &shader);
                }
            }
            else
            {
                fprintf(stderr, "No shader entry in asset database for id %u\n",
                    request.id);
            }
            break;
        }
        case AssetType_ComputeShader:
        {
            Asset_ComputeShader shader = {};
            if (GetAssetData_(assetSystem, AssetType_ComputeShader, request.id,
                    sizeof(shader), &shader))
            {
                // Confirm asset is still not loaded
                if (shader.handle == 0 ||
                    (request.flags & AssetLoadRequestFlags_ForceReload))
                {
                    if (shader.handle != 0)
                    {
                        DestroyShader(shader.handle);
                    }

                    // Get path to asset on disk
                    ASSERT(shader.path != NULL);

                    u32 handle = LoadSPIRVComputeShaderFromDisk(shader.path);

                    shader.handle = handle;
                    SetAssetData_(assetSystem, AssetType_ComputeShader,
                        request.id, sizeof(shader), &shader);

                    // TODO: Replace this with a platform event for asset
                    // reloaded
                    if (request.flags & AssetLoadRequestFlags_ForceReload)
                    {
                        if (request.id ==
                            HashStringU32("shaders/generate_height_map"))
                        {
                            assetSystem->terrainShaderReloaded = true;
                        }
                    }
                }
            }
            else
            {
                fprintf(stderr, "No shader entry in asset database for id %u\n",
                    request.id);
            }
            break;
        }
        case AssetType_Font:
        {
            Asset_Font font = {};
            if (GetAssetData_(assetSystem, AssetType_Font, request.id,
                    sizeof(font), &font))
            {
                // Confirm asset is still not loaded
                if (font.data == NULL ||
                    (request.flags & AssetLoadRequestFlags_ForceReload))
                {
                    // TODO: Free opengl resources on reload, in practice
                    // we won't reload these often enough to care
                    ASSERT(font.path != NULL);
                    // Load asset from disk
                    Font fontData =
                        LoadFontFromDisk(assetArena, font.path, font.size);

                    // Store it
                    Font *memory = ALLOCATE_STRUCT(assetArena, Font);
                    *memory = fontData;

                    font.data = memory;
                    SetAssetData_(assetSystem, AssetType_Font, request.id,
                        sizeof(font), &font);
                }
            }
            else
            {
                fprintf(stderr, "No font entry in asset database for id %u\n",
                    request.id);
            }
            break;
        }
        case AssetType_Mesh:
        {
            Asset_Mesh mesh = {};
            if (GetAssetData_(assetSystem, AssetType_Mesh, request.id,
                    sizeof(mesh), &mesh))
            {
                // Confirm asset is still not loaded
                if (mesh.data == NULL ||
                    (request.flags & AssetLoadRequestFlags_ForceReload))
                {
                    // TODO: Free opengl resources on reload, in practice
                    // we won't reload these often enough to care
                    // Get path to asset on disk
                    ASSERT(mesh.path != NULL);

                    // Load asset from disk
                    MeshImportResult result =
                        LoadMeshFromDisk(mesh.path, assetArena);

                    if (result.count > 0)
                    {
                        // Store it
                        MeshState *memory =
                            ALLOCATE_STRUCT(assetArena, MeshState);
                        *memory = CreateMeshFromData(result.meshes[0]);

                        MeshData *meshData =
                            ALLOCATE_STRUCT(assetArena, MeshData);
                        *meshData = result.meshes[0];

                        mesh.data = memory;
                        mesh.meshData = meshData;
                        SetAssetData_(assetSystem, AssetType_Mesh, request.id,
                            sizeof(mesh), &mesh);
                    }
                }
            }
            else
            {
                fprintf(stderr, "No mesh entry in asset database for id %u\n",
                    request.id);
            }
            break;
        }
        case AssetType_Material:
        {
            // FIXME: Loading materials from file not implemented yet
            // fprintf(stderr, "loading materials from file not implemented
            // yet!\n");
            break;
        }
#if 0
        case AssetType_Skeleton:
        {
            Asset_Skeleton skeleton = {};
            if (GetAssetData_(assetSystem, AssetType_Skeleton, request.id,
                    sizeof(skeleton), &skeleton))
            {
                // Confirm asset is still not loaded
                if (skeleton.data == NULL ||
                    (request.flags & AssetLoadRequestFlags_ForceReload))
                {
                    // TODO: Free opengl resources on reload, in practice
                    // we won't reload these often enough to care
                    // Get path to asset on disk
                    ASSERT(skeleton.path != NULL);

                    // Load asset from disk
                    Skeleton *data = ALLOCATE_STRUCT(assetArena, Skeleton);
                    ASSERT(data);
                    if (data != NULL)
                    {
                        ImportSceneResult result =
                            ImportSceneFromDisk(skeleton.path, assetArena);
                        memcpy(data, &result.skeleton, sizeof(Skeleton));
                        skeleton.data = data;
                        SetAssetData_(assetSystem, AssetType_Skeleton,
                            request.id, sizeof(skeleton), &skeleton);
                    }
                }
            }
            break;
        }
#endif
        default:
            ASSERT(!"Unknown asset type!\n");
            break;
        }
    }

    assetSystem->loadQueueLength = 0;
}

// NOTE: By default we load the compute shader as soon as its registered
// TODO: Bit ick that we have to have this function in a different place to the
// other AddAssetFromPath functions
inline void AddComputeShaderFromPath(AssetSystem *assetSystem, u32 id,
    const char *path, b32 forceInitialLoad = true)
{
    Asset_ComputeShader shader = {};
    shader.path = path;
    SetAssetData_(
        assetSystem, AssetType_ComputeShader, id, sizeof(shader), &shader);
    AddEntryToCache(assetSystem->fileCache, AssetType_ComputeShader, id, path);
    AddAssetToLoadQueue(assetSystem, AssetType_ComputeShader, id);
}

