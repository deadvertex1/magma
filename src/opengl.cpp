internal void GLAPIENTRY OpenGLMessageCallback(GLenum source, GLenum type,
    GLuint id, GLenum severity, GLsizei length, const GLchar *message,
    const void *userParam)
{
    LOG_MSG("OPENGL MESSAGE: %s type = 0x%x, severity = 0x%x, message = %s",
        (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""), type, severity,
        message);
}

internal b32 LoadSPIRVShaderCode(u32 shader, u32 *byteCode, u32 length)
{
    glShaderBinary(
        1, &shader, GL_SHADER_BINARY_FORMAT_SPIR_V, byteCode, length);

    // TODO: Wrapper library to abstract this?
    if (GLEW_VERSION_4_6)
    {
        glSpecializeShader(shader, "main", 0, NULL, NULL);
    }
    else if (GLEW_ARB_gl_spirv)
    {
        glSpecializeShaderARB(shader, "main", 0, NULL, NULL);
    }

    i32 status = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

    return (status == GL_TRUE);
}

internal u32 CreateSPIRVShader(u32 *vertexSource, u32 vertexLength,
    u32 *fragmentSource, u32 fragmentLength)
{
    char errorLog[512];

    // Create shaders
    u32 vertexShader = glCreateShader(GL_VERTEX_SHADER);
    u32 fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    u32 program = 0;
    if (LoadSPIRVShaderCode(vertexShader, vertexSource, vertexLength))
    {
        if (LoadSPIRVShaderCode(fragmentShader, fragmentSource, fragmentLength))
        {
            program = glCreateProgram();
            glAttachShader(program, vertexShader);
            glAttachShader(program, fragmentShader);
            glLinkProgram(program);

            i32 success;
            glGetProgramiv(program, GL_LINK_STATUS, &success);
            if (!success)
            {
                glGetProgramInfoLog(program, sizeof(errorLog), NULL, errorLog);
                LOG_MSG("** SHADER LINKER ERROR **\n%s\n", errorLog);
            }
        }
        else
        {
            LOG_MSG("Failed to load fragment shader SPIR-V code");
        }
    }
    else
    {
        LOG_MSG("Failed to load vertex shader SPIR-V code");
    }

    return program;
}

internal u32 CreateSPIRVComputeShader(u32 *computeSource, u32 length)
{
    char errorLog[512];
    u32 computeShader = glCreateShader(GL_COMPUTE_SHADER);

    u32 program = 0;
    if (LoadSPIRVShaderCode(computeShader, computeSource, length))
    {
        program = glCreateProgram();
        glAttachShader(program, computeShader);
        glLinkProgram(program);

        i32 success;
        glGetProgramiv(program, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(program, sizeof(errorLog), NULL, errorLog);
            LOG_MSG("** SHADER LINKER ERROR **\n%s\n", errorLog);
        }
    }
    else
    {
        LOG_MSG("Failed to load compute shader SPIR-V code");
    }

    return program;
}

struct FileContents
{
    void *data;
    u32 length;
};

internal FileContents ReadEntireFile(const char *path)
{
    FileContents result = {};
    FILE *file = fopen(path, "rb");
    if (file != NULL)
    {
        fseek(file, 0, SEEK_END);
        u32 length = ftell(file);
        fseek(file, 0, SEEK_SET);
        void *data = malloc(length);
        ASSERT(data != NULL);
        u32 bytesRead = fread(data, 1, length, file);
        ASSERT(bytesRead == length);
        fclose(file);

        result.data = data;
        result.length = length;
    }
    else
    {
        LOG_MSG("Failed to open file %s", path);
    }
    return result;
}

internal void FreeFileContents(FileContents contents)
{
    if (contents.data != NULL)
    {
        free(contents.data);
    }
}

internal u32 LoadSPIRVShaderFromDisk(
    const char *vertexPath, const char *fragmentPath)
{
    u32 program = 0;
    FileContents vertexShaderContents = ReadEntireFile(vertexPath);
    FileContents fragmentShaderContents = ReadEntireFile(fragmentPath);

    if (vertexShaderContents.length > 0 && fragmentShaderContents.length > 0)
    {
        if (vertexShaderContents.length % sizeof(u32) == 0)
        {
            if (fragmentShaderContents.length % sizeof(u32) == 0)
            {
                program = CreateSPIRVShader((u32 *)vertexShaderContents.data,
                    vertexShaderContents.length,
                    (u32 *)fragmentShaderContents.data,
                    fragmentShaderContents.length);
            }
            else
            {
                LOG_MSG("%s is not valid SPIR-V code", fragmentPath);
            }
        }
        else
        {
            LOG_MSG("%s is not valid SPIR-V code", vertexPath);
        }

        FreeFileContents(vertexShaderContents);
        FreeFileContents(fragmentShaderContents);
    }
    else
    {
        LOG_MSG("vertexShaderContents.length = %u, "
                "fragmentShaderContents.length = %u",
            vertexShaderContents.length, fragmentShaderContents.length);
    }

    return program;
}

internal u32 LoadSPIRVComputeShaderFromDisk(const char *computePath)
{
    u32 program = 0;
    FileContents computeShaderContents = ReadEntireFile(computePath);

    if (computeShaderContents.length > 0)
    {
        if (computeShaderContents.length % sizeof(u32) == 0)
        {
            program =
                CreateSPIRVComputeShader((u32 *)computeShaderContents.data,
                    computeShaderContents.length);
        }
        else
        {
            LOG_MSG("%s is not valid SPIR-V code", computePath);
        }

        FreeFileContents(computeShaderContents);
    }

    return program;
}

internal u32 CreateCameraUniformBuffer()
{
    u32 cameraUniformBuffer;
    glGenBuffers(1, &cameraUniformBuffer);
    glBindBuffer(GL_UNIFORM_BUFFER, cameraUniformBuffer);
    glBufferData(
        GL_UNIFORM_BUFFER, sizeof(CameraUniformBuffer), NULL, GL_DYNAMIC_DRAW);

    return cameraUniformBuffer;
}

internal u32 CreateLightingUniformBuffer()
{
    u32 buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_UNIFORM_BUFFER, buffer);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(LightingUniformBuffer), NULL,
        GL_DYNAMIC_DRAW);

    return buffer;
}

internal MeshState CreateMesh(void *vertices, u32 vertexSize, u32 vertexCount,
    u32 *indices, u32 indexCount)
{
    glBindVertexArray(0);

    // Create vertex array object
    u32 vertexArrayObject;
    glGenVertexArrays(1, &vertexArrayObject);
    glBindVertexArray(vertexArrayObject);

    // Create vertex buffer
    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(
        GL_ARRAY_BUFFER, vertexSize * vertexCount, vertices, GL_STATIC_DRAW);

    // Create element buffer
    u32 elementBuffer;
    glGenBuffers(1, &elementBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(u32) * indexCount, indices,
        GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);

    if (vertexSize == sizeof(Vertex))
    {
        // TODO: Use offsetof
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertexSize, (void *)0);
        // FIXME: Are we actually using the color field!?!?!
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(
            1, 3, GL_FLOAT, GL_FALSE, vertexSize, (void *)(sizeof(f32) * 3));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(
            2, 3, GL_FLOAT, GL_FALSE, vertexSize, (void *)(sizeof(f32) * 6));
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(
            3, 2, GL_FLOAT, GL_FALSE, vertexSize, (void *)(sizeof(f32) * 9));
    }
    else if (vertexSize == sizeof(SkinnedVertex))
    {
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertexSize,
            (void *)OffsetOf(SkinnedVertex, position));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, vertexSize,
            (void *)OffsetOf(SkinnedVertex, normal));
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, vertexSize,
            (void *)OffsetOf(SkinnedVertex, tangent));
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, vertexSize,
            (void *)OffsetOf(SkinnedVertex, bitangent));
        glEnableVertexAttribArray(5);
        glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, vertexSize,
            (void *)OffsetOf(SkinnedVertex, textureCoord));
        glEnableVertexAttribArray(6);
        glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, vertexSize,
            (void *)OffsetOf(SkinnedVertex, jointWeights));
        glEnableVertexAttribArray(7);
        glVertexAttribPointer(7, 1, GL_UNSIGNED_INT, GL_FALSE, vertexSize,
            (void *)OffsetOf(SkinnedVertex, jointIndices));
    }
    else
    {
        INVALID_CODE_PATH("Unknown vertex type");
    }

    glBindVertexArray(0);

    MeshState result = {};
    result.vertexArrayObject = vertexArrayObject;
    result.vertexBuffer = vertexBuffer;
    result.elementBuffer = elementBuffer;
    result.elementCount = indexCount;

    return result;
}

internal MeshState CreateMeshFromData(MeshData data)
{
    MeshState result = CreateMesh(data.vertices, data.vertexSize,
        data.vertexCount, data.indices, data.indexCount);
    return result;
}

internal u32 LoadTextureFromDisk(
    const char *path, b32 useAlpha = false, b32 repeat = true)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    if (repeat)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
    glTexParameteri(
        GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    i32 width, height, channelCount;
    u8 *pixels =
        stbi_load(path, &width, &height, &channelCount, useAlpha ? 4 : 3);
    if (pixels != NULL)
    {
        if (useAlpha)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8_ALPHA8, width, height, 0,
                GL_RGBA, GL_UNSIGNED_BYTE, pixels);
        }
        else
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8, width, height, 0, GL_RGB,
                GL_UNSIGNED_BYTE, pixels);
        }
        glGenerateMipmap(GL_TEXTURE_2D);

        stbi_image_free(pixels);
    }
    else
    {
        LOG_MSG("Failed to load texture %s", path);
    }

    return texture;
}

internal u32 CreateTerrainHeighMapTexture(u32 width, u32 height)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, NULL);

    return texture;
}

internal u32 LoadSkyboxFromDisk()
{
    // Generate cube map
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    // Load from disk
    const char *paths[] = {
        "../assets/textures/skybox/px.png",
        "../assets/textures/skybox/nx.png",
        "../assets/textures/skybox/py.png",
        "../assets/textures/skybox/ny.png",
        "../assets/textures/skybox/pz.png",
        "../assets/textures/skybox/nz.png",
    };

    for (u32 i = 0; i < ARRAY_LENGTH(paths); ++i)
    {
        i32 width, height, channelCount;
        u8 *pixels = stbi_load(paths[i], &width, &height, &channelCount, 3);
        if (pixels != NULL)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width,
                height, 0, GL_RGB, GL_UNSIGNED_BYTE, pixels);

            stbi_image_free(pixels);
        }
        else
        {
            LOG_MSG("Failed to load texture %s", paths[i]);
        }
    }

    return texture;
}

internal u32 CreateMissingTexture()
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    f32 pixels[] = {
        1.0f,
        0.0f,
        1.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        1.0f,
        0.0f,
        1.0f,
    };
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, pixels);

    return texture;
}

internal u32 CreateSolidColorTexture(vec4 color)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, color.data);

    return texture;
}

internal DebugDrawVerticesBuffer CreateDebugDrawVerticesBuffer(u32 vertexCount)
{
    // Create vertex buffer
    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(
        GL_ARRAY_BUFFER, sizeof(VertexPC) * vertexCount, NULL, GL_DYNAMIC_DRAW);

    // Create vertex array object
    u32 vertexArrayObject;
    glGenVertexArrays(1, &vertexArrayObject);
    glBindVertexArray(vertexArrayObject);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPC), 0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPC),
        (void *)OffsetOf(VertexPC, color));

    glBindVertexArray(0);

    DebugDrawVerticesBuffer result = {};
    result.vertexBuffer = vertexBuffer;
    result.vertexArrayObject = vertexArrayObject;
    result.vertices = (VertexPC *)malloc(sizeof(VertexPC) * vertexCount);
    ASSERT(result.vertices != NULL);
    result.max = vertexCount;

    return result;
}

internal TextVerticesBuffer CreateTextVerticesBuffer(u32 vertexCount)
{
    // Create vertex buffer
    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(VertexText) * vertexCount, NULL,
        GL_DYNAMIC_DRAW);

    // Create vertex array object
    u32 vertexArrayObject;
    glGenVertexArrays(1, &vertexArrayObject);
    glBindVertexArray(vertexArrayObject);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexText), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(VertexText),
        (void *)OffsetOf(VertexText, textureCoordinates));

    glBindVertexArray(0);

    TextVerticesBuffer result = {};
    result.vertexBuffer = vertexBuffer;
    result.vertexArrayObject = vertexArrayObject;
    // FIXME: Replace with persistently mapped buffers!
    result.vertices = (VertexText *)malloc(sizeof(VertexText) * vertexCount);
    result.max = vertexCount;

    return result;
}

internal ShadowMapBuffer CreateShadowMapBuffer(u32 width, u32 height)
{
    u32 fbo;
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, width, height, 0,
        GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    f32 borderColor[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    // NOTE: Copied code which in theory does PCF
    // Not very clear in OpenGL docs what this actually does, in theory it
    // should enable hardware PCF shadows which in theory are fetching 4 texels
    // per sample. Probably still need to do normal PCF in shader as this is
    // intended to be mainly used as an optimization rather than a replacement
    // for a software implementation.
    glTexParameteri(
        GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    glFramebufferTexture2D(
        GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texture, 0);

    ASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    ShadowMapBuffer result = {};
    result.framebufferObject = fbo;
    result.texture = texture;

    return result;
}

internal u32 CreateGlyphSheetTexture(u32 width, u32 height, u8 *pixels)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, width, height, 0, GL_RED,
        GL_UNSIGNED_BYTE, pixels);

    return texture;
}

internal u32 LoadHdriFromDisk(const char *path)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    i32 width, height, channelCount;
    f32 *pixels = stbi_loadf(path, &width, &height, &channelCount, 4);
    if (pixels != NULL)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA,
            GL_FLOAT, pixels);

        stbi_image_free(pixels);
    }
    else
    {
        LOG_MSG("Failed to load texture %s", path);
    }

    return texture;
}

internal u32 BuildCubeMapFromEquirectangularTexture(
    u32 program, const char *path, u32 cubeMapWidth = 512)
{
    LOG_MSG("Generating cube map from equirectangular texture...");

    u32 inputTexture = LoadHdriFromDisk(path);
    LOG_MSG("Finished reading HDRi from disk");

    // Create output cube map
    u32 outputTexture;
    glGenTextures(1, &outputTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, outputTexture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexStorage2D(
        GL_TEXTURE_CUBE_MAP, 1, GL_RGBA16F, cubeMapWidth, cubeMapWidth);

    glUseProgram(program);

    // Bind resources
    glActiveTexture(GL_TEXTURE0);
    glBindImageTexture(
        0, outputTexture, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA16F);
    glBindTexture(GL_TEXTURE_CUBE_MAP, outputTexture);
    glUniform1i(0, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, inputTexture);
    glUniform1i(1, 1);

    glUniform1ui(2, cubeMapWidth);

    // Dispatch compute shader
    glDispatchCompute(cubeMapWidth, cubeMapWidth, 6);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

    LOG_MSG("Done!");
    return outputTexture;
}

internal u32 CreateSingleColorHdriCubeMap(vec4 color)
{
    // Create output cube map
    u32 outputTexture;
    glGenTextures(1, &outputTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, outputTexture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    for (u32 i = 0; i < 6; i++)
    {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA32F, 1, 1, 0,
            GL_RGBA, GL_FLOAT, &color);
    }

    return outputTexture;
}

internal HdrFramebuffer CreateHdrFramebuffer(u32 width, u32 height)
{
    u32 fbo;
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);

    u32 colorTexture;
    glGenTextures(1, &colorTexture);
    glBindTexture(GL_TEXTURE_2D, colorTexture);

    // TODO: Have we decided on where we use glTexImage or glTexStorage2D for
    // allocating texture data?
    // TODO: OPTIMIZE
    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glFramebufferTexture2D(
        GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTexture, 0);

    u32 depthTexture;
    glGenTextures(1, &depthTexture);
    glBindTexture(GL_TEXTURE_2D, depthTexture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0,
        GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glFramebufferTexture2D(
        GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);

    ASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    HdrFramebuffer result = {};
    result.framebufferObject = fbo;
    result.colorTexture = colorTexture;
    result.depthTexture = depthTexture;

    return result;
}

internal void DestroyHdrFramebuffer(HdrFramebuffer hdrFramebuffer)
{
    glDeleteFramebuffers(1, &hdrFramebuffer.framebufferObject);
    // TODO: Do we need to delete the textures?
}

internal void DestroyShader(u32 shader) { glDeleteProgram(shader); }
