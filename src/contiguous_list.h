#pragma once

struct ContiguousList
{
    void *base;
    u32 entrySize;
    u32 count;
    u32 capacity;
};

inline ContiguousList CreateContiguousList_(
    MemoryArena *arena, u32 entrySize, u32 capacity)
{
    ContiguousList list = {};
    list.base = AllocateBytes(arena, entrySize * capacity);
    list.entrySize = entrySize;
    list.capacity = capacity;
    return list;
}

inline void* AllocateFromContiguousList_(ContiguousList *list)
{
    void *result = NULL;
    if (list->count < list->capacity)
    {
        u32 index = list->count++;
        result = (u8 *)list->base + index * list->entrySize;
    }

    return result;
}

inline void* AllocateFromContiguousList_(ContiguousList *list, u32 count)
{
    void *result = NULL;
    if (list->count + count <= list->capacity)
    {
        u32 index = list->count;
        result = (u8 *)list->base + index * list->entrySize;
        list->count += count;
    }

    return result;
}

inline void* FreeFromContiguousList_(ContiguousList *list, void *entry)
{
    ASSERT(list->count > 0);
    ASSERT(entry >= list->base);
    ASSERT(entry < (u8 *)list->base + list->count * list->entrySize);

    u32 lastIndex = --list->count;
    void *lastEntry = (u8 *)list->base + lastIndex * list->entrySize;
    memcpy(entry, lastEntry, list->entrySize);
    return entry;
}

inline void* ContiguousListStart(ContiguousList *list)
{
    void *start = list->base;
    return start;
}

inline void* ContiguousListEnd(ContiguousList *list)
{
    void *end = (u8 *)list->base + list->count * list->entrySize;
    return end;
}

inline void* GetNext(ContiguousList *list, void *entry)
{
    ASSERT(entry >= list->base);
    void *end = ContiguousListEnd(list);
    void *next = (u8 *)entry + list->entrySize;
    if (next > end)
    {
        next = end;
    }
    return next;
}

inline void ClearContiguousList(ContiguousList *list)
{
    list->count = 0;
}

typedef b32 ContiguousListFilterFunction(void *entry);
inline void FilterContiguousList(
    ContiguousList *list, ContiguousListFilterFunction *fn)
{
    void *entry = list->base;
    while (entry < ContiguousListEnd(list))
    {
        if (fn(entry))
        {
            entry = GetNext(list, entry);
        }
        else
        {
            entry = FreeFromContiguousList_(list, entry);
        }
    }
}

typedef void ContiguousListMapFunction(void *entry, void *userData);
inline void MapContiguousList(
    ContiguousList *list, ContiguousListMapFunction *fn, void *userData)
{
    void *entry = list->base;
    while (entry < ContiguousListEnd(list))
    {
        fn(entry, userData);
        entry = GetNext(list, entry);
    }
}

#define CONTIGUOUS_LIST_FOREACH(LIST, TYPE, ITER) \
    for (TYPE *ITER = (TYPE *)LIST->base; \
         ITER != (TYPE *)ContiguousListEnd(LIST); \
         ITER++)
