#version 450

layout(location = 0) out vec4 outputColor;

layout(binding = 0) uniform sampler2D hdrTexture;
layout(location = 1) uniform float u_exposure;

layout(location=0) in vec2 fragTextureCoords;

vec3 ReinhardToneMapping(vec3 color, float exposure)
{
    color *= exposure; // exposure adjustment

    color = color / (vec3(1.0) + color);

    return color;

}

vec3 Uncharted2Tonemap(vec3 x)
{
    float A = 0.15;
    float B = 0.50;
    float C = 0.10;
    float D = 0.20;
    float E = 0.02;
    float F = 0.30;
    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

void main()
{
    vec3 hdrColor = texture(hdrTexture, fragTextureCoords).rgb;
    //vec3 tonemappedColor = ReinhardToneMapping(hdrColor, u_exposure);

    hdrColor *= u_exposure;
    float exposureBias = 2.0;
    float lum = 0.2126f * hdrColor.r + 0.7152 * hdrColor.g + 0.0722 * hdrColor.b;
    vec3 newLum = Uncharted2Tonemap(vec3(exposureBias*lum));
    float lumScale = float(newLum / vec3(lum));
    vec3 tonemappedColor = hdrColor*lumScale;

    float W = 11.2;
    vec3 whiteScale = 1.0f/Uncharted2Tonemap(vec3(W));
    tonemappedColor *= whiteScale;

    // Perfrom linear to sRGB conversion
    tonemappedColor = pow(tonemappedColor, vec3(1.0 / 2.2));
    outputColor = vec4(tonemappedColor, 1);
}
