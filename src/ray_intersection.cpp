inline f32 RayIntersectAabb(
    vec3 boxMin, vec3 boxMax, vec3 rayOrigin, vec3 rayDirection)
{
    f32 tmin = 0.0f;
    f32 tmax = F32_MAX;

    for (u32 axis = 0; axis < 3; ++axis)
    {
        f32 s = rayOrigin.data[axis];
        f32 d = rayDirection.data[axis];
        f32 min = boxMin.data[axis];
        f32 max = boxMax.data[axis];

        if (Abs(d) < EPSILON)
        {
            // Ray is parallel to axis plane
            if (s < min || s > max)
            {
                // No collision
                tmin = -1.0f;
                break;
            }
        }
        else
        {
            f32 a = (min - s) / d;
            f32 b = (max - s) / d;

            f32 t0 = Min(a, b);
            f32 t1 = Max(a, b);

            tmin = Max(tmin, t0);
            tmax = Min(tmax, t1);

            if (tmin > tmax)
            {
                // No collision
                tmin = -1.0f;
                break;
            }
        }
    }

    return tmin;
}

// https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
internal RayIntersectTriangleResult RayIntersectTriangleMT(vec3 rayOrigin,
    vec3 rayDirection, vec3 a, vec3 b, vec3 c, f32 tmin = F32_MAX)
{
    RayIntersectTriangleResult result = {};
    result.t = -1.0f;

    vec3 T = rayOrigin - a;
    vec3 e1 = b - a;
    vec3 e2 = c - a;

    vec3 p = Cross(rayDirection, e2);
    vec3 q = Cross(T, e1);
    vec3 n = Cross(e1, e2);

    vec3 m = Vec3(Dot(q, e2), Dot(p, T), Dot(q, rayDirection));

    f32 det = 1.0f / Dot(p, e1);

    f32 t = det * m.x;
    f32 u = det * m.y;
    f32 v = det * m.z;
    f32 w = 1.0f - u - v;

    f32 f = Dot(rayDirection, n);

    if (u >= 0.0f && u <= 1.0f && v >= 0.0f && v <= 1.0f && w >= 0.0f &&
        w <= 1.0f && f < 0.0f)
    {
        result.t = t;
        result.normal = n;
        result.uv = Vec2(u, v);
    }

    return result;
}
