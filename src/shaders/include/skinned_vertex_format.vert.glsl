// Vertex format
layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_color;
layout(location = 2) in vec3 v_normal;
layout(location = 3) in vec3 v_tangent;
layout(location = 4) in vec3 v_bitangent;
layout(location = 5) in vec2 v_texture_coordinates;
layout(location = 6) in vec3 v_jointWeights;
layout(location = 7) in uint v_jointIndices;
