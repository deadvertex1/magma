internal void AddQuadToTextBuffer(TextBuffer *buffer, vec2 p, vec2 dims)
{
    if (buffer->count + 6 <= buffer->max)
    {
        VertexText *vertices = buffer->vertices + buffer->count;
        vertices[0].position = p;
        vertices[0].textureCoordinates = Vec2(0, 1);
        vertices[1].position = p + Vec2(dims.x, 0);
        vertices[1].textureCoordinates = Vec2(1, 1);
        vertices[2].position = p + Vec2(dims.x, dims.y);
        vertices[2].textureCoordinates = Vec2(1, 0);

        vertices[3].position = p + Vec2(dims.x, dims.y);
        vertices[3].textureCoordinates = Vec2(1, 0);
        vertices[4].position = p + Vec2(0, dims.y);
        vertices[4].textureCoordinates = Vec2(0, 0);
        vertices[5].position = p;
        vertices[5].textureCoordinates = Vec2(0, 1);
        buffer->count += 6;
    }
}

internal void AddGlyphToTextBuffer(TextBuffer *buffer, FontGlyph glyph, vec2 p)
{
    if (buffer->count + 6 <= buffer->max)
    {
        VertexText *vertices = buffer->vertices + buffer->count;
        vertices[0].position = p + Vec2(glyph.x0, glyph.y0);
        vertices[0].textureCoordinates = Vec2(glyph.u0, glyph.v0);
        vertices[1].position = p + Vec2(glyph.x1, glyph.y0);
        vertices[1].textureCoordinates = Vec2(glyph.u1, glyph.v0);
        vertices[2].position = p + Vec2(glyph.x1, glyph.y1);
        vertices[2].textureCoordinates = Vec2(glyph.u1, glyph.v1);

        vertices[3].position = p + Vec2(glyph.x1, glyph.y1);
        vertices[3].textureCoordinates = Vec2(glyph.u1, glyph.v1);
        vertices[4].position = p + Vec2(glyph.x0, glyph.y1);
        vertices[4].textureCoordinates = Vec2(glyph.u0, glyph.v1);
        vertices[5].position = p + Vec2(glyph.x0, glyph.y0);
        vertices[5].textureCoordinates = Vec2(glyph.u0, glyph.v0);
        buffer->count += 6;
    }
}

internal void DrawText(TextBuffer *buffer, Font font, const char *text,
    vec2 position, vec4 color, f32 depth)
{
    // FIXME: What is this?!?!?
    f32 advance = font.glyphs[2].x1 - font.glyphs[2].x0;

    u32 start = buffer->count;
    f32 x = 0.0f;
    f32 y = 0.0f;
    while (*text)
    {
        u8 index = (u8)(*text);

        // Handle newline character
        if (index == '\n')
        {
            y -= font.lineSpacing + font.height;
            x = 0.0f;
        }

        // TODO: Constant for the magic 32 offset
        if (index >= 32)
        {
            index -= 32;
            ASSERT(index < font.glyphCount);
            FontGlyph glyph = font.glyphs[index];

            if (index > 0) // space
            {
                AddGlyphToTextBuffer(buffer, glyph, position + Vec2(x, y));
            }

            x += glyph.advance;
        }
        // else: Skip unprintable character, usually its '\n'
        text++;
    }

    u32 count = buffer->count - start;

    if (buffer->drawCallCount < buffer->maxDrawCalls)
    {
        TextDrawCall *drawCall = buffer->drawCalls + buffer->drawCallCount++;
        drawCall->offset = start;
        drawCall->count = count;
        drawCall->color = color;
        drawCall->texture = font.texture;
        drawCall->shaderAssetId = Shader_Text;
        drawCall->depth = depth;
    }
}

internal f32 CalculateTextWidth(Font font, const char *text)
{
    f32 x = 0.0f;
    while (*text)
    {
        u8 index = (u8)(*text);

        // TODO: Constant for the magic 32 offset
        ASSERT(index >= 32);
        index -= 32;
        ASSERT(index < font.glyphCount);
        FontGlyph glyph = font.glyphs[index];

        x += glyph.advance;
        text++;
    }
    return x;
}

internal void DrawQuad(TextBuffer *buffer, AssetSystem *assetSystem,
    vec2 position, vec2 dims, vec4 color, f32 depth)
{
    u32 start = buffer->count;
    AddQuadToTextBuffer(buffer, position, dims);
    if (buffer->drawCallCount < buffer->maxDrawCalls)
    {
        TextDrawCall *drawCall = buffer->drawCalls + buffer->drawCallCount++;
        drawCall->offset = start;
        drawCall->count = buffer->count - start;
        drawCall->color = color;
        drawCall->texture = GetTexture(assetSystem, Texture_SolidWhite);
        drawCall->shaderAssetId = Shader_Text;
        drawCall->depth = depth;
    }
}

inline void DrawQuad(TextBuffer *buffer, AssetSystem *assetSystem, rect2 rect,
    vec4 color, f32 depth)
{
    vec2 p = rect.min;
    vec2 dims = rect.max - rect.min;
    DrawQuad(buffer, assetSystem, p, dims, color, depth);
}

internal void DrawTexturedQuad(TextBuffer *buffer, AssetSystem *assetSystem,
    rect2 rect, u32 texture, f32 depth, vec4 color = Vec4(1, 1, 1, 1))
{
    u32 start = buffer->count;
    vec2 p = rect.min;
    vec2 dims = rect.max - rect.min;
    AddQuadToTextBuffer(buffer, p, dims);
    if (buffer->drawCallCount < buffer->maxDrawCalls)
    {
        TextDrawCall *drawCall = buffer->drawCalls + buffer->drawCallCount++;
        drawCall->offset = start;
        drawCall->count = buffer->count - start;
        drawCall->color = color;
        drawCall->texture = GetTexture(assetSystem, texture);
        drawCall->shaderAssetId = Shader_UITexture;
        drawCall->depth = depth;
    }
}

internal void DrawProgressBar(TextBuffer *buffer, AssetSystem *assetSystem,
    rect2 rect, vec4 fgColor, vec4 bgColor, f32 t, f32 depth)
{
    t = Max(t, 0.0f);

    DrawQuad(buffer, assetSystem, rect, bgColor, depth);

    f32 width = rect.max.x - rect.min.x;
    rect2 progressRect = rect;
    progressRect.max.x = rect.min.x + t * width;
    DrawQuad(buffer, assetSystem, progressRect, fgColor, depth + 0.01f);
}
