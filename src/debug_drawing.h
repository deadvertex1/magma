#pragma once

struct DebugDrawingBuffer
{
    VertexPC *vertices;
    u32 count;
    u32 max;
};

struct DebugDrawingSystem
{
    enum
    {
        Shape_Line,
        Shape_Box,
        Shape_Point,
        Shape_Triangle,
        Shape_Capsule,
    };

    struct Line
    {
        vec3 start;
        vec3 end;
    };

    struct Box
    {
        vec3 min;
        vec3 max;
    };

    struct Point
    {
        vec3 position;
        f32 size;
    };

    struct Triangle
    {
        vec3 a;
        vec3 b;
        vec3 c;
    };

    struct Capsule
    {
        vec3 a;
        vec3 b;
        f32 radius;
    };

    struct Shape
    {
        u32 type;
        f32 timeRemaining;
        vec3 color;

        union
        {
            Line line;
            Box box;
            Point point;
            Triangle triangle;
            Capsule capsule;
        };
    };

    MemoryArena arena;

    ContiguousList shapes;
};

global DebugDrawingSystem *g_DebugDrawingSystem;

#ifdef ENABLE_GLOBAL_DEBUG_DRAWING
#define DRAW_LINE(START, END, COLOR, LIFETIME)                                 \
    DrawLine(g_DebugDrawingSystem, START, END, COLOR, LIFETIME)

#define DRAW_BOX(MIN, MAX, COLOR, LIFETIME)                                    \
    DrawBox(g_DebugDrawingSystem, MIN, MAX, COLOR, LIFETIME)

#define DRAW_POINT(POSITION, SIZE, COLOR, LIFETIME)                            \
    DrawPoint(g_DebugDrawingSystem, POSITION, SIZE, COLOR, LIFETIME)

#define DRAW_TRIANGLE(A, B, C, COLOR, LIFETIME)                                \
    DrawTriangle(g_DebugDrawingSystem, A, B, C, COLOR, LIFETIME)

#define DRAW_CAPSULE(A, B, RADIUS, COLOR, LIFETIME)                            \
    DrawCapsule(g_DebugDrawingSystem, A, B, RADIUS, COLOR, LIFETIME)

#else
#define DRAW_LINE(START, END, COLOR, LIFETIME)
#define DRAW_BOX(MIN, MAX, COLOR, LIFETIME)
#define DRAW_POINT(POSITION, SIZE, COLOR, LIFETIME)
#define DRAW_TRIANGLE(A, B, C, COLOR, LIFETIME)
#define DRAW_CAPSULE(A, B, RADIUS, COLOR, LIFETIME)
#endif
