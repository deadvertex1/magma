#pragma once

struct Bullet
{
    vec3 position;
    vec3 velocity;
    vec3 prevPosition;

    u32 owner;
    f32 lifeTime;
};

struct BulletSystem
{
    Bullet *bullets;
    u32 count;
    u32 capacity;
};
