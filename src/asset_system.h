#pragma once

enum
{
    AssetType_Mesh,
    AssetType_Texture,
    AssetType_Shader,
    AssetType_Font,
    AssetType_Material,
    AssetType_Scene,
    AssetType_Skeleton,
    AssetType_ComputeShader,
    MAX_ASSET_TYPES,
};

struct AssetLoadRequest
{
    u32 type;
    u32 id;
    u32 flags;
};

enum
{
    AssetLoadRequestFlags_None = 0x0,
    AssetLoadRequestFlags_ForceReload = 0x1,
};

struct FileCache;

struct AssetSystem
{
    MemoryArena arena;

    Dictionary tables[MAX_ASSET_TYPES];

    AssetLoadRequest loadQueue[16];
    u32 loadQueueLength;

    FileCache *fileCache;

    // FIXME: Replace this with an event system?
    b32 terrainShaderReloaded;
};

struct AssetSystemConfig
{
    u64 assetMemorySize;
    u32 maxTextures;
    u32 maxShaders;
    u32 maxFonts;
    u32 maxMeshes;
    u32 maxScenes;
    u32 maxMaterials;
    u32 maxSkeletons;
    u32 maxComputeShaders;
    FileCache *fileCache;
};

struct Asset_Texture
{
    u32 handle;
    const char *path;
    b32 isCubeMap;
    b32 useAlpha;
    b32 repeat;
};

struct Asset_Shader
{
    u32 handle;
    const char *vertexShaderPath;
    const char *fragmentShaderPath;
};

struct Asset_ComputeShader
{
    u32 handle;
    const char *path;
};

struct Font;
struct Asset_Font
{
    Font *data;
    const char *path;
    f32 size;
};

struct MeshState;
struct MeshData;
struct Asset_Mesh
{
    MeshState *data;
    MeshData *meshData;
    const char *path;
};

struct Asset_Scene
{
    u32 *meshes;
    u32 *materials;
    u32 count;
};

// Inputs for PBR Material model (nothing OpenGL specific here)
struct Asset_Material
{
    u32 shader; // Not sure if this should be here
    u32 albedo;
};

struct Skeleton;
struct SkeletonPose;
struct Asset_Skeleton
{
    Skeleton *data;
    SkeletonPose *bindPose;
    const char *path;
};
