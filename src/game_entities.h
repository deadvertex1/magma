#pragma once

const f32 g_DefaultCharacterControllerSpeed = 60.0f;
const f32 g_DefaultCharacterControllerFriction = 12.0f;

enum
{
    AiGoal_CollectPickups,
    AiGoal_Hunt,
    AiGoal_HarvestResources,
    AiGoal_DepositItems,
    AiGoal_BuildLumberCamp,
};

enum
{
    Interaction_Pickup,
    Interaction_Container,
    Interaction_CraftingStation,
};

enum
{
    Structure_Invalid,
    Structure_Foundation,
    Structure_Wall,
    Structure_Doorway,
    Structure_Ceiling,
    Structure_Stairs,
    Structure_Window,
};
