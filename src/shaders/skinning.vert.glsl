#extension GL_GOOGLE_include_directive : require

#include "skinned_vertex_format.vert.glsl"
#include "material_interface.vert.glsl"

void main()
{
    uint jointIndices[4];
    jointIndices[0] = (v_jointIndices & 0xFF000000) >> 24;
    jointIndices[1] = (v_jointIndices & 0x00FF0000) >> 16;
    jointIndices[2] = (v_jointIndices & 0x0000FF00) >> 8;
    jointIndices[3] = (v_jointIndices & 0x000000FF);

    float weights[4];
    weights[0] = v_jointWeights.x;
    weights[1] = v_jointWeights.y;
    weights[2] = v_jointWeights.z;
    weights[3] = 1.0f - (weights[0] + weights[1] + weights[2]);

    mat4 skinningMatrices[4];
    skinningMatrices[0] = matrixPalette[jointIndices[0]];
    skinningMatrices[1] = matrixPalette[jointIndices[1]];
    skinningMatrices[2] = matrixPalette[jointIndices[2]];
    skinningMatrices[3] = matrixPalette[jointIndices[3]];

    vec4 positions[4];
    positions[0] = skinningMatrices[0] * vec4(v_position, 1.0);
    positions[1] = skinningMatrices[1] * vec4(v_position, 1.0);
    positions[2] = skinningMatrices[2] * vec4(v_position, 1.0);
    positions[3] = skinningMatrices[3] * vec4(v_position, 1.0);

    vec3 p = positions[0].xyz;
    //vec4 p = positions[0] * weights[0] + positions[1] * weights[1] +
             //positions[2] * weights[2] + positions[3] * weights[3];

    StandardVertexShaderSetup(p, model_matrix);
}
