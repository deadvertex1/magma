#pragma once

void AssertWithinVec3(f32 delta, vec3 expected, vec3 actual);
void AssertWithinRangeVec3(vec3 min, vec3 max, vec3 actual);
void AssertWithinQuat(
    f32 delta, vec3 expectedAxis, f32 expectedAngle, quat actual);
