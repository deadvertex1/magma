#pragma once

// FNV-1a hash function see
// https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
inline u32 HashU32(const u8 *data, u32 length)
{
    // 32-bit values from
    // http://www.isthe.com/chongo/tech/comp/fnv/index.html
    u64 hash = 2166136261;
    u32 prime = 16777619;

    for (u32 byteIndex = 0; byteIndex < length; ++byteIndex)
    {
        hash = hash ^ data[byteIndex];
        hash = hash * prime;
    }

    return hash;
}

inline u32 HashStringU32(const char *str)
{
    u32 result = HashU32((const u8*)str, strlen(str));
    return result;
}
