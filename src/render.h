#pragma once

struct RenderCommand
{
    mat4 modelMatrix;

    u32 material;
    u32 mesh;
    u32 cameraIndex;

    u32 shadowMapTexture;
    u32 irradianceCubeMap;
};

struct RenderCommandBuffer
{
    RenderCommand *commands;
    u32 count;
    u32 max;
};

inline RenderCommandBuffer AllocateRenderCommandBuffer(
    MemoryArena *arena, u32 maxCommands)
{
    RenderCommandBuffer buffer = {};
    buffer.commands = ALLOCATE_ARRAY(arena, RenderCommand, maxCommands);
    buffer.max = maxCommands;

    return buffer;
}

inline void ClearRenderCommandBuffer(RenderCommandBuffer *buffer)
{
    buffer->count = 0;
}

inline RenderCommand *AllocateRenderCommand(RenderCommandBuffer *buffer)
{
    RenderCommand *result = NULL;
    if (buffer->count < buffer->max)
    {
        result = buffer->commands + buffer->count++;
    }

    return result;
}

inline mat4 BuildModelMatrix(Entity *entity)
{
    PROF_SCOPE();
    mat4 modelMatrix = Translate(entity->position) * Rotate(entity->rotation) *
                       Scale(entity->scale);
    return modelMatrix;
}

inline mat4 BuildInverseModelMatrix(Entity *entity)
{
    PROF_SCOPE();
    mat4 modelMatrix = Scale(Inverse(entity->scale)) *
                       Rotate(Conjugate(entity->rotation)) *
                       Translate(-entity->position);
    return modelMatrix;
}
