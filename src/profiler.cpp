internal ProfilingMetaDataSystem CreateProfilingMetaDataSystem(
    MemoryArena *arena, u32 capacity)
{
    ProfilingMetaDataSystem system = {};
    system.buffer = ALLOCATE_ARRAY(arena, ProfilingMetaData, capacity);
    system.capacity = capacity;
    system.hashTable = ALLOCATE_ARRAY(arena, ProfilingMetaData *, capacity);
    return system;
}

internal ProfilingEventsBuffer CreateProfilingEventsBuffer(
    MemoryArena *arena, u32 capacity)
{
    ProfilingEventsBuffer buffer = {};
    buffer.events = ALLOCATE_ARRAY(arena, ProfilingEvent, capacity);
    buffer.capacity = capacity;

    return buffer;
}

internal ProfilingSystem CreateProfilingSystem(
    MemoryArena *arena, ProfilingSystemConfig config)
{
    ProfilingSystem result = {};
    result.buffers =
        ALLOCATE_ARRAY(arena, ProfilingEventsBuffer, config.maxBuffers);
    result.bufferCount = config.maxBuffers;
    for (u32 i = 0; i < config.maxBuffers; i++)
    {
        result.buffers[i] =
            CreateProfilingEventsBuffer(arena, config.maxEventsPerBuffer);
    }
    result.metaData = CreateProfilingMetaDataSystem(arena, config.maxMetaData);
    return result;
}

internal u32 prof_ProcessEvents(ProfilingSystem *profiler, i32 readIndex,
    ProfiledBlock *blocks, u32 maxBlocks)
{
    u32 blockCount = 0;

    ProfilingEvent *stack[64];
    u32 stackSize = 0;

    if (readIndex == profiler->writeIndex)
    {
        readIndex = (profiler->writeIndex - 1) % profiler->bufferCount;
    }

    ASSERT(readIndex < (i32)profiler->bufferCount);
    ProfilingEventsBuffer *buffer = &profiler->buffers[readIndex];

    // Walk event log
    for (u32 i = 0; i < buffer->count; i++)
    {
        ProfilingEvent *event = buffer->events + i;
        switch (event->type)
        {
        case ProfilerEvent_BeginBlock:
            stack[stackSize++] = event;
            break;
        case ProfilerEvent_EndBlock:
        {
            ASSERT(stackSize > 0);
            ProfilingEvent *top = stack[stackSize - 1];
            if (event->metaDataGuid == top->metaDataGuid)
            {
                // Pop stack
                stackSize--;

                ProfiledBlock newBlock = {};
                newBlock.metaDataGuid = event->metaDataGuid;
                newBlock.cyclesElapsed = event->timestamp - top->timestamp;
                newBlock.callCount = 1;

                // Find existing block with same metaDataIndex
                ProfiledBlock *existingBlock = NULL;
                for (u32 j = 0; j < blockCount; j++)
                {
                    ProfiledBlock *block = blocks + j;
                    if (block->metaDataGuid == newBlock.metaDataGuid)
                    {
                        existingBlock = block;
                        break;
                    }
                }

                if (existingBlock != NULL)
                {
                    // Combine blocks
                    existingBlock->cyclesElapsed += newBlock.cyclesElapsed;
                    existingBlock->callCount++;
                }
                else
                {
                    // Allocate new block
                    ASSERT(blockCount < maxBlocks);
                    ProfiledBlock *block = blocks + blockCount++;
                    *block = newBlock;
                }
            }
            break;
        }
        default:
            break;
        }
    }

    return blockCount;
}

internal void prof_SwapBuffers(ProfilingSystem *system)
{
    // Swap profiling event buffers
    if (system->mode == ProfilingMode_Overwrite)
    {
        system->writeIndex = (system->writeIndex + 1) % system->bufferCount;
    }
    else if (system->mode == ProfilingMode_Fill)
    {
        // Don't change write index
        system->writeIndex =
            MinU32(system->writeIndex + 1, system->bufferCount);
    }
    else
    {
        INVALID_CODE_PATH("Unknown profiler mode");
    }

    // Clear new write buffer
    ProfilingEventsBuffer *buffer = system->buffers + system->writeIndex;
    buffer->count = 0;
}

internal void prof_Clear(ProfilingSystem *system)
{
    // Clear meta data system
    system->metaData.count = 0;
    memset(system->metaData.hashTable, 0,
        sizeof(ProfilingMetaData *) * system->metaData.capacity);

    // Clear event buffers
    for (u32 i = 0; i < system->bufferCount; i++)
    {
        system->buffers[i].count = 0;
    }
}
