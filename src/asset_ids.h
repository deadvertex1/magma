#pragma once

const u32 Texture_Missing = HashStringU32("textures/builtin/missing");
const u32 Texture_SolidWhite = HashStringU32("textures/builtin/solid_white");
const u32 Texture_SolidGreen = HashStringU32("textures/builtin/solid_green");
const u32 Texture_SolidRed = HashStringU32("textures/builtin/solid_red");
const u32 Texture_SolidOrange = HashStringU32("textures/builtin/solid_orange");
const u32 Texture_SolidBlue = HashStringU32("textures/builtin/solid_blue");
const u32 Texture_TerrainHeightMap =
    HashStringU32("textures/builtin/terrain_heightmap");
const u32 Texture_TerrainNormalMap =
    HashStringU32("textures/builtin/terrain_normal_map");
const u32 Texture_Grass = HashStringU32("textures/grass");
const u32 Texture_Bark = HashStringU32("textures/bark");
const u32 Texture_Concrete = HashStringU32("textures/concrete");
const u32 Texture_PrototypeGround = HashStringU32("textures/prototype_ground");
const u32 Texture_Skybox = HashStringU32("cube_maps/skybox");
// const u32 Texture_HDRI_CubeMap = HashStringU32("cube_maps/clarens_midday");
// const u32 Texture_HDRI_CubeMapIrradiance =
// HashStringU32("cube_maps/clarens_midday_irradiance");
const u32 Texture_ItemIcon_Wood = HashStringU32("textures/item_icons/wood");
const u32 Texture_ItemIcon_Meat = HashStringU32("textures/item_icons/meat");
const u32 Texture_ItemIcon_CookedMeat =
    HashStringU32("textures/item_icons/cooked_meat");
const u32 Texture_ItemIcon_Stone = HashStringU32("textures/item_icons/stone");
const u32 Texture_ItemIcon_Hatchet =
    HashStringU32("textures/item_icons/hatchet");
const u32 Texture_ItemIcon_Hammer = HashStringU32("textures/item_icons/hammer");
const u32 Texture_ItemIcon_WoodenBow =
    HashStringU32("textures/item_icons/wooden_bow");
const u32 Texture_ItemIcon_WoodenArrow =
    HashStringU32("textures/item_icons/wooden_arrow");
const u32 Texture_ItemIcon_Carrot = HashStringU32("textures/item_icons/carrot");
const u32 Texture_ItemIcon_WoodenSpear =
    HashStringU32("textures/item_icons/wooden_spear");
const u32 Texture_PreethamSky_CubeMap = HashStringU32("textures/preetham_sky");
const u32 Texture_PreethamSky_Irradiance =
    HashStringU32("textures/preetham_sky_irradiance");

const u32 Shader_Default = HashStringU32("shaders/default");
const u32 Shader_Particle = HashStringU32("shaders/particle");
const u32 Shader_CubeMap = HashStringU32("shaders/cube_map");
const u32 Shader_WorldTriplanar = HashStringU32("shaders/world_triplanar");
const u32 Shader_DebugDrawing = HashStringU32("shaders/debug_drawing");
const u32 Shader_Text = HashStringU32("shaders/text");
const u32 Shader_UITexture = HashStringU32("shaders/ui_texture");
const u32 Shader_PostProcessing = HashStringU32("shaders/post_processing");
const u32 Shader_MicrofacetBrdf = HashStringU32("shaders/microfacet_brdf");
const u32 Shader_Terrain = HashStringU32("shaders/terrain");
const u32 Shader_Skinning = HashStringU32("shaders/skinning");
const u32 Shader_Instancing = HashStringU32("shaders/instancing");

const u32 Font_Debug = HashStringU32("fonts/debug");
const u32 Font_FpsDisplay = HashStringU32("fonts/fps_display");

const u32 Mesh_Plane = HashStringU32("meshes/builtin/plane");
const u32 Mesh_Cube = HashStringU32("meshes/builtin/cube");
const u32 Mesh_TerrainPatch = HashStringU32("meshes/builtin/terrain_patch");
const u32 Mesh_Sphere = HashStringU32("meshes/sphere");
const u32 Mesh_Capsule = HashStringU32("meshes/capsule");
const u32 Mesh_Hatchet = HashStringU32("meshes/hatchet");
const u32 Mesh_PickAxe = HashStringU32("meshes/pickaxe");
const u32 Mesh_Hammer = HashStringU32("meshes/hammer");
const u32 Mesh_CraftingBench = HashStringU32("meshes/crafting_bench");
const u32 Mesh_WoodenBow = HashStringU32("meshes/wooden_bow");
const u32 Mesh_WoodenSpear = HashStringU32("meshes/wooden_spear");
const u32 Mesh_PineTree = HashStringU32("meshes/pine_tree");
// const u32 Mesh_Xbot = HashStringU32("meshes/xbot");

const u32 Skeleton_Xbot = HashStringU32("skeletons/xbot");

const u32 Material_Missing = HashStringU32("materials/builtin/missing");
const u32 Material_PrototypeTriplanar =
    HashStringU32("materials/prototype_triplanar");
const u32 Material_WoodPickup = HashStringU32("materials/wood_pickup");
const u32 Material_CarrotPickup = HashStringU32("materials/carrot_pickup");
const u32 Material_StonePickup = HashStringU32("materials/stone_pickup");
const u32 Material_Bark = HashStringU32("materials/bark");
const u32 Material_DeployableVisualization =
    HashStringU32("materials/deployable_visualization");
