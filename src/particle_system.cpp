inline vec3 RandomUnilateralVec3(RandomNumberGenerator *rng)
{
    vec3 result = Vec3(
        RandomUnilateral(rng), RandomUnilateral(rng), RandomUnilateral(rng));
    return result;
}

inline mat4 BuildParticleModelMatrix(vec3 position, vec3 cameraPosition)
{
    vec3 forward = Normalize(cameraPosition - position);
    vec3 right = Normalize(Cross(Vec3(0, 1, 0), forward));
    vec3 up = Normalize(Cross(forward, right));

    mat4 result;
    result.columns[0] = Vec4(right, 0.0f);
    result.columns[1] = Vec4(up, 0.0f);
    result.columns[2] = Vec4(forward, 0.0f);
    result.columns[3] = Vec4(position, 1.0f);

    return result;
}

internal void DrawParticleBuffer(
    ParticleBuffer *buffer, vec3 cameraPosition, RenderData *renderer)
{
    AssetSystem *assetSystem = renderer->assetSystem;
    ParticleSpec spec = buffer->spec;

    u32 shaderId = Shader_Particle;
    u32 textureId = HashStringU32("textures/particles/smoke");
    if (spec.flags & ParticleFlags_Mesh)
    {
        shaderId = Shader_Default;
        textureId = HashStringU32("textures/rock");
    }

    u32 shader = GetShader(assetSystem, shaderId);
    MeshState *mesh = GetMesh(assetSystem, spec.mesh);
    u32 texture = GetTexture(assetSystem, textureId);

    if (mesh != NULL && shader != 0 && texture != 0)
    {
        glDepthMask(GL_FALSE); // Disable depth write
        glUseProgram(shader);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, renderer->cameraUniformBuffer);

        glBindVertexArray(mesh->vertexArrayObject);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glUniform1i(0, 0);

        f32 minLifeTime = 6.0f;
        f32 maxLifeTime = 8.0f;

        for (u32 i = 0; i < buffer->count; i++)
        {
            vec3 p = buffer->positions[i];
            vec3 rotation = buffer->rotation[i];

            RandomNumberGenerator rng = {buffer->seed[i]};

            // Recreate the pseudo rng sequence to get initial lifetime
            f32 initialLifeTime =
                Lerp(minLifeTime, maxLifeTime, RandomUnilateral(&rng));

            // Compute t normalized t value for animation
            f32 t = 1.0f - Clamp(buffer->remainingLifeTime[i] / initialLifeTime,
                               0.0f, 1.0f);

            // Want a general polynomial equation
            // f32 y = [initial_min..initial_max] + [delta_min..delta_max] * t;

            f32 scale =
                Lerp(spec.scale[0], spec.scale[1], RandomUnilateral(&rng));
            if (spec.flags & ParticleFlags_UpdateScaleOverTime)
            {
                scale *= t;
            }

            mat4 modelMatrix = {};
            if (spec.flags & ParticleFlags_Mesh)
            {
                modelMatrix = Translate(p) * RotateZ(rotation.z) *
                              RotateY(rotation.y) * RotateX(rotation.x) *
                              Scale(Vec3(scale));
            }
            else
            {
                modelMatrix = BuildParticleModelMatrix(p, cameraPosition) *
                              RotateZ(rotation.z) * Scale(Vec3(scale));
            }

            glUniformMatrix4fv(1, 1, false, modelMatrix.data);
            glUniform1ui(2, 0); // camera index

            if (shaderId == Shader_Particle)
            {
                vec4 color = Vec4(
                    Vec3(0.1f * RandomUnilateral(&rng) + 0.05f), 1.0f - t * t);
                glUniform4fv(3, 1, color.data);
            }

            glDrawElements(
                GL_TRIANGLES, mesh->elementCount, GL_UNSIGNED_INT, 0);
        }

        glDepthMask(GL_TRUE);
    }
}

internal void DrawParticles(
    ParticleSystem *particleSystem, RenderData *renderer, vec3 cameraPosition)
{
    Dictionary *buffers = &particleSystem->buffers;

    for (u32 i = 0; i < buffers->count; i++)
    {
        ParticleBuffer *buffer =
            Dict_GetItemByIndex(buffers, ParticleBuffer, i);
        DrawParticleBuffer(buffer, cameraPosition, renderer);
    }
}

internal void InitializeParticleBufferRange(ParticleBuffer *buffer,
    RandomNumberGenerator *rng, vec3 origin, u32 offsetIndex, u32 count)
{
    ParticleSpec spec = buffer->spec;
    ASSERT(offsetIndex + count <= PARTICLE_BUFFER_LENGTH);

    f32 minLifeTime = 6.0f;
    f32 maxLifeTime = 8.0f;
    for (u32 i = offsetIndex; i < offsetIndex + count; i++)
    {
        buffer->seed[i] = XorShift32(rng);
        buffer->positions[i] = origin;
        buffer->velocity[i] = LerpVec3(spec.initialVelocity[0],
            spec.initialVelocity[1], RandomUnilateralVec3(rng));

        buffer->rotation[i] = Vec3(0);
        buffer->rotationalVelocity[i] =
            LerpVec3(spec.initialRotationalVelocity[0],
                spec.initialRotationalVelocity[1], RandomUnilateralVec3(rng));

        RandomNumberGenerator localRng = {buffer->seed[i]};
        buffer->remainingLifeTime[i] =
            Lerp(minLifeTime, maxLifeTime, RandomUnilateral(&localRng));
    }
}

internal void UpdateParticleBuffer(
    ParticleBuffer *buffer, RandomNumberGenerator *rng, f32 dt)
{
    ParticleSpec spec = buffer->spec;

    u32 deletionQueue[PARTICLE_BUFFER_LENGTH];
    u32 deletionQueueLength = 0;

    if ((spec.flags & ParticleFlags_Continuous) > 0 &&
        buffer->timeUntilNextSpawn <= 0.0f)
    {
        if (buffer->count < PARTICLE_BUFFER_LENGTH)
        {
            InitializeParticleBufferRange(
                buffer, rng, buffer->spec.origin, buffer->count, 1);
            buffer->count++;

            buffer->timeUntilNextSpawn =
                Lerp(0.1f, 0.4f, RandomUnilateral(rng));
        }
    }
    else
    {
        buffer->timeUntilNextSpawn -= dt;
    }

    for (u32 i = 0; i < buffer->count; i++)
    {
        buffer->velocity[i] += spec.acceleration * dt;
        buffer->positions[i] += buffer->velocity[i] * dt;

        // TODO: Rotational friction?
        buffer->rotation[i] += buffer->rotationalVelocity[i] * dt;
        buffer->remainingLifeTime[i] -= dt;

        if (buffer->remainingLifeTime[i] <= 0.0f)
        {
            deletionQueue[deletionQueueLength++] = i;
        }
    }

    for (u32 queueIndex = 0; queueIndex < deletionQueueLength; queueIndex++)
    {
        u32 i = deletionQueue[queueIndex];
        u32 last = buffer->count - 1;

        buffer->positions[i] = buffer->positions[last];
        buffer->velocity[i] = buffer->velocity[last];
        buffer->rotation[i] = buffer->rotation[last];
        buffer->rotationalVelocity[i] = buffer->rotationalVelocity[i];
        buffer->remainingLifeTime[i] = buffer->remainingLifeTime[last];
        buffer->seed[i] = buffer->seed[last];

        buffer->count--;
    }
}

internal void UpdateParticles(ParticleSystem *particleSystem, f32 dt)
{
    RandomNumberGenerator *rng = &particleSystem->rng;

    Dictionary *buffers = &particleSystem->buffers;
    for (u32 i = 0; i < buffers->count; i++)
    {
        ParticleBuffer *buffer =
            Dict_GetItemByIndex(buffers, ParticleBuffer, i);
        UpdateParticleBuffer(buffer, rng, dt);
    }
}

internal void InitializeParticleBuffer(
    ParticleBuffer *buffer, RandomNumberGenerator *rng, ParticleSpec spec)
{
    buffer->spec = spec;

    if (spec.flags & ParticleFlags_Continuous)
    {
        buffer->timeUntilNextSpawn = Lerp(0.2f, 1.2f, RandomUnilateral(rng));
    }
    else
    {
        buffer->count = 48;
        InitializeParticleBufferRange(
            buffer, rng, spec.origin, 0, buffer->count);
    }
}

internal u32 CreateParticleBuffer(
    ParticleSystem *particleSystem, ParticleSpec spec)
{
    u32 id = particleSystem->nextId;
    ParticleBuffer *buffer =
        Dict_AddItem(&particleSystem->buffers, ParticleBuffer, id);
    if (buffer != NULL)
    {
        memset(buffer, 0, sizeof(*buffer));
        InitializeParticleBuffer(buffer, &particleSystem->rng, spec);
        particleSystem->nextId++;
    }

    return id;
}

internal void DeleteParticleBuffer(ParticleSystem *particleSystem, u32 id)
{
    Dict_RemoveItem(&particleSystem->buffers, id);
}

internal void InitializeParticleSystem(ParticleSystem *particleSystem,
    MemoryArena *arena, u32 maxBuffers, u32 maxSpecs)
{
    particleSystem->rng.state = 0x898F234C;

    particleSystem->arena = SubAllocateArena(
        arena, (sizeof(ParticleBuffer) + sizeof(u32)) * maxBuffers +
                   (sizeof(ParticleSpec) + sizeof(u32)) * maxSpecs);
    particleSystem->buffers = Dict_CreateFromArena(
        &particleSystem->arena, ParticleBuffer, maxBuffers);
    particleSystem->nextId = 1;

    particleSystem->specs =
        Dict_CreateFromArena(&particleSystem->arena, ParticleSpec, maxSpecs);
}

internal ParticleSpec *FindParticleSpec(ParticleSystem *particleSystem, u32 id)
{
    ParticleSpec *spec =
        Dict_FindItem(&particleSystem->specs, ParticleSpec, id);
    return spec;
}

internal void RegisterParticleSpec(
    ParticleSystem *particleSystem, ParticleSpec spec, u32 id)
{
    ParticleSpec *data = Dict_AddItem(&particleSystem->specs, ParticleSpec, id);
    ASSERT(data)
    *data = spec;
}

internal void ClearParticleSpecs(ParticleSystem *particleSystem)
{
    Dict_Clear(&particleSystem->specs);
}
