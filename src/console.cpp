internal void UpdateConsole(
    ConsoleState *console, InputState *input, CommandSystem *commandSystem)
{
    // Text input
    for (u32 i = 0; i < input->textInputLength; i++)
    {
        // FIXME: Don't blindly cast unicode down to ASCII
        char c = (char)input->textInput[i];
        if (c != '`')
        {
            if (console->inputBufferLength < ARRAY_LENGTH(console->inputBuffer))
            {
                console->inputBuffer[console->inputBufferLength++] = c;
            }
        }
    }

    if (WasPressed(input->buttonStates[KEY_BACKSPACE]))
    {
        if (console->inputBufferLength > 0)
        {
            console->inputBufferLength -= 1;
        }
    }

    if (WasPressed(input->buttonStates[KEY_ENTER]))
    {
        QueueCommandForExecution(commandSystem, console->inputBuffer);
        LOG_MSG(console->inputBuffer);
        console->inputBufferLength = 0;
        memset(console->inputBuffer, 0, sizeof(console->inputBuffer));
    }
}

internal void DrawConsole(ConsoleState *console, RenderData *renderer,
    LogBuffer *logBuffer, TextBuffer *textBuffer, AssetSystem *assetSystem,
    vec2 topLeft, f32 framebufferWidth, f32 framebufferHeight)
{
    vec4 outputPanelColor = Vec4(0.2, 0.2, 0.2, 1);
    vec4 inputPanelColor = Vec4(0.15, 0.15, 0.16, 1);
    f32 panelDepth = 0.9f;
    f32 inputDepth = 0.95f;
    f32 lineGap = 2.0f;
    Font *debugFont = GetFont(renderer->assetSystem, Font_Debug);
    if (debugFont != NULL)
    {
        i32 maxLinesToDisplay = 20;
        f32 height =
            (debugFont->lineSpacing + lineGap) * (maxLinesToDisplay + 1);
        vec2 p = topLeft + Vec2(5, -5) + Vec2(0, -height);

        rect2 panelRect = Rect2(Vec2(0.0f, topLeft.y - height - 10),
            Vec2(framebufferWidth, framebufferHeight));
        DrawQuad(
            textBuffer, assetSystem, panelRect, outputPanelColor, panelDepth);

        // Draw output
        u32 count = LogBufferLength(logBuffer);
        if (count > 0)
        {
            i32 start = logBuffer->read + (count - 1);
            u32 linesToDisplay = Min(count - 1, maxLinesToDisplay);
            i32 end = start - linesToDisplay;
            for (i32 i = start; i >= end; i--)
            {
                u32 index = i % logBuffer->capacity;

                LogEntry entry = logBuffer->entries[index];
                DrawText(textBuffer, *debugFont, entry.text, p,
                    Vec4(1, 1, 1, 1), panelDepth + 0.01f);
                p.y += debugFont->lineSpacing + lineGap;
            }
        }

        // Draw input
        rect2 inputPanelRect = Rect2(
            panelRect.min - Vec2(0, debugFont->lineSpacing + lineGap * 4.0f),
            Vec2(framebufferWidth, panelRect.min.y));
        DrawQuad(textBuffer, assetSystem, inputPanelRect, inputPanelColor,
            inputDepth);

        const char *prompt = ">";
        char inputText[90];
        snprintf(inputText, sizeof(inputText), "%s %.*s", prompt,
            console->inputBufferLength, console->inputBuffer);
        DrawText(textBuffer, *debugFont, inputText,
            inputPanelRect.min + Vec2(5, lineGap * 2.0f), Vec4(1),
            inputDepth + 0.01f);
    }
}

