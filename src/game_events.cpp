internal void InitializeEventQueue(
    GameEventQueue *queue, MemoryArena *arena, u32 capacity)
{
    memset(queue, 0, sizeof(*queue));
    queue->buffer = ALLOCATE_ARRAY(arena, GameEvent, capacity);
    queue->capacity = capacity;
}

internal void HandleInteractionEvent(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, ItemDatabase *itemDatabase,
    PlayerUIState *playerUIState, InteractionEvent *event)
{
    Entity *entity = FindEntityById(entityWorld, event->dstEntityId);
    if (entity != NULL)
    {
        if (entity->flags & EntityFlags_Interactable)
        {
            LOG_MSG("Entity %u interacted with entity %u "
                    "type: %u",
                event->srcEntityId, event->dstEntityId,
                entity->interactionType);

            if (entity->interactionType == Interaction_Pickup)
            {
                QueueEntityForDeletion(entityWorld, event->dstEntityId);

                AddItemToInventory(entityWorld, inventorySystem, itemDatabase,
                    event->srcEntityId, entity->itemId, entity->quantity);
            }
            else if (entity->interactionType == Interaction_Container)
            {
                playerUIState->showInventory = true;
                playerUIState->containerEntityId = event->dstEntityId;
            }
            else if (entity->interactionType == Interaction_CraftingStation)
            {
                playerUIState->showInventory = true;
                playerUIState->craftingStationEntityId = event->dstEntityId;
            }
        }
    }
}

internal void HandleReceiveItemEvent(EntityWorld *entityWorld,
    InventorySystem *inventorySystem, ItemDatabase *itemDatabase,
    ReceiveItemEvent *event)
{
    AddItemToInventory(entityWorld, inventorySystem, itemDatabase,
        event->dstEntityId, event->itemId, event->quantity);
}

internal void HandleHarvestResourceEvent(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, InventorySystem *inventorySystem,
    ItemDatabase *itemDatabase, HarvestResourceEvent *event)
{
    Entity *entity = FindEntityById(entityWorld, event->entityId);
    if (entity != NULL)
    {
        // TODO: Want to delay all this so that it syncs up with the impact
        // in the view model animation -> Make this part of the harvest
        // event
        // TODO: Do we make this async, i.e. as a command
        f32 range = 3.0f;
        GetEntityBeingLookedAtResult result =
            GetEntityBeingLookedAt(entityWorld, collisionWorld, entity, range,
                EntityFlags_Harvestable);

        if (result.entityId != NULL_ENTITY_ID)
        {
            Entity *hitEntity = FindEntityById(entityWorld, result.entityId);
            if (hitEntity != NULL)
            {
                // Logic for checking if we can harvest this resource with
                // activeItemId
                u32 amount = 0;
                u32 particleEffectId =
                    HashStringU32("particles/smoke_one_shot");
                if (event->toolItemId == Item_Hatchet &&
                    hitEntity->itemId == Item_Wood)
                {
                    amount = 5;
                    particleEffectId = HashStringU32("particles/splinters");
                }
                else if (event->toolItemId == Item_PickAxe &&
                         hitEntity->itemId == Item_MetalOre)
                {
                    amount = 10;
                }
                else if (event->toolItemId == Item_PickAxe &&
                         hitEntity->itemId == Item_Stone)
                {
                    amount = 10;
                }

                amount = MinU32(amount, hitEntity->quantity);
                if (amount > 0)
                {
                    AddItemToInventory(entityWorld, inventorySystem,
                        itemDatabase, event->entityId, hitEntity->itemId,
                        amount);

                    hitEntity->quantity -= amount;

                    LOG_MSG("Entity %u harvested %u from entity %u - %u "
                            "remaining",
                        event->entityId, amount, result.entityId,
                        hitEntity->quantity);

                    // TODO: What particle effect is this?
                    AddParticleEmitter(
                        entityWorld, result.position, particleEffectId, 10.0f);
                }

                if (hitEntity->quantity == 0)
                {
                    QueueEntityForDeletion(entityWorld, result.entityId);
                }

                entity->weaponController.screenShakeAmount = 1.0f;
            }
        }
    }
}

internal void HandlePlayAnimationEvent(
    EntityWorld *entityWorld, f32 currentTime, PlayAnimationEvent *event)
{
    Entity *entity = FindEntityById(entityWorld, event->entityId);
    if (entity != NULL)
    {
        entity->weaponController.animationStartTime = currentTime;
        entity->weaponController.isAnimationPlaying = true;

        if (event->applyScreenShake)
        {
            entity->weaponController.screenShakeAmount = 0.2f;
        }
    }
}

internal void HandleSpawnBulletEvent(EntityWorld *entityWorld,
    BulletSystem *bulletSystem, SpawnBulletEvent *event)
{
    CreateBullet(bulletSystem, event->position, event->velocity, event->owner);
}

internal void HandleBulletImpactEvent(
    EntityWorld *entityWorld, BulletImpactEvent *event)
{
    // TODO: Apply damage
    u32 particleEffectId = HashStringU32("particles/smoke_one_shot");
    AddParticleEmitter(entityWorld, event->position, particleEffectId, 10.0f);
}

internal void ProcessEventQueue(GameState *gameState)
{
    EntityWorld *entityWorld = &gameState->entityWorld;
    InventorySystem *inventorySystem = &gameState->inventorySystem;
    CollisionWorld *collisionWorld = &gameState->collisionWorld;
    ItemDatabase *itemDatabase = &gameState->itemDatabase;
    PlayerUIState *playerUIState = &gameState->playerUIState;
    GameEventQueue *eventQueue = &gameState->eventQueue;
    BulletSystem *bulletSystem = &gameState->bulletSystem;

    for (u32 i = 0; i < eventQueue->length; i++)
    {
        GameEvent *event = eventQueue->buffer + i;
        switch (event->type)
        {
        case EventType_HarvestResource:
            HandleHarvestResourceEvent(entityWorld, collisionWorld,
                inventorySystem, itemDatabase, &event->harvestResource);
            break;
        case EventType_TransferItem:
            // TODO
            break;
        case EventType_ReceiveDamage:
            break;
        case EventType_Interaction:
            HandleInteractionEvent(entityWorld, inventorySystem, itemDatabase,
                playerUIState, &event->interaction);
            break;
        case EventType_ReceiveItem:
            HandleReceiveItemEvent(entityWorld, inventorySystem, itemDatabase,
                &event->receiveItem);
            break;
        case EventType_ConsumeFood:
            // TODO
            break;
        case EventType_PlaceDeployable:
            // TODO
            break;
        case EventType_PlayAnimation:
            HandlePlayAnimationEvent(
                entityWorld, gameState->currentTime, &event->playAnimation);
            break;
        case EventType_SpawnBullet:
            HandleSpawnBulletEvent(
                entityWorld, bulletSystem, &event->spawnBullet);
            break;
        case EventType_BulletImpact:
            HandleBulletImpactEvent(entityWorld, &event->bulletImpact);
            break;
        default:
            LOG_MSG("Unknown event type %u", event->type);
            break;
        }
    }
    eventQueue->length = 0;
}
