internal void InitializeScatterSystem(
    ScatterSystem *system, MemoryArena *arena, ScatterSystemConfig config)
{
    memset(system, 0, sizeof(*system));

    system->arena = SubAllocateArena(arena, config.arenaSize);

    system->scatterSpecs = Dict_CreateFromArena(
        &system->arena, ScatterSpec, config.maxScatterSpecs);

    system->volumes =
        ALLOCATE_ARRAY(&system->arena, ScatterVolume, config.maxVolumes);
    system->volumeCapacity = config.maxVolumes;

    system->rng.state = config.seed;
}

internal void AddScatterVolume(ScatterSystem *scatterSystem, vec3 position,
    vec3 min, vec3 max, u32 count, const char *specName)
{
    if (scatterSystem->volumeCount < scatterSystem->volumeCapacity)
    {
        ScatterVolume *volume =
            scatterSystem->volumes + scatterSystem->volumeCount++;
        memset(volume, 0, sizeof(*volume));

        volume->min = position + min;
        volume->max = position + max;
        volume->count = count;
        volume->specId = HashStringU32(specName);
    }
    else
    {
        LOG_MSG("Failed to allocate scatter volume");
    }
}

internal b32 PickRandomEntityDefinition(EntityDefinition *entityDef,
    EntityWorld *entityWorld, ScatterSpec *spec, RandomNumberGenerator *rng)
{
    ASSERT(spec->entityDefinitionsCount > 0);
    ASSERT(
        spec->entityDefinitionsCount <= ARRAY_LENGTH(spec->entityDefinitions));

    // Choose random entity definition ID from spec
    u32 index = spec->entityDefinitionsCount > 1
                    ? XorShift32(rng) % spec->entityDefinitionsCount
                    : 0;
    ASSERT(index < ARRAY_LENGTH(spec->entityDefinitions));
    u32 entityDefId = spec->entityDefinitions[index];

    // Find entity definition for that ID
    b32 result = FindEntityDefinitionByID(entityWorld, entityDefId, entityDef);
    return result;
}

internal void ScatterEntitiesInVolume(EntityWorld *entityWorld,
    CollisionWorld *collisionWorld, RandomNumberGenerator *rng, vec3 min,
    vec3 max, u32 count, ScatterSpec spec)
{
    PROF_SCOPE();

    for (u32 i = 0; i < count; i++)
    {
        // Random point in quad defined by spec.aabb
        f32 x = Lerp(min.x, max.x, RandomUnilateral(rng));
        f32 z = Lerp(min.z, max.z, RandomUnilateral(rng));

        vec3 rayOrigin = Vec3(x, max.y, z);
        vec3 rayDirection = Vec3(0, -1, 0);
        RayIntersectWorldResult rayIntersection = RayIntersectCollisionWorld(
            collisionWorld, rayOrigin, rayDirection, 1000.0f);

        if (rayIntersection.t >= 0.0f)
        {
            vec3 p = rayOrigin + rayIntersection.t * rayDirection;

            // TODO: Spec
            quat rotation =
                Quat(Vec3(1, 0, 0), PI * 0.04f * RandomBilateral(rng)) *
                Quat(Vec3(0, 1, 0), PI * RandomBilateral(rng));

            f32 scale =
                spec.maxScale > 0.0f
                    ? Lerp(spec.minScale, spec.maxScale, RandomUnilateral(rng))
                    : 1.0f;

            EntityDefinition entityDef = {};
            if (PickRandomEntityDefinition(&entityDef, entityWorld, &spec, rng))
            {
                // Override the transform values for the instance
                PUSH_VALUE(&entityDef, position, p + spec.positionOffset);
                PUSH_VALUE(&entityDef, rotation, rotation);
                PUSH_VALUE(&entityDef, scale, Vec3(scale));

                if (spec.maxTimeUntilMatured > 0.0f)
                {
                    f32 timeUntilMatured = Lerp(spec.minTimeUntilMatured,
                        spec.maxTimeUntilMatured, RandomUnilateral(rng));
                    PushValueF32(&entityDef, OffsetOf(Entity, timeUntilMatured),
                        timeUntilMatured);
                }

                // Create entity from definition instance
                u32 entityId =
                    CreateEntityFromDefinition(entityWorld, &entityDef);
                if (entityId == NULL_ENTITY_ID)
                {
                    LOG_MSG("Failed to scatter entity - Failed to create "
                            "entity from definition");
                }
            }
            else
            {
                LOG_MSG("Failed to scatter entity - PickRandomEntityDefinition "
                        "failed");
            }
        }
        else
        {
            LOG_MSG("Failed to scatter entity from (%g, %g, %g) as it is "
                    "not on the terrain",
                rayOrigin.x, rayOrigin.y, rayOrigin.z);
        }
    }
}

internal void ScatterEntities(ScatterSystem *scatterSystem,
    EntityWorld *entityWorld, CollisionWorld *collisionWorld)
{
    for (u32 i = 0; i < scatterSystem->volumeCount; i++)
    {
        ScatterVolume *volume = scatterSystem->volumes + i;

        ScatterSpec *spec = Dict_FindItem(
            &scatterSystem->scatterSpecs, ScatterSpec, volume->specId);
        if (spec)
        {
            ScatterEntitiesInVolume(entityWorld, collisionWorld,
                &scatterSystem->rng, volume->min, volume->max, volume->count,
                *spec);
        }
        else
        {
            LOG_MSG("Unable to find scatter spec %u", volume->specId);
        }
    }
}

internal void DebugDrawScatterVolumes(
    ScatterSystem *scatterSystem, DebugDrawingBuffer *debugDrawingBuffer)
{
    for (u32 i = 0; i < scatterSystem->volumeCount; i++)
    {
        ScatterVolume *volume = scatterSystem->volumes + i;

        DrawBox(
            debugDrawingBuffer, volume->min, volume->max, Vec3(1, 0.8, 0.2));
    }
}

internal void ResetScatterSystem(ScatterSystem *scatterSystem)
{
    Dict_Clear(&scatterSystem->scatterSpecs);
    scatterSystem->volumeCount = 0;
}

internal ScatterSpec *AddScatterSpec(ScatterSystem *scatterSystem,
    const char *name, f32 minScale = 0.0f, f32 maxScale = 0.0f)
{
    ScatterSpec *spec = Dict_AddItem(
        &scatterSystem->scatterSpecs, ScatterSpec, HashStringU32(name));
    ASSERT(spec != NULL);
    if (spec != NULL)
    {
        memset(spec, 0, sizeof(*spec));
        spec->minScale = minScale;
        spec->maxScale = maxScale;
    }

    return spec;
}

internal ScatterSpec *FindScatterSpecByName(
    ScatterSystem *scatterSystem, const char *name)
{
    ScatterSpec *spec = Dict_FindItem(
        &scatterSystem->scatterSpecs, ScatterSpec, HashStringU32(name));
    return spec;
}
