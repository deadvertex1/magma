#!/bin/bash

compiler="clang++"
CXXFLAGS="-g -O0"

# Duplicated with ./build.sh
cflags="-O0 -g -Wall -Werror -Wextra -Wno-missing-braces -Wno-unused-parameter -Wno-unused-variable -Wno-unused-function -Wno-unused-but-set-variable"

mkdir -p build

# Buid unity test frame work
if [ ! -e build/unity.o ]; then
    $compiler thirdparty/unity/unity.c -o build/unity.o -c -g -O0 -I thirdparty/unity 
fi

if [ ! -e build/unity_fixture.o ]; then
    $compiler thirdparty/unity_fixture/unity_fixture.c -o build/unity_fixture.o -c -DUNITY_FIXTURE_NO_EXTRAS -g -O0 -I thirdparty/unity -I thirdparty/unity_fixture 
fi

# Build and then run unit tests
$compiler tests/*.cpp \
    -o build/test_runner \
    -I thirdparty/unity \
    -I thirdparty/unity_fixture \
    -I src \
    -DUNITY_FIXTURE_NO_EXTRAS \
    $cflags \
    build/unity.o build/unity_fixture.o && \
    build/test_runner
