internal SkeletonJointPose *BuildGlobalPose(
    SkeletonPose pose, MemoryArena *arena)
{
    ASSERT(pose.skeleton);
    u32 count = pose.skeleton->count;
    SkeletonJoint *joints = pose.skeleton->joints;

    SkeletonJointPose *result = ALLOCATE_ARRAY(arena, SkeletonJointPose, count);

    // NOTE: Array is in depth-first order which ensures that a parent node
    // will always have a lower index than its child node.
    for (u32 i = 0; i < count; ++i)
    {
        SkeletonJointPose *local = pose.localPose + i;

        SkeletonJoint *joint = joints + i;
        if (joint->parent == -1)
        {
            // root node
            result[i] = *local;
        }
        else
        {
            // child node
            SkeletonJointPose *parentGlobal = result + joint->parent;
            result[i] = ConcatJointPose(*local, *parentGlobal);
        }
    }

    return result;
}

inline mat4 BuildModelMatrix(vec3 translation, quat rotation, vec3 scaling)
{
    mat4 modelMatrix =
        Translate(translation) * Rotate(rotation) * Scale(scaling);
    return modelMatrix;
}

internal mat4 *BuildGlobalMatrices(SkeletonPose pose, MemoryArena *arena)
{
    ASSERT(pose.skeleton);
    u32 count = pose.skeleton->count;
    SkeletonJoint *joints = pose.skeleton->joints;

    mat4 *result = ALLOCATE_ARRAY(arena, mat4, count);

    // NOTE: Array is in depth-first order which ensures that a parent node
    // will always have a lower index than its child node.
    for (u32 i = 0; i < count; ++i)
    {
        SkeletonJointPose *local = pose.localPose + i;

        SkeletonJoint *joint = joints + i;
        if (joint->parent == -1)
        {
            // root node
            result[i] = BuildModelMatrix(
                local->translation, local->rotation, Vec3(local->scaling));
        }
        else
        {
            // child node
            mat4 *parentGlobal = result + joint->parent;

            mat4 m = BuildModelMatrix(
                local->translation, local->rotation, Vec3(local->scaling));
            result[i] = (*parentGlobal) * m;
        }
    }

    return result;
}

internal SkeletonPose CreateSkeletonPose(Skeleton *skeleton, MemoryArena *arena)
{
    SkeletonPose pose = {};
    pose.skeleton = skeleton;
    pose.localPose = ALLOCATE_ARRAY(arena, SkeletonJointPose, skeleton->count);

    return pose;
}
