internal b32 EvaluateMoveToPositionAction(vec3 currentPosition,
    vec3 targetPosition, f32 targetDistance, AiCommand *command)
{
    // NOTE: Only using 2D position for now
    targetPosition.y = 0.0f;
    currentPosition.y = 0.0f;

    b32 targetReached = LengthSq(currentPosition - targetPosition) <=
                        targetDistance * targetDistance;
    if (targetReached)
    {
        // Stop moving when we reach our target position
        command->speed = 0.0f;
    }
    else
    {
        vec3 delta = targetPosition - currentPosition;
        delta.y = 0.0f;
        command->targetDir = Normalize(delta);

        command->speed = 1.0f; // Move at max speed
    }

    return targetReached;
}

internal ExecutePlanResult ExecutePlan(
    ActionPlan plan, u32 currentStepIdx, AiState state)
{
    ExecutePlanResult result = {};
    result.nextStepIdx = currentStepIdx;

    if (currentStepIdx < plan.length)
    {
        ActionPlanStep currentStep = plan.steps[currentStepIdx];

        vec3 currentPosition = state.position;

        b32 advanceToNextStep = false;
        switch (currentStep.action)
        {
        case Action_MoveToPosition:
            advanceToNextStep = EvaluateMoveToPositionAction(currentPosition,
                currentStep.moveToPosition, currentStep.targetDistance,
                &result.command);
            break;
        case Action_InteractWithEntity:
            result.command.targetEntity = currentStep.targetEntity;
            result.command.action = AiCommandAction_Interact;
            advanceToNextStep = true;
            break;
        case Action_AttackEntity:
            result.command.targetEntity = currentStep.targetEntity;
            result.command.action = AiCommandAction_Attack;
            advanceToNextStep = true;
            break;
        case Action_TransferItems:
            result.command.targetEntity = currentStep.targetEntity;
            result.command.action = AiCommandAction_DepositInventory;
            advanceToNextStep = true;
            break;
        case Action_PlaceDeployable:
            result.command.action = AiCommandAction_PlaceDeployable;
            advanceToNextStep = true;
            break;
        case Action_ClearGoal:
            result.updateGoal = true;
            result.newGoal = currentStep.newGoal;
            advanceToNextStep = true;
            break;
        default:
            LOG_MSG("Unknown action: %u", currentStep.action);
            break;
        }

        if (advanceToNextStep)
        {
            result.nextStepIdx = MinU32(currentStepIdx + 1, plan.length);
        }
    }

    return result;
}

internal void InitializeAiPlanningSystem(
    AiPlanningSystem *planningSystem, MemoryArena *arena, u32 maxPlans)
{
    memset(planningSystem, 0, sizeof(*planningSystem));
    planningSystem->entries =
        Dict_CreateFromArena(arena, AiPlanningState, maxPlans);
    planningSystem->nextId = 1;
}

inline void PushPlanStep(ActionPlan *plan, ActionPlanStep step)
{
    ASSERT(plan->length < ARRAY_LENGTH(plan->steps));
    plan->steps[plan->length++] = step;
}

internal ActionPlan BuildWanderPlan(
    AiState currentState, RandomNumberGenerator *rng)
{
    ActionPlan plan = {};
    ActionPlanStep moveStep = {};
    moveStep.action = Action_MoveToPosition;
    moveStep.moveToPosition =
        currentState.position +
        Vec3(40.0f * RandomBilateral(rng), 0.0f, 40.0f * RandomBilateral(rng));
    moveStep.targetDistance = 5.0f; // Just guess close enough
    PushPlanStep(&plan, moveStep);

    return plan;
}

internal ActionPlan BuildPlan(AiState currentState, u32 goal,
    u32 *sensedEntityIds, vec3 *sensedEntityPositions, u32 *sensedEntityTypes,
    u32 sensedEntityCount, RandomNumberGenerator *rng)
{
    ActionPlan plan = {};

    if (goal == AiGoal_CollectPickups)
    {
        // Pick closest item pickup
        i32 closest = FindClosest(
            currentState.position, sensedEntityPositions, sensedEntityCount);
        if (closest >= 0)
        {
            ActionPlanStep moveStep = {};
            moveStep.action = Action_MoveToPosition;
            moveStep.moveToPosition = sensedEntityPositions[closest];
            moveStep.targetDistance = 1.0f; // FIXME: Need an interaction range
            PushPlanStep(&plan, moveStep);

            ActionPlanStep interactStep = {};
            interactStep.action = Action_InteractWithEntity;
            interactStep.targetEntity = sensedEntityIds[closest];
            PushPlanStep(&plan, interactStep);
        }
        else
        {
            plan = BuildWanderPlan(currentState, rng);
        }
    }
    else if (goal == AiGoal_Hunt || goal == AiGoal_HarvestResources)
    {
        // Pick closest item pickup
        i32 closest = FindClosest(
            currentState.position, sensedEntityPositions, sensedEntityCount);
        if (closest >= 0)
        {
            ActionPlanStep moveStep = {};
            moveStep.action = Action_MoveToPosition;
            moveStep.moveToPosition = sensedEntityPositions[closest];
            moveStep.targetDistance = 1.0f; // FIXME: Need a target attack range
            PushPlanStep(&plan, moveStep);

            ActionPlanStep attackStep = {};
            attackStep.action = Action_AttackEntity;
            attackStep.targetEntity = sensedEntityIds[closest];
            PushPlanStep(&plan, attackStep);
        }
        else
        {
            // None found so wander instead
            plan = BuildWanderPlan(currentState, rng);
        }
    }
    else if (goal == AiGoal_DepositItems)
    {
        i32 closest = FindClosest(
            currentState.position, sensedEntityPositions, sensedEntityCount);
        if (closest >= 0)
        {
            ActionPlanStep moveStep = {};
            moveStep.action = Action_MoveToPosition;
            moveStep.moveToPosition = sensedEntityPositions[closest];
            moveStep.targetDistance = 1.0f; // Interaction range
            PushPlanStep(&plan, moveStep);

            ActionPlanStep interactStep = {};
            interactStep.action = Action_TransferItems;
            interactStep.targetEntity = sensedEntityIds[closest];
            PushPlanStep(&plan, interactStep);
        }
        else
        {
            // None found so wander instead
            plan = BuildWanderPlan(currentState, rng);
        }
    }
    else if (goal == AiGoal_BuildLumberCamp)
    {
        // TODO: Better system for finding where to place lumber camps
        i32 closest = FindClosest(
            currentState.position, sensedEntityPositions, sensedEntityCount);
        if (closest >= 0)
        {
            ActionPlanStep moveStep = {};
            moveStep.action = Action_MoveToPosition;
            moveStep.moveToPosition = sensedEntityPositions[closest];
            moveStep.targetDistance = 5.0f;
            PushPlanStep(&plan, moveStep);

            ActionPlanStep buildStep = {};
            buildStep.action = Action_PlaceDeployable;
            PushPlanStep(&plan, buildStep);

            ActionPlanStep clearGoalStep = {};
            clearGoalStep.action = Action_ClearGoal;
            clearGoalStep.newGoal = AiGoal_HarvestResources;
            PushPlanStep(&plan, clearGoalStep);
        }
        else
        {
            // None found so wander instead
            plan = BuildWanderPlan(currentState, rng);
        }
    }
    return plan;
}

internal u32 StoreActionPlan(
    AiPlanningSystem *planningSystem, AiPlanningState state)
{
    // TODO: Handle wrapping?
    u32 id = planningSystem->nextId;

    AiPlanningState *dst =
        Dict_AddItem(&planningSystem->entries, AiPlanningState, id);
    if (dst != NULL)
    {
        *dst = state;
        planningSystem->nextId++;
    }
    else
    {
        LOG_MSG("Failed to store planning state");
    }

    return id;
}

internal void DumpPlan(ActionPlan plan)
{
    for (u32 i = 0; i < plan.length; i++)
    {
        ActionPlanStep step = plan.steps[i];
        const char *stepName = "Action_None";
        switch (step.action)
        {
        case Action_MoveToPosition:
            stepName = "Action_MoveToPosition";
            break;
        case Action_InteractWithEntity:
            stepName = "Action_InteractWithEntity";
            break;
        case Action_ObserveAndPlan:
            stepName = "Action_ObserveAndPlan";
            break;
        case Action_AttackEntity:
            stepName = "Action_AttackEntity";
            break;
        case Action_TransferItems:
            stepName = "Action_TransferItems";
            break;
        case Action_PlaceDeployable:
            stepName = "Action_PlaceDeployable";
            break;
        case Action_ClearGoal:
            stepName = "Action_ClearGoal";
            break;
        default:
            break;
        }

        LOG_MSG("Step %u : %s", i, stepName);
        switch (step.action)
        {
        case Action_MoveToPosition:
            LOG_MSG("\tmoveToPosition: [%g, %g, %g]", step.moveToPosition.x,
                step.moveToPosition.y, step.moveToPosition.z);
            LOG_MSG("\ttargetDistance: %g", step.targetDistance);
            break;
        case Action_InteractWithEntity:
        case Action_AttackEntity:
        case Action_TransferItems:
            LOG_MSG("\ttargetEntity: %u", step.targetEntity);
            break;
        case Action_ObserveAndPlan:
        case Action_PlaceDeployable:
            break;
        case Action_ClearGoal:
            LOG_MSG("\tnewGoal: %u", step.newGoal);
            break;
        default:
            break;
        }
    }
}
