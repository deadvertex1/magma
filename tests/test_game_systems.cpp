#include <cmath>
#include <cfloat>  // For FLT_EPSILON
#include <cstring> // For memcpy

#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "dictionary.h"
#include "hash.h"
#include "math_utils.h"
#include "math_lib.h"
#include "entity_world.h"
#include "ai.h"
#include "aabb_tree.h"
#include "collision_world.h"
#include "assertions.h"
#include "asset_ids.h"

#define PROF_SCOPE()
#define LOG_MSG(...)

internal RayIntersectWorldResult RayIntersectCollisionWorld(
    CollisionWorld *collisionWorld, vec3 rayOrigin, vec3 rayDirection, f32 tmax,
    u32 ignoreId = 0, u32 flags = 0)
{
    RayIntersectWorldResult result = {};
    result.t = 0.1f;
    return result;
}

#include "game_entities.h"
#include "game_scatter_system.h"
#include "inventory.h"

#include "entity_world.cpp"
#include "game_entities.cpp"
// #include "game_systems.cpp"

TEST_GROUP(GameSystems);

global MemoryArena g_Arena;

TEST_SETUP(GameSystems)
{
    u32 size = Kilobytes(64);
    InitializeMemoryArena(&g_Arena, malloc(size), size);
}

TEST_TEAR_DOWN(GameSystems) { free(g_Arena.base); }

internal EntityWorldConfig CreateTestEntityWorldConfig()
{
    EntityWorldConfig config = {};
    config.worldArenaSize = Kilobytes(32);
    config.maxEntities = 32;
    config.maxEntityDefinitions = 4;
    config.deletionQueueCapacity = 2;
    return config;
}

#if 0
TEST(GameSystems, ScatterVolumes)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);
    REGISTER_COMPONENT(&entityWorld, TransformComponent, config.maxEntities);
    REGISTER_COMPONENT(
        &entityWorld, ScatterVolumeComponent, config.maxEntities);

    EntityDefinition entityDefinition = {};
    PUSH_COMPONENT(&entityDefinition, TransformComponent);
    RegisterEntityDefinition(&entityWorld, "entity0", &entityDefinition);

    CollisionWorld collisionWorld = {};

    RandomNumberGenerator rng = {1234};

    u32 scatterSpecId = 1234;
    Dictionary scatterSpecs = Dict_CreateFromArena(&g_Arena, ScatterSpec, 2);
    ScatterSpec *spec = Dict_AddItem(&scatterSpecs, ScatterSpec, scatterSpecId);
    spec->minScale = 0.8f;
    spec->maxScale = 1.35f;
    spec->entityDefinitionsCount = 1;
    spec->entityDefinitions[0] = HashStringU32("entity0");

    // Given a scatter volume with a registered scatter spec
    u32 entityId = AllocateEntityId(&entityWorld);
    TransformComponent transform = {};
    transform.position = Vec3(5, 0, 5);
    ADD_COMPONENT(&entityWorld, TransformComponent, entityId, &transform);

    ScatterVolumeComponent volume = {};
    volume.min = Vec3(5.0f);
    volume.max = Vec3(10.0f);
    volume.scatterSpecId = scatterSpecId;
    volume.targetCount = 8;
    ADD_COMPONENT(&entityWorld, ScatterVolumeComponent, entityId, &volume);

    // When we run the scatter volume system on startup
    RunScatterVolumeSystem(
        &entityWorld, &collisionWorld, &rng, scatterSpecs, true, 0.0f);

    // Then we spawn the expected number of entities in the volume
    Dictionary *transforms =
        GetComponentTable(&entityWorld, TransformComponent_TypeID);

    TEST_ASSERT_EQUAL_UINT(9, transforms->count);
    for (u32 i = 0; i < transforms->count; i++)
    {
        if (transforms->keys[i] != entityId)
        {
            TransformComponent scatterTransform = {};
            GET_COMPONENT(&entityWorld, TransformComponent, transforms->keys[i],
                &scatterTransform);

            AssertWithinRangeVec3(transform.position + volume.min,
                transform.position + volume.max, scatterTransform.position);
        }
    }
}

TEST_GROUP_RUNNER(GameSystems) { RUN_TEST_CASE(GameSystems, ScatterVolumes); }
#endif
