internal void DebugPrintf(
    DebugPrintSystem *system, vec4 color, const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    // Allocate entry
    if (system->count < ARRAY_LENGTH(system->entries))
    {
        DebugPrintEntry *entry = system->entries + system->count++;
        u32 len = vsnprintf(entry->text, sizeof(entry->text), fmt, args);
        entry->length = len;
        entry->color = color;
    }

    va_end(args);
}

internal void DebugPrintVec3(
    DebugPrintSystem *system, vec4 color, const char *name, vec3 value)
{
    DebugPrintf(system, color, "%s: %0.4g, %0.4g, %0.4g", name, value.x,
        value.y, value.z);
}

internal void DebugPrintCapacity(
    DebugPrintSystem *system, const char *name, u32 current, u32 max)
{
    f32 t = (f32)current / (f32)max;
    vec4 color = Lerp(Vec4(0, 0.8, 0.1, 1), Vec4(1, 0, 0, 1), t);

    if (max > 1024 * 1024)
    {
        DebugPrintf(system, color, "%s: %u M | %u M", name,
            current / (1024 * 1024), max / (1024 * 1024));
    }
    else if (max > 1024)
    {
        DebugPrintf(
            system, color, "%s: %u K | %u K", name, current / 1024, max / 1024);
    }
    else
    {
        DebugPrintf(system, color, "%s: %u | %u", name, current, max);
    }
}

internal void DebugPrintDrawStuff(DebugPrintSystem *system,
    RenderData *renderer, TextBuffer *textBuffer, vec2 position)
{
    Font *font = GetFont(renderer->assetSystem, Font_FpsDisplay);
    if (font != NULL)
    {
        f32 yOffset = font->lineSpacing + 2.0f;
        for (u32 i = 0; i < system->count; i++)
        {
            DebugPrintEntry *entry = system->entries + i;
            vec2 offset =
                Vec2(-CalculateTextWidth(*font, entry->text), -yOffset);
            DrawText(textBuffer, *font, entry->text, position + offset,
                entry->color, 0.0f);
            position.y -= yOffset;
        }
    }
}

