#pragma once

enum
{
    ParticleFlags_None = 0x0,
    ParticleFlags_Continuous = BIT(0),
    ParticleFlags_Mesh = BIT(1),
    ParticleFlags_UpdateScaleOverTime = BIT(2),
};

struct ParticleSpec
{
    u32 flags;
    u32 mesh;
    vec3 origin;
    vec3 acceleration;
    vec3 initialVelocity[2];
    vec3 initialRotationalVelocity[2];
    float scale[2];
};

// TODO: Should we avoid allocating buffers for properties we don't update?
#define PARTICLE_BUFFER_LENGTH 64
struct ParticleBuffer
{
    vec3 positions[PARTICLE_BUFFER_LENGTH];
    vec3 velocity[PARTICLE_BUFFER_LENGTH];
    vec3 rotation[PARTICLE_BUFFER_LENGTH];           // NOTE: Not often used
    vec3 rotationalVelocity[PARTICLE_BUFFER_LENGTH]; // NOTE: Not often used
    f32 remainingLifeTime[PARTICLE_BUFFER_LENGTH];
    u32 seed[PARTICLE_BUFFER_LENGTH];

    u32 count;
    f32 timeUntilNextSpawn;

    ParticleSpec spec;
};

struct ParticleSystem
{
    MemoryArena arena;
    RandomNumberGenerator rng;
    Dictionary buffers;
    u32 nextId;

    Dictionary specs;
};
