internal void InitializeGrassSystem(GrassSystem *grassSystem,
    MemoryArena *arena, u32 arenaSize, u32 instanceCapacity)
{
    memset(grassSystem, 0, sizeof(*grassSystem));

    grassSystem->arena = SubAllocateArena(arena, arenaSize);
    grassSystem->capacity = instanceCapacity;

    grassSystem->modelMatrices =
        ALLOCATE_ARRAY(&grassSystem->arena, mat4, instanceCapacity);

    // FIXME: This probably shouldn't be here...
    glGenBuffers(1, &grassSystem->modelMatrixBuffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, grassSystem->modelMatrixBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(mat4) * instanceCapacity,
        NULL, GL_DYNAMIC_DRAW);
}

internal f32 GetTerrainHeight(Terrain *terrain, vec2 p)
{
    // map to 0..1
    vec2 result = p - Vec2(terrain->position.x, terrain->position.z);
    result.x = result.x / terrain->scale.x;
    result.y = result.y / terrain->scale.z;

    f32 sample =
        BilinearSampleHeightMap(terrain->heightMap, result.x, result.y);
    f32 height = sample * terrain->scale.y + terrain->position.y;

    return height;
}

internal void ScatterGrassInChunk(GrassSystem *grassSystem, Terrain *terrain,
    vec2 min, vec2 max, u32 seed, f32 increment)
{
    RandomNumberGenerator rng = {seed};
    for (f32 z = min.y; z < max.y; z += increment)
    {
        for (f32 x = min.x; x < max.x; x += increment)
        {
            f32 xOffset = Lerp(-0.5, 0.5, RandomUnilateral(&rng));
            f32 zOffset = Lerp(-0.5, 0.5, RandomUnilateral(&rng));

            // FIXME: Don't hardcode max height
            vec3 rayOrigin = Vec3(x + xOffset, 200, z + zOffset);
            f32 height =
                GetTerrainHeight(terrain, Vec2(rayOrigin.x, rayOrigin.z));

            // TODO: Check material?
            vec3 p = Vec3(rayOrigin.x, height, rayOrigin.z);
            quat rotation = Quat(Vec3(0, 1, 0), PI * RandomBilateral(&rng));
            f32 scale = Lerp(0.1f, 0.6f, RandomUnilateral(&rng));

            if (grassSystem->count < grassSystem->capacity)
            {
                grassSystem->modelMatrices[grassSystem->count++] =
                    Translate(p) * Rotate(rotation) * Scale(Vec3(scale));
            }
        }
    }
}

internal void ScatterGrass(
    GrassSystem *grassSystem, Terrain *terrain, vec3 cameraPosition)
{
    PROF_SCOPE();

    grassSystem->count = 0;

    i32 chunkX = Floor(cameraPosition.x);
    i32 chunkZ = Floor(cameraPosition.z);

    u32 halfDim = 52;
    i32 minChunkX = chunkX - halfDim;
    i32 minChunkZ = chunkZ - halfDim;
    i32 maxChunkX = chunkX + halfDim;
    i32 maxChunkZ = chunkZ + halfDim;

    for (i32 z = minChunkZ; z < maxChunkZ; z++)
    {
        for (i32 x = minChunkX; x < maxChunkX; x++)
        {
            u32 seed = x * 1034235 + z * 459354;

            vec2 min = Vec2(x, z);
            vec2 max = min + Vec2(1);

            f32 dist =
                (Length(min - Vec2(chunkX, chunkZ)) / (f32)halfDim) + 0.25f;
            ScatterGrassInChunk(grassSystem, terrain, min, max, seed, dist);
        }
    }

    // Choose points in rect
    // TODO: Chunk based generation
    // * Work out number of chunks to generate (rect2)
    // * Allocate seed value to each chunk
    // * Generate instances in each chunk using the seed
}

internal void DrawGrass(
    GrassSystem *grassSystem, RenderData *renderer, vec3 cameraPosition)
{
    PROF_SCOPE();

    u32 shadowMapTexture = renderer->shadowMapBuffer.texture;
    u32 irradianceCubeMap =
        GetTexture(renderer->assetSystem, Texture_PreethamSky_Irradiance);

    // Update model matrices
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, grassSystem->modelMatrixBuffer);

    // FIXME: Use persistently mapped buffer
    glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0,
        grassSystem->count * sizeof(mat4), grassSystem->modelMatrices);

    u32 shader = GetShader(renderer->assetSystem, Shader_Instancing);
    if (shader != 0)
    {
        glUseProgram(shader);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        // --- Standard PBR material vertex shader interface ---
        {
            // Camera uniform buffer
            glBindBufferBase(
                GL_UNIFORM_BUFFER, 0, renderer->cameraUniformBuffer);

            // Camera index
            u32 cameraIndex = 0;
            glUniform1ui(2, cameraIndex);

            // Model matrix
            // mat4 identity = Identity();
            // glUniformMatrix4fv(1, 1, false, identity.data);
        }

        // -- Standard PBR material fragment shader interface ---
        {
#if ENABLE_LIGHTING
            // Lighting uniform buffer
            glBindBufferBase(
                GL_UNIFORM_BUFFER, 1, renderer->lightingUniformBuffer);
#endif

            // Albedo
            u32 albedo = GetTexture(
                renderer->assetSystem, HashStringU32("textures/grass_alpha"));
            if (albedo != 0)
            {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, albedo);
            }

            // TODO: Metallic
            // TODO: Roughness
            // TODO: Normal

#if ENABLE_LIGHTING
            // Shadow map
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, shadowMapTexture);
            glUniform1i(3, 1); // Use GL_TEXTURE1 for shadow map

            // Irradiance cube map
            if (irradianceCubeMap != 0)
            {
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceCubeMap);
                glUniform1i(4, 2);
            }
#endif
        }

        glBindBufferBase(
            GL_SHADER_STORAGE_BUFFER, 3, grassSystem->modelMatrixBuffer);

        MeshState *mesh =
            GetMesh(renderer->assetSystem, HashStringU32("meshes/grass"));
        if (mesh != NULL)
        {
            glBindVertexArray(mesh->vertexArrayObject);
            glDrawElementsInstanced(GL_TRIANGLES, mesh->elementCount,
                GL_UNSIGNED_INT, NULL, grassSystem->count);
            glBindVertexArray(0);
        }
    }

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}
