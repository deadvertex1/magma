#pragma once

struct HeightMap
{
    f32 *values;
    u32 width;
    u32 height;
};

struct TerrainGridCellCoordinate
{
    u32 x;
    u32 y;
};

struct TerrainQuadTreeNode
{
    rect2 boundingQuad;
    b32 isLeaf;
    TerrainQuadTreeNode *children[4]; //TODO: Use indices to save some memory?
};

struct Terrain
{
    MemoryArena heightMapArena;
    MemoryArena quadTreeArena;
    HeightMap heightMap;
    vec3 position;
    vec3 scale;
    u32 gridCellWidth;
    TerrainQuadTreeNode *root;
};
