layout(location = 0) in VS_OUT
{
    vec2 texture_coordinates;
} fs_in;

layout(location = 0) out vec4 o_color;

layout(location = 0) uniform sampler2D u_texture;
layout(location = 1) uniform vec4 u_color;

void main()
{
    float alpha = texture(u_texture, fs_in.texture_coordinates).r;
    o_color = u_color;
    o_color.a *= alpha;
}
