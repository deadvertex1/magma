#extension GL_GOOGLE_include_directive : require

#include "pbr_material_interface.glsl"

void main()
{
    vec3 albedo = vec3(0.18, 0.18, 0.18);
    vec3 N = normalize(fs_in.world_normal);
    vec3 color = pbr_material_model(albedo, N);
    o_color = vec4(color, 1.0);
}
