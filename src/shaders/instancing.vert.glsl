#extension GL_GOOGLE_include_directive : require

#include "vertex_format.vert.glsl"
#include "material_interface.vert.glsl"

layout(std140, binding=3) buffer InstancingModelMatrixBuffer
{
    mat4 modelMatrices[];
};

void main()
{
    mat4 modelMatrix = modelMatrices[gl_InstanceID];

    vec3 p = v_position;
    p.x += 0.1f* sin(t*1.5f + modelMatrix[3][0]) * p.y;
    p.z += 0.1f* sin(t*1.6f + modelMatrix[3][2]) * p.y;
    StandardVertexShaderSetup(p, modelMatrix);
}
