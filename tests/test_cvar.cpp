#include <cstring>

#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "dictionary.h"
#include "hash.h"
#include "cvar.h"

TEST_GROUP(CVar);

global MemoryArena g_Arena;

TEST_SETUP(CVar)
{
    u32 size = Kilobytes(1);
    InitializeMemoryArena(&g_Arena, malloc(size), size);
}

TEST_TEAR_DOWN(CVar) { free(g_Arena.base); }

TEST(CVar, Get)
{
    CVarSystem cvarSystem = {};
    InitializeCVarSystem(&cvarSystem, &g_Arena, 1);
    SetCVarU32(&cvarSystem, "test_cvar", 22);
    u32 value = 0;
    b32 found = GetCVarU32(&cvarSystem, "test_cvar", &value);
    TEST_ASSERT_TRUE(found);
    TEST_ASSERT_EQUAL_UINT(22, value);
}

TEST(CVar, SetAndUpdate)
{
    CVarSystem cvarSystem = {};
    InitializeCVarSystem(&cvarSystem, &g_Arena, 1);
    SetCVarU32(&cvarSystem, "test_cvar", 1);
    SetCVarU32(&cvarSystem, "test_cvar", 2);
    u32 value = 0;
    GetCVarU32(&cvarSystem, "test_cvar", &value);
    TEST_ASSERT_EQUAL_UINT(2, value);
}

TEST(CVar, GetMissing)
{
    CVarSystem cvarSystem = {};
    InitializeCVarSystem(&cvarSystem, &g_Arena, 1);

    u32 value = 0;
    b32 found = GetCVarU32(&cvarSystem, "missing_cvar", &value);
    TEST_ASSERT_FALSE(found);
}

TEST(CVar, Find)
{
    CVarSystem cvarSystem = {};
    InitializeCVarSystem(&cvarSystem, &g_Arena, 1);
    SetCVarU32(&cvarSystem, "test_cvar", 1);
    CVar *cvar = FindCVar(&cvarSystem, "test_cvar");
    TEST_ASSERT_NOT_NULL(cvar);
}

TEST(CVar, StoresName)
{
    CVarSystem cvarSystem = {};
    InitializeCVarSystem(&cvarSystem, &g_Arena, 1);
    SetCVarU32(&cvarSystem, "test_cvar", 1);
    CVar *cvar = FindCVar(&cvarSystem, "test_cvar");
    TEST_ASSERT_TRUE(strcmp(cvar->name, "test_cvar") == 0);
}

TEST_GROUP_RUNNER(CVar)
{
    RUN_TEST_CASE(CVar, Get);
    RUN_TEST_CASE(CVar, SetAndUpdate);
    RUN_TEST_CASE(CVar, GetMissing);
    RUN_TEST_CASE(CVar, Find);
    RUN_TEST_CASE(CVar, StoresName);
}
