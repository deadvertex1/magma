internal RayIntersectWorldResult RayIntersectCollisionWorld(
    CollisionWorld *collisionWorld, vec3 rayOrigin, vec3 rayDirection, f32 tmax,
    u32 ignoreId = 0, u32 flags = CollisionWorldDebugDrawFlags_None)
{
    PROF_SCOPE();
    RayIntersectWorldResult worldResult = {};
    worldResult.t = -1.0f;

    Dictionary *objects = &collisionWorld->objects;
    for (u32 i = 0; i < objects->count; i++)
    {
        u32 id = objects->keys[i];
        CollisionObject *object =
            Dict_GetItemByIndex(&collisionWorld->objects, CollisionObject, i);

        if (id != ignoreId && object != NULL)
        {
            if (object->shape == CollisionShape_Mesh)
            {
                // Broadphase check
                f32 broadphaseT = RayIntersectAabb(object->aabb.min,
                    object->aabb.max, rayOrigin, rayDirection);
                if (broadphaseT >= 0.0f)
                {
                    CollisionMesh *collisionMesh = Dict_FindItem(
                        &collisionWorld->meshes, CollisionMesh, object->mesh);
                    if (collisionMesh != NULL)
                    {
                        vec3 localRayOrigin =
                            TransformPoint(rayOrigin, object->invTransform);
                        vec3 localRayDirection =
                            TransformVector(rayDirection, object->invTransform);

                        // TODO: tmax
                        RayIntersectTriangleResult localTriangleResult =
                            RayIntersectTriangleMesh(collisionMesh,
                                localRayOrigin, localRayDirection, F32_MAX);

                        if (localTriangleResult.t >= 0.0f)
                        {
                            vec3 localP =
                                localRayOrigin +
                                localRayDirection * localTriangleResult.t;
                            vec3 worldP =
                                TransformPoint(localP, object->transform);

                            f32 t = Length(worldP - rayOrigin);

                            if (t < tmax)
                            {
                                if (t < worldResult.t || worldResult.t < 0.0f)
                                {
                                    worldResult.t = t;
                                    worldResult.normal =
                                        Normalize(TransformVector(
                                            localTriangleResult.normal,
                                            object->transform));
                                    worldResult.collisionObjectId = id;
                                }
                            }
                        }
                    }
                }
            }
            else if (object->shape == CollisionShape_Terrain)
            {
                RayIntersectTriangleResult terrainResult =
                    RayIntersectQuadTreeTerrain(collisionWorld->terrain,
                        rayOrigin, rayDirection, tmax, flags);

                if (terrainResult.t >= 0.0f && terrainResult.t < tmax)
                {
                    if (terrainResult.t < worldResult.t || worldResult.t < 0.0f)
                    {
                        worldResult.t = terrainResult.t;
                        worldResult.normal = terrainResult.normal;
                        worldResult.collisionObjectId = id;
                    }
                }
            }
        }
    }

    return worldResult;
}

