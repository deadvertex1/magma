internal void InitializeTerrain(Terrain *terrain, MemoryArena *arena)
{
    memset(terrain, 0, sizeof(*terrain));

    // FIXME: Should this rather come from transient memory!
    u32 heightMapArenaSize = Megabytes(100);
    terrain->heightMapArena = SubAllocateArena(arena, heightMapArenaSize);

    u32 quadTreeArenaSize = Megabytes(2);
    terrain->quadTreeArena = SubAllocateArena(arena, quadTreeArenaSize);

    terrain->gridCellWidth = TERRAIN_PATCH_WIDTH;
    terrain->position = Vec3(0, 0, 0);
    terrain->scale = Vec3(TERRAIN_SCALE_XZ, TERRAIN_SCALE_Y, TERRAIN_SCALE_XZ);
}

internal TerrainQuadTreeNode *CreateNode(
    MemoryArena *arena, u32 x, u32 y, u32 width, u32 minWidth, vec2 offset)
{
    TerrainQuadTreeNode *node = ALLOCATE_STRUCT(arena, TerrainQuadTreeNode);
    memset(node, 0, sizeof(*node));
    node->boundingQuad.min = offset + Vec2((f32)x, (f32)y) * (f32)width;
    node->boundingQuad.max =
        node->boundingQuad.min + Vec2((f32)width, (f32)width);

    if (width > minWidth)
    {
        u32 halfWidth = width / 2;
        node->children[0] = CreateNode(
            arena, 0, 0, halfWidth, minWidth, node->boundingQuad.min);
        node->children[1] = CreateNode(
            arena, 1, 0, halfWidth, minWidth, node->boundingQuad.min);
        node->children[2] = CreateNode(
            arena, 1, 1, halfWidth, minWidth, node->boundingQuad.min);
        node->children[3] = CreateNode(
            arena, 0, 1, halfWidth, minWidth, node->boundingQuad.min);
    }
    else
    {
        node->isLeaf = true;
    }

    return node;
}

internal void BuildTerrainQuadTree(Terrain *terrain)
{
    u32 nodeWidth = terrain->gridCellWidth;
    u32 terrainWidth = (u32)terrain->scale.x;
    ASSERT(terrainWidth % nodeWidth == 0);

    ClearMemoryArena(&terrain->quadTreeArena);

    // Top down construction
    // Build root node
    TerrainQuadTreeNode *root = CreateNode(
        &terrain->quadTreeArena, 0, 0, terrainWidth, nodeWidth, Vec2(0, 0));

    terrain->root = root;
}

internal void DebugDrawQuadTreeNode(
    DebugDrawingBuffer *debugDrawingBuffer, TerrainQuadTreeNode *node)
{
    vec3 min = Vec3(node->boundingQuad.min.x, 0.0f, node->boundingQuad.min.y);
    vec3 max = Vec3(node->boundingQuad.max.x, 0.0f, node->boundingQuad.max.y);
    DrawBox(debugDrawingBuffer, min, max, Vec3(0, 0, 1));

    if (!node->isLeaf)
    {
        for (u32 i = 0; i < 4; i++)
        {
            DebugDrawQuadTreeNode(debugDrawingBuffer, node->children[i]);
        }
    }
}

internal void DebugDrawTerrainAllQuadTreeNodes(
    Terrain *terrain, DebugDrawingBuffer *debugDrawingBuffer)
{
    DebugDrawQuadTreeNode(debugDrawingBuffer, terrain->root);
}

internal void DebugDrawTerrainQuadTree(
    Terrain *terrain, DebugDrawingBuffer *debugDrawingBuffer, vec2 testP)
{
    TerrainQuadTreeNode *stack[0x1000];
    stack[0] = terrain->root;
    u32 stackSize = 1;

    DrawPoint(
        debugDrawingBuffer, Vec3(testP.x, 0.0f, testP.y), 0.5f, Vec3(0, 1, 0));

    vec2 rootMin = terrain->root->boundingQuad.min;
    vec2 rootMax = terrain->root->boundingQuad.max;
    DrawBox(debugDrawingBuffer, Vec3(rootMin.x, 0.0f, rootMin.y),
        Vec3(rootMax.x, 0.0f, rootMax.y), Vec3(0, 0, 1));

    while (stackSize > 0)
    {
        TerrainQuadTreeNode *node = stack[--stackSize];
        vec2 closestP = RectClosestPoint(node->boundingQuad, testP);

        vec3 min =
            Vec3(node->boundingQuad.min.x, 0.0f, node->boundingQuad.min.y);
        vec3 max =
            Vec3(node->boundingQuad.max.x, 0.0f, node->boundingQuad.max.y);
        vec3 color = Vec3(0, 0, 1);
        vec3 lineColor = Vec3(1, 0, 1) * 0.2f;

        if (node->isLeaf)
        {
            color = Vec3(0, 1, 0);
        }
        else
        {
            f32 rSq =
                LengthSq(node->boundingQuad.max - node->boundingQuad.min) *
                0.5f;
            f32 distSq = LengthSq(closestP - testP);
            if (distSq < rSq)
            {
                lineColor = Vec3(1, 0, 1);
                for (u32 i = 0; i < 4; i++)
                {
                    ASSERT(stackSize < ARRAY_LENGTH(stack));
                    stack[stackSize++] = node->children[i];
                }
            }
        }

        DrawBox(debugDrawingBuffer, min, max, color);
        DrawLine(debugDrawingBuffer, Vec3(closestP.x, 0.0f, closestP.y),
            Vec3(testP.x, 0.0f, testP.y), lineColor);
    }
}

// TODO: Draw command
internal void DrawTerrainMesh(RenderData *renderer, u32 cameraIndex,
    vec3 position, vec3 scale, vec4 textureCoordsOffset)
{
    b32 showWireframe = false;
    u32 shadowMapTexture = renderer->shadowMapBuffer.texture;
    u32 irradianceCubeMap =
        GetTexture(renderer->assetSystem, Texture_PreethamSky_Irradiance);

    mat4 modelMatrix = Translate(position) * Scale(scale);
    // Apply material
    u32 shader = GetShader(renderer->assetSystem, Shader_Terrain);
    if (shader != 0)
    {
        glUseProgram(shader);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);

        if (showWireframe)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }

        // --- Standard PBR material vertex shader interface ---
        {
            // Camera uniform buffer
            glBindBufferBase(
                GL_UNIFORM_BUFFER, 0, renderer->cameraUniformBuffer);

            // Camera index
            glUniform1ui(2, cameraIndex);

            // Model matrix
            glUniformMatrix4fv(1, 1, false, modelMatrix.data);
        }

        // -- Standard PBR material fragment shader interface ---
        {
#if ENABLE_LIGHTING
            // Lighting uniform buffer
            glBindBufferBase(
                GL_UNIFORM_BUFFER, 1, renderer->lightingUniformBuffer);
#endif

            // Albedo
            u32 albedoTextureId =
                showWireframe ? Texture_SolidWhite : Texture_Grass;
            u32 albedo = GetTexture(renderer->assetSystem, albedoTextureId);
            if (albedo != 0)
            {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, albedo);
            }

            // TODO: Metallic
            // TODO: Roughness
            // TODO: Normal

#if ENABLE_LIGHTING
            // Shadow map
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, shadowMapTexture);
            glUniform1i(3, 1); // Use GL_TEXTURE1 for shadow map

            // Irradiance cube map
            if (irradianceCubeMap != 0)
            {
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceCubeMap);
                glUniform1i(4, 2);
            }
#endif

            // height
            u32 heightmap =
                GetTexture(renderer->assetSystem, Texture_TerrainHeightMap);
            if (heightmap != 0)
            {
                glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_2D, heightmap);
                glUniform1i(5, 3);
            }

            // u_textureCoordsOffset
            glUniform4fv(6, 1, textureCoordsOffset.data);

            // u_world_texture_scaling
            vec3 worldTextureScaling = Vec3(0.1);
            glUniform3fv(7, 1, worldTextureScaling.data);

            // u_normal
            u32 normalMap =
                GetTexture(renderer->assetSystem, Texture_TerrainNormalMap);
            if (normalMap != 0)
            {
                glActiveTexture(GL_TEXTURE4);
                glBindTexture(GL_TEXTURE_2D, normalMap);
                glUniform1i(8, 4);
            }

            // u_rock
            u32 rockTexture = GetTexture(
                renderer->assetSystem, HashStringU32("textures/rock"));
            if (rockTexture != 0)
            {
                glActiveTexture(GL_TEXTURE5);
                glBindTexture(GL_TEXTURE_2D, rockTexture);
                glUniform1i(9, 5);
            }
        }

        // Get mesh data
        MeshState *mesh = GetMesh(renderer->assetSystem, Mesh_TerrainPatch);
        if (mesh != NULL)
        {
            glBindVertexArray(mesh->vertexArrayObject);
            glDrawElements(
                GL_TRIANGLES, mesh->elementCount, GL_UNSIGNED_INT, 0);
        }

        if (showWireframe)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        glDisable(GL_CULL_FACE);
    }
}

internal void DrawTerrainQuadTree(Terrain *terrain, RenderData *renderer,
    u32 cameraIndex, vec3 cameraPosition)
{
    PROF_SCOPE();
    vec2 cameraP = Vec2(cameraPosition.x, cameraPosition.z);

    // TODO: Constant for stack size
    TerrainQuadTreeNode *stack[0x1000];
    stack[0] = terrain->root;
    u32 stackSize = 1;

    vec2 rootMin = terrain->root->boundingQuad.min;
    vec2 rootMax = terrain->root->boundingQuad.max;

    while (stackSize > 0)
    {
        TerrainQuadTreeNode *node = stack[--stackSize];
        vec2 closestP = RectClosestPoint(node->boundingQuad, cameraP);

        vec3 min =
            Vec3(node->boundingQuad.min.x, 0.0f, node->boundingQuad.min.y);
        vec3 max =
            Vec3(node->boundingQuad.max.x, 0.0f, node->boundingQuad.max.y);

        b32 drawMesh = false;
        if (node->isLeaf)
        {
            // TODO: VIsualize quad tree level with color tint?
            drawMesh = true;
        }
        else
        {
            f32 rSq =
                LengthSq(node->boundingQuad.max - node->boundingQuad.min) *
                0.5f;
            f32 distSq = LengthSq(closestP - cameraP);
            if (distSq < rSq)
            {
                for (u32 i = 0; i < 4; i++)
                {
                    ASSERT(stackSize < ARRAY_LENGTH(stack));
                    stack[stackSize++] = node->children[i];
                }
            }
            else
            {
                drawMesh = true;
            }
        }

        if (drawMesh)
        {
            vec3 position = min;
            vec3 scale = max - min;
            scale.y = terrain->scale.y;

            // Map position and scale to UV space

            vec4 textureCoordsOffset =
                Vec4(MapToUnitRange(position.x, rootMin.x, rootMax.x),
                    MapToUnitRange(position.z, rootMin.y, rootMax.y),
                    MapToUnitRange(scale.x, rootMin.x, rootMax.x),
                    MapToUnitRange(scale.z, rootMin.y, rootMax.y));

            DrawTerrainMesh(
                renderer, cameraIndex, position, scale, textureCoordsOffset);
        }
    }
}

internal TerrainGridCellCoordinate MapWorldSpaceToTerrainGridCell(
    Terrain *terrain, vec3 worldP)
{
    TerrainGridCellCoordinate result = {};

    result.x = (u32)Floor(Clamp(worldP.x, 0.0f, (f32)terrain->gridCellWidth));
    result.y = (u32)Floor(Clamp(worldP.z, 0.0f, (f32)terrain->gridCellWidth));

    return result;
}

internal void DebugDrawTerrainAabb(
    Terrain *terrain, DebugDrawingBuffer *debugDrawingBuffer)
{
    vec3 min = terrain->position;
    vec3 max = terrain->position + terrain->scale;
    DrawBox(debugDrawingBuffer, min, max, Vec3(0, 1, 0));
}

internal void UpdateTexture(
    AssetSystem *assetSystem, u32 assetId, u32 width, u32 height, f32 *pixels)
{
    // FIXME: Don't assume resolution/format hasn't changed
    // Get existing texture handle
    u32 handle = GetTexture(assetSystem, assetId);
    if (handle != 0)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, handle);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED,
            GL_FLOAT, pixels);

        glBindTexture(GL_TEXTURE_2D, 0);
    }
    else
    {
        LOG_MSG("Failed to update texture %u, could not be found", assetId);
    }
}

internal void GenerateTerrainHeightMap(
    Terrain *terrain, AssetSystem *assetSystem)
{
    ClearMemoryArena(&terrain->heightMapArena);

    GenerateTerrainHeightMap(assetSystem);
    GenerateTerrainNormalMap(assetSystem);

    terrain->heightMap =
        CreateHeightMapFromTexture(assetSystem, Texture_TerrainHeightMap,
            TERRAIN_TEXTURE_WIDTH, &terrain->heightMapArena);
}

inline vec2 TransformWorldSpaceToTerrainSpace(Terrain *terrain, vec2 p)
{
    vec2 result = p - Vec2(terrain->position.x, terrain->position.z);
    result.x = result.x / (f32)terrain->gridCellWidth;
    result.y = result.y / (f32)terrain->gridCellWidth;

    return result;
}

inline f32 SampleHeightMap(HeightMap heightMap, u32 x, u32 y)
{
    x = MinU32(x, heightMap.width - 1);
    y = MinU32(y, heightMap.height - 1);
    return heightMap.values[y * heightMap.width + x];
}

internal f32 BilinearSampleHeightMap(HeightMap heightMap, f32 u, f32 v)
{
    // Offset by -0.5f to sample from pixel center
    f32 s = u * heightMap.width - 0.5f;
    f32 t = v * heightMap.height - 0.5f;

    // Map to integer coordinate
    i32 x0 = (i32)Floor(s);
    i32 y0 = (i32)Floor(t);

    // Find adjacent pixels in positive direction
    i32 x1 = x0 + 1;
    i32 y1 = y0 + 1;

    f32 fx = s - x0;
    f32 fy = t - y0;

    f32 samples[4];
    samples[0] = SampleHeightMap(heightMap, x0, y0);
    samples[1] = SampleHeightMap(heightMap, x1, y0);
    samples[2] = SampleHeightMap(heightMap, x0, y1);
    samples[3] = SampleHeightMap(heightMap, x1, y1);

    f32 p = Lerp(samples[0], samples[1], fx);
    f32 q = Lerp(samples[2], samples[3], fx);

    f32 result = Lerp(p, q, fy);
    return result;
}

internal Aabb GetAabbForTerrainGridCell(
    Terrain *terrain, TerrainGridCellCoordinate coord, vec4 textureCoordsOffset)
{
    Aabb result = {};
    result.min = Vec3(coord.x, 0.0f, coord.y);
    result.max = result.min + Vec3(1.0f, 0.0f, 1.0f);

    vec3 min = result.min;
    vec3 max = result.max;

    vec3 vertices[4] = {
        Vec3(min.x, 0.0f, min.z),
        Vec3(max.x, 0.0f, min.z),
        Vec3(max.x, 0.0f, max.z),
        Vec3(min.x, 0.0f, max.z),
    };

    f32 minHeight = F32_MAX;
    f32 maxHeight = -F32_MAX;
    for (u32 i = 0; i < 4; i++)
    {
        // Transform each world space vertex into terrain space
        // to calculate UV coordinates
        vec2 uv = TransformWorldSpaceToTerrainSpace(
            terrain, Vec2(vertices[i].x, vertices[i].z));
        uv = Clamp(uv, Vec2(0), Vec2(1));
        uv = Hadamard(uv, textureCoordsOffset.zw) + textureCoordsOffset.xy;

        // Height map returns values from 0 to 1
        f32 height = BilinearSampleHeightMap(terrain->heightMap, uv.x, uv.y);

        // Transform height back to world space to get our Y coord
        vertices[i].y = height * terrain->scale.y + terrain->position.y;

        minHeight = Min(minHeight, vertices[i].y);
        maxHeight = Max(maxHeight, vertices[i].y);
    }

    result.min.y = minHeight;
    result.max.y = maxHeight;

    return result;
}

internal void GetVerticesForGridCellAabb(
    Terrain *terrain, Aabb gridCellAabb, vec4 textureCoordsOffset, vec3 *output)
{
    vec3 min = gridCellAabb.min;
    vec3 max = gridCellAabb.max;

    vec3 vertices[4] = {
        Vec3(min.x, 0.0f, min.z),
        Vec3(max.x, 0.0f, min.z),
        Vec3(max.x, 0.0f, max.z),
        Vec3(min.x, 0.0f, max.z),
    };

    for (u32 i = 0; i < 4; i++)
    {
        // Transform each world space vertex into terrain space
        // to calculate UV coordinates
        vec2 uv = TransformWorldSpaceToTerrainSpace(
            terrain, Vec2(vertices[i].x, vertices[i].z));
        uv = Clamp(uv, Vec2(0), Vec2(1));
        uv = Hadamard(uv, textureCoordsOffset.zw) + textureCoordsOffset.xy;

        // Height map returns values from 0 to 1
        f32 height = BilinearSampleHeightMap(terrain->heightMap, uv.x, uv.y);

        // Transform height back to world space to get our Y coord
        vertices[i].y = height * terrain->scale.y + terrain->position.y;

        output[i] = vertices[i];
    }
}

internal void DebugDrawTerrainCollision(Terrain *terrain,
    DebugDrawingBuffer *debugDrawingBuffer, vec2 offset,
    vec4 textureCoordsOffset, Aabb worldQueryAabb,
    u32 flags = CollisionWorldDebugDrawFlags_None)
{
    vec3 queryMin = worldQueryAabb.min - Vec3(offset.x, 0.0f, offset.y);
    vec3 queryMax = worldQueryAabb.max - Vec3(offset.x, 0.0f, offset.y);

    TerrainGridCellCoordinate gridMin =
        MapWorldSpaceToTerrainGridCell(terrain, queryMin);
    TerrainGridCellCoordinate gridMax =
        MapWorldSpaceToTerrainGridCell(terrain, queryMax);
    for (u32 y = gridMin.y; y < gridMax.y; y++)
    {
        for (u32 x = gridMin.x; x < gridMax.x; x++)
        {
            TerrainGridCellCoordinate coord = {x, y};
            Aabb aabb =
                GetAabbForTerrainGridCell(terrain, coord, textureCoordsOffset);

            if (flags & CollisionWorldDebugDrawFlags_TriangleAabbs)
            {
                vec3 min = aabb.min + Vec3(offset.x, 0.0f, offset.y);
                vec3 max = aabb.max + Vec3(offset.x, 0.0f, offset.y);
                DrawBox(debugDrawingBuffer, min, max, Vec3(0, 0, 1));
            }

            if (flags & CollisionWorldDebugDrawFlags_Triangles)
            {
                vec3 vertices[4];
                GetVerticesForGridCellAabb(
                    terrain, aabb, textureCoordsOffset, vertices);

                for (u32 i = 0; i < 4; i++)
                {
                    vertices[i].x += offset.x;
                    vertices[i].z += offset.y;
                }

                DrawTriangle(debugDrawingBuffer, vertices[0], vertices[1],
                    vertices[3], Vec3(0, 1, 0));
                DrawTriangle(debugDrawingBuffer, vertices[1], vertices[2],
                    vertices[3], Vec3(0, 1, 0));
            }
        }
    }
}

internal void DebugDrawQuadTreeTerrainCollision(Terrain *terrain,
    DebugDrawingBuffer *debugDrawingBuffer, Aabb worldQueryAabb,
    u32 flags = CollisionWorldDebugDrawFlags_None)
{
    DrawBox(debugDrawingBuffer, worldQueryAabb.min, worldQueryAabb.max,
        Vec3(1, 0, 1));

    rect2 queryRect = Rect2(worldQueryAabb.min.x, worldQueryAabb.min.z,
        worldQueryAabb.max.x, worldQueryAabb.max.z);

    // Walk quad tree
    TerrainQuadTreeNode *stack[0x1000]; // TODO: Constant
    stack[0] = terrain->root;
    u32 stackSize = 1;

    vec2 rootMin = terrain->root->boundingQuad.min;
    vec2 rootMax = terrain->root->boundingQuad.max;

    while (stackSize > 0)
    {
        TerrainQuadTreeNode *node = stack[--stackSize];
        vec3 min =
            Vec3(node->boundingQuad.min.x, 0.0f, node->boundingQuad.min.y);
        vec3 max =
            Vec3(node->boundingQuad.max.x, 0.0f, node->boundingQuad.max.y);

        if (RectOverlap(queryRect, node->boundingQuad))
        {
            if (node->isLeaf)
            {
                DrawBox(debugDrawingBuffer, min, max, Vec3(0, 0, 1));
                vec3 position = min;
                vec3 scale = max - min;

                vec4 textureCoordsOffset =
                    Vec4(MapToUnitRange(position.x, rootMin.x, rootMax.x),
                        MapToUnitRange(position.z, rootMin.y, rootMax.y),
                        MapToUnitRange(scale.x, rootMin.x, rootMax.x),
                        MapToUnitRange(scale.z, rootMin.y, rootMax.y));

                DebugDrawTerrainCollision(terrain, debugDrawingBuffer,
                    node->boundingQuad.min, textureCoordsOffset, worldQueryAabb,
                    flags);
            }
            else
            {
                // Check for AABB overlap
                for (u32 i = 0; i < 4; i++)
                {
                    ASSERT(stackSize < ARRAY_LENGTH(stack));
                    stack[stackSize++] = node->children[i];
                }
            }
        }
    }
}

// Assuming coarse AABB test already done for the ray
// NOTE: This test is assumed to be performed in WORLD SPACE
internal RayIntersectTriangleResult RayIntersectTerrain(Terrain *terrain,
    vec2 offset, vec4 textureCoordsOffset, vec3 rayOrigin, vec3 rayDirection,
    f32 tmax = F32_MAX, u32 flags = CollisionWorldDebugDrawFlags_None)
{
    RayIntersectTriangleResult result = {};
    result.t = -1.0f;

    vec3 p0 = rayOrigin;
    vec3 p1 = p0 + rayDirection * tmax;

    // Computing bounding box for ray
    Aabb rayAabb;
    rayAabb.min = Min(p0, p1);
    rayAabb.max = Max(p0, p1);

    if (flags & CollisionWorldDebugDrawFlags_TriangleAabbs)
    {
        DRAW_BOX(rayAabb.min, rayAabb.max, Vec3(0, 1, 0), 0.1f);
    }

    rayAabb.min -= Vec3(offset.x, 0.0f, offset.y);
    rayAabb.max -= Vec3(offset.x, 0.0f, offset.y);

    TerrainGridCellCoordinate gridMin =
        MapWorldSpaceToTerrainGridCell(terrain, rayAabb.min);
    TerrainGridCellCoordinate gridMax =
        MapWorldSpaceToTerrainGridCell(terrain, rayAabb.max);
    if (gridMin.x == gridMax.x)
    {
        gridMax.x += 1;
    }
    if (gridMin.y == gridMax.y)
    {
        gridMax.y += 1;
    }
    for (u32 y = gridMin.y; y <= gridMax.y; y++)
    {
        for (u32 x = gridMin.x; x <= gridMax.x; x++)
        {
            TerrainGridCellCoordinate coord = {x, y};
            Aabb gridCellAabb =
                GetAabbForTerrainGridCell(terrain, coord, textureCoordsOffset);

            Aabb testAabb;
            testAabb.min = gridCellAabb.min + Vec3(offset.x, 0.0f, offset.y);
            testAabb.max = gridCellAabb.max + Vec3(offset.x, 0.0f, offset.y);

            if (flags & CollisionWorldDebugDrawFlags_TriangleAabbs)
            {
                DRAW_BOX(testAabb.min, testAabb.max, Vec3(0, 0, 1), 0.1);
            }

            f32 t = RayIntersectAabb(
                testAabb.min, testAabb.max, rayOrigin, rayDirection);
            if (t >= 0.0f && t < tmax)
            {
                vec3 vertices[4];
                GetVerticesForGridCellAabb(
                    terrain, gridCellAabb, textureCoordsOffset, vertices);

                for (u32 i = 0; i < 4; i++)
                {
                    vertices[i].x += offset.x;
                    vertices[i].z += offset.y;
                }

                // FIXME: Vertex ordering is still somehow a mess!
                if (flags & CollisionWorldDebugDrawFlags_Triangles)
                {
                    DRAW_TRIANGLE(vertices[2], vertices[1], vertices[0],
                        Vec3(0, 1, 0), 0.1f);
                    DRAW_TRIANGLE(vertices[2], vertices[0], vertices[3],
                        Vec3(0, 1, 0), 0.1f);
                }

                // Build our triangles for test
                RayIntersectTriangleResult triangleResult[2];

                // NOTE: Vertex order is very touchy
                triangleResult[0] = RayIntersectTriangleMT(rayOrigin,
                    rayDirection, vertices[2], vertices[1], vertices[0]);
                triangleResult[1] = RayIntersectTriangleMT(rayOrigin,
                    rayDirection, vertices[2], vertices[0], vertices[3]);

                for (u32 i = 0; i < 2; i++)
                {
                    if (triangleResult[i].t >= 0.0f &&
                        triangleResult[i].t < tmax)
                    {
                        if (triangleResult[i].t < result.t || result.t < 0.0f)
                        {
                            result = triangleResult[i];
                        }
                    }
                }
            }
        }
    }

    return result;
}

internal RayIntersectTriangleResult RayIntersectQuadTreeTerrain(
    Terrain *terrain, vec3 rayOrigin, vec3 rayDirection, f32 tmax = F32_MAX,
    u32 flags = CollisionWorldDebugDrawFlags_None)
{
    PROF_SCOPE();
    RayIntersectTriangleResult result = {};
    result.t = -1.0f;

    vec3 p0 = rayOrigin;
    vec3 p1 = p0 + rayDirection * tmax;

    // Computing bounding box for ray
    Aabb rayAabb;
    rayAabb.min = Min(p0, p1);
    rayAabb.max = Max(p0, p1);

    rect2 queryRect =
        Rect2(rayAabb.min.x, rayAabb.min.z, rayAabb.max.x, rayAabb.max.z);

    // Walk quad tree
    TerrainQuadTreeNode *stack[0x1000]; // TODO: Constant
    stack[0] = terrain->root;
    u32 stackSize = 1;

    vec2 rootMin = terrain->root->boundingQuad.min;
    vec2 rootMax = terrain->root->boundingQuad.max;

    while (stackSize > 0)
    {
        TerrainQuadTreeNode *node = stack[--stackSize];
        vec3 min =
            Vec3(node->boundingQuad.min.x, 0.0f, node->boundingQuad.min.y);
        vec3 max =
            Vec3(node->boundingQuad.max.x, 0.0f, node->boundingQuad.max.y);

        if (RectOverlap(queryRect, node->boundingQuad))
        {
            if (node->isLeaf)
            {
                if (flags & CollisionWorldDebugDrawFlags_Aabbs)
                {
                    DRAW_BOX(min, max, Vec3(0, 0.5, 1), 0.01f);
                }

                vec3 position = min;
                vec3 scale = max - min;

                vec4 textureCoordsOffset =
                    Vec4(MapToUnitRange(position.x, rootMin.x, rootMax.x),
                        MapToUnitRange(position.z, rootMin.y, rootMax.y),
                        MapToUnitRange(scale.x, rootMin.x, rootMax.x),
                        MapToUnitRange(scale.z, rootMin.y, rootMax.y));

                RayIntersectTriangleResult nodeResult = RayIntersectTerrain(
                    terrain, node->boundingQuad.min, textureCoordsOffset,
                    rayOrigin, rayDirection, tmax, flags);
                if (nodeResult.t >= 0.0f && nodeResult.t < tmax)
                {
                    if (nodeResult.t < result.t || result.t < 0.0f)
                    {
                        result = nodeResult;
                    }
                }
            }
            else
            {
                // Check for AABB overlap
                for (u32 i = 0; i < 4; i++)
                {
                    ASSERT(stackSize < ARRAY_LENGTH(stack));
                    stack[stackSize++] = node->children[i];
                }
            }
        }
    }

    return result;
}
