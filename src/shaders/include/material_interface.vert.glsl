// Uniforms interface
// Camera uniform buffer is always bound to binding 0
#include "camera_uniform_buffer.glsl"

layout(location = 1) uniform mat4 model_matrix;
layout(location = 2) uniform uint u_camera_index;

// Standard vertex shader output interface
layout(location = 0) out VS_OUT
{
    vec3 color;
    vec2 texture_coordinates;
    vec3 local_position;
    vec3 world_position;
    vec3 world_normal;
    vec4 light_position;
} vs_out;

void StandardVertexShaderSetup(vec3 position, mat4 modelMatrix)
{
    mat4 projection_matrix = projection_matrices[u_camera_index];
    mat4 view_matrix = view_matrices[u_camera_index];

    gl_Position = projection_matrix * view_matrix * modelMatrix * vec4(position, 1.0);
    vs_out.color = v_color;
    vs_out.texture_coordinates = v_texture_coordinates;
    vs_out.local_position = position;
    vs_out.world_position = (modelMatrix * vec4(position, 1.0)).xyz;
    vs_out.world_normal = (transpose(modelMatrix) * vec4(v_normal, 1.0)).xyz; // Not quite right

    uint shadow_pass_camera = 1;
    vs_out.light_position = projection_matrices[shadow_pass_camera] *
        view_matrices[shadow_pass_camera] * modelMatrix * vec4(position, 1.0);
}
