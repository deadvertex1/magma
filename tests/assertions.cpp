#include <cstring>
#include <cfloat>

#include "unity.h"

#include "platform.h"
#include "math_utils.h"
#include "math_lib.h"
#include "assertions.h"

void AssertWithinVec3(f32 delta, vec3 expected, vec3 actual)
{
    b32 x = Abs(expected.x - actual.x) <= delta;
    b32 y = Abs(expected.y - actual.y) <= delta;
    b32 z = Abs(expected.z - actual.z) <= delta;

    char buffer[256];
    snprintf(buffer, sizeof(buffer), "Expected [%g, %g, %g] Was [%g, %g, %g].",
        expected.x, expected.y, expected.z, actual.x, actual.y, actual.z);
    TEST_ASSERT_MESSAGE(x && y && z, buffer);
}

void AssertWithinQuat(
    f32 delta, vec3 expectedAxis, f32 expectedAngle, quat actual)
{
    vec3 axis;
    f32 angle;
    AxisAngle(actual, &axis, &angle);

    b32 x = Abs(expectedAxis.x - axis.x) <= delta;
    b32 y = Abs(expectedAxis.y - axis.y) <= delta;
    b32 z = Abs(expectedAxis.z - axis.z) <= delta;
    b32 w = Abs(expectedAngle - angle) <= delta;

    char buffer[256];
    snprintf(buffer, sizeof(buffer),
        "Expected axis: [%g, %g, %g] and angle %g but was axis [%g, %g, %g] "
        "and angle %g.",
        expectedAxis.x, expectedAxis.y, expectedAxis.z, expectedAngle, axis.x,
        axis.y, axis.z, angle);
    TEST_ASSERT_MESSAGE(x && y && z, buffer);
}

void AssertWithinRangeVec3(vec3 min, vec3 max, vec3 actual)
{
    b32 x = actual.x >= min.x && actual.x < max.x;
    b32 y = actual.y >= min.y && actual.y < max.y;
    b32 z = actual.z >= min.z && actual.z < max.z;

    char buffer[256];
    snprintf(buffer, sizeof(buffer),
        "Expected point [%g, %g, %g] to be within [%g, %g, %g] and [%g, %g, "
        "%g].",
        actual.x, actual.y, actual.z, min.x, min.y, min.z, max.x, max.y, max.z);
    TEST_ASSERT_MESSAGE(x && y && z, buffer);
}

