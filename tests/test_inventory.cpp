#include <cmath>
#include <cfloat>  // For FLT_EPSILON
#include <cstring> // For memcpy

#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "dictionary.h"
#include "hash.h"
#include "math_utils.h"
#include "math_lib.h"
#include "inventory.h"
#include "entity_world.h"

// Stubs
#define PROF_SCOPE()
#define LOG_MSG(MSG, ...)

#include "game_entities.h"
#include "entity_world.cpp"
#include "inventory.cpp"

TEST_GROUP(Inventory);

global MemoryArena g_Arena;

TEST_SETUP(Inventory)
{
    u32 size = Megabytes(1); // To handle hardcoded arena sizes!
    InitializeMemoryArena(&g_Arena, malloc(size), size);
}

TEST_TEAR_DOWN(Inventory) { free(g_Arena.base); }

internal EntityWorldConfig CreateTestEntityWorldConfig()
{
    EntityWorldConfig config = {};
    config.worldArenaSize = Kilobytes(512);
    config.maxEntities = 32;
    config.maxEntityDefinitions = 4;
    config.deletionQueueCapacity = 2;
    return config;
}

TEST(Inventory, AddItemToInventory)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    ItemDatabase itemDatabase = {};
    InitializeItemDatabase(&itemDatabase, &g_Arena);

    InventorySystemConfig inventoryConfig = {};
    inventoryConfig.arenaSize = Kilobytes(2);
    inventoryConfig.maxInventories = 4;
    inventoryConfig.maxItems = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, inventoryConfig, &g_Arena);

    u32 itemId = 2;
    TEST_ASSERT_TRUE(RegisterItem(&itemDatabase, itemId, "item_test", 0, 1));

    Inventory inventory = {};
    TEST_ASSERT_TRUE(AddItemToInventory(
        &entityWorld, &itemDatabase, &inventorySystem, &inventory, itemId, 3));

    u32 count = CountItem(&entityWorld, &inventorySystem, &inventory, itemId);
    TEST_ASSERT_EQUAL_UINT(3, count);
}

TEST(Inventory, StackItems)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    ItemDatabase itemDatabase = {};
    InitializeItemDatabase(&itemDatabase, &g_Arena);

    InventorySystemConfig inventoryConfig = {};
    inventoryConfig.arenaSize = Kilobytes(2);
    inventoryConfig.maxInventories = 4;
    inventoryConfig.maxItems = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, inventoryConfig, &g_Arena);

    u32 itemId = 2;
    TEST_ASSERT_TRUE(RegisterItem(&itemDatabase, itemId, "item_test", 0, 5));

    Inventory inventory = {};
    TEST_ASSERT_TRUE(AddItemToInventory(
        &entityWorld, &itemDatabase, &inventorySystem, &inventory, itemId, 1));

    TEST_ASSERT_EQUAL_UINT(1, inventory.items[0]);
    TEST_ASSERT_EQUAL_UINT(0, inventory.items[1]);

    b32 inventoryModified = AddItemToInventory(
        &entityWorld, &itemDatabase, &inventorySystem, &inventory, itemId, 1);
    TEST_ASSERT_FALSE(inventoryModified);

    TEST_ASSERT_EQUAL_UINT(1, inventory.items[0]);
    TEST_ASSERT_EQUAL_UINT(0, inventory.items[1]);
    TEST_ASSERT_EQUAL_UINT(
        2, CountItem(&entityWorld, &inventorySystem, &inventory, itemId));

    inventoryModified = AddItemToInventory(
        &entityWorld, &itemDatabase, &inventorySystem, &inventory, itemId, 5);
    TEST_ASSERT_TRUE(inventoryModified);

    TEST_ASSERT_EQUAL_UINT(1, inventory.items[0]);
    TEST_ASSERT_EQUAL_UINT(2, inventory.items[1]);
    TEST_ASSERT_EQUAL_UINT(
        7, CountItem(&entityWorld, &inventorySystem, &inventory, itemId));
}

TEST(Inventory, ConsumeFromInventorySlot)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    ItemDatabase itemDatabase = {};
    InitializeItemDatabase(&itemDatabase, &g_Arena);

    InventorySystemConfig inventoryConfig = {};
    inventoryConfig.arenaSize = Kilobytes(2);
    inventoryConfig.maxInventories = 4;
    inventoryConfig.maxItems = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, inventoryConfig, &g_Arena);

    u32 itemId = 2;
    TEST_ASSERT_TRUE(RegisterItem(&itemDatabase, itemId, "item_test", 0, 1));

    // Given an inventory with multiple items in different slots
    Inventory inventory = {};
    TEST_ASSERT_TRUE(AddItemToInventory(
        &entityWorld, &itemDatabase, &inventorySystem, &inventory, itemId, 3));

    // When we consume a single item instance from a slot
    u32 slot = 1;
    TEST_ASSERT_TRUE(
        ConsumeItemFromSlot(&inventorySystem, &inventory, slot, itemId, 1));

    // Then only that slot is modified
    TEST_ASSERT_EQUAL_UINT(NULL_ENTITY_ID, inventory.items[1]);
    TEST_ASSERT_NOT_EQUAL_UINT(NULL_ENTITY_ID, inventory.items[0]);
    TEST_ASSERT_NOT_EQUAL_UINT(NULL_ENTITY_ID, inventory.items[2]);
}

TEST(Inventory, ConsumeFromBeltSlot)
{
    u32 itemId = 2;
    u32 beltSlot = 1;

    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    ItemDatabase itemDatabase = {};
    InitializeItemDatabase(&itemDatabase, &g_Arena);

    TEST_ASSERT_TRUE(RegisterItem(&itemDatabase, itemId, "item_test", 0, 10));

    InventorySystemConfig inventoryConfig = {};
    inventoryConfig.arenaSize = Kilobytes(2);
    inventoryConfig.maxInventories = 4;
    inventoryConfig.maxItems = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, inventoryConfig, &g_Arena);

    // Given an inventory with a stack of items in a belt slot
    Inventory inventory = {};
    u32 itemEntityId = AllocateEntityId(&entityWorld);

    u32 itemInstanceId = AllocateItemInstance(&inventorySystem);
    ItemInstance *itemInstance =
        FindItemInstanceById(&inventorySystem, itemInstanceId);
    itemInstance->itemId = itemId;
    itemInstance->quantity = 5;
    u32 slot = MapBeltSlotToInventorySlot(beltSlot);
    inventory.items[slot] = itemInstanceId;

    // When I consume a single item from the item stack
    b32 inventoryUpdated = false;
    TEST_ASSERT_TRUE(
        ConsumeItemFromSlot(&inventorySystem, &inventory, slot, itemId, 1));
    TEST_ASSERT_FALSE(inventoryUpdated);

    // Then only a single item is removed from that stack
    u32 inventorySlot = MapBeltSlotToInventorySlot(beltSlot);
    u32 entityId = inventory.items[inventorySlot];
}

TEST(Inventory, InitializeInventorySystem)
{
    InventorySystemConfig config = {};
    config.arenaSize = Kilobytes(2);
    config.maxInventories = 4;
    config.maxItems = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, config, &g_Arena);

    TEST_ASSERT_EQUAL_UINT(Kilobytes(2), inventorySystem.arena.capacity);
    TEST_ASSERT_EQUAL_UINT(1, inventorySystem.nextInventoryId);
    TEST_ASSERT_EQUAL_UINT(4, inventorySystem.inventories.capacity);

    TEST_ASSERT_EQUAL_UINT(1, inventorySystem.nextItemInstanceId);
    TEST_ASSERT_EQUAL_UINT(4, inventorySystem.items.capacity);
}

TEST(Inventory, AllocateInventory)
{
    InventorySystemConfig config = {};
    config.arenaSize = Kilobytes(2);
    config.maxInventories = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, config, &g_Arena);

    u32 id = AllocateInventory(&inventorySystem);
    TEST_ASSERT_EQUAL_UINT(1, id);
    TEST_ASSERT_EQUAL_UINT(2, inventorySystem.nextInventoryId);

    Inventory *inventory = FindInventoryById(&inventorySystem, id);
    TEST_ASSERT_NOT_NULL(inventory);
}

TEST(Inventory, AllocatedInventoryIsEmpty)
{
    InventorySystemConfig config = {};
    config.arenaSize = Kilobytes(2);
    config.maxInventories = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, config, &g_Arena);

    u32 id = AllocateInventory(&inventorySystem);
    Inventory *inventory = FindInventoryById(&inventorySystem, id);

    for (u32 i = 0; i < ARRAY_LENGTH(inventory->items); i++)
    {
        TEST_ASSERT_EQUAL_UINT(0, inventory->items[i]);
    }
}

TEST(Inventory, RemoveInventory)
{
    InventorySystemConfig config = {};
    config.arenaSize = Kilobytes(2);
    config.maxInventories = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, config, &g_Arena);

    u32 id = AllocateInventory(&inventorySystem);

    RemoveInventory(&inventorySystem, id);
    Inventory *inventory = FindInventoryById(&inventorySystem, id);
    TEST_ASSERT_NULL(inventory);
}

TEST(Inventory, ProcessTransferItemCommand)
{
    InventorySystemConfig config = {};
    config.arenaSize = Kilobytes(2);
    config.maxInventories = 4;
    config.maxItems = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, config, &g_Arena);

    EntityWorldConfig entityWorldConfig = CreateTestEntityWorldConfig();

    EntityWorld entityWorld = {};
    InitializeEntityWorld(&entityWorld, &g_Arena, entityWorldConfig);

    ItemDatabase itemDatabase = {};
    InitializeItemDatabase(&itemDatabase, &g_Arena);

    u32 entityId = AllocateEntityId(&entityWorld);

    Entity entity = {};
    entity.inventoryId = AllocateInventory(&inventorySystem);

    AddEntity(&entityWorld, entityId, &entity);

    // Put item in slot 0
    u32 itemId = 123;
    RegisterItem(&itemDatabase, itemId, "item_test", 0, 1);

    AddItemToInventory(
        &entityWorld, &inventorySystem, &itemDatabase, entityId, itemId, 1);

    Inventory *inventory =
        FindInventoryById(&inventorySystem, entity.inventoryId);
    TEST_ASSERT_NOT_NULL(inventory);
    u32 itemEntityId = inventory->items[0];

    InventoryCommand command = {};
    command.type = 0;
    command.srcContainer = entityId;
    command.dstContainer = entityId;
    command.srcSlot = 0;
    command.dstSlot = 24;

    ProcessTransferItemCommand(
        &entityWorld, &itemDatabase, &inventorySystem, command);

    TEST_ASSERT_EQUAL_UINT(itemEntityId, inventory->items[24]);
}

TEST(Inventory, ItemInstanceAdd)
{
    InventorySystemConfig config = {};
    config.arenaSize = Kilobytes(2);
    config.maxInventories = 4;
    config.maxItems = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, config, &g_Arena);

    u32 id = AllocateItemInstance(&inventorySystem);

    ItemInstance *instance = FindItemInstanceById(&inventorySystem, id);
    TEST_ASSERT_NOT_NULL(instance);

    // Check that memory is zeroed
    TEST_ASSERT_EQUAL_UINT(0, instance->itemId);
    TEST_ASSERT_EQUAL_UINT(0, instance->quantity);
}

TEST(Inventory, ItemInstanceRemove)
{
    InventorySystemConfig config = {};
    config.arenaSize = Kilobytes(2);
    config.maxInventories = 4;
    config.maxItems = 4;

    InventorySystem inventorySystem = {};
    InitializeInventorySystem(&inventorySystem, config, &g_Arena);

    u32 id = AllocateItemInstance(&inventorySystem);

    RemoveItemInstance(&inventorySystem, id);
    ItemInstance *instance = FindItemInstanceById(&inventorySystem, id);
    TEST_ASSERT_NULL(instance);
}

TEST_GROUP_RUNNER(Inventory)
{
    RUN_TEST_CASE(Inventory, ItemInstanceAdd);
    RUN_TEST_CASE(Inventory, ItemInstanceRemove);

    RUN_TEST_CASE(Inventory, AddItemToInventory);
    RUN_TEST_CASE(Inventory, StackItems);
    RUN_TEST_CASE(Inventory, ConsumeFromInventorySlot);
    RUN_TEST_CASE(Inventory, ConsumeFromBeltSlot);
    RUN_TEST_CASE(Inventory, InitializeInventorySystem);
    RUN_TEST_CASE(Inventory, AllocateInventory);
    RUN_TEST_CASE(Inventory, AllocatedInventoryIsEmpty);
    RUN_TEST_CASE(Inventory, RemoveInventory);
    RUN_TEST_CASE(Inventory, ProcessTransferItemCommand);
}
