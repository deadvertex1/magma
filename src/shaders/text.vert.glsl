layout(std140, binding=0) uniform CameraUniformBuffer
{
    mat4 view_matrices[8];
    mat4 projection_matrices[8];
};

layout(location = 0) in vec2 v_position;
layout(location = 1) in vec2 v_texture_coordinates;

// FIXME: Can we use location 2?
layout(location = 2) uniform float u_depth = 0.0;

layout(location = 0) out VS_OUT
{
    vec2 texture_coordinates;
} vs_out;

void main()
{
    uint camera_index = 2;
    mat4 projection_matrix = projection_matrices[camera_index];

    gl_Position = projection_matrix * vec4(v_position, u_depth, 1.0);
    vs_out.texture_coordinates = v_texture_coordinates;
}
