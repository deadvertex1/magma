#include <cmath>
#include <cfloat>  // For FLT_EPSILON
#include <cstring> // For memcpy

#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "dictionary.h"
#include "hash.h"
#include "math_utils.h"
#include "math_lib.h"
#include "aabb_tree.h"
#include "collision_world.h"
#include "entity_world.h"
#include "asset_system.h"
#include "assertions.h"

#define PROF_SCOPE()
#define LOG_MSG(...)

#include "game_entities.h"
#include "render.h"

const u32 Texture_PreethamSky_Irradiance = 0;
struct RenderData
{
    AssetSystem *assetSystem;
};

internal void DrawStaticMesh(RenderData *renderer, u32 materialId, u32 meshId,
    mat4 modelMatrix, u32 cameraIndex, u32 shadowMapTexture,
    u32 irradianceCubeMap)
{
}

inline u32 GetTexture(AssetSystem *assetSystem, u32 id) { return 0; }

internal u32 CreateCollisionObjectFromEntity(Entity *entity,
    CollisionWorld *collisionWorld, AssetSystem *assetSystem,
    MemoryArena *tempArena)
{
    return 1;
}

internal AabbTree BuildAabbTree(vec3 *boxMins, vec3 *boxMaxes, u32 count,
    u32 maxNodes, MemoryArena *arena, MemoryArena *tempArena)
{
    AabbTree tree = {};
    return tree;
}

#include "entity_world.cpp"
#include "collision_world.cpp"
#include "game_new_entities.cpp"

TEST_GROUP(GameNewEntities);

global MemoryArena g_Arena;

TEST_SETUP(GameNewEntities)
{
    u32 size = Kilobytes(64);
    InitializeMemoryArena(&g_Arena, malloc(size), size);
}

TEST_TEAR_DOWN(GameNewEntities) { free(g_Arena.base); }

internal EntityWorldConfig CreateTestEntityWorldConfig()
{
    EntityWorldConfig config = {};
    config.worldArenaSize = Kilobytes(16);
    config.maxEntities = 4;
    config.maxEntityDefinitions = 4;
    config.deletionQueueCapacity = 2;
    return config;
}

TEST(GameNewEntities, DrawStaticMeshes)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    // Given an entity is visible
    u32 entityId = 1234;
    Entity entity = {};
    entity.visible = true;
    AddEntity(&entityWorld, entityId, &entity);

    // When we draw static meshes system
    RenderData renderer = {};
    RenderCommandBuffer commandBuffer =
        AllocateRenderCommandBuffer(&g_Arena, 4);
    DrawStaticMeshesSystem(&entityWorld, &renderer, 0, 0, &commandBuffer);

    // Then we add a render command to the buffer
    TEST_ASSERT_EQUAL_UINT(1, commandBuffer.count);
}

TEST(GameNewEntities, EntityDefinition)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    EntityDefinition entityDef = {};
    PushValue_(&entityDef, OffsetOf(Entity, scale), Vec3(1, 1, 1));

    u32 entityId = CreateEntityFromDefinition(&entityWorld, &entityDef);
    Entity *entity = FindEntityById(&entityWorld, entityId);
    TEST_ASSERT_NOT_NULL(entity);

    AssertWithinVec3(EPSILON, Vec3(1), entity->scale);
}

TEST(GameNewEntities, EntityDefinitionCombineFlags)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    EntityDefinition entityDef = {};
    PUSH_VALUE(&entityDef, flags, EntityFlags_Collider);
    PUSH_VALUE(&entityDef, flags, EntityFlags_Inventory);

    u32 entityId = CreateEntityFromDefinition(&entityWorld, &entityDef);
    Entity *entity = FindEntityById(&entityWorld, entityId);
    TEST_ASSERT_NOT_NULL(entity);

    TEST_ASSERT_EQUAL_UINT(
        EntityFlags_Collider | EntityFlags_Inventory, entity->flags);
}

TEST(GameNewEntities, Collider)
{
    EntityWorld entityWorld = {};
    EntityWorldConfig config = CreateTestEntityWorldConfig();
    InitializeEntityWorld(&entityWorld, &g_Arena, config);

    CollisionWorldConfig collisionWorldConfig = {};
    collisionWorldConfig.maxCollisionMeshes = 1;
    collisionWorldConfig.maxCollisionObjects = 1;
    collisionWorldConfig.meshDataStorageSize = 4;

    CollisionWorld collisionWorld = {};
    InitializeCollisionWorld(&collisionWorld, &g_Arena, collisionWorldConfig);
    AssetSystem assetSystem = {};

    // Given an entity has collision
    u32 entityId = 1234;
    Entity initialEntityData = {};
    initialEntityData.flags = EntityFlags_Collider;
    AddEntity(&entityWorld, entityId, &initialEntityData);

    // When we sync entity world with collision world
    SyncEntityWorldWithCollisionWorld(
        &entityWorld, &collisionWorld, &assetSystem, &g_Arena);

    // Then we create a collision object for the entity
    Entity *entity = FindEntityById(&entityWorld, entityId);
    TEST_ASSERT_EQUAL_UINT(1, entity->collisionObjectId);
}

TEST_GROUP_RUNNER(GameNewEntities)
{
    RUN_TEST_CASE(GameNewEntities, DrawStaticMeshes);
    RUN_TEST_CASE(GameNewEntities, EntityDefinition);
    RUN_TEST_CASE(GameNewEntities, EntityDefinitionCombineFlags);
    RUN_TEST_CASE(GameNewEntities, Collider);
}
