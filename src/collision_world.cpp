internal void InitializeCollisionWorld(CollisionWorld *collisionWorld,
    MemoryArena *arena, CollisionWorldConfig config)
{
    memset(collisionWorld, 0, sizeof(CollisionWorld));

    collisionWorld->meshes =
        Dict_CreateFromArena(arena, CollisionMesh, config.maxCollisionMeshes);

    collisionWorld->objects = Dict_CreateFromArena(
        arena, CollisionObject, config.maxCollisionObjects);

    collisionWorld->meshDataArena =
        SubAllocateArena(arena, config.meshDataStorageSize);

    collisionWorld->nextId = 1;
}

internal void ResetCollisionWorld(CollisionWorld *collisionWorld)
{
    // Dict_Clear(&collisionWorld->meshes);
    Dict_Clear(&collisionWorld->objects);
    // ClearMemoryArena(&collisionWorld->meshDataArena);
    //  TODO: Zero memory?
}

inline void TransformVertices(vec3 *vertices, u32 count, mat4 transform)
{
    PROF_SCOPE();
    for (u32 i = 0; i < count; i++)
    {
        vertices[i] = TransformPoint(vertices[i], transform);
    }
}

inline Aabb ComputeAabb(vec3 *vertices, u32 count)
{
    // PROF_SCOPE();
    Aabb aabb = {};
    if (count > 0)
    {
        aabb.min = vertices[0];
        aabb.max = vertices[0];
        for (u32 i = 1; i < count; i++)
        {
            aabb.min = Min(aabb.min, vertices[i]);
            aabb.max = Max(aabb.max, vertices[i]);
        }
    }

    return aabb;
}

inline Aabb TransformAabb(Aabb aabb, mat4 transform)
{
    PROF_SCOPE();

    vec3 m[2] = {aabb.min, aabb.max};
    vec3 vertices[8] = {
        Vec3(m[0].x, m[0].y, m[0].z),
        Vec3(m[1].x, m[0].y, m[0].z),
        Vec3(m[1].x, m[0].y, m[1].z),
        Vec3(m[0].x, m[0].y, m[1].z),
        Vec3(m[0].x, m[1].y, m[0].z),
        Vec3(m[1].x, m[1].y, m[0].z),
        Vec3(m[1].x, m[1].y, m[1].z),
        Vec3(m[0].x, m[1].y, m[1].z),
    };

    TransformVertices(vertices, ARRAY_LENGTH(vertices), transform);

    Aabb result = ComputeAabb(vertices, ARRAY_LENGTH(vertices));
    return result;
}

internal u32 AddCollisionObject(CollisionWorld *collisionWorld,
    u32 collisionMeshId, mat4 transform, mat4 invTransform)
{
    u32 id = collisionWorld->nextId++;

    CollisionObject *object =
        Dict_AddItem(&collisionWorld->objects, CollisionObject, id);
    if (object != NULL)
    {
        object->transform = transform;
        object->invTransform = invTransform;

        CollisionMesh *collisionMesh = Dict_FindItem(
            &collisionWorld->meshes, CollisionMesh, collisionMeshId);
        ASSERT(collisionMesh != NULL);
        ASSERT(collisionMesh->aabbTree.root != NULL);
        Aabb baseAabb;
        baseAabb.min = collisionMesh->aabbTree.root->min;
        baseAabb.max = collisionMesh->aabbTree.root->max;

        object->aabb = TransformAabb(baseAabb, transform);
        object->mesh = collisionMeshId;
        object->shape = CollisionShape_Mesh;
    }
    else
    {
        id = 0;
        LOG_MSG("Failed to create collision object");
    }

    return id;
}

internal u32 AddTerrainCollisionObject(CollisionWorld *collisionWorld)
{
    u32 id = collisionWorld->nextId++;

    CollisionObject *object =
        Dict_AddItem(&collisionWorld->objects, CollisionObject, id);
    if (object != NULL)
    {
        object->transform = Identity();
        object->invTransform = Identity();

        object->aabb.min = Vec3(-1);
        object->aabb.max = Vec3(1);
        object->mesh = 0;
        object->shape = CollisionShape_Terrain;
    }

    return id;
}

internal void RemoveCollisionObject(
    CollisionWorld *collisionWorld, u32 objectId)
{
    Dict_RemoveItem(&collisionWorld->objects, objectId);
}

inline b32 AabbOverlap(vec3 minA, vec3 maxA, vec3 minB, vec3 maxB)
{
    for (u32 axis = 0; axis < 3; ++axis)
    {
        f32 a0 = minA.data[axis];
        f32 a1 = maxA.data[axis];
        f32 b0 = minB.data[axis];
        f32 b1 = maxB.data[axis];

        // No overlap
        if (b1 < a0 || b0 > a1)
        {
            return false;
        }
    }

    return true;
}

internal u32 SpatialQuery(CollisionWorld *collisionWorld, vec3 min, vec3 max,
    u32 *collisionObjectIds, u32 capacity)
{
    PROF_SCOPE();

    u32 count = 0;

    Dictionary *objects = &collisionWorld->objects;
    for (u32 i = 0; i < objects->count; i++)
    {
        u32 id = objects->keys[i];
        CollisionObject *object =
            Dict_GetItemByIndex(&collisionWorld->objects, CollisionObject, i);

        if (object != NULL)
        {
            if (AabbOverlap(min, max, object->aabb.min, object->aabb.max))
            {
                if (count < capacity)
                {
                    collisionObjectIds[count++] = id;
                }
                else
                {
                    break;
                }
            }
        }
    }

    return count;
}

// Helper function, not sure where this should live
internal void BuildTriangleAabbs(vec3 *meshVertices, u32 meshVertexCount,
    u32 *meshIndices, vec3 *aabbMins, vec3 *aabbMaxes, u32 triangleCount)
{
    PROF_SCOPE();
    // Fudge value to make sure we don't have degenerate AABBs
    f32 thickness = 0.01f;

    for (u32 i = 0; i < triangleCount; i++)
    {
        u32 indices[3] = {
            meshIndices[i * 3 + 0],
            meshIndices[i * 3 + 1],
            meshIndices[i * 3 + 2],
        };

        vec3 vertices[3] = {
            meshVertices[indices[0]],
            meshVertices[indices[1]],
            meshVertices[indices[2]],
        };

        Aabb aabb = ComputeAabb(vertices, 3);
        aabbMins[i] = aabb.min - Vec3(thickness);
        aabbMaxes[i] = aabb.max + Vec3(thickness);
    }
}

// TODO: Could we avoid using a tempArena here if BuildTriangleAabbs
// actually built the leaf nodes for us? Still need for unmerged indices
// array in BuildAabbTree
internal CollisionMesh CreateCollisionMesh(vec3 *vertices, u32 vertexCount,
    u32 *indices, u32 indexCount, MemoryArena *arena, MemoryArena *tempArena)
{
    PROF_SCOPE();
    CollisionMesh collisionMesh = {};
    collisionMesh.vertices = vertices;
    collisionMesh.vertexCount = vertexCount;
    collisionMesh.indices = indices;
    collisionMesh.indexCount = indexCount;

    // Build acceleration structure
    // Compute AABBs for each triangle
    u32 triangleCount = indexCount / 3;
    vec3 *triangleAabbMins = ALLOCATE_ARRAY(tempArena, vec3, triangleCount);
    vec3 *triangleAabbMaxes = ALLOCATE_ARRAY(tempArena, vec3, triangleCount);
    BuildTriangleAabbs(collisionMesh.vertices, collisionMesh.vertexCount,
        collisionMesh.indices, triangleAabbMins, triangleAabbMaxes,
        triangleCount);

    u32 maxNodes = triangleCount * 3;
    collisionMesh.aabbTree = BuildAabbTree(triangleAabbMins, triangleAabbMaxes,
        triangleCount, maxNodes, arena, tempArena);

    return collisionMesh;
}

internal void AddCollisionMesh(
    CollisionWorld *collisionWorld, CollisionMesh collisionMesh, u32 key)
{
    CollisionMesh *dst =
        Dict_AddItem(&collisionWorld->meshes, CollisionMesh, key);
    ASSERT(dst != NULL); // TODO: Remove this and handle failure
    *dst = collisionMesh;
}
