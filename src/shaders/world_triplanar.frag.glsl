#extension GL_GOOGLE_include_directive : require

#include "pbr_material_interface.glsl"

void main()
{
    TriplanarUV tri_uv =
        GetTriplanarUV(fs_in.world_position * u_world_texture_scaling);
    vec3 albedo_x = texture(u_albedo, tri_uv.x).rgb;
    vec3 albedo_y = texture(u_albedo, tri_uv.y).rgb;
    vec3 albedo_z = texture(u_albedo, tri_uv.z).rgb;
    vec3 tri_w = GetTriplanarWeights(fs_in.world_normal);

    vec3 albedo = albedo_x * tri_w.x + albedo_y * tri_w.y + albedo_z * tri_w.z;
#if DISABLE_LIGHTING
    vec3 color = albedo;
#else
    vec3 N = normalize(fs_in.world_normal);
    vec3 color = pbr_material_model(albedo, N);
#endif
    o_color = vec4(color, 1.0);
}
