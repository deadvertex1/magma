// FIXME: Problably not in right file
internal u32 CreateCollisionObjectFromEntity(Entity *entity,
    CollisionWorld *collisionWorld, AssetSystem *assetSystem,
    MemoryArena *tempArena)
{
    u32 collisionObjectId = 0;

    // Check if collision mesh already exists for our mesh
    u32 meshId = entity->mesh;
    b32 meshFound = false;
    CollisionMesh *collisionMesh =
        Dict_FindItem(&collisionWorld->meshes, CollisionMesh, meshId);
    if (collisionMesh == NULL)
    {
        // Create collision mesh based on renderer mesh
        Asset_Mesh mesh = {};
        if (GetAssetData_(
                assetSystem, AssetType_Mesh, entity->mesh, sizeof(mesh), &mesh))
        {
            if (mesh.meshData != NULL)
            {
                MeshData *meshData = mesh.meshData;

                vec3 *vertexBuffer =
                    ALLOCATE_ARRAY(&collisionWorld->meshDataArena, vec3,
                        meshData->vertexCount);
                u32 *indicesBuffer = ALLOCATE_ARRAY(
                    &collisionWorld->meshDataArena, u32, meshData->indexCount);
                memcpy(indicesBuffer, meshData->indices,
                    sizeof(u32) * meshData->indexCount);

                ASSERT(meshData->vertexSize == sizeof(Vertex));
                CopyVertices(vertexBuffer, (Vertex *)meshData->vertices,
                    meshData->vertexCount);

                CollisionMesh newCollisionMesh =
                    CreateCollisionMesh(vertexBuffer, meshData->vertexCount,
                        indicesBuffer, meshData->indexCount,
                        &collisionWorld->meshDataArena, tempArena);

                AddCollisionMesh(collisionWorld, newCollisionMesh, meshId);
                meshFound = true;
            }
        }
    }
    else
    {
        meshFound = true;
    }

    if (meshFound)
    {
        // FIXME: This won't work for child entities!
        mat4 modelMatrix = BuildModelMatrix(entity);
        mat4 invModelMatrix = BuildInverseModelMatrix(entity);
        collisionObjectId = AddCollisionObject(
            collisionWorld, meshId, modelMatrix, invModelMatrix);
    }
    return collisionObjectId;
}

internal void CleanUpCollisionObjectsSystem(
    EntityWorld *entityWorld, CollisionWorld *collisionWorld)
{
    PROF_SCOPE();
    for (u32 i = 0; i < entityWorld->deletionQueueLength; i++)
    {
        u32 entityId = entityWorld->deletionQueue[i];
        Entity *entity = FindEntityById(entityWorld, entityId);
        if (entity != NULL)
        {
            if (entity->flags & EntityFlags_Collider)
            {
                RemoveCollisionObject(
                    collisionWorld, entity->collisionObjectId);
            }
        }
    }
}

internal void CleanUpParticleEmitters(
    EntityWorld *entityWorld, ParticleSystem *particleSystem)
{
    for (u32 i = 0; i < entityWorld->deletionQueueLength; i++)
    {
        u32 entityId = entityWorld->deletionQueue[i];
        Entity *entity = FindEntityById(entityWorld, entityId);
        if (entity != NULL)
        {
            if (entity->flags & EntityFlags_ParticleEmitter)
            {
                if (entity->particleEmitterId != 0)
                {
                    DeleteParticleBuffer(
                        particleSystem, entity->particleEmitterId);
                }
            }
        }
    }
}

// FIXME: Remove this duplicate code
internal void DrawStaticMesh(RenderData *renderer, u32 materialId, u32 meshId,
    mat4 modelMatrix, u32 cameraIndex, u32 shadowMapTexture,
    u32 irradianceCubeMap)
{
    PROF_SCOPE();
    // Get material data
    Asset_Material material = {};
    if (GetMaterial(renderer->assetSystem, materialId, &material))
    {
        // Apply material
        u32 shader = GetShader(renderer->assetSystem, material.shader);
        if (shader != 0)
        {
            glUseProgram(shader);

            // --- Standard PBR material vertex shader interface ---
            {
                // Camera uniform buffer
                glBindBufferBase(
                    GL_UNIFORM_BUFFER, 0, renderer->cameraUniformBuffer);

                // Camera index
                glUniform1ui(2, cameraIndex);

                // Model matrix
                glUniformMatrix4fv(1, 1, false, modelMatrix.data);
            }

            // -- Standard PBR material fragment shader interface ---
            {
#if ENABLE_LIGHTING
                // Lighting uniform buffer
                glBindBufferBase(
                    GL_UNIFORM_BUFFER, 1, renderer->lightingUniformBuffer);
#endif

                // Albedo
                u32 albedo = GetTexture(renderer->assetSystem, material.albedo);
                if (albedo != 0)
                {
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, albedo);
                }

                // TODO: Metallic
                // TODO: Roughness
                // TODO: Normal

#if ENABLE_LIGHTING
                // Shadow map
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, shadowMapTexture);
                glUniform1i(3, 1); // Use GL_TEXTURE1 for shadow map

                // Irradiance cube map
                if (irradianceCubeMap != 0)
                {
                    glActiveTexture(GL_TEXTURE2);
                    glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceCubeMap);
                    glUniform1i(4, 2);
                }
#endif

                // Ewwww, this uniform is getting optimized out on the
                // default shader which is then generating an error when
                // we try set it.
                if (material.shader == Shader_WorldTriplanar)
                {
                    // u_world_texture_scaling
                    vec3 worldTextureScaling = Vec3(1);
                    glUniform3fv(7, 1, worldTextureScaling.data);
                }
            }

            // Get mesh data
            MeshState *mesh = GetMesh(renderer->assetSystem, meshId);
            if (mesh != NULL)
            {
                glBindVertexArray(mesh->vertexArrayObject);
                glDrawElements(
                    GL_TRIANGLES, mesh->elementCount, GL_UNSIGNED_INT, 0);
            }
        }
    }
}

internal void DebugDrawSunLight(
    SunLight *sunLight, DebugDrawingBuffer *debugDrawingBuffer)
{
    vec3 sunDirection =
        Normalize(RotateVector(Vec3(0, 0, -1), sunLight->rotation));
    DrawBox(debugDrawingBuffer, sunLight->position - Vec3(0.1),
        sunLight->position + Vec3(0.1), Vec3(0, 1, 0));
    DrawLine(debugDrawingBuffer, sunLight->position,
        sunLight->position + sunDirection * 2.0f, Vec3(0, 1, 0));
}

internal void DebugDrawSkyLight(
    SkyLight *skyLight, DebugDrawingBuffer *debugDrawingBuffer)
{
    DrawPoint(debugDrawingBuffer, skyLight->position, 0.5f, Vec3(0, 1, 1));
}

struct AnimationKeyFrame
{
    f32 t;
    vec3 position;
    quat rotation;
    f32 scale;
};

inline void InitializeKeyFrames(AnimationKeyFrame *keyFrames, u32 count)
{
    for (u32 i = 0; i < count; i++)
    {
        AnimationKeyFrame *keyFrame = keyFrames + i;
        keyFrame->t = 0.0f;
        keyFrame->position = Vec3(0);
        keyFrame->scale = 1.0f;
        keyFrame->rotation = Quat();
    }
}

inline mat4 BuildModelMatrix(AnimationKeyFrame keyFrame)
{
    mat4 result = Translate(keyFrame.position) * Rotate(keyFrame.rotation) *
                  Scale(keyFrame.scale);
    return result;
}

inline AnimationKeyFrame LerpKeyFrames(
    AnimationKeyFrame start, AnimationKeyFrame end, f32 t)
{
    AnimationKeyFrame result = {};
    result.t = Lerp(start.t, end.t, t);
    result.position = Lerp(start.position, end.position, t);
    result.rotation = LerpQuat(start.rotation, end.rotation, t);
    result.scale = Lerp(start.scale, end.scale, t);
    return result;
}

// FIXME: Replace this with entities ASAP!
internal void DrawViewModel(RenderData *renderer, GameEventQueue *eventQueue,
    u32 entityId, f32 t, u32 itemId, WeaponController *weaponController)
{
    // Figure out transform
    f32 y = 0.1f * Sin(1.0f * t);
    f32 zRot = 0.2f * Sin(0.6f * t) * Cos(1.1f * t);
    mat4 baseViewModelTransform =
        Translate(Vec3(0.03, -0.01 + y * 0.01f, -0.06)) *
        RotateZ(PI * -0.02f * PI * zRot) * RotateY(PI * -0.48f) *
        Scale(Vec3(0.05f));

    u32 irradianceCubeMap =
        GetTexture(renderer->assetSystem, Texture_PreethamSky_Irradiance);

    if (itemId == Item_Hatchet)
    {
        // Figure out transform
        mat4 modelMatrix = baseViewModelTransform * Scale(Vec3(0.35));

        // FIXME: Remove duplication
        if (weaponController->isAnimationPlaying)
        {
            // NOTE: Specifically creating 0.0f and 1.0f key frames for easier
            // searching
            AnimationKeyFrame keyFrames[4];
            InitializeKeyFrames(keyFrames, 4);

            // PROBLEM: Not worldspace rotation....
            keyFrames[0].t = 0.0f;
            keyFrames[1].t = 0.4f;
            keyFrames[1].rotation = Quat(Vec3(0, 0, 1), PI * -0.2f);
            keyFrames[1].position = Vec3(1, 0, 0) * 0.5f;
            keyFrames[2].t = 0.5f;
            keyFrames[2].rotation = Quat(Vec3(0, 0, 1), PI * 0.3f);
            keyFrames[2].position = Vec3(-1, 0, 0) * 0.3f;
            keyFrames[3].t = 1.0f;

            f32 animLength = 1.3f;
            f32 animationStartTime = weaponController->animationStartTime;
            f32 animationTime = t - animationStartTime;

            if (animationTime > animLength)
            {
                weaponController->isAnimationPlaying = false;
            }

            f32 animT = Fmod(animationTime, animLength);
            animT = animT / animLength; // Map to 0..1

            // Find key frames either side off animT
            u32 currentFrames[2] = {};
            for (i32 i = 0; i < (i32)ARRAY_LENGTH(keyFrames); i++)
            {
                if (keyFrames[i].t > animT)
                {
                    currentFrames[1] = i;
                    currentFrames[0] = Max(0, i - 1);
                    break;
                }
            }

            ASSERT(currentFrames[0] <= currentFrames[1]);
            ASSERT(currentFrames[0] < ARRAY_LENGTH(keyFrames));
            ASSERT(currentFrames[1] < ARRAY_LENGTH(keyFrames));

            AnimationKeyFrame startKeyFrame = keyFrames[currentFrames[0]];
            AnimationKeyFrame endKeyFrame = keyFrames[currentFrames[1]];

            f32 lerpLength = endKeyFrame.t - startKeyFrame.t;
            ASSERT(lerpLength >= 0.0f);

            f32 lerpT = (animT - startKeyFrame.t) / lerpLength;
            if (currentFrames[1] == 2)
            {
                lerpT = EaseOutElastic(lerpT);
                lerpT = Clamp(lerpT, 0.0f, 2.0f);
            }
            else
            {
                lerpT = EaseInQuad(lerpT);
            }

            if (currentFrames[1] == 2)
            {
                if (lerpT >= 1.0f)
                {
                    // NOTE: Don't worry about multiple events being triggered
                    // as we've still got a check for
                    // weaponController.timeUntilNextAttack

                    if (weaponController->harvestEventTriggered == false)
                    {
                        GameEvent *event =
                            QueueEvent(eventQueue, EventType_HarvestResource);
                        if (event != NULL)
                        {
                            event->harvestResource.toolItemId = itemId;
                            event->harvestResource.entityId = entityId;
                        }
                        weaponController->harvestEventTriggered = true;
                    }
                }
            }

            AnimationKeyFrame lerpedKeyFrame =
                LerpKeyFrames(startKeyFrame, endKeyFrame, lerpT);

            mat4 animModelmatrix = BuildModelMatrix(lerpedKeyFrame);

            modelMatrix = modelMatrix * animModelmatrix;
        }

        DrawStaticMesh(renderer, Material_Missing, Mesh_Hatchet, modelMatrix, 3,
            renderer->shadowMapBuffer.texture, irradianceCubeMap);
    }
    else if (itemId == Item_PickAxe)
    {
        // Figure out transform
        mat4 modelMatrix = baseViewModelTransform * Scale(Vec3(0.35));

        if (weaponController->isAnimationPlaying)
        {
            // NOTE: Specifically creating 0.0f and 1.0f key frames for easier
            // searching
            AnimationKeyFrame keyFrames[4];
            InitializeKeyFrames(keyFrames, 4);

            // PROBLEM: Not worldspace rotation....
            keyFrames[0].t = 0.0f;
            keyFrames[1].t = 0.4f;
            keyFrames[1].rotation = Quat(Vec3(0, 0, 1), PI * -0.2f);
            keyFrames[1].position = Vec3(1, 0, 0) * 0.5f;
            keyFrames[2].t = 0.5f;
            keyFrames[2].rotation = Quat(Vec3(0, 0, 1), PI * 0.3f);
            keyFrames[2].position = Vec3(-1, 0, 0) * 0.3f;
            keyFrames[3].t = 1.0f;

            f32 animLength = 1.3f;
            f32 animationStartTime = weaponController->animationStartTime;
            f32 animationTime = t - animationStartTime;

            if (animationTime > animLength)
            {
                weaponController->isAnimationPlaying = false;
            }

            f32 animT = Fmod(animationTime, animLength);
            animT = animT / animLength; // Map to 0..1

            // Find key frames either side off animT
            u32 currentFrames[2] = {};
            for (i32 i = 0; i < (i32)ARRAY_LENGTH(keyFrames); i++)
            {
                if (keyFrames[i].t > animT)
                {
                    currentFrames[1] = i;
                    currentFrames[0] = Max(0, i - 1);
                    break;
                }
            }

            ASSERT(currentFrames[0] <= currentFrames[1]);
            ASSERT(currentFrames[0] < ARRAY_LENGTH(keyFrames));
            ASSERT(currentFrames[1] < ARRAY_LENGTH(keyFrames));

            AnimationKeyFrame startKeyFrame = keyFrames[currentFrames[0]];
            AnimationKeyFrame endKeyFrame = keyFrames[currentFrames[1]];

            f32 lerpLength = endKeyFrame.t - startKeyFrame.t;
            ASSERT(lerpLength >= 0.0f);

            f32 lerpT = (animT - startKeyFrame.t) / lerpLength;
            if (currentFrames[1] == 2)
            {
                lerpT = EaseOutElastic(lerpT);
                lerpT = Clamp(lerpT, 0.0f, 2.0f);
            }
            else
            {
                lerpT = EaseInQuad(lerpT);
            }

            if (currentFrames[1] == 2)
            {
                if (lerpT >= 1.0f)
                {
                    // NOTE: Don't worry about multiple events being triggered
                    // as we've still got a check for
                    // weaponController.timeUntilNextAttack

                    if (weaponController->harvestEventTriggered == false)
                    {
                        GameEvent *event =
                            QueueEvent(eventQueue, EventType_HarvestResource);
                        if (event != NULL)
                        {
                            event->harvestResource.toolItemId = itemId;
                            event->harvestResource.entityId = entityId;
                        }
                        weaponController->harvestEventTriggered = true;
                    }
                }
            }

            AnimationKeyFrame lerpedKeyFrame =
                LerpKeyFrames(startKeyFrame, endKeyFrame, lerpT);

            mat4 animModelmatrix = BuildModelMatrix(lerpedKeyFrame);

            modelMatrix = modelMatrix * animModelmatrix;
        }

        DrawStaticMesh(renderer, HashStringU32("materials/metal_ore"),
            Mesh_PickAxe, modelMatrix, 3, renderer->shadowMapBuffer.texture,
            irradianceCubeMap);
    }
    else if (itemId == Item_Hammer)
    {
        mat4 modelMatrix = baseViewModelTransform;

        // Draw mesh...
        DrawStaticMesh(renderer, Material_Missing, Mesh_Hammer, modelMatrix, 3,
            renderer->shadowMapBuffer.texture, irradianceCubeMap);
    }
    else if (itemId == Item_WoodenBow)
    {
        f32 maxDrawTime = 1.2f;
        f32 t = weaponController->drawTime / maxDrawTime;
        t = t * t;
        f32 z = 1.0f + 1.4f * t;
        mat4 modelMatrix = baseViewModelTransform *
                           Translate(Vec3(0.2 * t, 0, 0)) *
                           Scale(Vec3(z, 1, 1));

        DrawStaticMesh(renderer, Material_PrototypeTriplanar, Mesh_WoodenBow,
            modelMatrix, 3, renderer->shadowMapBuffer.texture,
            irradianceCubeMap);
    }
    else if (itemId == Item_WoodenSpear)
    {
        f32 z = 0.025f * Sin(20.0f * t);
        mat4 modelMatrix = Translate(Vec3(-0.0, -0.01, z)) *
                           baseViewModelTransform * RotateZ(PI * 0.45f);

        DrawStaticMesh(renderer, Material_Missing, Mesh_WoodenSpear,
            modelMatrix, 3, renderer->shadowMapBuffer.texture,
            irradianceCubeMap);
    }
    else if (itemId == Item_TestGun)
    {
        mat4 modelMatrix = baseViewModelTransform *
                           Translate(Vec3(0.02, -0.15, 0.04)) * Scale(10.0f) *
                           RotateY(PI * -0.5f);

        if (weaponController->isAnimationPlaying)
        {
            // FIXME: Stop the duplication!!!
            // NOTE: Specifically creating 0.0f and 1.0f key frames for easier
            // searching
            AnimationKeyFrame keyFrames[4];
            InitializeKeyFrames(keyFrames, 4);

            // PROBLEM: Not worldspace rotation....
            keyFrames[0].t = 0.0f;
            keyFrames[1].t = 0.05f;
            keyFrames[1].rotation = Quat(Vec3(1, 0, 0), PI * -0.04f) *
                                    Quat(Vec3(0, 0, 1), PI * 0.02f);
            keyFrames[1].position = Vec3(0, 0, -1) * 0.02f;
            keyFrames[2].t = 0.9f;
            keyFrames[3].t = 1.0f;

            f32 animLength = 0.4f;
            f32 animationStartTime = weaponController->animationStartTime;
            f32 animationTime = t - animationStartTime;

            if (animationTime > animLength)
            {
                weaponController->isAnimationPlaying = false;
            }

            // FIXME: Apply this to other view models
            f32 animT = animationTime / animLength;
            animT = Clamp01(animT);
            // f32 animT = Fmod(animationTime, animLength);
            // animT = animT / animLength; // Map to 0..1

            // Find key frames either side off animT
            u32 currentFrames[2] = {};
            for (i32 i = 0; i < (i32)ARRAY_LENGTH(keyFrames); i++)
            {
                if (keyFrames[i].t > animT)
                {
                    currentFrames[1] = i;
                    currentFrames[0] = Max(0, i - 1);
                    break;
                }
            }

            ASSERT(currentFrames[0] <= currentFrames[1]);
            ASSERT(currentFrames[0] < ARRAY_LENGTH(keyFrames));
            ASSERT(currentFrames[1] < ARRAY_LENGTH(keyFrames));

            AnimationKeyFrame startKeyFrame = keyFrames[currentFrames[0]];
            AnimationKeyFrame endKeyFrame = keyFrames[currentFrames[1]];

            f32 lerpLength = endKeyFrame.t - startKeyFrame.t;
            ASSERT(lerpLength >= 0.0f);

            f32 lerpT = (animT - startKeyFrame.t) / lerpLength;
            lerpT = Clamp01(lerpT);
            if (currentFrames[1] == 2)
            {
                lerpT = EaseOutElastic(lerpT);
                lerpT = Clamp(lerpT, 0.0f, 2.0f);
            }
            else
            {
                lerpT = EaseOutQuad(lerpT);
            }

            AnimationKeyFrame lerpedKeyFrame =
                LerpKeyFrames(startKeyFrame, endKeyFrame, lerpT);

            mat4 animModelmatrix = BuildModelMatrix(lerpedKeyFrame);

            modelMatrix = modelMatrix * animModelmatrix;
        }

        DrawStaticMesh(renderer, Material_PrototypeTriplanar,
            HashStringU32("meshes/test_gun"), modelMatrix, 3,
            renderer->shadowMapBuffer.texture, irradianceCubeMap);
    }
}

internal void ParticleEmitterSystem(
    EntityWorld *entityWorld, ParticleSystem *particleSystem)
{
    for (u32 i = 0; i < entityWorld->entities.count; i++)
    {
        u32 entityId = entityWorld->entities.keys[i];
        Entity *entity = FindEntityById(entityWorld, entityId);
        ASSERT(entity != NULL);
        if (entity->flags & EntityFlags_ParticleEmitter)
        {
            if (entity->particleEmitterId == 0)
            {
                ParticleSpec *spec =
                    FindParticleSpec(particleSystem, entity->particleSpecId);
                if (spec != NULL)
                {
                    ParticleSpec localSpec = *spec;
                    localSpec.origin = entity->position;

                    entity->particleEmitterId =
                        CreateParticleBuffer(particleSystem, localSpec);
                }
                else
                {
                    LOG_MSG("Failed to spawn particle emitter: Unknown "
                            "particle spec id %u",
                        entity->particleSpecId);
                }
            }
        }
    }
}

// FIXME: Add System for removing inventories when entity deleted
internal void CreateMissingInventoriesSystem(
    EntityWorld *entityWorld, InventorySystem *inventorySystem)
{
    PROF_SCOPE();

    for (u32 i = 0; i < entityWorld->entities.count; i++)
    {
        u32 entityId = entityWorld->entities.keys[i];
        Entity *entity = FindEntityById(entityWorld, entityId);
        ASSERT(entity != NULL);
        if (entity->flags & EntityFlags_Inventory)
        {
            if (entity->inventoryId == 0)
            {
                entity->inventoryId = AllocateInventory(inventorySystem);
            }
        }
    }
}

internal void ProcessEntitiesScheduledForDeletion(
    EntityWorld *entityWorld, f32 dt)
{
    for (u32 i = 0; i < entityWorld->entities.count; i++)
    {
        u32 entityId = entityWorld->entities.keys[i];
        Entity *entity = FindEntityById(entityWorld, entityId);
        ASSERT(entity != NULL);
        if (entity->flags & EntityFlags_ScheduledDeletion)
        {
            if (entity->timeRemaining > 0.0f)
            {
                entity->timeRemaining -= dt;
            }
            else
            {
                QueueEntityForDeletion(entityWorld, entityId);
            }
        }
    }
}

internal void UpdateEntityWorldTransforms(EntityWorld *entityWorld)
{
    for (u32 i = 0; i < entityWorld->entities.count; i++)
    {
        u32 entityId = entityWorld->entities.keys[i];
        Entity *entity = FindEntityById(entityWorld, entityId);

        if (entity->parentEntityId != NULL_ENTITY_ID)
        {
            Entity *parent =
                FindEntityById(entityWorld, entity->parentEntityId);
            if (parent != NULL)
            {
                mat4 localTransform = BuildModelMatrix(entity);
                entity->worldTransform =
                    parent->worldTransform * localTransform;
#if 0
                // TODO: This is special case logic for the player/view models,
                // need to still implement the generic/common case
                // FIXME: Assuming the parent worldTransform has already been
                // updated!
                // FIXME: Seems like we're getting small floating point errors
                // causing the gun to shimmer
                Entity *body = parent;
                Entity *viewModel = entity;

                vec3 cameraEulerAngles = parent->player.cameraEulerAngles;
                mat4 bodyWorldTransform = body->worldTransform;
                // Translate(body->position) * Rotate(body->rotation);

                // Construct a fake head entity to parent view model to
                vec3 headPosition = g_PlayerHeadOffset;
                quat headRotation = Quat(Vec3(1, 0, 0), cameraEulerAngles.x);
                mat4 headLocalTransform =
                    Translate(headPosition) * Rotate(headRotation);
                mat4 headWorldTransform =
                    bodyWorldTransform * headLocalTransform;

                // TODO: View model control point for animations

                // View model
                mat4 viewModelLocalTransform = BuildModelMatrix(viewModel);
                viewModel->worldTransform =
                    headWorldTransform * viewModelLocalTransform;
#endif
            }
        }
        else
        {
            entity->worldTransform = BuildModelMatrix(entity);
        }
    }
}
