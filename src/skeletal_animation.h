#pragma once

#define MAX_SKELETON_JOINTS 255

struct SkeletonJoint
{
    mat4 invBindPose;
    const char *name;
    u32 nameId;
    i32 parent;
};

// Responsible for the heirarchy
struct Skeleton
{
    SkeletonJoint *joints;
    u32 count;
};

struct SkeletonJointPose
{
    quat rotation;
    vec3 translation;
    f32 scaling; // Limit to uniform scaling only
};

struct SkeletonPose
{
    Skeleton *skeleton;
    SkeletonJointPose *localPose;
};

struct JointIndexWeightPair
{
    i32 jointIndex;
    f32 weight;
};

inline SkeletonJointPose ConcatJointPose(
    SkeletonJointPose a, SkeletonJointPose b)
{
    SkeletonJointPose result;
    result.translation = a.translation + b.translation;
    result.rotation = a.rotation * b.rotation; // TODO: Double check order here
    result.scaling = a.scaling * b.scaling;
    return result;
}

inline int CompareJointIndexWeightPairs(const void *p1, const void *p2)
{
    JointIndexWeightPair *a = (JointIndexWeightPair *)p1;
    JointIndexWeightPair *b = (JointIndexWeightPair *)p2;

    if (a->weight > b->weight)
        return -1;
    else if (a->weight < b->weight)
        return 1;
    else
        return 0;
}

